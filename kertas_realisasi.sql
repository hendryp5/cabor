-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 20, 2020 at 04:07 AM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.2.26

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `cabor_dbase`
--

-- --------------------------------------------------------

--
-- Table structure for table `kertas_realisasi`
--

CREATE TABLE `kertas_realisasi` (
  `id` int(11) NOT NULL,
  `kertas_id` int(11) DEFAULT NULL,
  `program_id` int(11) DEFAULT NULL,
  `kegiatan_id` int(11) DEFAULT NULL,
  `kategori_id` int(11) DEFAULT NULL,
  `belanja_id` int(11) DEFAULT NULL,
  `tanggal` date DEFAULT NULL,
  `jumlah` double DEFAULT NULL,
  `tahun` varchar(4) COLLATE utf8_unicode_ci DEFAULT NULL,
  `dokumen` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_id` int(11) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_id` int(11) DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `deleted_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT;

--
-- Dumping data for table `kertas_realisasi`
--

INSERT INTO `kertas_realisasi` (`id`, `kertas_id`, `program_id`, `kegiatan_id`, `kategori_id`, `belanja_id`, `tanggal`, `jumlah`, `tahun`, `dokumen`, `created_at`, `created_id`, `updated_at`, `updated_id`, `deleted_at`, `deleted_id`) VALUES
(1, 8, 1, 1, 1, 1, '2020-02-07', 20000, '2020', 'C00002_file_realisasi_1.pdf', '2020-02-20 09:48:49', NULL, '2020-02-20 09:54:01', 8, NULL, NULL),
(2, 8, 1, 1, 1, 2, '2020-02-08', 200000, '2020', 'C00002_file_realisasi_2.pdf', '2020-02-20 10:50:24', NULL, NULL, 8, NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `kertas_realisasi`
--
ALTER TABLE `kertas_realisasi`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `kertas_realisasi`
--
ALTER TABLE `kertas_realisasi`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
