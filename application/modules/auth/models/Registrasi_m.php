<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Registrasi_m extends MY_Model
{
	public $table = 'cabor'; // you MUST mention the table name
	public $primary_key = 'id'; // you MUST mention the primary key
	public $fillable = array(); // If you want, you can set an array with the fields that can be filled by insert/update
	public $protected = array(); // ...Or you can set an array with the fields that cannot be filled by insert/update
	
	//ajax datatable
    public $column_order = array(null); //set kolom field database pada datatable secara berurutan
    public $column_search = array(); //set kolom field database pada datatable untuk pencarian
    public $order = array('id' => 'DESC'); //order baku 
	
	public function __construct()
	{
		$this->timestamps = TRUE;
		$this->soft_deletes = FALSE;
		parent::__construct();
    }
    
    public function insert_data($cabor = NULL, $user = NULL){
        $this->db->trans_start(); # Starting Transaction
        //$this->db->trans_strict(FALSE); 
		$this->db->insert('cabor', $cabor); # Inserting data
		$cabor_id = $this->db->insert_id();
        $this->db->insert('users', $user); # Inserting data
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return FALSE;
        } else {
            $this->db->trans_commit();
            return $cabor_id;
        }
    }

    public function get_kode() {
		$query = $this->db->query("SELECT MAX(RIGHT(kode,5)) AS kode FROM cabor");
		$kode = "";
	  
		if($query->num_rows() > 0){ 
			  foreach($query->result() as $k){
				  $tmp = ((int)$k->kode)+1;
				  $kode = sprintf("%05s", $tmp);
			  }
		 }else{
		  $kode = "00001";
		}
		$karakter = "C"; 
		return $karakter.$kode;
    }

   

    public function get_kota()
	{
		$query = $this->db->order_by('id', 'ASC')->get('kota');
        if($query->num_rows() > 0){
        $dropdown[''] = 'Pilih Salah Satu';
		foreach ($query->result() as $row)
		{
			$dropdown[$row->id] = $row->kota;
		}
        }else{
            $dropdown[''] = 'Belum Ada Daftar Kota / Kabupaten Tersedia';
        }
		return $dropdown;
    }


     public function get_kecamatan($id=null)
	{
		$this->db->where('kota_id', $id);
        $query = $this->db->order_by('id', 'ASC')->get('kecamatan');
        if($query->num_rows() > 0){
        $dropdown[''] = 'Pilih Salah Satu';
		foreach ($query->result() as $row)
		{
			$dropdown[$row->id] = $row->kecamatan;
		} 
        }else{
            $dropdown[''] = 'Belum Ada Daftar Kecamatan Tersedia';
        }
		return $dropdown;
	}

    public function get_kelurahan($id=null)
	{
		$this->db->where('kecamatan_id', $id);
        $query = $this->db->order_by('id', 'ASC')->get('kelurahan');
        if($query->num_rows() > 0){
        $dropdown[''] = 'Pilih Salah Satu';
		foreach ($query->result() as $row)
		{
			$dropdown[$row->id] = $row->kelurahan;
		} 
        }else{
            $dropdown[''] = 'Belum Ada Daftar Kelurahan Tersedia';
        }
		return $dropdown;
	}

	public function get_jenis_cabor()
    {
        $query = $this->db->order_by('id', 'ASC')->get_where('jenis_cabor',array('deleted_at'=>null));
        if($query->num_rows() > 0){
        //$dropdown[''] = 'Pilih Cabang Olahraga';
        foreach ($query->result() as $row)
        {
            $dropdown[$row->id] = $row->cabor;
        }
        }else{
            $dropdown[''] = 'Belum Ada Daftar Cabang Olahraga Tersedia';
        }
        return $dropdown;
    }

	public function get_cabor_jenis($id=null) {
        $this->db->where('cabor_id', $id);
        $this->db->where('deleted_at', NULL);
        $sql = $this->db->get('cabor_jenis');
        if($sql->num_rows() > 0){
            return $sql->result();
        }else{
            return FALSE;
        }
    }
    
    public function delete_jenis_cabor($id=null) {
		$this->db->where('cabor_id', $id);
        $this->db->update('cabor_jenis', array('deleted_at'=>date('Y-m-d H:i:s')));
        return $this->db->affected_rows();        
	}
}