<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Registrasi extends CI_Controller {

	/**
	 * code by rifqie rusyadi
	 * email rifqie.rusyadi@gmail.com
	 */
	public $folder = 'auth/registrasi/';
	
	public function __construct()
	{
		parent::__construct();
		$this->load->helper('my_helper');
		$this->load->model('registrasi_m', 'data');
		//signin();
	}
	
	public function index()
	{
		$data['title'] 		= 'REGISTRASI PENGPROV SEGI 3 CARAKA | DINAS PEMUDA DAN OLAHRAGA PROVINSI KALIMANTAN SELATAN';
		$data['record'] 	= FALSE;
		$data['content'] 	= $this->folder.'default';
		$data['style'] 		= $this->folder.'style';
		$data['js'] 		= $this->folder.'js';
		$data['kota'] 		= $this->data->get_kota();
		$data['kode'] 		= $this->data->get_kode();
		$data['jenis_cabor']= $this->data->get_jenis_cabor();
		
		$this->load->view($data['content'], $data);
	}

	public function register()
    {	
		if($this->validation()){
			$this->upload_file();
			if($_FILES['file']['name'])
			 {
			if ($this->upload->do_upload('file')){
				$dokumen = $this->upload->data();
				$cabor = array(
				'kode' => $this->data->get_kode(),
				'cabor' => $this->input->post('cabor', TRUE),
				'nickname' => $this->input->post('nickname', TRUE),
				//'npsn' => $this->input->post('npsn', TRUE),
				//'bentuk' => $this->input->post('bentuk', TRUE),
				'status' => $this->input->post('status', TRUE),
				'kota_id' => $this->input->post('kota', TRUE),
				'kecamatan_id' => $this->input->post('kecamatan', TRUE),
				'kelurahan_id' => $this->input->post('kelurahan', TRUE),
				'telpon' => $this->input->post('telpon', TRUE),
				'alamat' => $this->input->post('alamat', TRUE),
				'dokumen' => $dokumen['file_name'],
				'created_at' => date('Y-m-d H:i:s')
			);
			
			$user = array(
				'kode_cabor' => $this->data->get_kode(),
				'fullname' => $this->input->post('cabor', TRUE),
				'email' => $this->input->post('email', TRUE),
				'password' => password_hash($this->input->post('password'), PASSWORD_BCRYPT),
				'telpon' => $this->input->post('telpon', TRUE),
				'level' => 3,
				//'bentuk' => $this->input->post('bentuk', TRUE),
				'active' => 0,
				'created_at' => date('Y-m-d H:i:s')
			);

			$insert = $this->data->insert_data($cabor, $user);
			if($insert){
				$jenis_cabor = $this->input->post('jenis_cabor');
                $result = array();
                foreach($jenis_cabor AS $key => $val){
                    if($_POST['jenis_cabor'][$key] != ''){
                        $result[] = array(
                            "cabor_id"  => $insert,
                            "jenis_cabor_id"  => $_POST['jenis_cabor'][$key]
                        );
                    }
                }
				$this->db->insert_batch('cabor_jenis', $result);
				
				helper_log("add", "Registrasi Cabor");
				$this->session->set_flashdata('flashconfirm','Proses Pendaftaran Berhasil, Mohon Cek Email Secara Berkala Jika Sudah Kami Aktivasi.');
				//$this->send_mail($user['fullname'], $user['email'], $user['password']);
				redirect('auth/registrasi');
			}else{
				$this->session->set_flashdata('flasherror','Proses Pendaftaran Gagal, Mohon Dicoba Kembali.');
				redirect('auth/registrasi');
			}
		}

		} else{
			$this->session->set_flashdata('flasherror','Ada Kesalahan Dalam Upload Data');
			redirect('auth/registrasi');
		}

		} else{
			redirect('auth/registrasi');
		}
    }
	
	private function validation()
    {
        //$data = array('success' => false, 'messages' => array());
        
		$this->form_validation->set_rules("cabor", "Nama Cabor", "trim|required");
		//$this->form_validation->set_rules("npsn", "NPSN Sekolah", "trim|required|callback_npsn");
		//$this->form_validation->set_rules("bentuk", "Bentuk Pendidikan", "trim|required");
		//$this->form_validation->set_rules("status", "Status Sekolah", "trim|required");
		$this->form_validation->set_rules("kota", "Kota", "trim|required");
		$this->form_validation->set_rules("kecamatan", "Kecamatan", "trim|required");
		$this->form_validation->set_rules("kelurahan", "Kelurahan", "trim|required");
		$this->form_validation->set_rules("telpon", "Telpon/HP Sekolah", "trim|required");
		$this->form_validation->set_rules("email", "Email Sekolah / Email Login", "trim|required|valid_email|callback_email");
		$this->form_validation->set_rules("password", "Password", "trim|required|min_length[8]|max_length[18]");
		$this->form_validation->set_rules("repassword", "Ulangi Password", "trim|required|matches[password]");
		
        return $this->form_validation->run();
	}
	
	public function email($str=null)
	{
		$query = $this->db->get_where('users', array('email'=>$str,'deleted_at'=>null));
		if($query->num_rows() > 0){
			$this->form_validation->set_message('email', '{field} sudah tersedia atau telah digunakan.');
			return FALSE;
		}else{
		 	return TRUE;
		}
	}

	/*public function npsn($str=null)
	{
		$query = $this->db->get_where('sekolah', array('npsn'=>$str,'deleted_at'=>null));
		if($query->num_rows() > 0){
			$this->form_validation->set_message('npsn', '{field} sudah tersedia atau telah digunakan.');
			return FALSE;
		}else{
		 	return TRUE;
		}
	}*/

	public function reset_password()
	{
		$data['head'] 		= 'Reset Password';
		$data['record'] 	= $this->data->get_all();
		$data['content'] 	= $this->folder.'password';
		$data['style'] 		= $this->folder.'style';
		$data['js'] 		= $this->folder.'js';
		
		$this->load->view($data['content'], $data);
	}

	public function send_password()
    {
		if($this->email_validation()){
			$find = $this->db->get_where('users', array('email'=>$this->input->post('email'), 'deleted_at'=> null, 'level'=>5))->row();
			if(!$find){
				$this->session->set_flashdata('flasherror','Alamat Email Yang Anda Masukan Tidak Terdapat Dalam Database Kami.');
				redirect('registrasi/asn/reset_password');
			}else{
				$password = $this->random_password();
				$data = array(
					'ip_address' => $this->input->ip_address(),
					'password' => password_hash($password, PASSWORD_BCRYPT),
					'updated_id' => '0',
					'updated_at' => date('Y-m-d H:i:s')
				);
				$this->data->update($data, $find->id);
				$this->session->set_flashdata('flashconfirm','Password Anda Sudah Terkirim, Mohon Cek Email Anda Secara Berkala.');
				$this->send_mail_password($this->input->post('email'), $password);
				redirect('registrasi/asn/reset_password');
			}
		}else{
			$this->index();
		}
    }

	private function email_validation()
    {
        //$data = array('success' => false, 'messages' => array());
		$this->form_validation->set_rules("email", "Email", "trim|required|valid_email");
        return $this->form_validation->run();
	}

	private function random_password() 
	{
		$alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
		$password = array(); 
		$alpha_length = strlen($alphabet) - 1; 
		for ($i = 0; $i < 8; $i++) 
		{
			$n = rand(0, $alpha_length);
			$password[] = $alphabet[$n];
		}
		return implode($password); 
	}

	private function send_mail($nama=null, $email=null, $password=null)
	{
		//Load email library
		$this->load->library('email');
		$this->load->library('encrypt');

		//SMTP & mail configuration
		$config = array(
			'protocol'  => 'smtp',
			'smtp_host' => 'ssl://smtp.gmail.com',
			'smtp_port' => 465,
			'smtp_user' => 'hendryprojek@gmail.com',
			'smtp_pass' => 'apakatadunia',
			'mailtype'  => 'html',
			'charset'   => 'utf-8'
		);
		$this->email->initialize($config);
		$this->email->set_mailtype("html");
		$this->email->set_newline("\r\n");

		//Email content
		$htmlContent = '<h3>Data Registrasi Cabang Olahraga (Cabor)</h3>';
		$htmlContent .= '<p>Anda telah mendaftarkan cabang Olahraga '.$nama.' dengan email pengguna sistem '.$email.' dan password '.$password.' pada Cabor.</p>';
		$htmlContent .= '<p>Jika anda belum dapat melakukan akses pada halaman login kemungkinan akun anda belum dapat diverifikasi atau diaktifkan oleh administrator.</p>';

		$this->email->to($email);
		$this->email->from('hendryprojek@gmail.com','SIRKAS DISDIK KOTA BANJARMASIN');
		$this->email->subject('no-replay : Data Registrasi Sekolah');
		$this->email->message($htmlContent);
		//Send email
		$this->email->send();
	}

	private function send_mail_password($email=null, $password=null)
	{
		//Load email library
		$this->load->library('email');
		$this->load->library('encrypt');

		//SMTP & mail configuration
		$config = array(
			'protocol'  => 'smtp',
			'smtp_host' => 'ssl://smtp.gmail.com',
			'smtp_port' => 465,
			'smtp_user' => 'hendryprojek@gmail.com',
			'smtp_pass' => 'apakatadunia',
			'mailtype'  => 'html',
			'charset'   => 'utf-8'
		);

		$this->email->initialize($config);
		$this->email->set_mailtype("html");
		$this->email->set_newline("\r\n");

		//Email content
		$htmlContent = '<h3>Permohonan Password Baru SIRKAS DISDIK</h3>';
		$htmlContent .= '<p>Password SIRKAS DISDIK Anda Adalah : '.$password.'</p>';

		$this->email->to($email);
		$this->email->from('hendryprojek5@gmail.com','SIRKAS DISDIK KOTA BANJARMASIN');
		$this->email->subject('no-replay : Permohonan Password Baru SIRKAS DISDIK KOTA BANJARMASIN');
		$this->email->message($htmlContent);
		//Send email
		$this->email->send();
	}

	/*private function upload_file(){
		$this->load->library('upload');
        $nmfile = "iup_".time(); //nama file saya beri nama langsung dan diikuti fungsi time
        $config['upload_path'] = './dokumen/'; //path folder
        $config['allowed_types'] = 'pdf'; //type yang dapat diakses bisa anda sesuaikan
        $config['max_size'] = '10240'; //maksimum besar file 2M
        $config['max_width']  = '5000'; //lebar maksimum 1288 px
        $config['max_height']  = '5000'; //tinggi maksimu 768 px
        $config['file_name'] = $nmfile; //nama yang terupload nantinya
        $this->upload->initialize($config);
	}*/

	private function upload_file(){
		$this->load->library('upload');
		$kode =  $this->data->get_kode();
		$modul =  'file';

		if($kode && $modul){
			folder($kode);
			$config['upload_path'] = './dokumen/'.$kode;
			$nmfile = $kode.'_'.$modul; //nama file saya beri nama langsung dan diikuti fungsi time
		}else{
			$config['upload_path'] = './dokumen/'; //path folder
			$nmfile = "dokumen_".time(); //nama file saya beri nama langsung dan diikuti fungsi time
		}

        $config['allowed_types'] = 'pdf|jpg|png|bmp'; //type yang dapat diakses bisa anda sesuaikan
        $config['max_size'] = '2048'; //maksimum besar file 2M
        $config['max_width']  = '5000'; //lebar maksimum 5000 px
		$config['max_height']  = '5000'; //tinggi maksimu 5000 px
		$config['overwrite']  = TRUE; //tinggi maksimu 5000 px
        $config['file_name'] = $nmfile; //nama yang terupload nantinya
        $this->upload->initialize($config);
	}

	public function get_kecamatan(){
        $id = $this->input->post('id');
        $kecamatan = $this->data->get_kecamatan($id);
        if(!empty($kecamatan)){
            //$selected = (set_value('parent')) ? set_value('parent') : '';
			$selected = set_value('kecamatan');
            echo form_dropdown('kecamatan', $kecamatan, $selected, "class='form-control select2' name='kecamatan' id='kecamatan'");
        }else{
            echo form_dropdown('kecamatan', array(''=>'Pilih Salah Satu'), '', "class='form-control select2' name='kecamatan' id='kecamatan'");
        }
    }

	public function get_kelurahan(){
        $id = $this->input->post('id');
        $kelurahan = $this->data->get_kelurahan($id);
        if(!empty($kelurahan)){
            //$selected = (set_value('parent')) ? set_value('parent') : '';
			$selected = set_value('kelurahan');
            echo form_dropdown('kelurahan', $kelurahan, $selected, "class='form-control select2' name='kelurahan' id='kelurahan'");
        }else{
            echo form_dropdown('kelurahan', array(''=>'Pilih Salah Satu'), '', "class='form-control select2' name='kelurahan' id='kelurahan'");
        }
    }
}
