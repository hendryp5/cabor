<script>
$(function () {
	$('.select2').select2();

	$("#kota").change(function(){
		var kota = $("#kota").val();
		$.ajax({
				type: "POST",
				async: false,
				url : "<?php echo site_url('auth/registrasi/get_kecamatan')?>",
				data: {
				'id': kota,
				'<?php echo $this->security->get_csrf_token_name(); ?>' : '<?php echo $this->security->get_csrf_hash(); ?>'
				},
				success: function(data){
					$('#kecamatan').html(data);
				}
		});
	});

	$("#kecamatan").change(function(){
		var kecamatan = $("#kecamatan").val();
		$.ajax({
				type: "POST",
				async: false,
				url : "<?php echo site_url('auth/registrasi/get_kelurahan')?>",
				data: {
				'id': kecamatan,
				'<?php echo $this->security->get_csrf_token_name(); ?>' : '<?php echo $this->security->get_csrf_hash(); ?>'
				},
				success: function(data){
					$('#kelurahan').html(data);
				}
		});
	});
});
</script>