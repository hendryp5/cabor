<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title><?= $title; ?></title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <link rel="stylesheet" href="<?= base_url('asset/bootstrap/css/bootstrap.min.css'); ?>">
  <link rel="stylesheet" href="<?= base_url('asset/font-awesome/css/font-awesome.min.css'); ?>">
  <link rel="stylesheet" href="<?= base_url('asset/ionicons/css/ionicons.min.css'); ?>">
  <link rel="stylesheet" href="<?= base_url('asset/plugins/select2/select2.min.css'); ?>" />
  <link rel="stylesheet" href="<?= base_url('asset/dist/css/AdminLTE.min.css'); ?>">
  <link rel="stylesheet" href="<?= base_url('asset/plugins/iCheck/square/blue.css'); ?>">
  
  <link rel="apple-touch-icon" sizes="57x57" href="<?php echo base_url(); ?>asset/images/apple-icon-57x57.png">
  <link rel="apple-touch-icon" sizes="60x60" href="<?php echo base_url(); ?>asset/images/apple-icon-60x60.png">
  <link rel="apple-touch-icon" sizes="72x72" href="<?php echo base_url(); ?>asset/images/apple-icon-72x72.png">
  <link rel="apple-touch-icon" sizes="76x76" href="<?php echo base_url(); ?>asset/images/apple-icon-76x76.png">
  <link rel="apple-touch-icon" sizes="114x114" href="<?php echo base_url(); ?>asset/images/apple-icon-114x114.png">
  <link rel="apple-touch-icon" sizes="120x120" href="<?php echo base_url(); ?>asset/images/apple-icon-120x120.png">
  <link rel="apple-touch-icon" sizes="144x144" href="<?php echo base_url(); ?>asset/images/apple-icon-144x144.png">
  <link rel="apple-touch-icon" sizes="152x152" href="<?php echo base_url(); ?>asset/images/apple-icon-152x152.png">
  <link rel="apple-touch-icon" sizes="180x180" href="<?php echo base_url(); ?>asset/images/apple-icon-180x180.png">
  <link rel="icon" type="image/png" sizes="192x192"  href="<?php echo base_url(); ?>asset/images/android-icon-192x192.png">
  <link rel="icon" type="image/png" sizes="32x32" href="<?php echo base_url(); ?>asset/images/favicon-32x32.png">
  <link rel="icon" type="image/png" sizes="96x96" href="<?php echo base_url(); ?>asset/images/favicon-96x96.png">
  <link rel="icon" type="image/png" sizes="16x16" href="<?php echo base_url(); ?>asset/images/favicon-16x16.png">
  <link rel="manifest" href="<?php echo base_url(); ?>asset/images/manifest.json">
  <meta name="msapplication-TileColor" content="#ffffff">
  <meta name="msapplication-TileImage" content="<?php echo base_url(); ?>asset/images/ms-icon-144x144.png">
  <meta name="theme-color" content="#ffffff">
  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
  <style>.login-page{background-repeat: no-repeat; background-attachment: fixed; background-position: center; background-size: cover;}.login-box-body{background: rgba(255,255,255, 0.93) !important}</style>
</head>
<body class="hold-transition login-page">
<section class="invoice">
      <!-- title row -->
      <div class="row">
        <div class="col-xs-12">
  <!-- <div class="login-logo">
    <a href="#"><b>E-</b>PELAPORAN</a>
  </div> -->
  <!-- /.login-logo -->
  
  <div class="login-logo">
    <a href="#"><b>REGISTRASI PENGURUS PROVINSI</b></a>
  </div>
    <p class="login-box-msg">Silahkan Isi Data Dengan Lengkap dan Benar</p>
	<?php if($this->session->flashdata('flashconfirm')): ?>
<div class="alert alert-success alert-dismissable">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    <?php echo $this->session->flashdata('flashconfirm'); ?>
</div>
<?php endif; ?>
<?php if($this->session->flashdata('flasherror')): ?>
<div class="alert alert-warning alert-dismissable">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    <?php echo $this->session->flashdata('flasherror'); ?>
</div>
<?php endif; ?>
  <?= form_open_multipart('auth/registrasi/register', array('class' => 'form', 'id' => 'formID', 'role'=>'form'));?>
  <!--  -->
<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
<!-- box-body -->
    <div class="row">
        <div class="col-md-12">
            <div class="form-group <?php echo form_error('kode_cabor') ? 'has-error' : null; ?>">
                <?php
                echo form_label('Kode Registrasi','kode_cabor');
                $data = array('readonly class'=>'form-control','name'=>'kode_cabor','id'=>'kode_cabor','type'=>'text','value'=>set_value('kode_cabor',$kode));
                echo form_input($data);
                echo form_error('kode_cabor') ? form_error('kode_cabor', '<p class="help-block">','</p>') : '';
                ?>
            </div>
        </div>

        <div class="col-md-12">
            <div class="form-group <?php echo form_error('cabor') ? 'has-error' : null; ?>">
                <?php
                echo form_label('Nama Pengprov','cabor');
                $data = array('required class'=>'form-control','name'=>'cabor','id'=>'cabor','type'=>'text','value'=>set_value('cabor'));
                echo form_input($data);
                echo form_error('cabor') ? form_error('cabor', '<p class="help-block">','</p>') : 'Contoh : Asosiasi Bola Tangan Indonesia';
                ?>
            </div>
        </div>

        <div class="col-md-12">
            <div class="form-group <?php echo form_error('nickname') ? 'has-error' : null; ?>">
                <?php
                echo form_label('Kode Pengprov','nickname');
                $data = array('required class'=>'form-control','name'=>'nickname','id'=>'nickname','type'=>'text','value'=>set_value('nickname'));
                echo form_input($data);
                echo form_error('nickname') ? form_error('nickname', '<p class="help-block">','</p>') : 'Contoh : ABTI';
                ?>
            </div>
        </div>

        <div class="col-md-12">
            <div class="form-group <?php echo form_error('jenis_cabor') ? 'has-error' : null; ?>">
                <?php 
                echo form_label('Cabang Olahraga','jenis_cabor');
                $selected = set_value('jenis_cabor[]');
                echo form_dropdown('jenis_cabor[]', $jenis_cabor, $selected, "required multiple class='form-control select2' data-placeholder='Pilih Cabang Olahraga' style='width: 100%;' ");
                echo form_error('jenis_cabor[]') ? form_error('jenis_cabor[]', '<p class="help-block">','</p>') : 'Pilih Cabang Olahraga. Bisa Lebih Dari Satu Pilihan';
                ?>
            </div>
        </div>
        
        <div class="col-md-12">
            <div class="form-group <?php echo form_error('alamat') ? 'has-error' : null; ?>">
                <?php
                echo form_label('Alamat Sekertariat','alamat');
                $data = array('required class'=>'form-control','name'=>'alamat','rows'=>'2','cols'=>'4','id'=>'alamat','type'=>'text','value'=>set_value('alamat'));
                echo form_textarea($data);
                echo form_error('alamat') ? form_error('alamat', '<p class="help-block">','</p>') : '';
                ?>
            </div>
        </div>

        <div class="col-md-4">
            <div class="form-group <?php echo form_error('kota') ? 'has-error' : null; ?>">
                <?php 
                echo form_label('Kota / Kabupaten','kota');
                $selected = set_value('kota');
                echo form_dropdown('kota', $kota, $selected, "required class='form-control select2' name='kota' id='kota'");
                echo form_error('kota') ? form_error('kota', '<p class="help-block">','</p>') : '';
                ?>
            </div>
        </div>

        <div class="col-md-4">
            <div class="form-group <?php echo form_error('kecamatan') ? 'has-error' : null; ?>">
                <?php 
                echo form_label('Kecamatan','kecamatan');
                $selected = set_value('kecamatan');
                $kecamatan = array(''=>'Pilih Salah Satu');
                echo form_dropdown('kecamatan', $kecamatan, $selected, "required class='form-control select2' name='kecamatan' id='kecamatan'");
                echo form_error('kecamatan') ? form_error('kecamatan', '<p class="help-block">','</p>') : '';
                ?>
            </div>
        </div>

        <div class="col-md-4">
            <div class="form-group <?php echo form_error('kelurahan') ? 'has-error' : null; ?>">
                <?php 
                echo form_label('Kelurahan','kelurahan');
                $selected = set_value('kelurahan');
                $kelurahan = array(''=>'Pilih Salah Satu');
                echo form_dropdown('kelurahan', $kelurahan, $selected, "required class='form-control select2' name='kelurahan' id='kelurahan'");
                echo form_error('kelurahan') ? form_error('kelurahan', '<p class="help-block">','</p>') : '';
                ?>
            </div>
        </div>

        <div class="col-md-12">
            <div class="form-group <?php echo form_error('telpon') ? 'has-error' : null; ?>">
                <?php
                echo form_label('Telpon Sekertariat/Pengelola/Pengurus','telpon');
                $data = array('required class'=>'form-control','name'=>'telpon','id'=>'telpon','type'=>'text','value'=>set_value('telpon'));
                echo form_input($data);
                echo form_error('telpon') ? form_error('telpon', '<p class="help-block">','</p>') : '';
                ?>
            </div>
        </div>

        <div class="col-md-12">
          <div class="form-group <?php echo form_error('file') ? 'has-error' : null; ?>">
            <?php
            echo form_label('Upload Dokumen SK Pengurus','file');
            $data = array('class'=>'form-control','name'=>'file','id'=>'file','type'=>'file','value'=>set_value('file'));
            echo form_input($data);
            echo form_error('file') ? form_error('file', '<p class="help-block">','</p>') : 'Silahkan Upload Dokumen PDF|JPG|PNG. Ukuran Maksimal 2MB';
            ?>
          </div>
        </div>
       
        <div class="col-md-12"><h4>Data Login Sistem Informasi</h4></div>
        <div class="col-md-12">
            <div class="form-group <?php echo form_error('email') ? 'has-error' : null; ?>">
                <?php
                echo form_label('Email Untuk Login','email');
                $data = array('required class'=>'form-control','name'=>'email','id'=>'email','type'=>'email','value'=>set_value('email'));
                echo form_input($data);
                echo form_error('email') ? form_error('email', '<p class="help-block">','</p>') : '';
                ?>
            </div>
        </div>
        <div class="col-md-12">
            <div class="form-group <?php echo form_error('password') ? 'has-error' : null; ?>">
                <?php
                echo form_label('Password','password');
                $data = array('required class'=>'form-control','name'=>'password','id'=>'password','type'=>'password','value'=>set_value('password'));
                echo form_input($data);
                echo form_error('password') ? form_error('password', '<p class="help-block">','</p>') : 'Masukan Minimal 8 Digit Password';
                ?>
            </div>
        </div>

        <div class="col-md-12">
            <div class="form-group <?php echo form_error('repassword') ? 'has-error' : null; ?>">
                <?php
                echo form_label('Re Password','repassword');
                $data = array('required class'=>'form-control','name'=>'repassword','id'=>'repassword','type'=>'password','value'=>set_value('repassword'));
                echo form_input($data);
                echo form_error('repassword') ? form_error('repassword', '<p class="help-block">','</p>') : 'Ulangi Password Diatas';
                ?>
            </div>
        </div>
        
    </div>
<!-- ./box-body -->
    <button type="submit" class="btn btn-sm btn-flat btn-primary"><i class="fa fa-save"></i> Registrasi</button>
    <button type="reset" class="btn btn-sm btn-flat btn-warning"><i class="fa fa-refresh"></i> Reset</button>
    <a class="btn btn-sm btn-flat btn-danger" href="<?= site_url('auth'); ?>"><i class="fa fa-sign-in"></i> Login</a>
<?php form_close(); ?>
  </div>
  <!-- /.login-box-body -->

</div>
</section>

<!-- /.login-box -->

<!-- jQuery 2.2.3 -->

<script src="<?= base_url('asset/plugins/jQuery/jquery-2.2.3.min.js'); ?>"></script>
<script src="<?= base_url('asset/bootstrap/js/bootstrap.min.js'); ?>"></script>
<script src="<?= base_url('asset/plugins/select2/select2.full.min.js'); ?>"></script>
<script>
$(function () {
  $('.select2').select2();

  $("#kota").change(function(){
    var kota = $("#kota").val();
    $.ajax({
        type: "POST",
        async: false,
        url : "<?php echo site_url('auth/registrasi/get_kecamatan')?>",
        data: {
        'id': kota,
        '<?php echo $this->security->get_csrf_token_name(); ?>' : '<?php echo $this->security->get_csrf_hash(); ?>'
        },
        success: function(data){
          $('#kecamatan').html(data);
        }
    });
  });

  $("#kecamatan").change(function(){
    var kecamatan = $("#kecamatan").val();
    $.ajax({
        type: "POST",
        async: false,
        url : "<?php echo site_url('auth/registrasi/get_kelurahan')?>",
        data: {
        'id': kecamatan,
        '<?php echo $this->security->get_csrf_token_name(); ?>' : '<?php echo $this->security->get_csrf_hash(); ?>'
        },
        success: function(data){
          $('#kelurahan').html(data);
        }
    });
  });
});
</script>
</body>
</html>
