<form id="formID" role="form" action="<?= site_url('rkas/detail2/'.$link); ?>" method="post">
<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
<input type="hidden" name="kertas_id" value="<?= $id; ?>" />
<input type="hidden" name="program_id" value="<?= $program1; ?>" />
<input type="hidden" name="kegiatan_id" value="<?= $kegiatan1; ?>" />
<input type="hidden" name="kategori_id" value="<?= $kategori1; ?>" />
<input type="hidden" name="belanja_id" value="<?= $belanja1; ?>" />
<input type="hidden" name="tahun" value="<?= $tahun; ?>" />
<!-- box-body -->
<div class="box-body">
	<div class="row">
		<div class="col-md-12">
			<dl>
				<dt>Program</dt>
				<dd><?= $program ? $program->kode.'-'.$program->program : ''; ?></dd>
				<dt>Kegiatan</dt>
				<dd><?= $kegiatan ? $kegiatan->kegiatan : ''; ?></dd>
				<dt>Kategori</dt>
				<dd><?= $kategori ? $kategori->kategori : ''; ?></dd>
				<dt>Belanja</dt>
				<dd><?= $belanja ? $belanja->belanja : ''; ?></dd>
			</dl>
		</div>


		<div class="col-md-12">
			<div class="form-group <?php echo form_error('jumlah') ? 'has-error' : null; ?>">
				<?php
				echo form_label('Total Jumlah','jumlah');
				$data = array('class'=>'form-control number','name'=>'jumlah','id'=>'jumlah','type'=>'text','value'=>set_value('jumlah', $record->jumlah));
				echo form_input($data);
				echo form_error('jumlah') ? form_error('jumlah', '<p class="help-block">','</p>') : '';
				?>
			</div>
		</div>

	
	</div>
</div>
<!-- ./box-body -->
<div class="box-footer">
<button type="submit" class="btn btn-sm btn-flat btn-success" ><i class="fa fa-save"></i> Simpan</button>
<button type="button" class="btn btn-sm btn-flat btn-danger" data-dismiss="modal"><i class="fa fa-close"></i> Keluar</button>
</div>
</form>
