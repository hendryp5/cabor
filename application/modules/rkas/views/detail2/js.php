<script>
$(function () {
	//$('.select2').select2();
	$('.number').number(true);

	$(document).on('click', '#getProgram', function(e){
		e.preventDefault();
		var uid = $(this).data('id'); // get id of clicked row
		var umodul = $(this).data('modul'); // get id of clicked row
		var ukode = $(this).data('kode'); // get id of clicked row
		$('#program-content').html(''); // leave this div blank
		$('#program-loader').show(); // load ajax loader on button click
		$.ajax({
			url: '<?= site_url('rkas/proses/get_program'); ?>',
			type: 'POST',
			data: {
					'id': uid,
					'modul' : umodul,
					'kode' : ukode,
					'<?php echo $this->security->get_csrf_token_name(); ?>' : '<?php echo $this->security->get_csrf_hash(); ?>'
			},
			dataType: 'html'
		})
		.done(function(data){
			$('.select2').select2();
			$('#program-content').html(''); // blank before load.
			$('#program-content').html(data); // load here
			$('#program-loader').hide(); // hide loader
		})
		.fail(function(){
			$('#program-content').html('<i class="glyphicon glyphicon-info-sign"></i> Something went wrong, Please try again...');
			$('#program-loader').hide();
		});
	});
});
</script>