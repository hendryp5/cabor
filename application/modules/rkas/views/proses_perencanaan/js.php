<script>
$(function () {
	$('.select2').select2();
	$('.number').number(true);


	$(document).on('click', '#getDetail', function(e){
		e.preventDefault();
		var uid = $(this).data('id'); // get id of clicked row
		var umodul = $(this).data('modul'); // get id of clicked row
		var ukode = $(this).data('kode'); // get id of clicked row
		var uprogram = $(this).data('program'); // get id of clicked row
		var ukegiatan = $(this).data('kegiatan'); // get id of clicked row
		var ukategori = $(this).data('kategori'); // get id of clicked row
		var ubelanja = $(this).data('belanja'); // get id of clicked row
		var utahun = $(this).data('tahun'); // get id of clicked row
		$('#detail-content').html(''); // leave this div blank
		$('#detail-loader').show(); // load ajax loader on button click
		$.ajax({
			url: '<?= site_url('rkas/detail2/created'); ?>',
			type: 'POST',
			data: {
					'id': uid,
					'modul' : umodul,
					'kode' : ukode,
					'program' : uprogram,
					'kegiatan' : ukegiatan,
					'kategori' : ukategori,
					'belanja' : ubelanja,
					'tahun' : utahun,
					'<?php echo $this->security->get_csrf_token_name(); ?>' : '<?php echo $this->security->get_csrf_hash(); ?>'
			},
			dataType: 'html'
		})
		.done(function(data){
			$('#detail-content').html(''); // blank before load.
			$('#detail-content').html(data); // load here
			$('.number').number(true);
			$('#detail-loader').hide(); // hide loader
		})
		.fail(function(){
			$('#detail-content').html('<i class="glyphicon glyphicon-info-sign"></i> Something went wrong, Please try again...');
			$('#detail-loader').hide();
		});
	});

	$(document).on('click', '#getDetailEdit', function(e){
		e.preventDefault();
		var uid = $(this).data('id'); // get id of clicked row
		var umodul = $(this).data('modul'); // get id of clicked row
		var ukode = $(this).data('kode'); // get id of clicked row
		var ukolom = $(this).data('kolom'); // get id of clicked row
		var uprogram = $(this).data('program'); // get id of clicked row
		var ukegiatan = $(this).data('kegiatan'); // get id of clicked row
		var ukategori = $(this).data('kategori'); // get id of clicked row
		var ubelanja = $(this).data('belanja'); // get id of clicked row
		var utahun = $(this).data('tahun'); // get id of clicked row
		$('#detail-content').html(''); // leave this div blank
		$('#detail-loader').show(); // load ajax loader on button click
		$.ajax({
			url: '<?= site_url('rkas/detail2/updated/'); ?>'+ukolom,
			type: 'POST',
			data: {
					'id': uid,
					'modul' : umodul,
					'kolom' : ukolom,
					'kode' : ukode,
					'program' : uprogram,
					'kegiatan' : ukegiatan,
					'kategori' : ukategori,
					'belanja' : ubelanja,
					'tahun' : utahun,
					'<?php echo $this->security->get_csrf_token_name(); ?>' : '<?php echo $this->security->get_csrf_hash(); ?>'
			},
			dataType: 'html'
		})
		.done(function(data){
			$('#detail-content').html(''); // blank before load.
			$('#detail-content').html(data); // load here
			$('.number').number(true);
			$('#detail-loader').hide(); // hide loader
		})
		.fail(function(){
			$('#detail-content').html('<i class="glyphicon glyphicon-info-sign"></i> Something went wrong, Please try again...');
			$('#detail-loader').hide();
		});
	});
});



function delDetail(id) {
  var r = confirm("Anda Yakin Ingin Menghapus Data Ini.");
  if (r == true) {
	console.log("You pressed OK!");
		var process = '<?= site_url('rkas/detail2/deleted/'); ?>'+id;
	  // ajax delete data to database
        $.ajax({
            url : process,
            type: "POST",
            data: {'<?php echo $this->security->get_csrf_token_name(); ?>' : '<?php echo $this->security->get_csrf_hash(); ?>'},
            dataType: "JSON",
            success: function(data)
            {
                $('#message').append('<div class="alert alert-danger">' +
                        '<span class="glyphicon glyphicon-ok"></span>' +
                        ' Data berhasil di hapus.' +
                        '</div>');
                       
                // tutup pesan
                $('.alert-danger').delay(250).show(10, function() {
                    $(this).delay(1000).hide(10, function() {
                    $(this).remove();
                    });
                })
                
                location.reload();
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error deleting data');
            }
        });
  } else {
    console.log("You pressed Cancel!");
  }
}

function pengajuan(id,id2) {
  var r = confirm("Apakah Anda Ingin Mengajukan Draf Rencana Anggaran Ini. Pastikan Semua Telah Benar.");
  if (r == true) {
	console.log("You pressed OK!");
		var process = '<?= site_url('rkas/proses_perencanaan/pengajuan/'); ?>'+id+'/'+id2;
	  // ajax delete data to database
        $.ajax({
            url : process,
            type: "POST",
            data: {'<?php echo $this->security->get_csrf_token_name(); ?>' : '<?php echo $this->security->get_csrf_hash(); ?>'},
            dataType: "JSON",
            success: function(data)
            {
                $('#message').append('<div class="alert alert-success">' +
                        '<span class="glyphicon glyphicon-ok"></span>' +
                        ' Draf Rencana Anggaran Berhasil Di Ajukan.' +
                        '</div>');
                       
                // tutup pesan
                $('.alert-danger').delay(250).show(10, function() {
                    $(this).delay(1000).hide(10, function() {
                    $(this).remove();
                    });
                })
                
                location.reload();
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error Pengajuan data');
            }
        });
  } else {
    console.log("You pressed Cancel!");
  }
}
</script>