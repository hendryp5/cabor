<form id="formID" role="form" action="<?= site_url('rkas/program/'.$link); ?>" method="post">
<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
<input type="hidden" name="kertas_id" value="<?= $id; ?>" />
<input type="hidden" name="program_id" value="<?= $kode; ?>" />
<input type="hidden" name="kategori_idx" value="<?= $record->kategori_id; ?>" />
<!-- box-body -->
<div class="box-body">
	<div class="row">
		<div class="col-md-12">
			<dl>
				<dt>Program</dt>
				<dd><?= $program ? $program->kode.'-'.$program->program : ''; ?></dd>
			</dl>
		</div>
		<!-- <div class="col-md-12">
			<div class="form-group <?php //echo form_error('bos') ? 'has-error' : null; ?>">
				<?php 
				// echo form_label('Kategori BOS','bos');
				// $selected = set_value('bos', $record->bos);
				// $kegiatan = array(''=>'Pilih Salah Satu','1'=>'BOS Reguler','2'=>'BOS Afirmasi','3'=>'BOS Khusus');
				// echo form_dropdown('bos', $kegiatan, $selected, "required class='form-control select2' name='bos' id='bos' style='width: 100% !important;
				// padding: 0;'");
				// echo form_error('bos') ? form_error('bos', '<p class="help-block">','</p>') : '';
				?>
			</div>
		</div> -->
		<div class="col-md-12">
			<div class="form-group <?php echo form_error('kegiatan') ? 'has-error' : null; ?>">
				<?php 
				echo form_label('Jenis kegiatan','kegiatan');
				$selected = set_value('kegiatan', $record->kegiatan_id);
				echo form_dropdown('kegiatan', $kegiatan, $selected, "required class='form-control select2' name='kegiatan' id='kegiatan' style='width: 100% !important;
				padding: 0;'");
				echo form_error('kegiatan') ? form_error('kegiatan', '<p class="help-block">','</p>') : '';
				?>
			</div>
		</div>

		<div class="col-md-12">
			<div class="form-group <?php echo form_error('kategori') ? 'has-error' : null; ?>">
				<?php 
				echo form_label('Kode kategori','kategori');
				$selected = set_value('kategori', $record->kategori_id);
				//$kategori = array(''=>'Pilih Salah Satu','5.2.1'=>'5.2.1 - kegiatan Pegawai','5.2.2'=>'5.2.2 - kegiatan Barang dan Jasa','5.2.3'=>'5.2.3 - kegiatan Modal');
				echo form_dropdown('kategori', $kategori, $selected, "required class='form-control select2' name='kategori' id='kategori' style='width: 100% !important;
				padding: 0;'");
				echo form_error('kategori') ? form_error('kategori', '<p class="help-block">','</p>') : '';
				?>
			</div>
		</div>

		<!-- <div class="col-md-12">
			<div class="form-group <?php //echo form_error('kategori') ? 'has-error' : null; ?>">
				<?php
				//echo form_label('Kode kategori','kategori');
				//$data = array('required class'=>'form-control','name'=>'kategori','id'=>'kategori','type'=>'text','value'=>set_value('kategori', $record->kategori));
				//echo form_input($data);
				//echo form_error('kategori') ? form_error('kategori', '<p class="help-block">','</p>') : '';
				?>
			</div>
		</div> -->

		<!-- <div class="col-md-12">
			<div class="form-group <?php echo form_error('uraian') ? 'has-error' : null; ?>">
				<?php
				echo form_label('Uraian Kegiatan','uraian');
				$data = array('required readonly class'=>'form-control','name'=>'uraian','id'=>'uraian','type'=>'text','value'=>set_value('uraian', $record->uraian));
				echo form_input($data);
				echo form_error('uraian') ? form_error('uraian', '<p class="help-block">','</p>') : '';
				?>
			</div>
		</div> -->
	</div>
</div>
<!-- ./box-body -->
<div class="box-footer">
<button type="submit" class="btn btn-sm btn-flat btn-success" ><i class="fa fa-save"></i> Simpan</button>
<button type="button" class="btn btn-sm btn-flat btn-danger" data-dismiss="modal"><i class="fa fa-close"></i> Keluar</button>
</div>
</form>
<script>
	$('.select2').select2({ width: 'resolve' });
    $("#subprogram").change(function(){
		var subprogram = $("#subprogram").val();
		$.ajax({
				type: "POST",
				async: false,
				url : "<?php echo site_url('rkas/program/get_kegiatan')?>",
				data: {
				'id': subprogram,
				'<?php echo $this->security->get_csrf_token_name(); ?>' : '<?php echo $this->security->get_csrf_hash(); ?>'
				},
				success: function(data){
					$('#kegiatan').html(data);
				}
		});
	});
	
	// $("#kegiatan").change(function(){
	// 	var kegiatan = $("#kegiatan").val();
	// 	$("#kategori").focus();
	// 	$("#kategori").val(kegiatan+'.');
    // });

	$("#kegiatan").change(function(){
		var kegiatan = $("#kegiatan").val();
		$.ajax({
				type: "POST",
				async: false,
				url : "<?php echo site_url('rkas/program/get_kegiatan2')?>",
				data: {
				'id': kegiatan,
				'<?php echo $this->security->get_csrf_token_name(); ?>' : '<?php echo $this->security->get_csrf_hash(); ?>'
				},
				success: function(data){
					$('#uraian').html(data);
					// var select = $("#kategori option:selected").text();
					// var hasil = select.split(" - ");
					// console.log('hallo');
					// //$('#uraian').val(hasil[1]);
				}
		});
    });

	$("#kegiatan").change(function(){
		var kegiatan = $("#kegiatan").val();
		var select = $("#kegiatan option:selected").text();
		var hasil = select.split(" - ");
		//console.log('hallo');
		$('#uraian').val(hasil[1]);
    });
</script>