<div class="row">
	<div class="col-md-12">
		<div id="message"></div>
		<?php if($this->session->flashdata('flashconfirm')): ?>
		<div class="alert alert-success alert-dismissable">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		<?php echo $this->session->flashdata('flashconfirm'); ?>
		</div>
		<?php endif; ?>
		<?php if($this->session->flashdata('flasherror')): ?>
		<div class="alert alert-warning alert-dismissable">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		<?php echo $this->session->flashdata('flasherror'); ?>
		</div>
		<?php endif; ?>
		<div class="box box-primary box-solid">
			<div class="box-header with-border">
				<h3 class="box-title"><?= isset($head) ? $head : ''; ?></h3>
				<div class="box-tools pull-right">
					<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
					<button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
				</div>
			</div>
			<!-- box-body -->
			<div class="box-body">
				<div class="row">
					<div class="col-md-12">
						<span id="key" style="display: none;"><?= $this->security->get_csrf_hash(); ?></span>
						<p></p>
						<?php if($record): ?>
						<?php 
						if($record->status == 1){
							$callout = 'callout-info';
						}elseif($record->status == 2){
							$callout = 'callout-success';
						}
						?>
						<div class="callout <?= $callout; ?>">
							<h4>Tahun Anggaran <?= $record->tahun; ?></h4>
							<table class="table">
								<tr>
									<td width="33%">
									<dl>
										<dt>Nama Sekolah</dt>
										<dd><?= sekolah(null,$record->sekolah_id)->sekolah; ?></dd>
										<dt>NPSN</dt>
										<dd><?= $record->sekolah_id; ?></dd>
										<dt>Jumlah Siswa</dt>
										<dd><?= number_format($record->siswa); ?></dd>
										<dt>Unit Cost Per Siswa</dt>
										<dd><?= rupiah($record->unitcost); ?></dd>
										<dt>Total Anggaran Dana BOS</dt>
										<dd><?= rupiah($record->unitcost*$record->siswa); ?></dd>
									</dl>
									</td>
									
									<td width="33%">
									<dl>
										<dt>Triwulan I</dt>
										<dd><?= rupiah(($record->unitcost*$record->siswa)*20/100); ?></dd>
										<dt>Triwulan II</dt>
										<dd><?= rupiah(($record->unitcost*$record->siswa)*40/100); ?></dd>
										<dt>Triwulan III</dt>
										<dd><?= rupiah(($record->unitcost*$record->siswa)*20/100); ?></dd>
										<dt>Triwulan IV</dt>
										<dd><?= rupiah(($record->unitcost*$record->siswa)*20/100); ?></dd>
									</dl>
									</td>
									
									<td width="33%">
									<a class="btn btn-sm btn-primary" style="text-decoration:none;" href="<?= site_url('rkas/proses/'.$record->id); ?>"><i class="fa fa-book"></i> Proses RKAS</a>
									</td>
								</tr>
							</table>
						</div>
						<?php if($standart): ?>
						<table class="table">
						<thead>
						<tr>
								<th>Kode</th>
								<th>Uraian</th>
								<th>Volume</th>
								<th>Satuan</th>
								<th>Jumlah</th>
								<th>TW I</th>
								<th>TW II</th>
								<th>TW III</th>
								<th>TW IV</th>
								<th></th>
						</tr>
						</thead>
						<tbody>
						<?php foreach($standart as $row): ?>
						<tr>
							<td><?= $row->kode; ?></td>
							<td colspan="3"><?= $row->program; ?></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td><button class="btn btn-xs btn-flat btn-success" data-toggle="modal" data-target="#program-modal" data-modul="program" data-id="<?= $record->id; ?>" data-program="<?= $row->id; ?>" id="getProgram"><i class="fa fa-plus"></i></button></td>
						</tr>
						<?php endforeach; ?>
						</tbody>
						</table>
						<?php endif; ?>


						<?php else: ?>
						<div class="callout callout-danger">
							<p>Belum Ada Kertas Kerja Tersedia. Silahkan Untuk Membuat</p>
						</div>
						<?php endif; ?>
					</div>
				</div>
			</div>
			<!-- ./box-body -->
		</div>
	</div>
</div>

<div id="program-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
  <div class="modal-dialog modal-lg"> 
     <div class="modal-content">  
        <div class="modal-header"> 
           <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button> 
           <h4 class="modal-title">
           <i class="fa fa-calendar-o"></i> Rencana Kegiatan Anggaran Sekolah
           </h4> 
        </div> 
        <div class="modal-body">                     
           <div id="program-loader" style="display: none; text-align: center;">
           <!-- ajax loader -->
           <img src="<?= base_url('asset/loading.gif'); ?>">
           </div>             
           <!-- mysql data will be load here -->                          
           <div id="program-content"></div>
        </div>       
    </div> 
  </div>
</div>