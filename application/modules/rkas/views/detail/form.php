<form id="formID" role="form" action="<?= site_url('rkas/detail/'.$link); ?>" method="post">
<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
<input type="hidden" name="kertas_id" value="<?= $id; ?>" />
<input type="hidden" name="program_id" value="<?= $kode; ?>" />
<input type="hidden" name="kegiatan_id" value="<?= $baris; ?>" />
<input type="hidden" name="kategori_id" value="<?= $baris2; ?>" />
<!-- box-body -->
<div class="box-body">
	<div class="row">
		<div class="col-md-12">
			<dl>
				<dt>Program</dt>
				<dd><?= $program ? $program->kode.'-'.$program->program : ''; ?></dd>
				<dt>Kegiatan</dt>
				<dd><?= $kegiatan ? $kegiatan->kegiatan : ''; ?></dd>
				<dt>Kategori</dt>
				<dd><?= $kategori ? $kategori->kategori : ''; ?></dd>
			</dl>
		</div>

		<div class="col-md-12">
			<div class="form-group <?php echo form_error('belanja') ? 'has-error' : null; ?>">
				<?php 
				echo form_label('Belanja','belanja');
				$selected = set_value('belanja', $record->uraian_id);
				echo form_dropdown('belanja', $belanja, $selected, "required class='form-control select2' name='belanja' id='belanja' style='width: 100% !important;
				padding: 0;'");
				echo form_error('belanja') ? form_error('belanja', '<p class="help-block">','</p>') : '';
				?>
			</div>
		</div>


		
		

	

		<div class="col-md-12">
			<div class="form-group <?php echo form_error('jumlah') ? 'has-error' : null; ?>">
				<?php
				echo form_label('Total Jumlah','jumlah');
				$data = array('class'=>'form-control number','name'=>'jumlah','id'=>'jumlah','type'=>'text','value'=>set_value('jumlah', $record->jumlah));
				echo form_input($data);
				echo form_error('jumlah') ? form_error('jumlah', '<p class="help-block">','</p>') : '';
				?>
			</div>
		</div>

	
	</div>
</div>
<!-- ./box-body -->
<div class="box-footer">
<button type="submit" class="btn btn-sm btn-flat btn-success" ><i class="fa fa-save"></i> Simpan</button>
<button type="button" class="btn btn-sm btn-flat btn-danger" data-dismiss="modal"><i class="fa fa-close"></i> Keluar</button>
</div>
</form>
<script>
	$('.select2').select2({ width: 'resolve' });
    $("#subprogram").change(function(){
		var subprogram = $("#subprogram").val();
		$.ajax({
				type: "POST",
				async: false,
				url : "<?php echo site_url('rkas/program/get_kegiatan')?>",
				data: {
				'id': subprogram,
				'<?php echo $this->security->get_csrf_token_name(); ?>' : '<?php echo $this->security->get_csrf_hash(); ?>'
				},
				success: function(data){
					$('#kegiatan').html(data);
				}
		});
	});
	
	$("#satuan, #vol1, #vol2, #vol3, #vol4").keyup(function(){
		var vol1 = parseInt($("#vol1").val());
		var vol2 = parseInt($("#vol2").val());
		var vol3 = parseInt($("#vol3").val());
		var vol4 = parseInt($("#vol4").val());
		var satuan = parseInt($("#satuan").val());
		if(vol1 !== 0){
			var jumlah = vol1*satuan;
		}
		if(vol1 !== 0 && vol2 !== 0 ){
			var jumlah = vol1*vol2*satuan;
		}
		if(vol1 !== 0 && vol2 !== 0 && vol3 !== 0 ){
			var jumlah = vol1*vol2*vol3*satuan;
		}
		if(vol1 !== 0 && vol2 !== 0 && vol3 !== 0 && vol4 !== 0 ){
			var jumlah = vol1*vol2*vol3*vol4*satuan;
		}

		$("#jumlah").val(jumlah);
    });
</script>