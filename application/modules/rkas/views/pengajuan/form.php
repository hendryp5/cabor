<form id="formID" role="form" action="<?= site_url('rkas/pengajuan/'.$link); ?>" method="post">
<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
<input type="hidden" name="kertas_id" value="<?= $id; ?>" />
<input type="hidden" name="program_id" value="<?= $kode; ?>" />
<input type="hidden" name="tahun" value="<?= $tahun; ?>" />
<!-- box-body -->
<div class="box-body">
	<div class="row">
		<div class="col-md-12">
			<div class="form-group <?php echo form_error('status') ? 'has-error' : null; ?>">
				<?php 
				echo form_label('Status Rencana','status');
				$selected = set_value('status');
				$status = array(''=>'Pilih Salah Satu','1'=>'Draf','2'=>'Pengajuan','3'=>'Disetujui','4'=>'Ditolak');
				echo form_dropdown('status', $status, $selected, "required class='form-control select2' name='status' id='status'");
				echo form_error('status') ? form_error('status', '<p class="help-block">','</p>') : '';
				?>
			</div>
		</div>

		<div class="col-md-12">
			<div class="form-group <?php echo form_error('catatan') ? 'has-error' : null; ?>">
				<?php
				echo form_label('Catatan','catatan');
				$data = array('class'=>'form-control','name'=>'catatan','id'=>'catatan','type'=>'text_area','value'=>set_value('catatan'));
				echo form_input($data);
				echo form_error('catatan') ? form_error('catatan', '<p class="help-block">','</p>') : '';
				?>
			</div>
		</div>
	</div>
</div>
<!-- ./box-body -->
<div class="box-footer">
<button type="submit" class="btn btn-sm btn-flat btn-success" ><i class="fa fa-save"></i> Simpan</button>
<button type="button" class="btn btn-sm btn-flat btn-danger" data-dismiss="modal"><i class="fa fa-close"></i> Keluar</button>
</div>
</form>