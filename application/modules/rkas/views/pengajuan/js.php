<script>
$(function () {
	$('.select2').select2();
	$('.number').number(true);
	
	$(document).on('click', '#getStatus', function(e){
		e.preventDefault();
		var uid = $(this).data('id'); // get id of clicked row
		var umodul = $(this).data('modul'); // get id of clicked row
		var ukode = $(this).data('kode'); // get id of clicked row
		var ubaris = $(this).data('baris'); // get id of clicked row
		var utahun = $(this).data('tahun'); // get id of clicked row
		$('#status-content').html(''); // leave this div blank
		$('#status-loader').show(); // load ajax loader on button click
		$.ajax({
			url: '<?= site_url('rkas/pengajuan/status'); ?>',
			type: 'POST',
			data: {
					'id': uid,
					'modul' : umodul,
					'kode' : ukode,
					'baris' : ubaris,
					'tahun' : utahun,
					'<?php echo $this->security->get_csrf_token_name(); ?>' : '<?php echo $this->security->get_csrf_hash(); ?>'
			},
			dataType: 'html'
		})
		.done(function(data){
			$('#status-content').html(''); // blank before load.
			$('#status-content').html(data); // load here
			$('#status-loader').hide(); // hide loader
		})
		.fail(function(){
			$('#status-content').html('<i class="glyphicon glyphicon-info-sign"></i> Something went wrong, Please try again...');
			$('#status-loader').hide();
		});
	});
});
</script>