<div class="row">
	<div class="col-md-12 text-center">
		<h5>Rencana Kegiatan Anggaran Sekolah</h5>
		<h4><?= $sekolah->sekolah; ?></h4>
		<h5><?= $sekolah->alamat; ?></h5>
	</div>
	<div class="col-md-12">
		<table>
			<tr>
				<td width="73px">Tahun</td>
				<td> : </td>
				<td> Dana BOS APBN Tahun <?= $record->tahun; ?></td>
			</tr>
			<tr>
				<td>Periode</td>
				<td> : </td>
				<td> <?= periode($record->periode); ?></td>
			</tr>
		</table>
	</div>
	<div class="col-md-12">
		<button type="button" class="btn btn-xs btn-flat btn-default no-print" style="margin-top:3px;margin-bottom:3px;" onclick="window.print()"><i class="fa fa-print"></i> Print</button>
	</div>
	<div class="col-md-12">
		<!-- box-body -->
		<?php if($record): ?>
		<!-- program standart -->
		<?php if($standart): ?>
		<table class="table table-bordered">
		<thead>
		<tr class="text-center">
				<th class="text-center">Kode</th>
				<th class="text-center">Uraian</th>
				<th class="text-center">Volume</th>
				<th class="text-center">Harga<br>Satuan</th>
				<th class="text-center">Jumlah</th>
				<th class="text-center">Belanja<br>Pegawai</th>
				<th class="text-center">Belanja<br>Barang dan Jasa</th>
				<th class="text-center">Belanja<br>Modal</th>
		</tr>
		</thead>
		<tbody>
		<?php foreach($standart as $row): ?>
		<tr style="font-weight: 600; background-color: #ddd;">
			<td><?= $row->kode; ?></td>
			<td colspan="3"><?= $row->program; ?></td>
			<td class="text-right"><?= rupiah(jml_program($record->id, $row->id)->jumlah); ?></td>
			<td class="text-right"><?= rupiah(jml_program_belanja($record->id, $row->id,'5.2.1')->jumlah); ?></td>
			<td class="text-right"><?= rupiah(jml_program_belanja($record->id, $row->id,'5.2.2')->jumlah); ?></td>
			<td class="text-right"><?= rupiah(jml_program_belanja($record->id, $row->id,'5.2.3')->jumlah); ?></td>
		</tr>
		<?php 
		//uraian;
		$uraian = $this->db->order_by('belanja','ASC')->get_where('kertas_uraian', array('program_id'=>$row->id,'deleted_at'=>null))->result(); 
		if($uraian):
		//uraian
			foreach($uraian as $baris):
			?>
			<tr style="background-color: #f4f4f4;">
				<td><?= $baris->rekening; ?></td>
				<td><?= $baris->uraian; ?></td>
				<td></td>
				<td></td>
				<td class="text-right"><?= rupiah(jml_baris($baris->kertas_id, $baris->program_id, $baris->belanja)->jumlah); ?></td>
				<td class="text-right"><?= jml_baris($baris->kertas_id, $baris->program_id, $baris->belanja)->belanja == '5.2.1' ? rupiah(jml_baris($baris->kertas_id, $baris->program_id, $baris->belanja)->jumlah) : 'Rp.0'; ?></td>
				<td class="text-right"><?= jml_baris($baris->kertas_id, $baris->program_id, $baris->belanja)->belanja == '5.2.2' ? rupiah(jml_baris($baris->kertas_id, $baris->program_id, $baris->belanja)->jumlah) : 'Rp.0'; ?></td>
				<td class="text-right"><?= jml_baris($baris->kertas_id, $baris->program_id, $baris->belanja)->belanja == '5.2.3' ? rupiah(jml_baris($baris->kertas_id, $baris->program_id, $baris->belanja)->jumlah) : 'Rp.0'; ?></td>
			</tr>
			<?php 
			//detail
			$detail = $this->db->order_by('nomor','ASC')->get_where('kertas_detail', array('uraian_id'=>$baris->id,'deleted_at'=>null))->result(); 
			//detail
			foreach($detail as $kolom):
			?>
			<tr>
				<td class="text-right"><?= $kolom->nomor.'.'; ?></td>
				<td><?= $kolom->uraian.volume($kolom->id); ?></td>
				<td class="text-right"><?= vol($kolom->id); ?></td>
				<td class="text-right"><?= rupiah($kolom->satuan); ?></td>
				<td class="text-right"><?= rupiah($kolom->jumlah); ?></td>
				<td class="text-right"><?= $baris->belanja == '5.2.1' ? rupiah($kolom->jumlah) : 'Rp.0'; ?></td>
				<td class="text-right"><?= $baris->belanja == '5.2.2' ? rupiah($kolom->jumlah) : 'Rp.0'; ?></td>
				<td class="text-right"><?= $baris->belanja == '5.2.3' ? rupiah($kolom->jumlah) : 'Rp.0'; ?></td>
			<?php endforeach; //endforeach detail ?>
			<?php 
			endforeach; //endforeach uraian
		endif; //endif uraian; 
		?>

		<?php endforeach; ?>
		</tbody>
		<tfoot>
			<tr>
				<th colspan="4">Total Jumlah</th>
				<th class="text-right"><?= rupiah(jml_total($record->id)->jumlah); ?></th>
				<th class="text-right"><?= rupiah(jml_total_belanja($record->id, '5.2.1')->jumlah); ?></th>
				<th class="text-right"><?= rupiah(jml_total_belanja($record->id, '5.2.2')->jumlah); ?></th>
				<th class="text-right"><?= rupiah(jml_total_belanja($record->id, '5.2.3')->jumlah); ?></th>
			</tr>
		</tfoot>
		</table>
		<?php endif; ?>
		<?php else: ?>
		<div class="callout callout-danger">
			<p>Belum Ada Kertas Kerja Tersedia. Silahkan Untuk Membuat</p>
		</div>
		<?php endif; ?>			
			<!-- ./box-body -->
	</div>
</div>