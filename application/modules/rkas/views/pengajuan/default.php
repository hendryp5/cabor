<div class="row">
	<div class="col-md-12">
		<div id="message"></div>
		<?php if($this->session->flashdata('flashconfirm')): ?>
		<div class="alert alert-success alert-dismissable">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		<?php echo $this->session->flashdata('flashconfirm'); ?>
		</div>
		<?php endif; ?>
		<?php if($this->session->flashdata('flasherror')): ?>
		<div class="alert alert-warning alert-dismissable">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		<?php echo $this->session->flashdata('flasherror'); ?>
		</div>
		<?php endif; ?>
		<div class="box box-primary box-solid">
			<div class="box-header with-border">
				<h3 class="box-title"><?= isset($head) ? $head : ''; ?></h3>
				<div class="box-tools pull-right">
					<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
					<button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
				</div>
			</div>
			<!-- box-body -->
			<div class="box-body">
				<div class="row">
					<div class="col-md-12">
						<span id="key" style="display: none;"><?= $this->security->get_csrf_hash(); ?></span>
						<p></p>
						<?php if($record): ?>
						<?php 
						if($record->status == 1){
							$callout = 'callout-info';
						}elseif($record->status == 2){
							$callout = 'callout-success';
						}elseif($record->status == 4){
							$callout = 'callout-danger';
						}else{
							$callout = 'callout-success';
						}
						?>
						<div class="callout <?= $callout; ?>">
							<h4>Tahun Anggaran <?= $record->tahun; ?> </h4>
							<table class="table">
								<tr>
									<td width="33.3%">
									<dl>
										<dt>Nama Pengurus Provinsi</dt>
										<dd><?= cabor($record->cabor_id,null)->cabor; ?></dd>
										<dt>Kode Registrasi</dt>
										<dd><?= kode_cabor($record->cabor_id); ?></dd>
										<dt>Cabang Olahraga</dt>
										<dd><?= cabang_olahraga($record->jenis_cabor_id); ?></dd>
									</dl>
									</td>
									
									<td width="33.3%">
									<dl>
										
										<dt>Kuota Anggaran</dt>
										<dd><?= kuota_cabor($record->jenis_cabor_id,$record->tahun) ? rupiah(kuota_cabor($record->jenis_cabor_id,$record->tahun)) : 'Belum diatur' ; ?></dd>
										<dt>Rencana Anggaran</dt>
										<dd><?= jumlah_total_semua($record->id)->jumlah ? rupiah(jumlah_total_semua($record->id)->jumlah): 'Rp.0' ; ?></dd>
										
									</dl>
									</td>
									<td width="33.3%">
									<dl>
										
										<dt>Status</dt>
										<dd><?= status($record->status); ?></dd>
										<dt>Catatan</dt>
										<dd><?= $record->catatan ? $record->catatan : '-' ; ?></dd>
										<dt>Dokumen</dt>
										<dd><?= $record->dokumen ? '<a href="'.base_url('dokumen/'.$record->cabor_id.'/'.$record->dokumen).'" target="_blank"><i class="fa fa-file-text"></i></a>': '-' ; ?></dd>
									</dl>
									<?php //if($record->status == 2): ?>
									<button type="button" class="btn btn-sm btn-danger" data-toggle="modal" data-target="#status-modal" data-modul="status" data-id="<?= $record->id; ?>" data-kode="<?= $record->cabor_id; ?>"  data-tahun="<?= $record->tahun; ?>" id="getStatus"><i class="fa fa-circle"></i> Status Persetujuan</button>
									<?php //endif; ?>
								
								</td>
								</tr>
							</table>
						</div>
						<?php if($standart): ?>
						<table class="table table-bordered">
						<thead>
						<tr class="text-center">
								<th class="text-center">Kode</th>
								<th class="text-center">Uraian</th>
								<th class="text-center">Jumlah</th>
						</tr>
						</thead>
						<tbody>
						<?php foreach($standart as $row): ?>
						<tr style="font-weight: 600; background-color: #ddd;">
							<td><?= kode_program($row->id); ?></td>
							<td><?= program($row->id); ?></td>
							<td class="text-right"></td>
						</tr>
						<?php 
						//uraian;
						$uraian = $this->db->order_by('id','ASC')->group_by('id')->get_where('kegiatan', array('program_id'=>$row->id,'deleted_at'=>null))->result(); 
						if($uraian):
						//uraian
							foreach($uraian as $baris):
							?>
							<tr style="background-color: #f4f4f4;">
								<td></td>
								<td><b><?= kegiatan($baris->id); ?></b></td>	
								<td></td>
							</tr>
							<?php 
							//detail
							$detail = $this->db->order_by('kategori_id','ASC')->group_by('kategori_id')->get_where('template_nomenklatur', array('program_id'=>$row->id,'kegiatan_id'=>$baris->id,'tahun'=>$record->tahun,'deleted_at'=>null))->result(); 
							//detail
							if($detail):
							foreach($detail as $kolom):
							?>
							<tr>
								<td class="text-right"></td>
								<td><b>- <?= kategori($kolom->kategori_id); ?></b></td>
								<td class="text-right"></td>
							</tr>
							<?php 
							//detail
							$belanja = $this->db->order_by('belanja_id','ASC')->get_where('template_nomenklatur', array('kategori_id'=>$kolom->kategori_id,'program_id'=>$row->id,'kegiatan_id'=>$baris->id,'tahun'=>$record->tahun,'deleted_at'=>null))->result(); 
							//detail
							if($belanja):
							$no = 0;
							foreach($belanja as $isi):
								$no++;
							?>
							
							<tr>
								<td class="text-right"></td>
								<td><?= $no; ?> .<?= belanja($isi->belanja_id); ?></td>
								<td class="text-right"><?= rupiah(jumlah_detail($record->id,$row->id,$baris->id,$kolom->kategori_id,$isi->belanja_id,$record->tahun)); ?></td>
							</tr>
							<?php endforeach; endif;//endforeach detail ?>
							<?php endforeach; endif;//endforeach uraian?>
							<?php endforeach; endif; //endif uraian; //endforeach uraian?>

						<?php endforeach; ?>
						</tbody>
						<tfoot>
							<tr>
								<th>Total Jumlah</th>
								<th class="text-right"></th>
								<th class="text-right"><?php echo rupiah(jumlah_total_semua($record->id)->jumlah); ?></th>
							</tr>
						</tfoot>
						</table>
						<?php endif; ?>

						<?php else: ?>
						<div class="callout callout-danger">
							<p>Belum Ada Kertas Kerja Tersedia. Silahkan Untuk Membuat</p>
						</div>
						<?php endif; ?>
					</div>
				</div>
			</div>
			<!-- ./box-body -->
		</div>
	</div>
</div>

<div id="status-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
  <div class="modal-dialog modal-lg"> 
     <div class="modal-content">  
        <div class="modal-header"> 
           <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button> 
           <h4 class="modal-title">
           <i class="fa fa-calendar-o"></i> Status - Rencana Anggaran 
           </h4> 
        </div> 
        <div class="modal-body">                     
           <div id="status-loader" style="display: none; text-align: center;">
           <!-- ajax loader -->
           <img src="<?= base_url('asset/loading.gif'); ?>">
           </div>             
           <!-- mysql data will be load here -->                          
           <div id="status-content"></div>
        </div>       
    </div> 
  </div>
</div>