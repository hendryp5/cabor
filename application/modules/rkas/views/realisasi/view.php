<div class="row">
	<div class="col-md-12">
	<table class="table table-bordered">
		<thead>
			<tr>
				<th>No</th>
				<th>Tanggal</th>
				<th>Kode</th>
				<th>Uraian</th>
				<th>Bukti</th>
				<th>Volume</th>
				<th>Harga Satuan</th>
				<th>Jumlah</th>
				<th>Pengeluaran</th>
				<th>PPN</th>
				<th>Pph21</th>
				<th>Pph22</th>
				<th>Pph23</th>
				<th>Pph4</th>
				<th>Keterangan</th>
			</tr>
		</thead>
		<tbody>
			<?php if($record): ?>
			<?php $no = 1; ?>
			<?php foreach($record as $row): ?>
				<tr>
					<td><?= $no; ?></td>
					<td><?= ddmmyyyy($row->tanggal); ?></td>
					<td><?= $row->kode; ?></td>
					<td><?= $row->uraian; ?></td>
					<td><?= $row->bukti; ?></td>
					<td class="text-right" nowrap><?= number_format($row->volume); ?></td>
					<td class="text-right" nowrap><?= rupiah($row->satuan); ?></td>
					<td class="text-right" nowrap><?= rupiah($row->jumlah); ?></td>
					<td class="text-right" nowrap><?= rupiah($row->pengeluaran); ?></td>
					<td class="text-right" nowrap><?= rupiah($row->ppn); ?></td>
					<td class="text-right" nowrap><?= rupiah($row->pph21); ?></td>
					<td class="text-right" nowrap><?= rupiah($row->pph22); ?></td>
					<td class="text-right" nowrap><?= rupiah($row->pph23); ?></td>
					<td class="text-right" nowrap><?= rupiah($row->pph4); ?></td>
					<td><?= $row->keterangan; ?></td>
				</tr>
			<?php $no++; ?>
			<?php endforeach; ?>
			<?php else: ?>
				<tr>
					<td colspan="15">Belum ada realisasi ditemukan.</td>
				</tr>
			<?php endif; ?>
		</tbody>
	</table>
	</div>
</div>