<div class="row">
	<div class="col-md-12">
		<div id="message"></div>
		<div class="box box-primary box-solid">
			<div class="box-header with-border">
				<h3 class="box-title"><?= isset($head) ? $head : ''; ?></h3>
				<div class="box-tools pull-right">
					<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
					<button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
				</div>
			</div>
			<form id="formID" role="form" action="<?= site_url('rkas/kertas/'.$link); ?>" method="post">
			<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
			<!-- box-body -->
			<div class="box-body">
				<div class="row">
					<div class="col-md-12">
						<div class="form-group <?php echo form_error('tahun') ? 'has-error' : null; ?>">
							<?php 
							echo form_label('Tahun Anggaran','tahun');
							$selected = set_value('tahun', $record->tahun);
							$tahun = array(''=>'Pilih Salah Satu','2019'=>'2019','2020'=>'2020');
							echo form_dropdown('tahun', $tahun, $selected, "required class='form-control select2' name='tahun' id='tahun'");
							echo form_error('tahun') ? form_error('tahun', '<p class="help-block">','</p>') : '';
							?>
						</div>
					</div>

					<div class="col-md-12">
						<div class="form-group <?php echo form_error('siswa') ? 'has-error' : null; ?>">
							<?php
							echo form_label('Jumlah Siswa','siswa');
							$data = array('class'=>'form-control number','name'=>'siswa','id'=>'siswa','type'=>'text','value'=>set_value('siswa', $record->siswa));
							echo form_input($data);
							echo form_error('siswa') ? form_error('siswa', '<p class="help-block">','</p>') : '';
							?>
						</div>
					</div>

					<div class="col-md-12">
						<div class="form-group <?php echo form_error('unitcost') ? 'has-error' : null; ?>">
							<?php
							echo form_label('Unit Cost Per Siswa','unitcost');
							$data = array('readonly class'=>'form-control number','name'=>'unitcost','id'=>'unitcost','type'=>'text','value'=>set_value('unitcost', $record->unitcost));
							echo form_input($data);
							echo form_error('unitcost') ? form_error('unitcost', '<p class="help-block">','</p>') : '';
							?>
						</div>
					</div>

					<div class="col-md-12">
						<div class="form-group <?php echo form_error('silpa') ? 'has-error' : null; ?>">
							<?php
							echo form_label('Silpa (Sisa Anggaran Tahun Lalu)','silpa');
							$data = array('class'=>'form-control number','name'=>'silpa','id'=>'silpa','type'=>'text','value'=>set_value('silpa', $record->silpa));
							echo form_input($data);
							echo form_error('silpa') ? form_error('silpa', '<p class="help-block">','</p>') : '';
							?>
						</div>
					</div>
				</div>
			</div>
			<!-- ./box-body -->
			<div class="box-footer">
				<button type="submit" class="btn btn-sm btn-flat btn-info"><i class="fa fa-save"></i> Simpan & Keluar</button>
				<button type="reset" class="btn btn-sm btn-flat btn-warning"><i class="fa fa-refresh"></i> Reset</button>
				<button type="button" class="btn btn-sm btn-flat btn-danger" onclick="back();"><i class="fa fa-close"></i> Keluar</button>
			</div>
			</form>
		</div>
	</div>
</div>