<div class="row">
	<div class="col-md-12">
		<div id="message"></div>
		<?php if($this->session->flashdata('flashconfirm')): ?>
		<div class="alert alert-success alert-dismissable">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		<?php echo $this->session->flashdata('flashconfirm'); ?>
		</div>
		<?php endif; ?>
		<?php if($this->session->flashdata('flasherror')): ?>
		<div class="alert alert-warning alert-dismissable">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		<?php echo $this->session->flashdata('flasherror'); ?>
		</div>
		<?php endif; ?>
		<div class="box box-primary box-solid">
			<div class="box-header with-border">
				<h3 class="box-title"><?= isset($head) ? $head : ''; ?></h3>
				<div class="box-tools pull-right">
					<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
					<button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
				</div>
			</div>
			<!-- box-body -->
			<div class="box-body">
				<div class="row">
					<div class="col-md-12">
						<a class="btn btn-sm btn-flat btn-success" onclick="add_data();" href="<?= site_url('rkas/kertas/created'); ?>"><i class="fa fa-file-text"></i> Buat Kertas Kerja</a>
						<span id="key" style="display: none;"><?= $this->security->get_csrf_hash(); ?></span>
						<p></p>
						<?php if($record): ?>
						<?php foreach($record as $row): ?>
						<?php 
						if($row->status == 1){
							$callout = 'callout-info';
						}elseif($row->status == 2){
							$callout = 'callout-success';
						}elseif($row->status == 4){
							$callout = 'callout-danger';
						}else{
							$callout = 'callout-success';
						}
						?>
						<div class="callout <?= $callout; ?>">
							<h4>Tahun Anggaran <?= $row->tahun; ?> </h4>
							<table class="table">
								<tr>
									<td width="33.3%">
									<dl>
										<dt>Nama Pengurus Provinsi</dt>
										<dd><?= cabor($row->cabor_id,null)->cabor; ?></dd>
										<dt>Kode Registrasi</dt>
										<dd><?= kode_cabor($row->cabor_id); ?></dd>
										<dt>Cabang Olahraga</dt>
										<dd><?= cabang_olahraga($row->jenis_cabor_id); ?></dd>
										
									</dl>
									</td>
									<td width="33.3%">
									<dl>
										<dt>Kuota Anggaran</dt>
										<dd><?= kuota_cabor($row->jenis_cabor_id,$row->tahun) ? rupiah(kuota_cabor($row->jenis_cabor_id,$row->tahun)) : 'Belum diatur' ; ?></dd>
										<dt>Rencana Anggaran</dt>
										<dd><?= jumlah_total_semua($row->id)->jumlah ? rupiah(jumlah_total_semua($row->id)->jumlah): 'Belum Diatur' ; ?></dd>
										
									</dl>
									</td>
									<td width="33.3%">
									<dl>
										
										<dt>Status</dt>
										<dd><?= status($row->status); ?></dd>
										
										<dt>Dokumen</dt>
										<dd><?= $row->dokumen ? '<a href="'.base_url('dokumen/'.kode_cabor($row->cabor_id).'/'.$row->dokumen).'" target="_blank"><i class="fa fa-file-text"></i></a>': '-' ; ?></dd>
									</dl>
									<?php if($row->status == 1 || $row->status == 4): ?>
									<a class="btn btn-sm btn-primary" style="text-decoration:none;" href="<?= site_url('rkas/proses_perencanaan/'.$row->id.'/'.$row->tahun); ?>"><i class="fa fa-book"></i> Buat Rencana</a>
									<a class="btn btn-sm btn-warning" style="text-decoration:none;" href="<?= site_url('rkas/kertas/updated/'.$row->id.'/'.$row->tahun); ?>"><i class="fa fa-edit"></i> Ubah</a>
									<a class="btn btn-sm btn-danger" style="text-decoration:none;" href="<?= site_url('rkas/kertas/deleted/'.$row->id); ?>"><i class="fa fa-trash"></i> Hapus</a>
									<?php else: ?>
									<!-- <?php if($row->status == 3): ?>
									<a class="btn btn-sm btn-primary" style="text-decoration:none;" href="<?= site_url('rkas/realisasi/'.$row->id); ?>"><i class="fa fa-cart-plus"></i> Realisasi Renaca</a>
									<?php endif; ?> -->
									<a class="btn btn-sm btn-primary" style="text-decoration:none;" href="<?= site_url('rkas/proses_perencanaan/'.$row->id.'/'.$row->tahun); ?>"><i class="fa fa-search"></i> Lihat Rencana</a>
									<!-- <button type="button" class="btn btn-sm btn-danger" style="text-decoration:none;" data-toggle="modal" data-target="#file-modal" data-modul="file" data-id="<?php //echo $row->id; ?>" data-kode="<?php //echo $row->cabor_id; ?>" id="getFile"><i class="fa fa-upload"></i> Upload File</button> -->
									<?php endif; ?>
									</td>
								</tr>
							</table>
							
						</div>
						<?php endforeach; ?>
						<?php else: ?>
						<div class="callout callout-danger">
							<p>Belum Ada Kertas Kerja Tersedia. Silahkan Untuk Membuat</p>
						</div>
						<?php endif; ?>
					</div>
				</div>
			</div>
			<!-- ./box-body -->
		</div>
	</div>
</div>

<div id="file-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
  <div class="modal-dialog modal-lg"> 
     <div class="modal-content">  
        <div class="modal-header"> 
           <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button> 
           <h4 class="modal-title">
           <i class="fa fa-calendar-o"></i> Upload Dokumen
           </h4> 
        </div> 
        <div class="modal-body">                     
           <div id="file-loader" style="display: none; text-align: center;">
           <!-- ajax loader -->
           <img src="<?= base_url('asset/loading.gif'); ?>">
           </div>             
           <!-- mysql data will be load here -->                          
           <div id="file-content"></div>
        </div>       
    </div> 
  </div>
</div>