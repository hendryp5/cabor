<script src="<?= base_url('asset/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js'); ?>"></script>
<script>
 var site_url  = "<?php echo site_url(); ?>";
$(function () {
	$('.select2').select2();
	$('.number').number(true);
	$(document).on('click', '#getFile', function(e){
		e.preventDefault();
		var uid = $(this).data('id'); // get id of clicked row
		var umodul = $(this).data('modul'); // get id of clicked row
		var ukode = $(this).data('kode'); // get id of clicked row
		$('#file-content').html(''); // leave this div blank
		$('#file-loader').show(); // load ajax loader on button click
		$.ajax({
			url: '<?= site_url('rkas/file/get_file'); ?>',
			type: 'POST',
			data: {
					'id': uid,
					'modul' : umodul,
					'kode' : ukode,
					'<?php echo $this->security->get_csrf_token_name(); ?>' : '<?php echo $this->security->get_csrf_hash(); ?>'
			},
			dataType: 'html'
		})
		.done(function(data){
			$('#file-content').html(''); // blank before load.
			$('#file-content').html(data); // load here
			$('#file-loader').hide(); // hide loader
		})
		.fail(function(){
			$('#file-content').html('<i class="glyphicon glyphicon-info-sign"></i> Something went wrong, Please try again...');
			$('#file-loader').hide();
		});
	});
});

$(function() {
  $('#tahun').change(update_kavling);
  function update_kavling() {
    get_kav(this.value);
  }
});

function get_kav(id) {
    // console.log('id',id);
    if (!id) {
      $('#silpa').val('');
     }
    $.getJSON(site_url + 'rkas/kertas/jsonkuota/' + id).then(function(data) {
      $('#silpa').val(data.jumlah_kuota);
      
    });
  }
</script>