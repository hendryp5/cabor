<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Detail_realisasi_m extends MY_Model
{
	public $table = 'kertas_realisasi'; // you MUST mention the table name
	public $primary_key = 'id'; // you MUST mention the primary key
	public $fillable = array(); // If you want, you can set an array with the fields that can be filled by insert/update
	public $protected = array(); // ...Or you can set an array with the fields that cannot be filled by insert/update
	
	//ajax datatable
    public $column_order = array(null); //set kolom field database pada datatable secara berurutan
    public $column_search = array(null); //set kolom field database pada datatable untuk pencarian
    public $order = array('id' => 'asc'); //order baku 
	
	public function __construct()
	{
		$this->timestamps = TRUE;
		$this->soft_deletes = TRUE;
		parent::__construct();
	}
	
	public function get_new()
    {
        $record = new stdClass();
        $record->bos = '';
        $record->nomor = '';
        $record->belanja = '';
        $record->uraian_id = '';
       
        $record->jumlah = '';
        $record->sat = '';
        $record->jenis_belanja = '';
        $record->tanggal = '';
        
		return $record;
    }
    

	
	public function get_id($id=null)
    {
        $this->db->where('id', $id);
		$this->db->where('deleted_at', NULL);
        $query = $this->db->get($this->table);
        return $query->row();
    }

    public function get_program($id=null)
    {
        $this->db->where('id', $id);
        $this->db->where('deleted_at', NULL);
        $query = $this->db->get('program');
        return $query->row();
    }

    public function get_kegiatan1($id=null)
    {
        $this->db->where('id', $id);
        $this->db->where('deleted_at', NULL);
        $query = $this->db->get('kegiatan');
        return $query->row();
    }

    public function get_kategori($id=null)
    {
        $this->db->where('id', $id);
        $this->db->where('deleted_at', NULL);
        $query = $this->db->get('kategori');
        return $query->row();
    }


    public function get_belanja($id=null)
	{
        $this->db->where('id', $id);
        $this->db->where('deleted_at', NULL);
        $query = $this->db->get('belanja');
        return $query->row();
    }



 
}