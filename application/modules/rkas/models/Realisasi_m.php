<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Realisasi_m extends MY_Model
{
	public $table = 'kertas'; // you MUST mention the table name
	public $primary_key = 'id'; // you MUST mention the primary key
	public $fillable = array(); // If you want, you can set an array with the fields that can be filled by insert/update
	public $protected = array(); // ...Or you can set an array with the fields that cannot be filled by insert/update
	
	//ajax datatable
    public $column_order = array(null); //set kolom field database pada datatable secara berurutan
    public $column_search = array(null); //set kolom field database pada datatable untuk pencarian
    public $order = array('id' => 'asc'); //order baku 
	
	public function __construct()
	{
		$this->timestamps = TRUE;
		$this->soft_deletes = TRUE;
		parent::__construct();
	}
	
	public function get_new()
    {
        $cost = 0;
        $bentuk = $this->session->userdata('bentuk');

        $record = new stdClass();
        $record->id = '';
        $record->tahun = '';
        $record->siswa = '';
        $record->unitcost = '';
        $record->silpa = '';
		return $record;
    }

    
    public function get_record($id=null, $rkas=null, $tahun=null)
    {
        $this->db->where('id', $rkas);
        $this->db->where('cabor_id', $id);
        $this->db->where('tahun', $tahun);
        $this->db->where('deleted_at', NULL);
        $query = $this->db->get($this->table);
        return $query->row();
    }
	
	
	public function get_id($id=null)
    {
        $this->db->where('id', $id);
		$this->db->where('deleted_at', NULL);
        $query = $this->db->get($this->table);
        return $query->row();
    }

    public function get_standart()
    {
        $this->db->where('deleted_at', NULL);
        //$this->db->group_by('program_id');
        $query = $this->db->get('program');
        return $query->result();
    }

     public function update_data($data = NULL, $id = NULL,$id2 = NULL){
        $this->db->trans_start(); # Starting Transaction
        //$this->db->trans_strict(FALSE); 
        $this->db->where('id', $id); # Inserting data
        $this->db->where('tahun', $id2); # Inserting data
        $this->db->update('kertas', $data); # Inserting data
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return FALSE;
        } else {
            $this->db->trans_commit();
            return TRUE;
        }
    }
}   