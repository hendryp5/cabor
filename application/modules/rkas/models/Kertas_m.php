<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kertas_m extends MY_Model
{
	public $table = 'kertas'; // you MUST mention the table name
	public $primary_key = 'id'; // you MUST mention the primary key
	public $fillable = array(); // If you want, you can set an array with the fields that can be filled by insert/update
	public $protected = array(); // ...Or you can set an array with the fields that cannot be filled by insert/update
	
	//ajax datatable
    public $column_order = array(null); //set kolom field database pada datatable secara berurutan
    public $column_search = array(null); //set kolom field database pada datatable untuk pencarian
    public $order = array('id' => 'asc'); //order baku 
	
	public function __construct()
	{
		$this->timestamps = TRUE;
		$this->soft_deletes = TRUE;
		parent::__construct();
	}
	
	public function get_new()
    {
        $cost = 0;
        $bentuk = $this->session->userdata('bentuk');

        if(isset($bentuk)){
            if($bentuk == 1){
                $cost = '200000';
            }elseif($bentuk == 2){
                $cost = '250000';
            }
        }

        $record = new stdClass();
        $record->id = '';
        $record->tahun = '';
        $record->jenis_cabor_id = '';
       
        $record->unitcost = '';
        $record->silpa = '';
        $record->lain = '';
		return $record;
    }
    
    public function get_record($id=null)
    {
        $this->db->where('cabor_id', $id);
        $this->db->where('deleted_at', NULL);
        $this->db->order_by('tahun','DESC');
        //$this->db->order_by('periode','ASC');
        $query = $this->db->get($this->table);
        return $query->result();
    }

    public function get_record_realisasi($id=null)
    {
        $this->db->where('cabor_id', $id);
        $this->db->where('deleted_at', NULL);
        $this->db->where('status', '3');
        $this->db->order_by('tahun','DESC');
        //$this->db->order_by('periode','ASC');
        $query = $this->db->get($this->table);
        return $query->result();
    }
	
	
	public function get_id($id=null)
    {
        $this->db->where('id', $id);
		$this->db->where('deleted_at', NULL);
        $query = $this->db->get($this->table);
        return $query->row();
    }

    public function get_kuota($id)
  {
    $id_cabor = $this->db->get_where('cabor', array('kode'=>$this->session->userdata('kode_cabor')))->row();
    $id_cabor1 = $id_cabor->id;
    $this->db->where('tahun',$id);
    $this->db->where('cabor_id',$id_cabor1);
    $query = $this->db->get('kuota_cabor');
    return $query->row();
  }

  public function get_tahun_kuota()
    {
        $query = $this->db->order_by('id', 'ASC')->get_where('template', array('deleted_at'=>null));
        if($query->num_rows() > 0){
        $dropdown[''] = 'Pilih Tahun';
        foreach ($query->result() as $row)
        {
            $dropdown[$row->tahun] = $row->tahun;
        }
        }else{
            $dropdown[''] = 'Belum Ada Data';
        }
        return $dropdown;
    }

    public function get_cabor_jenis()
    {
        $id = $this->db->get_where('cabor', array('kode'=>$this->session->userdata('kode_cabor'),'deleted_at'=>null))->row();

        $this->db->select('*, jenis_cabor.cabor as cabor, jenis_cabor.id as id');
        $this->db->from('cabor_jenis');
        $this->db->join('jenis_cabor','jenis_cabor.id = cabor_jenis.jenis_cabor_id','left');
        $this->db->where('cabor_jenis.cabor_id', $id->id);
        $this->db->where('jenis_cabor.deleted_at', null);
        $query = $this->db->order_by('jenis_cabor.id', 'ASC')->get();
        if($query->num_rows() > 0){
        $dropdown[''] = 'Pilih Cabor';
        foreach ($query->result() as $row)
        {
            $dropdown[$row->id] = $row->cabor;
        }
        }else{
            $dropdown[''] = 'Belum Ada Data';
        }
        return $dropdown;
    }

}