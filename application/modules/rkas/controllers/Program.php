<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Program extends CI_Controller {

	/**
	 * code by rifqie rusyadi
	 * email rifqie.rusyadi@gmail.com
	 */
	
	public $folder = 'rkas/program/';
	
	public function __construct()
	{
		parent::__construct();
		$this->load->model('program_m', 'data');
		$this->load->helper('my_helper');
		signin();
		//group(array('1'));
	}
	
	//halaman index
	public function index()
	{
        $data['head'] 		= 'Rencana Kegiatan dan Anggaran Sekolah';
		$data['record'] 	= FALSE;
		$data['content'] 	= $this->folder.'default';
		$data['style'] 		= $this->folder.'style';
        $data['js'] 		= $this->folder.'js';
        
        $data['standart'] 	= $this->data->get_standart();
		
		$this->load->view('template/default', $data);
    }
	
	public function created()
	{
        $data['id']         = $this->input->post('id');
        $data['kode']       = $this->input->post('kode');
        $data['record']     = $this->data->get_new();
        $data['program']    = $this->data->get_program($this->input->post('kode'));
        $data['kegiatan']   = $this->data->get_kegiatan1();
        $data['kategori']   = $this->data->get_kategori();
        //$data['satuan']     = $this->data->get_satuan();
        $data['link']       = 'ajax_save';

        $this->load->view('rkas/program/form', $data);
    }
    
    public function get_kegiatan(){
        //$record = $this->data->get_id($this->uri->segment(4));
        $id = $this->input->post('id');
        $kegiatan = $this->data->get_kegiatan($id);
        if(!empty($kegiatan)){
            //$selected = set_value('subprogram', $record ? $record->subprogram_id : '');
            $selected = set_value('kegiatan');
            echo form_dropdown('kegiatan', $kegiatan, $selected, "class='form-control select2' name='kegiatan' id='kegiatan'style='width: 100% !important;
            padding: 0;'");
        }else{
            echo form_dropdown('kegiatan', array(''=>'Pilih Salah Satu'), '', "class='form-control select2' name='kegiatan' id='kegiatan'style='width: 100% !important;
            padding: 0;'");
        }
    }
	
	public function updated($id)
	{
        $data['id']         = $this->input->post('id');
        $data['kode']       = $this->input->post('kode');
		$data['record'] 	= $this->data->get_id($id);
        $data['program']    = $this->data->get_program($this->input->post('kode'));
         $data['kegiatan']   = $this->data->get_kegiatan();
        $data['kategori']   = $this->data->get_kategori();
       // $data['satuan']     = $this->data->get_satuan();
        $data['link']       = 'ajax_update/'.$id;
        
        
		$this->load->view('rkas/program/form', $data);
	}
	
	
	public function ajax_save()
    {
            $data = array(
                'kertas_id' => $this->input->post('kertas_id'),
                'program_id' => $this->input->post('program_id'),
                //'bos' => $this->input->post('bos'),
                'kegiatan_id' => $this->input->post('kegiatan'),
                'kategori_id' => $this->input->post('kategori'),
                //'rekening' => $this->input->post('rekening'),
                //'uraian' => $this->input->post('uraian'),
                'created_at' => date('Y-m-d H:i:s')
            );
            
            if($this->validation()){
                $insert = $this->db->insert('kertas_uraian', $data);
                helper_log("add", "Menambah Uraian Rencana Kegiatan dan Anggaran Sekolah");
                $this->session->set_flashdata('flashconfirm','Uraian Kegiatan Berhasil Di Tambahkan');
                redirect('rkas/proses/'.$this->input->post('kertas_id'));
            }else{
                $this->session->set_flashdata('flasherror','Uraian Kegiatan Gagal Di Tambahkan. Mohon Isi Dengan Lengkap Seluruh Isian Yang Tersedia.');
                redirect('rkas/proses/'.$this->input->post('kertas_id'));
            }
    }
    
    public function ajax_update($id)
    {
        $data = array(
            'kertas_id' => $this->input->post('kertas_id'),
                'program_id' => $this->input->post('program_id'),
                //'bos' => $this->input->post('bos'),
                'kegiatan_id' => $this->input->post('kegiatan'),
                'kategori_id' => $this->input->post('kategori'),
                //'rekening' => $this->input->post('rekening'),
                //'uraian' => $this->input->post('uraian'),
            'updated_at' => date('Y-m-d H:i:s')
        );
		
        if($this->validation($id)){
            $this->data->update($data, $id);
            helper_log("edit", "Merubah Uraian Rencana Kegiatan dan Anggaran Sekolah");
            $this->session->set_flashdata('flashconfirm','Uraian Kegiatan Berhasil Di Tambahkan');
            redirect('rkas/proses/'.$this->input->post('kertas_id'));
        }else{
            $this->session->set_flashdata('flasherror','Uraian Kegiatan Gagal Di Tambahkan. Mohon Isi Dengan Lengkap Seluruh Isian Yang Tersedia.');
            redirect('rkas/proses/'.$this->input->post('kertas_id'));
        }
    }
    
    public function deleted($id)
    {
        $this->data->delete($id);
		helper_log("trash", "Menghapus Uraian Rencana Kegiatan dan Anggaran Sekolah");
        echo json_encode(array("success" => TRUE));
    }
    
	
	private function validation($id=null)
    {
        //$data = array('success' => false, 'messages' => array());
        //$this->form_validation->set_rules("bos", "Jenis BOS", "trim|required"); 
        $this->form_validation->set_rules("kegiatan", "Jenis Belanja", "trim|required");
        $this->form_validation->set_rules("kategori", "Rekening", "trim|required");
        //$this->form_validation->set_rules("uraian", "Uraian Kegiatan", "trim|required");
        //$this->form_validation->set_rules("satuan", "Satuan", "trim");
        
		$this->form_validation->set_error_delimiters('<p class="text-danger">', '</p>');
        
        // if($this->form_validation->run()){
        //     $data['success'] = true;
        // }else{
        //     foreach ($_POST as $key => $value) {
        //         $data['messages'][$key] = form_error($key);
        //     }
        // }
        // echo json_encode($data);

        return $this->form_validation->run();
    }

    public function get_kegiatan2(){
		$kode = $this->input->post('id');
		//$record = $this->data->get_id($kode);
		$kegiatan = $this->data->get_kegiatan1($kode);
        if(!empty($kegiatan)){
            //$selected = $record ? set_select('unker_id', $record->unker_id) : set_select('unker_id');
            //$selected = set_value('unker_id', $record->unker_id);
            $selected = set_value('kegiatan');
            echo form_dropdown('kegiatan', $kegiatan, $selected, "class='form-control select2' name='kegiatan' id='kegiatan'");
        }else{
            echo form_dropdown('kegiatan', array(''=>'Pilih Kode Rekening'), '', "class='form-control select2' name='kegiatan' id='kegiatan'");
        }
    }
}
