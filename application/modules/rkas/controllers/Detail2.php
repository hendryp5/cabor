<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Detail2 extends CI_Controller {

	/**
	 * code by rifqie rusyadi
	 * email rifqie.rusyadi@gmail.com
	 */
	
	public $folder = 'rkas/detail2/';
	
	public function __construct()
	{
		parent::__construct();
		$this->load->model('detail2_m', 'data');
		$this->load->helper('my_helper');
		signin();
		//group(array('1'));
	}
	
	//halaman index
	public function index()
	{
        $data['head'] 		= 'Detail Rencana Kegiatan dan Anggaran Sekolah';
		$data['record'] 	= FALSE;
		$data['content'] 	= $this->folder.'default';
		$data['style'] 		= $this->folder.'style';
        $data['js'] 		= $this->folder.'js';
        
        $data['standart'] 	= $this->data->get_standart();
		
		$this->load->view('template/default', $data);
    }
	
	public function created()
	{
        $data['id']         = $this->input->post('id');
        $data['kode']       = $this->input->post('kode');
        $data['program1']      = $this->input->post('program');
        $data['kegiatan1']      = $this->input->post('kegiatan');
        $data['kategori1']      = $this->input->post('kategori');
        $data['belanja1']    = $this->input->post('belanja');
        $data['tahun']    = $this->input->post('tahun');
        $data['record']     = $this->data->get_new();
        $data['program']    = $this->data->get_program($this->input->post('program'));
        $data['kegiatan']     = $this->data->get_kegiatan1($this->input->post('kegiatan'));
        $data['kategori']     = $this->data->get_kategori($this->input->post('kategori'));
        $data['belanja']     = $this->data->get_belanja($this->input->post('belanja'));
        $data['link']       = 'ajax_save';

        $this->load->view('rkas/detail2/form', $data);
    }
	
	public function updated($id)
	{
        $data['id']         = $this->input->post('id');
        $data['kode']       = $this->input->post('kode');
        $data['program1']      = $this->input->post('program');
        $data['kegiatan1']      = $this->input->post('kegiatan');
        $data['kategori1']      = $this->input->post('kategori');
        $data['belanja1']= $this->input->post('belanja');
        $data['tahun']    = $this->input->post('tahun');
        $data['record']     = $this->data->get_id($id);
        $data['program']    = $this->data->get_program($this->input->post('program'));
        $data['kegiatan']     = $this->data->get_kegiatan1($this->input->post('kegiatan'));
        $data['kategori']     = $this->data->get_kategori($this->input->post('kategori'));
        $data['belanja']     = $this->data->get_belanja($this->input->post('belanja'));
        //$data['satuan']     = $this->data->get_satuan();
        //$data['kode_belanja']= $this->input->post('belanja');
        $data['link']       = 'ajax_update/'.$id;
        
        
		$this->load->view('rkas/detail2/form', $data);
	}
	
	
	public function ajax_save()
    {

            $data = array(
                'kertas_id' => $this->input->post('kertas_id'),
                'program_id' => $this->input->post('program_id'),
                'kegiatan_id' => $this->input->post('kegiatan_id'),
                'kategori_id' => $this->input->post('kategori_id'),
                'belanja_id' => $this->input->post('belanja_id'),

                'jumlah' => replacecoma($this->input->post('jumlah')),
                'tahun' => $this->input->post('tahun'),
                //'sat' => $this->input->post('sat'), 
                'created_at' => date('Y-m-d H:i:s')
            );

            
            if($this->validation()){

                $a = replacecoma($this->input->post('jumlah'));
                $b = kuota_terpakai($this->input->post('kertas_id'),$this->input->post('tahun'));
                $c = $a + $b;

                if ($c <= kuota_cabor1($this->input->post('kertas_id'),$this->input->post('tahun'))) {

                $insert = $this->db->insert('kertas_detail', $data);
                helper_log("add", "Menambah Uraian Detail Rencana Kegiatan dan Anggaran Sekolah");
                $this->session->set_flashdata('flashconfirm','Uraian Kegiatan Berhasil Di Tambahkan');
                redirect('rkas/proses_perencanaan/'.$this->input->post('kertas_id').'/'.$this->input->post('tahun'));

                } else {
                    $this->session->set_flashdata('flasherror','Anggaran Tidak Mencukupi');
                redirect('rkas/proses_perencanaan/'.$this->input->post('kertas_id').'/'.$this->input->post('tahun'));
                }
                
            }else{
                $this->session->set_flashdata('flasherror','Uraian Kegiatan Gagal Di Tambahkan. Mohon Isi Dengan Lengkap Seluruh Isian Yang Tersedia.');
                redirect('rkas/proses_perencanaan/'.$this->input->post('kertas_id').'/'.$this->input->post('tahun'));
            }
    }
    
    public function ajax_update($id)
    {
        $data = array(
             /*'kertas_id' => $this->input->post('kertas_id'),
                'program_id' => $this->input->post('program_id'),
                'kegiatan_id' => $this->input->post('kegiatan_id'),
                'kategori_id' => $this->input->post('kategori_id'),*/
                //'belanja_id' => $this->input->post('belanja_id'),
               
                
                'jumlah' => replacecoma($this->input->post('jumlah')),
            'updated_at' => date('Y-m-d H:i:s')
        );

		
        if($this->validation($id)){
            $this->data->update($data, $id);
            helper_log("edit", "Merubah Uraian Detail Rencana Kegiatan dan Anggaran Sekolah");
            $this->session->set_flashdata('flashconfirm','Uraian Kegiatan Berhasil Di Tambahkan');
            redirect('rkas/proses_perencanaan/'.$this->input->post('kertas_id').'/'.$this->input->post('tahun'));
        }else{
            $this->session->set_flashdata('flasherror','Uraian Kegiatan Gagal Di Tambahkan. Mohon Isi Dengan Lengkap Seluruh Isian Yang Tersedia.');
            redirect('rkas/proses_perencanaan/'.$this->input->post('kertas_id').'/'.$this->input->post('tahun'));
        }
    }
    
    public function deleted($id)
    {
        $this->data->delete($id);
		helper_log("trash", "Menghapus Uraian Detail Rencana Kegiatan dan Anggaran Sekolah");
        echo json_encode(array("success" => TRUE));
    }
    
	
	private function validation($id=null)
    {
        //$data = array('success' => false, 'messages' => array());
        
        $this->form_validation->set_rules("jumlah", "Jumlah Anggaran", "trim|required");
        /*$this->form_validation->set_rules("nomor", "Nomor Urut", "trim|required");
        $this->form_validation->set_rules("uraian", "Uraian Kegiatan", "trim|required");
        $this->form_validation->set_rules("vol1", "Volume", "trim|required");
        $this->form_validation->set_rules("satuan", "Harga Satuan", "trim");*/
		$this->form_validation->set_error_delimiters('<p class="text-danger">', '</p>');
        
        // if($this->form_validation->run()){
        //     $data['success'] = true;
        // }else{
        //     foreach ($_POST as $key => $value) {
        //         $data['messages'][$key] = form_error($key);
        //     }
        // }
        // echo json_encode($data);

        return $this->form_validation->run();
    }
}
