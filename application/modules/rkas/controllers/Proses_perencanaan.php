<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Proses_perencanaan extends CI_Controller {

	/**
	 * code by rifqie rusyadi
	 * email rifqie.rusyadi@gmail.com
	 */
	
	public $folder = 'rkas/proses_perencanaan/';
	
	public function __construct()
	{
		parent::__construct();
		$this->load->model('proses_perencanaan_m', 'data');
		$this->load->helper('my_helper');
		signin();
		//group(array('1'));
	}
	
	//halaman index
	public function index()
	{
        //$find = $this->db->get_where('users', array('id'=>$this->session->userdata('userid')))->row()->sekolah_id;
       $kode_cabor = $this->db->get_where('users', array('id'=>$this->session->userdata('userid')))->row();
        if($kode_cabor){
            $kode_cabor1 = cabor_id($kode_cabor->kode_cabor);
        }else{
            $kode_cabor1 = cabor_id($this->session->userdata('kode_cabor'));
        }


        $id = $this->uri->segment(3);
        $tahun = $this->uri->segment(4);
        $data['head'] 		= 'Rencana Anggaran Cabor';
		$data['record'] 	= $this->data->get_record($kode_cabor1, $id,$tahun);
		$data['content'] 	= $this->folder.'default';
		$data['style'] 		= $this->folder.'style';
        $data['js'] 		= $this->folder.'js';
        $data['kertas']     = $id;
        
        $data['standart'] 	= $this->data->get_standart();
		
		$this->load->view('template/default', $data);
    }
    
    public function get_program()
	{
        $this->load->view('rkas/proses_perencanaan/program');
	}
	
	public function created()
	{
        $data['head'] 		= 'Tambah Rencana Anggaran Cabor';
		$data['record'] 	= $this->data->get_new();
		$data['content'] 	= $this->folder.'form';
		$data['style'] 		= $this->folder.'style';
        $data['js'] 		= $this->folder.'js';
        
        $data['link']       = 'ajax_save';
        
		$this->load->view('template/default', $data);
	}
	
	public function updated($id)
	{
        $data['head'] 		= 'Ubah Rencana Anggaran Cabor';
		$data['record'] 	= $this->data->get_id($id);
		$data['content'] 	= $this->folder.'form';
		$data['style'] 		= $this->folder.'style';
        $data['js'] 		= $this->folder.'js';
        $data['link']       = 'ajax_update/'.$id;
        
        
		$this->load->view('template/default', $data);
	}
	
	
	public function ajax_save()
    {
        //$find = $this->db->get_where('users', array('id'=>$this->session->userdata('userid')))->row()->sekolah_id;

        $sekolah_id = $this->db->get_where('users', array('id'=>$this->session->userdata('userid')))->row();
        if($sekolah_id){
            $npsn = $sekolah_id->sekolah_id;
        }else{
            $npsn = $this->session->userdata('npsn');
        }

        $check = $this->db->get_where('kertas', array('sekolah_id'=>$npsn, 'tahun'=>$this->input->post('tahun'),'deleted_at'=>null))->row();

        if(!$check){
            $data = array(
                'sekolah_id' => $npsn, 
                'tahun' => $this->input->post('tahun'),    
                'siswa' => replacecoma($this->input->post('siswa')),
                'unitcost' => replacecoma($this->input->post('unitcost')),
                'silpa' => replacecoma($this->input->post('silpa')),
                'status' => 1,
            );
            
            if($this->validation()){
                $insert = $this->data->insert($data);
                helper_log("add", "Menambah Rencana Kegiatan dan Anggaran Sekolah");
                $this->session->set_flashdata('flashconfirm','Kertas Kerja Baru Berhasil Di Tambahkan');
                redirect('rkas/proses_perencanaan');
            }
        }else{
                $this->session->set_flashdata('flasherror','Tahun Anggaran Sudah Di Gunakan.');
                redirect('rkas/proses_perencanaan');
        }
        
    }
    
    public function ajax_update($id)
    {
        $data = array(
            'tahun' => $this->input->post('tahun'),    
            'siswa' => replacecoma($this->input->post('siswa')),
            'unitcost' => replacecoma($this->input->post('unitcost')),
            'silpa' => replacecoma($this->input->post('silpa')),
        );
		
        if($this->validation($id)){
            $this->data->update($data, $id);
            helper_log("edit", "Merubah Rencana Kegiatan dan Anggaran Sekolah");
            $this->session->set_flashdata('flashconfirm','Kertas Kerja Baru Berhasil Di Perbaharui');
            redirect('rkas/proses_perencanaan');
        }
    }
    
    public function deleted($id)
    {
        $this->data->delete($id);
        helper_log("trash", "Menghapus Rencana Kegiatan dan Anggaran Sekolah");
        $this->session->set_flashdata('flashconfirm','Kertas Kerja Baru Berhasil Di Hapus');
        redirect('rkas/proses_perencanaan');
    }

    public function pengajuan($id,$id2)
    {
        $data = array(
            'status' => 2,
        );

        $this->data->update_data($data, $id,$id2);
        helper_log("edit", "Mengajukan Draf Rencana Anggaran");
        $this->session->set_flashdata('flashconfirm','Draf Rencana Berhasil Diajukan.');
        echo json_encode(array("status" => TRUE));
        //redirect('rkas/kertas');
    }
    
	
	private function validation($id=null)
    {
        //$data = array('success' => false, 'messages' => array());
        
        $this->form_validation->set_rules("tahun", "Tahun Anggaran", "trim|required");
        $this->form_validation->set_rules("siswa", "Siswa Sekolah", "trim|required");
        $this->form_validation->set_rules("unitcost", "Unit Cost Per Siswa", "trim|required");
        $this->form_validation->set_rules("silpa", "Silpa (Sisa Anggaran Tahun Lalu)", "trim|required");
        
		$this->form_validation->set_error_delimiters('<p class="text-danger">', '</p>');
        
        // if($this->form_validation->run()){
        //     $data['success'] = true;
        // }else{
        //     foreach ($_POST as $key => $value) {
        //         $data['messages'][$key] = form_error($key);
        //     }
        // }
        // echo json_encode($data);

        return $this->form_validation->run();
    }

    public function print($id=null)
	{
        $find = $this->db->get_where('users', array('id'=>$this->session->userdata('userid')))->row()->sekolah_id;
        
        $data['head'] 		= 'Print Rencana Kegiatan dan Anggaran Sekolah';
		$data['record'] 	= $this->data->get_record($find, $id);
		$data['content'] 	= $this->folder.'print';
		$data['style'] 		= $this->folder.'style';
        $data['js'] 		= $this->folder.'js';

        $data['sekolah'] 	= $this->data->get_sekolah($find);
        $data['standart'] 	= $this->data->get_standart();
		
		$this->load->view('template/print', $data);
    }
}
