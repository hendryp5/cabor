<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class File extends CI_Controller {

	/**
	 * code by rifqie rusyadi
	 * email rifqie.rusyadi@gmail.com
	 */
	
	public $folder = 'data/file/';
	
	public function __construct()
	{
		parent::__construct();
		$this->load->model('file_m', 'data');
		$this->load->helper('my_helper');
		signin();
	}
	
	//halaman index
	public function index()
	{
		redirect('dashboard');
	}

	public function get_file()
	{
		$data['id'] = $this->input->post('id');
		$data['kode'] = $this->input->post('kode');
		$data['modul'] = $this->input->post('modul');

		$this->load->view('rkas/file/form', $data);
	}

	public function get_file_realisasi()
	{
		$data['id'] = $this->input->post('id');
		$data['kode'] = $this->input->post('kode');
		$data['modul'] = $this->input->post('modul');
		$data['id2'] = $this->input->post('id2');
		$data['tahun'] = $this->input->post('tahun');

		$this->load->view('rkas/file/form_realisasi', $data);
	}
	
	public function upload()
    {	
		$id = $this->input->post('id');
		$kode = $this->input->post('kode');
		$modul = $this->input->post('modul');

		$this->upload_file($id, $kode, $modul);
		if($_FILES['file']['name'])
		{
			if ($this->upload->do_upload('file')){
				$dokumen = $this->upload->data();
				$data = array(
					'dokumen' => $dokumen['file_name']
				);
				$find = $this->db->get_where('kertas', array('id'=>$id,'cabor_id'=>$kode,'deleted_at'=>null))->row();
				if($find){
					$data['updated_id'] = $this->session->userdata('userid');
					$this->db->where('id', $find->id);
					$proses = $this->db->update('kertas', $data);
					helper_log("edit", "Memperbaharui Dokumen Data RKAS");
				}
				// else{
				// 	$data['created_id'] = $this->session->userdata('userID');
				// 	$proses = $this->data->insert($data);
				// 	helper_log("add", "Menambah Data Dokumen/File", $this->input->post('nip'));
				// }

				if($proses){
					$this->session->set_flashdata('flashconfirm','Dokumen Telah Di Upload');
					redirect('rkas/kertas');
				}else{
					$this->session->set_flashdata('flasherror','Ada Kesalahan Dalam Upload Data');
					redirect('rkas/kertas');
				}
			}
		}else{
			$this->session->set_flashdata('flasherror','Ada Kesalahan Dalam Upload Data');
			redirect('rkas/kertas');
		}
    }

    public function upload_realisasi()
    {	
		$id = $this->input->post('id');
		$kode = $this->input->post('kode');
		$modul = $this->input->post('modul');
		$id2 = $this->input->post('id2');
		$tahun = $this->input->post('tahun');

		$this->upload_file($id, $kode, $modul);
		if($_FILES['file']['name'])
		{
			if ($this->upload->do_upload('file')){
				$dokumen = $this->upload->data();
				$data = array(
					'dokumen' => $dokumen['file_name']
				);
				$find = $this->db->get_where('kertas_realisasi', array('id'=>$id,'deleted_at'=>null))->row();
				if($find){
					$data['updated_id'] = $this->session->userdata('userid');
					$this->db->where('id', $find->id);
					$proses = $this->db->update('kertas_realisasi', $data);
					helper_log("edit", "Memperbaharui Dokumen Data RKAS");
				}
				// else{
				// 	$data['created_id'] = $this->session->userdata('userID');
				// 	$proses = $this->data->insert($data);
				// 	helper_log("add", "Menambah Data Dokumen/File", $this->input->post('nip'));
				// }

				if($proses){
					$this->session->set_flashdata('flashconfirm','Dokumen Telah Di Upload');
					redirect('rkas/realisasi/'.$id2.'/'.$tahun);
				}else{
					$this->session->set_flashdata('flasherror','Ada Kesalahan Dalam Upload Data');
					redirect('rkas/realisasi/'.$id2.'/'.$tahun);
				}
			}
		}else{
			$this->session->set_flashdata('flasherror','Ada Kesalahan Dalam Upload Data');
			redirect('rkas/kertas');
		}
    }
	
	private function upload_file($id=null,$kode=null, $modul=null ){
		$this->load->library('upload');
		$kode_cabor = kode_cabor($kode);
		if($id && $kode_cabor && $modul){
			folder($kode_cabor);
			$config['upload_path'] = './dokumen/'.$kode_cabor;
			$nmfile = $kode_cabor.'_'.$modul.'_'.$id; //nama file saya beri nama langsung dan diikuti fungsi time
		}else{
			$config['upload_path'] = './dokumen/'; //path folder
			$nmfile = "dokumen_".time(); //nama file saya beri nama langsung dan diikuti fungsi time
		}

        $config['allowed_types'] = 'pdf|jpg|png|bmp'; //type yang dapat diakses bisa anda sesuaikan
        $config['max_size'] = '2048'; //maksimum besar file 2M
        $config['max_width']  = '5000'; //lebar maksimum 5000 px
		$config['max_height']  = '5000'; //tinggi maksimu 5000 px
		$config['overwrite']  = TRUE; //tinggi maksimu 5000 px
        $config['file_name'] = $nmfile; //nama yang terupload nantinya
        $this->upload->initialize($config);
	}

}
