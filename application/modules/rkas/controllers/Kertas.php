<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kertas extends CI_Controller {

	/**
	 * code by rifqie rusyadi
	 * email rifqie.rusyadi@gmail.com
	 */
	
	public $folder = 'rkas/kertas/';
	
	public function __construct()
	{
		parent::__construct();
		$this->load->model('kertas_m', 'data');
		$this->load->helper('my_helper');
		signin();
		//group(array('1'));
	}
	
	//halaman index
	public function index()
	{
        $kode_cabor = $this->db->get_where('users', array('id'=>$this->session->userdata('userid')))->row();
        if($kode_cabor){
            $kode_cabor1 = cabor_id($kode_cabor->kode_cabor);
        }else{
            $kode_cabor1 = cabor_id($this->session->userdata('kode_cabor'));
        }

        if(!cek_profil($kode_cabor1)){
            $this->session->set_flashdata('flasherror','Silahkan Lengkapi Seluruh Data Profil Sekolah Terlebih Dahulu');
            redirect('profil');
        }
        
        //$find = $this->db->get_where('users', array('id'=>$this->session->userdata('userid')))->row()->sekolah_id;
        // $sekolah_id = $this->db->get_where('users', array('id'=>$this->session->userdata('userid')))->row();
        // if($sekolah_id){
        //     $npsn = $sekolah_id->sekolah_id;
        // }else{
        //     $npsn = $this->session->userdata('npsn');
        // }

        $data['head'] 		= 'Kertas Rencana Anggaran';
		$data['record'] 	= $this->data->get_record($kode_cabor1);
		$data['content'] 	= $this->folder.'default';
		$data['style'] 		= $this->folder.'style';
		$data['js'] 		= $this->folder.'js';
		
		$this->load->view('template/default', $data);
	}

    public function realisasi()
    {
        $kode_cabor = $this->db->get_where('users', array('id'=>$this->session->userdata('userid')))->row();
        if($kode_cabor){
            $kode_cabor1 = cabor_id($kode_cabor->kode_cabor);
        }else{
            $kode_cabor1 = cabor_id($this->session->userdata('kode_cabor'));
        }

        if(!cek_profil($kode_cabor1)){
            $this->session->set_flashdata('flasherror','Silahkan Lengkapi Seluruh Data Profil Sekolah Terlebih Dahulu');
            redirect('profil');
        }
        
        //$find = $this->db->get_where('users', array('id'=>$this->session->userdata('userid')))->row()->sekolah_id;
        // $sekolah_id = $this->db->get_where('users', array('id'=>$this->session->userdata('userid')))->row();
        // if($sekolah_id){
        //     $npsn = $sekolah_id->sekolah_id;
        // }else{
        //     $npsn = $this->session->userdata('npsn');
        // }

        $data['head']       = 'Kertas Realiasasi Anggaran';
        $data['record']     = $this->data->get_record_realisasi($kode_cabor1);
        $data['content']    = $this->folder.'default_realisasi';
        $data['style']      = $this->folder.'style';
        $data['js']         = $this->folder.'js';
        
        $this->load->view('template/default', $data);
    }
	
	public function created()
	{
         $kode_cabor = $this->db->get_where('users', array('id'=>$this->session->userdata('userid')))->row();
        if($kode_cabor){
            $kode_cabor1 = cabor_id($kode_cabor->kode_cabor);
        }else{
            $kode_cabor1 = cabor_id($this->session->userdata('kode_cabor'));
        }

        if(!cek_profil($kode_cabor1)){
            $this->session->set_flashdata('flasherror','Silahkan Lengkapi Seluruh Data Profil Sekolah Terlebih Dahulu');
            redirect('profil');
        }
        
        $data['head'] 		= 'Tambah Rencana Anggaran';
        $data['record']     = $this->data->get_new();
        $data['tahun']      = $this->data->get_tahun_kuota();
		$data['cabor'] 	    = $this->data->get_cabor_jenis();
		$data['content'] 	= $this->folder.'form';
		$data['style'] 		= $this->folder.'style';
        $data['js'] 		= $this->folder.'js';
        
        $data['link']       = 'ajax_save';
        var_dump($this->session->userdata('kode_cabor'));
		$this->load->view('template/default', $data);
	}
	
	public function updated($id)
	{
        $kode_cabor = $this->db->get_where('users', array('id'=>$this->session->userdata('userid')))->row();
        if($kode_cabor){
            $kode_cabor1 = cabor_id($kode_cabor->kode_cabor);
        }else{
            $kode_cabor1 = cabor_id($this->session->userdata('kode_cabor'));
        }

        if(!cek_profil($kode_cabor1)){
            $this->session->set_flashdata('flasherror','Silahkan Lengkapi Seluruh Data Profil Sekolah Terlebih Dahulu');
            redirect('profil');
        }

        $data['head'] 		= 'Ubah Kertas Rencana Anggaran';
		$data['record'] 	= $this->data->get_id($id);
        $data['tahun']      = $this->data->get_tahun_kuota();
		$data['content'] 	= $this->folder.'form';
        $data['cabor']      = $this->data->get_cabor_jenis();
		$data['style'] 		= $this->folder.'style';
        $data['js'] 		= $this->folder.'js';
        $data['link']       = 'ajax_update/'.$id;
        
        
		$this->load->view('template/default', $data);
	}
	
	
	public function ajax_save()
    {
        //$find = $this->db->get_where('users', array('id'=>$this->session->userdata('userid')))->row()->sekolah_id;
        $kode_cabor = $this->db->get_where('users', array('id'=>$this->session->userdata('userid')))->row();
        if($kode_cabor){
            $kode_cabor1 = cabor_id($kode_cabor->kode_cabor);
        }else{
            $kode_cabor1 = cabor_id($this->session->userdata('kode_cabor'));
        }

        $check = $this->db->get_where('kertas', array('cabor_id'=>$kode_cabor1, 'tahun'=>$this->input->post('tahun'), 'jenis_cabor_id'=>$this->input->post('jenis_cabor_id'),'deleted_at'=>null))->row();

        if(!$check){
            $data = array(
                'cabor_id' => cabor_id($this->session->userdata('kode_cabor')), 
                'tahun' => $this->input->post('tahun'),  
                'jenis_cabor_id' => $this->input->post('jenis_cabor_id'),  
                //'silpa' => replacecoma($this->input->post('silpa')),
             
                'status' => 1,
            );
            
            if($this->validation()){
                $insert = $this->data->insert($data);
                helper_log("add", "Menambah Kertas Kerja RKAS");
                $this->session->set_flashdata('flashconfirm','Kertas Kerja Baru Berhasil Di Tambahkan');
                redirect('rkas/kertas');
            }
        }else{
                $this->session->set_flashdata('flasherror','Tahun Anggaran Sudah Di Gunakan.');
                redirect('rkas/kertas');
        }
        
    }
    
    public function ajax_update($id)
    {
        $data = array(
            'cabor_id' => cabor_id($this->session->userdata('kode_cabor')), 
            'tahun' => $this->input->post('tahun'),  
            //'silpa' => replacecoma($this->input->post('silpa')),
        );
		
        if($this->validation($id)){
            $this->data->update($data, $id);
            helper_log("edit", "Merubah Kertas Kerja RKAS");
            $this->session->set_flashdata('flashconfirm','Kertas Kerja Baru Berhasil Di Perbaharui');
            redirect('rkas/kertas');
        }
    }
    
    public function deleted($id)
    {
        $this->data->delete($id);
        helper_log("trash", "Menghapus Kertas Kerja RKAS");
        $this->session->set_flashdata('flashconfirm','Kertas Kerja Baru Berhasil Di Hapus');
        redirect('rkas/kertas');
    }
    
    public function jsonkuota($id)
      {
        echo json_encode($this->data->get_kuota($id));
      }
	
	private function validation($id=null)
    {
        //$data = array('success' => false, 'messages' => array());
        
        $this->form_validation->set_rules("tahun", "Tahun Anggaran", "trim|required");
       
        //$this->form_validation->set_rules("silpa", "Kuota Anggaran)", "trim|required");
       
		$this->form_validation->set_error_delimiters('<p class="text-danger">', '</p>');
        
        // if($this->form_validation->run()){
        //     $data['success'] = true;
        // }else{
        //     foreach ($_POST as $key => $value) {
        //         $data['messages'][$key] = form_error($key);
        //     }
        // }
        // echo json_encode($data);

        return $this->form_validation->run();
    }
}
