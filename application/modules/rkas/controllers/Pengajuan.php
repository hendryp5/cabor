<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pengajuan extends CI_Controller {

	/**
	 * code by rifqie rusyadi
	 * email rifqie.rusyadi@gmail.com
	 */
	
	public $folder = 'rkas/pengajuan/';
	
	public function __construct()
	{
		parent::__construct();
		$this->load->model('pengajuan_m', 'data');
		$this->load->helper('my_helper');
		signin();
		//group(array('1'));
	}
	
	//halaman index
	public function index()
	{
       
        $find = $this->uri->segment(3);
        $id = $this->uri->segment(4);
         $tahun = $this->uri->segment(5);

        $data['head'] 		= 'Rencana Anggaran Cabor';
		$data['record'] 	= $this->data->get_record($find, $id, $tahun);
		$data['content'] 	= $this->folder.'default';
		$data['style'] 		= $this->folder.'style';
        $data['js'] 		= $this->folder.'js';
        $data['kertas']     = $id;
        
        $data['standart'] 	= $this->data->get_standart();
		
		$this->load->view('template/default', $data);
    }

    public function status()
	{
        $data['id']         = $this->input->post('id');
        $data['kode']       = $this->input->post('kode');
        $data['baris']      = $this->input->post('baris');
        $data['tahun']      = $this->input->post('tahun');
        $data['link']       = 'ajax_update/'.$this->input->post('kode').'/'.$this->input->post('id').'/'.$this->input->post('tahun');

        $this->load->view('rkas/pengajuan/form', $data);
    }
    
    public function ajax_update($npsn=null,$id=null,$tahun=null)
    {
        $data = array(
            'status' => $this->input->post('status'),  
            'catatan' => $this->input->post('catatan'),
            //'update_at' => date('Y-m-d H:m:s'),
        );
		
        $this->data->update($data, $id);
        helper_log("edit", "Merubah Status Rencana Anggaran");
        $this->session->set_flashdata('flashconfirm','Kertas Kerja Baru Berhasil Di Perbaharui Statusnya');
        redirect('rkas/pengajuan/'.$npsn.'/'.$id.'/'.$tahun);
    }
    
    public function deleted($id)
    {
        $this->data->delete($id);
        helper_log("trash", "Menghapus Rencana Kegiatan dan Anggaran Sekolah");
        $this->session->set_flashdata('flashconfirm','Kertas Kerja Baru Berhasil Di Hapus');
        redirect('rkas/pengajuan');
    }

    public function pengajuan($id)
    {
        $data = array(
            'status' => 2,
        );

        $this->data->update($data, $id);
        helper_log("edit", "Mengajukan Draf Rencana Kegiatan dan Anggaran Sekolah");
        $this->session->set_flashdata('flashconfirm','Draf RKAS Berhasil Di Ajukan.');
        echo json_encode(array("status" => TRUE));
        //redirect('rkas/kertas');
    }

    public function print($find=null, $id=null)
	{
        // $find = $this->db->get_where('users', array('id'=>$this->session->userdata('userid')))->row()->sekolah_id;
        
        $data['head'] 		= 'Print Rencana Kegiatan dan Anggaran Sekolah';
		$data['record'] 	= $this->data->get_record($find, $id);
		$data['content'] 	= $this->folder.'print';
		$data['style'] 		= $this->folder.'style';
        $data['js'] 		= $this->folder.'js';

        $data['sekolah'] 	= $this->data->get_sekolah($find);
        
        $data['standart'] 	= $this->data->get_standart();
		
		$this->load->view('template/print', $data);
    }
}
