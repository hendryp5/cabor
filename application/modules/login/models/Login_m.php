<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login_m extends MY_Model
{
	public $table = 'users'; // you MUST mention the table name
	
	
	// public function get_user($email)
	// {
	// 	return $this->db->where('email', $email)->where('active',1)->where('deleted_at',null)->get($this->table)->row('password');
	// }
	
	// public function get_userid($email)
	// {
	// 	return $this->db->where('email', $email)->where('active',1)->where('deleted_at',null)->get($this->table)->row('id');
	// }
	
	// public function get_level($email)
	// {
	// 	return $this->db->where('email', $email)->where('active',1)->where('deleted_at',null)->get($this->table)->row('level');
	// }

	// public function get_shipping($email)
	// {
	// 	return $this->db->where('email', $email)->where('active',1)->where('deleted_at',null)->get($this->table)->row('shipping');
	// }

	// public function get_bentuk($email)
	// {
	// 	return $this->db->where('email', $email)->where('active',1)->where('deleted_at',null)->get($this->table)->row('bentuk');
	// }

	public function get_dapodik($user=null, $password=null)
	{
		$db2   = $this->load->database('dapodik', TRUE);
		return $db2->where('username', $user)->where('password',$password)->get('pengguna')->row();
	}

	public function get_user_dapodik()
	{
		$db2   = $this->load->database('dapodik', TRUE);
		return $db2->get('pengguna')->result();
	}
	
}