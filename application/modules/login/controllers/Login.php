<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	/**
	 * code by rifqie rusyadi
	 * email rifqie.rusyadi@gmail.com
	 */
	public $folder = 'login/login/';
	
	public function __construct()
	{
		parent::__construct();
		$this->load->model('login_m', 'data');
	}
	
	public function index()
	{
		//echo password_hash('admin', PASSWORD_BCRYPT);
		
		$data['title'] 		= 'LOGIN BAKULKAS - Dinas Pendidikan Kota Banjarmasin';
		$data['record'] 	= FALSE;
		$data['content'] 	= $this->folder.'default';
		$data['style'] 		= $this->folder.'style';
		$data['js'] 		= $this->folder.'js';
		
		$this->load->view($data['content'], $data);
	}
	
	public function masuk()
	{
		$validation = array(
			array('field'=>'email', 'rules'=>'required|valid_email'),
			array('field'=>'password','rules'=>'required')
		);
		
		$this->form_validation->set_rules($validation);
		if($this->form_validation->run() == TRUE){
			$email_post = $this->input->post('email');
			$pass_post = $this->input->post('password');
			$ip_address = $this->input->ip_address();

			$find = $this->data->get_dapodik($email_post,md5($pass_post));
			if($find){
				$create_session = array(
					'userid'=> null,
					'npsn'=> $find->npsn,
					'ip_address'=> $ip_address,
					'signin' => TRUE,
					'level' => 3,
					//'shipping' => $shipping,
					'bentuk' => $find->jenjang_id
				);
				$this->session->set_userdata($create_session);
				
				$find_sekolah = $this->db->get_where('sekolah', array('npsn'=>$find->npsn))->row();
				if($find_sekolah){
					$data = array(
						'kode' => $find->npsn,
						'sekolah' => $find->sekolah,
						'npsn' => $find->npsn,
						'bentuk' => $find->jenjang_id
					);
					$this->db->where('npsn', $find->npsn);
					$this->db->update('sekolah', $data);
				}else{
					$data = array(
						'kode' => $find->npsn,
						'sekolah' => $find->sekolah,
						'npsn' => $find->npsn,
						'bentuk' => $find->jenjang_id
					);
					$this->db->insert('sekolah', $data);
				}

				$find_user = $this->db->get_where('users', array('sekolah_id'=>$find->npsn))->row();
				if($find_user){
					$data = array(
						'sekolah_id' => $find->npsn,
						'fullname' => $find->sekolah,
						'email' => $email_post,
						'password' => md5($pass_post),
						'level' => 3,
						'active' => 1,
						'bentuk' => $find->jenjang_id
					);
					$this->db->where('npsn', $find->npsn);
					$this->db->update('users', $data);
				}else{
					$data = array(
						'sekolah_id' => $find->npsn,
						'fullname' => $find->sekolah,
						'email' => $email_post,
						'password' => md5($pass_post),
						'level' => 3,
						'active' => 1,
						'bentuk' => $find->jenjang_id
					);
					$this->db->insert('users', $data);
				}
				
				helper_log("login", "Login Pada Sistem");
				redirect('dashboard');
			}else{
				$this->session->set_flashdata('flasherror','Email/Password Tidak Ditemukan.<br>Harap Periksa Kembali atau Hubungi Administrator Dapodik.');
				//$this->index();
				redirect('login');
			}

			// if($this->_resolve_user_login($email_post, $pass_post)){
			// 	$userid = $this->_get_userID($email_post);
			// 	$ip_address = $this->input->ip_address();
			// 	$level = $this->_get_level($email_post);
			// 	//$shipping = $this->_get_shipping($email_post);
			// 	$bentuk = $this->_get_bentuk($email_post);
			// 	$create_session = array(
			// 		'userid'=> $userid,
			// 		'ip_address'=> $ip_address,
			// 		'signin' => TRUE,
			// 		'level' => $level,
			// 		//'shipping' => $shipping,
			// 		'bentuk' => $bentuk
			// 	);
				
			// 	$this->session->set_userdata($create_session);
			// 	helper_log("login", "Login Pada Sistem");
			// 	redirect('dashboard');
			// }else{
			// 	$this->session->set_flashdata('flasherror','Email/Password Tidak Ditemukan atau Akun Anda Belum di Aktifasi.<br>Harap Periksa Kembali atau Hubungai Administrator.');
			// 	$this->index();
			// }
		}else{
			$this->session->set_flashdata('flasherror', validation_errors('<div class="error">', '</div>'));
			$this->index();
		}
	}
	
	// private function _resolve_user_login($email_post, $pass_post)
	// {
	// 	$hash = $this->data->get_user($email_post);
	// 	return $this->_verify_password_hash($pass_post, $hash);
	// }
	
	// private function _get_userid($email_post){
	// 	$userID = $this->data->get_userid($email_post);
	// 	return $userID;
	// }
	
	// private function _get_username($email_post){
	// 	$username = $this->data->get_username($email_post);
	// 	return $username;
	// }
	
	// private function _get_level($email_post){
	// 	$level = $this->data->get_level($email_post);
	// 	return $level;
	// }

	// private function _get_shipping($email_post){
	// 	$shipping = $this->data->get_shipping($email_post);
	// 	return $shipping;
	// }

	// private function _get_bentuk($email_post){
	// 	$bentuk = $this->data->get_bentuk($email_post);
	// 	return $bentuk;
	// }
	
	// private function _verify_password_hash($pass_post, $hash)
	// {
	// 	return password_verify($pass_post, $hash);	
	// }
	
	public function logout()
	{
		$this->session->unset_userdata('userid');
		$this->session->unset_userdata('password');
		$this->session->sess_destroy();
		helper_log("logout", "Logout Pada Sistem");
		redirect('home/dashboard');
	}

	// public function sandi()
	// {
	// 	$data['title'] 		= 'RESET PASSWORD BAKULKAS';
	// 	$data['record'] 	= FALSE;
	// 	$data['content'] 	= $this->folder.'sandi';
	// 	$data['style'] 		= $this->folder.'style';
	// 	$data['js'] 		= $this->folder.'js';
		
	// 	$this->load->view($data['content'], $data);
	// }

	// public function reset()
	// {
	// 	$find = $this->db->get_where('users', array('email'=>$this->input->post('email'), 'deleted_at'=> null, 'level'=>3))->row();
	// 	if(!$find){
	// 		$this->session->set_flashdata('flasherror','Alamat Email Yang Anda Masukan Tidak Terdapat Dalam Database Kami, Hubungi Administrator.');
	// 		redirect('login/sandi');
	// 	}else{
	// 		$password = $this->random_password();
	// 		$data = array(
	// 			'password' => password_hash($password, PASSWORD_BCRYPT),
	// 			'updated_at' => date('Y-m-d H:i:s')
	// 		);
	// 		$this->data->update($data, $find->id);
	// 		$this->session->set_flashdata('flashconfirm','Password Anda Adalah <b>'.$password.'</b><br>Mohon Dicatat Segera, Silahkan Login Langsung Dengan Password Tertera dan Setelah Login Mohon Segera Mengganti Password. <br>Jika Ada Kendala Hubungi Administrator.');
	// 		redirect('login/sandi');
	// 	}
	// }

	// private function random_password() 
	// {
	// 	$alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
	// 	$password = array(); 
	// 	$alpha_length = strlen($alphabet) - 1; 
	// 	for ($i = 0; $i < 8; $i++) 
	// 	{
	// 		$n = rand(0, $alpha_length);
	// 		$password[] = $alphabet[$n];
	// 	}
	// 	return implode($password); 
	// }

	public function dapodik()
	{
		$user = 'erkas@disdik.banjarmasinkota.go.id';
		$password = '1234';
		$check = $this->data->get_dapodik($user,md5($password));
		if($check){
			echo 'TRUE';
		}else{
			echo 'FALSE';
		}
	}
}
