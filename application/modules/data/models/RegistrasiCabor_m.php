<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class RegistrasiCabor_m extends MY_Model
{
	public $table = 'cabor'; // you MUST mention the table name
	public $primary_key = 'id'; // you MUST mention the primary key
	public $fillable = array(); // If you want, you can set an array with the fields that can be filled by insert/update
	public $protected = array(); // ...Or you can set an array with the fields that cannot be filled by insert/update
	
	//ajax datatable
    public $column_order = array('a.kode','a.cabor','c.kota','d.kecamatan','e.kelurahan',null, null,'b.active'); //set kolom field database pada datatable secara berurutan
    public $column_search = array('a.kode','a.cabor','c.kota','d.kecamatan','a.telpon','b.email'); //set kolom field database pada datatable untuk pencarian
    public $order = array('a.id' => 'asc'); //order baku 
	
	public function __construct()
	{
		$this->timestamps = TRUE;
		$this->soft_deletes = TRUE;
		parent::__construct();
	}
	
	public function get_new()
    {
        $record = new stdClass();
        $record->id = '';
        return $record;
    }
	
	//urusan lawan datatable
    private function _get_datatables_query()
    {
        $this->db->select('a.*, b.email, b.active, c.kota as kota ,d.kecamatan as kecamatan, e.kelurahan as kelurahan');
        $this->db->from('cabor a');
        $this->db->join('users b','a.kode = b.kode_cabor','LEFT');
        $this->db->join('kota c','a.kota_id = c.id','LEFT');
        $this->db->join('kecamatan d','a.kecamatan_id = d.id','LEFT');
        $this->db->join('kelurahan e','a.kelurahan_id = e.id','LEFT');
        
        $i = 0;
        foreach ($this->column_search as $item) // loop column 
        {
            if($_POST['search']['value']) // if datatable send POST for search
            {
                if($i===0) // first loop
                {
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                }
                else
                {
                    $this->db->or_like($item, $_POST['search']['value']);
                }
 
                if(count($this->column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }
         
        if(isset($_POST['order'])) // here order processing
        {
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } 
        else if(isset($this->order))
        {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }
 
    function count_filtered()
    {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }
 
    public function count_all()
    {
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }
    
    //urusan lawan ambil data
    function get_datatables()
    {
        $this->_get_datatables_query();
        if($_POST['length'] != -1)
        // $id = $this->uri->segment(3);
        // if($id){
        //     $this->db->where('bentuk', $id);
        // }

        //$this->db->where('a.bentuk', '5');
        $this->db->where('b.level', '3');
        $this->db->where('a.deleted_at', NULL);
        $this->db->where('b.deleted_at', NULL);
        $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }
	
	function get_id($id=null)
    {
        $this->db->where('id', $id);
		$this->db->where('deleted_at', NULL);
        $query = $this->db->get($this->table);
        return $query->row();
    }

    public function delete_data($npsn = NULL){
        $this->db->trans_start(); # Starting Transaction
        //$this->db->trans_strict(FALSE); 
        $data = array(
			'deleted_at' => date('Y-m-d H:i:s')
		);
        $this->db->where('kode', $npsn);
        $this->db->update('cabor', $data); # Inserting data
        
        $this->db->where('kode_cabor', $npsn);
        $this->db->update('users', $data); # Inserting data
        
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return FALSE;
        } else {
            $this->db->trans_commit();
            return TRUE;
        }
    }
}