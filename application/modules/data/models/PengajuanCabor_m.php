<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class PengajuanSD_m extends MY_Model
{
	public $table = 'sekolah'; // you MUST mention the table name
	public $primary_key = 'id'; // you MUST mention the primary key
	public $fillable = array(); // If you want, you can set an array with the fields that can be filled by insert/update
	public $protected = array(); // ...Or you can set an array with the fields that cannot be filled by insert/update
	
	//ajax datatable
    public $column_order = array('a.npsn','a.sekolah','c.nama','d.nama',null, null,'b.active'); //set kolom field database pada datatable secara berurutan
    public $column_search = array('a.npsn','a.sekolah','c.nama','d.nama','a.telpon','b.email'); //set kolom field database pada datatable untuk pencarian
    public $order = array('a.id' => 'asc'); //order baku 
	
	public function __construct()
	{
		$this->timestamps = TRUE;
		$this->soft_deletes = TRUE;
		parent::__construct();
	}
	
	public function get_new()
    {
        $record = new stdClass();
        $record->id = '';
        return $record;
    }
	
    function get_record($tahun=null, $periode=null, $status=null)
    {
        
        if($tahun){
            $this->db->where('e.tahun', $tahun);
        }

        if($periode){
            $this->db->where('e.periode', $periode);
        }

        if($status){
            $this->db->where('e.status', $status);
        }
        
        $this->db->select('a.*, b.email, b.active, c.nama as kecamatan, d.nama as kelurahan, e.tahun, e.periode, e.status, e.id as kertas_id');
        $this->db->from('sekolah a');
        $this->db->join('users b','a.npsn = b.sekolah_id','LEFT');
        $this->db->join('kecamatan c','a.kecamatan_id = c.id','LEFT');
        $this->db->join('kelurahan d','a.kelurahan_id = d.id','LEFT');
        $this->db->join('kertas e','a.npsn = e.sekolah_id','LEFT');

        $this->db->where('a.bentuk', '5');
        $this->db->where('b.level', '3');
        $this->db->where('a.deleted_at', NULL);
        $this->db->where('b.deleted_at', NULL);
        $query = $this->db->get();
        return $query->result();
    }
	
	function get_id($id=null)
    {
        $this->db->where('id', $id);
		$this->db->where('deleted_at', NULL);
        $query = $this->db->get($this->table);
        return $query->row();
    }
}