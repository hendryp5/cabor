<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class PengajuanRencana_m extends MY_Model
{
    public $table = 'cabor'; // you MUST mention the table name
    public $primary_key = 'id'; // you MUST mention the primary key
    public $fillable = array(); // If you want, you can set an array with the fields that can be filled by insert/update
    public $protected = array(); // ...Or you can set an array with the fields that cannot be filled by insert/update
    
    //ajax datatable
    public $column_order = array('a.npsn','a.sekolah','c.nama','d.nama',null, null,'b.active'); //set kolom field database pada datatable secara berurutan
    public $column_search = array('a.npsn','a.sekolah','c.nama','d.nama','a.telpon','b.email'); //set kolom field database pada datatable untuk pencarian
    public $order = array('a.id' => 'asc'); //order baku 
    
    public function __construct()
    {
        $this->timestamps = TRUE;
        $this->soft_deletes = TRUE;
        parent::__construct();
    }
    
    public function get_new()
    {
        $record = new stdClass();
        $record->id = '';
        return $record;
    }
    
    function get_record($tahun=null, $status=null)
    {
        
        if($tahun){
            $this->db->where('c.tahun', $tahun);
        }


        if($status){
            $this->db->where('c.status', $status);
        }
        
        
        $this->db->select('*, a.id as id_cabor,d.id as id_kuota, c.id as kertas_id, c.tahun, c.status, c.jenis_cabor_id as jenis_cabor_id');
        $this->db->from('cabor a');
        $this->db->join('cabor_jenis b','a.id = b.cabor_id','INNER');
        $this->db->join('kertas c','c.jenis_cabor_id = b.jenis_cabor_id','INNER');
        $this->db->join('platform_anggaran d','d.jenis_cabor_id = c.jenis_cabor_id','INNER');
        
        $this->db->where('a.deleted_at', NULL);
        $this->db->where('b.deleted_at', NULL);
        $this->db->where('c.deleted_at', NULL);
        $this->db->where('d.deleted_at', NULL);
        $this->db->group_by('c.jenis_cabor_id');
        $this->db->group_by('c.tahun');
        $this->db->order_by('c.tahun','asc');
        $query = $this->db->get();
        return $query->result();
    }
    
    function get_id($id=null)
    {
        $this->db->where('id', $id);
        $this->db->where('deleted_at', NULL);
        $query = $this->db->get($this->table);
        return $query->row();
    }
}