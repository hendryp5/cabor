<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class PlatformAnggaran extends CI_Controller {

	/**
	 * code by rifqie rusyadi
	 * email rifqie.rusyadi@gmail.com
	 */
	
	public $folder = 'data/platformAnggaran/';
	
	public function __construct()
	{
		parent::__construct();
		$this->load->model('platformAnggaran_m', 'data');
		$this->load->helper('my_helper');
		signin();
		group(array('1'));
	}
	
	//halaman index
	public function index()
	{
		$data['head'] 		= 'Data Kuota Anggaran Cabor';
		$data['record'] 	= FALSE;
		$data['content'] 	= $this->folder.'default';
		$data['style'] 		= $this->folder.'style';
		$data['js'] 		= $this->folder.'js';
		
		$this->load->view('template/default', $data);
	}

	public function created()
	{
		
		$data['head'] 		= 'Tambah Kuota';
		$data['record'] 	= $this->data->get_new();
		$data['cabor'] 		= $this->data->get_cabor();
		$data['content'] 	= $this->folder.'form';
		$data['style'] 		= $this->folder.'style';
        $data['js'] 		= $this->folder.'js';
        
        //$data['blok'] 	    = $this->data->get_blok();
		
		$this->load->view('template/default', $data);
	}

	public function updated_history($id)
	{
		
		$data['head'] 		= 'Perubahan Anggaran Cabor';
		$data['record'] 	= $this->data->get_id($id);
		$data['cabor'] 		= $this->data->get_cabor();
		$data['content'] 	= $this->folder.'form_edit';
		$data['style'] 		= $this->folder.'style';
        $data['js'] 		= $this->folder.'js';
        
        //$data['blok'] 	    = $this->data->get_blok();
		
		$this->load->view('template/default', $data);
	}

    public function updated($id)
    {
        
        $data['head']       = 'Perubahan Anggaran Cabor';
        $data['record']     = $this->data->get_id($id);
        $data['cabor']      = $this->data->get_cabor();
        $data['content']    = $this->folder.'form';
        $data['style']      = $this->folder.'style';
        $data['js']         = $this->folder.'js';
        
        //$data['blok']       = $this->data->get_blok();
        
        $this->load->view('template/default', $data);
    }

    public function detail_history($id)
    {
        
        $data['head']       = 'History Perubahan Anggaran Cabor';
        $data['record']     = $this->data->get_history($id);
        //$data['cabor']      = $this->data->get_cabor();
        $data['content']    = $this->folder.'default_history';
        $data['style']      = $this->folder.'style';
        $data['js']         = $this->folder.'js';
        
      
        
        $this->load->view('template/default', $data);
    }
	
	public function ajax_list()
    {
        $record	= $this->data->get_datatables();
        $data 	= array();
        $no 	= $_POST['start'];
		
        foreach ($record as $row) {
            $no++;
            $col = array();
             $col[] = '<input type="checkbox" class="data-check" value="'.$row->id.'">';
			$col[] = $row->cabor;
			$col[] = $row->tahun;
			$col[] = rupiah($row->jumlah);
			$col[] = '<a class="btn btn-xs btn-flat btn-warning" onclick="edit_data();" href="'.site_url('data/platformAnggaran/updated/'.$row->id).'" data-toggle="tooltip" title="Edit"><i class="glyphicon glyphicon-pencil"></i></a>
                    <a class="btn btn-xs btn-flat btn-primary" onclick="edit_data();" href="'.site_url('data/platformAnggaran/updated_history/'.$row->id).'" data-toggle="tooltip" title="Edit"><i class="fa fa-paper-plane-o"></i></a>
                    <a class="btn btn-xs btn-flat btn-info" onclick="edit_data();" href="'.site_url('data/platformAnggaran/detail_history/'.$row->jenis_cabor_id).'" data-toggle="tooltip" title="Edit"><i class="fa fa-history"></i></a>
                  <a class="btn btn-xs btn-flat btn-danger" data-toggle="tooltip" title="Hapus" onclick="deleted('."'".$row->id."'".')"><i class="glyphicon glyphicon-trash"></i></a>';
			
            //add html for action
            $col[] = '<a class="btn btn-xs btn-flat btn-danger" data-toggle="tooltip" title="Hapus" onclick="deleted('."'".$row->jenis_cabor_id."'".')"><i class="glyphicon glyphicon-trash"></i></a>';
 
            $data[] = $col;
        }
 
        $output = array(
                        "draw" => $_POST['draw'],
                        "recordsTotal" => $this->data->count_all(),
                        "recordsFiltered" => $this->data->count_filtered(),
                        "data" => $data,
                );
        
		echo json_encode($output);
    }

   public function ajax_save()
    {
    	
        $data = array(
            //'kode' => $this->input->post('kode'),    
            'jenis_cabor_id' => $this->input->post('cabor_id'),
            'tahun' => $this->input->post('tahun'),
            'jumlah' => $this->input->post('jumlah'),
        );
        $check = $this->db->get_where('platform_anggaran', array('jenis_cabor_id'=>$this->input->post('cabor_id'), 'tahun'=>$this->input->post('tahun'),'deleted_at'=>null))->row();
    	if(!$check){

        if($this->validation()){
            $insert = $this->data->insert($data);
			helper_log("add", "Menambah Data PlatformAnggaran");
        }

        }else{
                $this->session->set_flashdata('flasherror','Tahun Anggaran Sudah Di Gunakan.');
                redirect('data/platformAnggaran');
        }
    }
    
    public function ajax_update($id)
    {

        //$perusahaan_id = $this->db->get_where('users', array('id'=>$this->session->userdata('userID')))->row();
        $data = array(
            'jenis_cabor_id' => $this->input->post('cabor_id'),
            'tahun' => $this->input->post('tahun'),
            'jumlah' => $this->input->post('jumlah'),
        );
		
        if($this->validation($id)){
            $this->data->update($data, $id);
			helper_log("edit", "Merubah Data PlatformAnggaran");
        }
    }

    public function perubahan_anggaran($id)
    {
        if($this->validation1()){

        	$a = $this->input->post('jenis');

        	if ($a == 1) {
        		$jumlah_berubah = $this->input->post('jumlah') + replacecoma($this->input->post('jumlah_perubahan'));
        	} else if ($a == 2) {
        		$jumlah_berubah = $this->input->post('jumlah') - replacecoma($this->input->post('jumlah_perubahan'));
        	} else {
        		$jumlah_berubah = $this->input->post('jumlah');
        	}

            $data1 = array(
               	'jenis_cabor_id' => $this->input->post('cabor_idx'),
            	'tahun' => $this->input->post('tahun'),
            	'jumlah' => $jumlah_berubah,
            );
            // $insert = $this->data->insert($sekolah);
            $data2 = array(
                'jenis_cabor_id' => $this->input->post('cabor_idx'),
	            'tahun' => $this->input->post('tahun'),
	            'jenis' => $this->input->post('jenis'),
	            'jumlah_kuota' => replacecoma($this->input->post('jumlah_perubahan')),
	            'keterangan' => $this->input->post('keterangan'),
                'created_at' => date('Y-m-d H:i:s')
            );

            $insert = $this->data->perubahan_data($data1, $data2, $id);
            if($insert){
                helper_log("add", "Perubahan Data Berhasil");
                $this->session->set_flashdata('flashconfirm','Proses Perubahan Anggaran Berhasil');
                //$this->send_mail($user['fullname'], $user['email'], $user['password']);
                redirect('data/platformAnggaran');
            }else{
                $this->session->set_flashdata('flasherror','Proses Perubahan Anggaran Gagal.');
                redirect('data/platformAnggaran');
            }
            
        }else{
            $this->index();
        }
    }
    
    public function ajax_delete($id)
    {
        $this->data->delete($id);
		helper_log("trash", "Menghapus Data belanja");
        echo json_encode(array("belanja" => TRUE));
    }
    
    public function ajax_delete_all()
    {
        $list_id = $this->input->post('id');
        foreach ($list_id as $id) {
            $this->data->delete($id);
			helper_log("trash", "Menghapus Data belanja");
        }
        echo json_encode(array("status" => TRUE));
    }

    public function deleted_history($id)
    {
        $data = array(
            'deleted_at' => date('Y-m-d H:m:s'),
        );

        $this->data->update_data($data, $id);
        helper_log("edit", "Mengajukan Draf Rencana Anggaran");
        $this->session->set_flashdata('flashconfirm','Draf Rencana Berhasil Diajukan.');
        echo json_encode(array("status" => TRUE));
        //redirect('rkas/kertas');
    }
	
	private function validation()
    {
        //$data = array('success' => false, 'messages' => array());
         $data = array('success' => false, 'messages' => array());
        
       //$this->form_validation->set_rules("kode", "Kode Standart belanja", "trim|required");
        //$this->form_validation->set_rules("cabor_id", "Pilih Cabor", "trim|required");
        $this->form_validation->set_rules("tahun", "Tahun", "trim|required");
        $this->form_validation->set_rules("jumlah", "Jumlah", "trim|required");
		
		$this->form_validation->set_error_delimiters('<p class="text-danger">', '</p>');
        
        if($this->form_validation->run()){
            $data['success'] = true;
        }else{
            foreach ($_POST as $key => $value) {
                $data['messages'][$key] = form_error($key);
            }
        }
        echo json_encode($data);
        return $this->form_validation->run();
    }

    private function validation1()
    {
        //$data = array('success' => false, 'messages' => array());
         $data = array('success' => false, 'messages' => array());
        
       //$this->form_validation->set_rules("kode", "Kode Standart belanja", "trim|required");
        //$this->form_validation->set_rules("cabor_id", "Pilih Cabor", "trim|required");
        $this->form_validation->set_rules("tahun", "Tahun", "trim|required");
        $this->form_validation->set_rules("jumlah", "Jumlah", "trim|required");
		
		$this->form_validation->set_error_delimiters('<p class="text-danger">', '</p>');
        
        
        return $this->form_validation->run();
    }
	
    
   
	
	

	
	
}
