<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class RegistrasiCabor extends CI_Controller {

	/**
	 * code by rifqie rusyadi
	 * email rifqie.rusyadi@gmail.com
	 */
	
	public $folder = 'data/registrasiCabor/';
	
	public function __construct()
	{
		parent::__construct();
		$this->load->model('RegistrasiCabor_m', 'data');
		$this->load->helper('my_helper');
		signin();
		group(array('1'));
	}
	
	//halaman index
	public function index()
	{
		$data['head'] 		= 'Data Registrasi Pengurus Provinsi';
		$data['record'] 	= FALSE;
		$data['content'] 	= $this->folder.'default';
		$data['style'] 		= $this->folder.'style';
		$data['js'] 		= $this->folder.'js';
		
		$this->load->view('template/default', $data);
	}
	
	public function ajax_list()
    {
        $record	= $this->data->get_datatables();
        $data 	= array();
        $no 	= $_POST['start'];
		
        foreach ($record as $row) {
            $no++;
            $col = array();
			$col[] = $row->kode;
			$col[] = $row->nickname;
			$col[] = $row->cabor;
			$col[] = jenis_cabor($row->id) ? jenis_cabor($row->id) : 'NA';
			$col[] = $row->kota;
			$col[] = $row->telpon;
			$col[] = $row->email;
			$col[] = $row->active ? '<a href="'.site_url('data/registrasiCabor/active/'.$row->kode).'" class="btn btn-xs btn-flat btn-success"><i class="fa fa-check"></i></a>' : '<a href="'.site_url('data/registrasiCabor/active/'.$row->kode).'" class="btn btn-xs btn-flat btn-block btn-danger"><i class="fa fa-minus"></i></a>';
			
            //add html for action
            $col[] = '<a class="btn btn-xs btn-flat btn-danger" data-toggle="tooltip" title="Hapus" onclick="deleted('."'".$row->kode."'".')"><i class="glyphicon glyphicon-trash"></i></a>';
 
            $data[] = $col;
        }
 
        $output = array(
                        "draw" => $_POST['draw'],
                        "recordsTotal" => $this->data->count_all(),
                        "recordsFiltered" => $this->data->count_filtered(),
                        "data" => $data,
                );
        
		echo json_encode($output);
    }
    
    public function ajax_delete($npsn=null)
    {
		$delete = $this->data->delete_data($npsn);
		
		$data = array(
			'deleted_at' => date('Y-m-d H:i:s')
		);
		
		// if($delete){
		// 	$this->db->where('sekolah_id', $id);
		// 	$this->db->update('users', $data );
		// }

		helper_log("trash", "Menghapus Data Registrasi Sekolah Dasar");
        echo json_encode(array("status" => TRUE));
	}
	
	public function active($id)
    {
		$find=$this->db->get_where('users', array('kode_cabor'=>$id, 'deleted_at'=>null))->row();
		
		if($find){
			$active = $find->active == 1 ? 0 : 1;
			$data = array(
					'active' => $active
			);
			
			$this->db->where('kode_cabor', $id);
			$this->db->update('users', $data);
			helper_log("edit", "Merubah Aktivasi Registrasi Cabor");
			//$this->send_mail($id, $active);
			redirect('data/registrasiCabor');
		}else{
			redirect('data/registrasiCabor');
		}
	}

	private function send_mail($id=null, $active=null)
	{
		//Load email library
		$this->load->library('email');
		$this->load->library('encrypt');
		$find=$this->db->get_where('users', array('kode_cabor'=>$id, 'deleted_at'=>null))->row();
		//SMTP & mail configuration
		$config = array(
			'protocol'  => 'smtp',
			'smtp_host' => 'ssl://smtp.googlemail.com',
			'smtp_port' => 465,
			'smtp_user' => 'hendryprojek@gmail.com',
			'smtp_pass' => 'apakatadunia',
			'mailtype'  => 'html',
			'charset'   => 'utf-8'
		);
		$this->email->initialize($config);
		$this->email->set_mailtype("html");
		$this->email->set_newline("\r\n");

		//Email content
		$htmlContent = '<h3>Data Aktivasi SIRKAS Dinas Pendidikan Kota Banjarmasin</h3>';
		if($active == 1){
			$htmlContent .= '<p>Anda telah mendaftarkan diri anda atas nama sekolah '.$find->fullname.' yang pada saat ini telah AKTIF.</p>';
			$htmlContent .= '<p>Jika anda belum dapat melakukan akses pada halaman login hubungi administrator.</p>';
		}else{
			$htmlContent .= '<p>Anda telah mendaftarkan diri anda atas nama '.$find->fullname.' yang pada saat ini telah NON AKTIF.</p>';
			$htmlContent .= '<p>Jika anda ingin dapat melakukan akses pada halaman login hubungi administrator.</p>';
		}
		

		$this->email->to($find->email);
		$this->email->from('hendryprojek@gmail.com','SIRKAS Dinas Pendidikan Kota Banjarmasin');
		$this->email->subject('no-replay : Data Aktivasi Registrasi Sekolah Dasar');
		$this->email->message($htmlContent);
		//Send email
		$this->email->send();
	}
	
}
