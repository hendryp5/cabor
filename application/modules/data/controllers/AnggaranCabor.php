<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class AnggaranCabor extends CI_Controller {

	/**
	 * code by rifqie rusyadi
	 * email rifqie.rusyadi@gmail.com
	 */
	
	public $folder = 'data/anggaranCabor/';
	
	public function __construct()
	{
		parent::__construct();
		$this->load->model('anggaranCabor_m', 'data');
		$this->load->helper('my_helper');
		signin();
		group(array('1'));
	}
	
	//halaman index
	public function index()
	{
		$data['head'] 		= 'Data Anggaran Cabor';
		$data['record'] 	= FALSE;
		$data['content'] 	= $this->folder.'default';
		$data['style'] 		= $this->folder.'style';
		$data['js'] 		= $this->folder.'js';
		
		$this->load->view('template/default', $data);
	}

	public function created()
	{
		
		$data['head'] 		= 'Tambah Anggaran Cabor';
		$data['record'] 	= $this->data->get_new();
		$data['cabor'] 		= $this->data->get_cabor();
		$data['content'] 	= $this->folder.'form';
		$data['style'] 		= $this->folder.'style';
        $data['js'] 		= $this->folder.'js';
        
        $data['blok'] 	    = $this->data->get_blok();
		
		$this->load->view('template/default', $data);
	}

	public function updated($id)
	{
		
		$data['head'] 		= 'Ubah Anggaran Cabor';
		$data['record'] 	= $this->data->get_id($id);
		$data['cabor'] 		= $this->data->get_cabor();
		$data['content'] 	= $this->folder.'form';
		$data['style'] 		= $this->folder.'style';
        $data['js'] 		= $this->folder.'js';
        
        $data['blok'] 	    = $this->data->get_blok();
		
		$this->load->view('template/default', $data);
	}
	
	public function ajax_list()
    {
        $record	= $this->data->get_datatables();
        $data 	= array();
        $no 	= $_POST['start'];
		
        foreach ($record as $row) {

            $selisih = $row->jumlah - jumlah_rencana_anggaran($row->kertas_id);
            $no++;
            $col = array();
             $col[] = '<input type="checkbox" class="data-check" value="'.$row->id.'">';
			$col[] = $row->kode;
			$col[] = $row->cabor;
            $col[] = $row->tahun;
            $col[] = rupiah($row->jumlah);
            $col[] = rupiah(jumlah_rencana_anggaran($row->kertas_id));
			$col[] = rupiah($selisih);
            $col[] = status($row->status);
			$col[] = '';
			
            //add html for action
           
 
            $data[] = $col;
        }
 
        $output = array(
                        "draw" => $_POST['draw'],
                        "recordsTotal" => $this->data->count_all(),
                        "recordsFiltered" => $this->data->count_filtered(),
                        "data" => $data,
                );
        
		echo json_encode($output);
    }

   public function ajax_save()
    {
        $data = array(
            //'kode' => $this->input->post('kode'),    
            'cabor_id' => $this->input->post('cabor_id'),
            //'plafrom_id' => $this->input->post('plafrom_id'),
            'tahun' => $this->input->post('tahun'),
            'jumlah_kuota' => $this->input->post('jumlah'),
        );
        
        if($this->validation()){
            $insert = $this->data->insert($data);
			helper_log("add", "Menambah Data anggaranCabor");
        }
    }
    
    public function ajax_update($id)
    {
        //$perusahaan_id = $this->db->get_where('users', array('id'=>$this->session->userdata('userID')))->row();
        $data = array(
            'cabor_id' => $this->input->post('cabor_id'),
            //'plafrom_id' => $this->input->post('plafrom_id'),
            'tahun' => $this->input->post('tahun'),
            'jumlah_kuota' => $this->input->post('jumlah'),
        );
		
        if($this->validation($id)){
            $this->data->update($data, $id);
			helper_log("edit", "Merubah Data anggaranCabor");
        }
    }
    
    public function ajax_delete($id)
    {
        $this->data->delete($id);
		helper_log("trash", "Menghapus Data belanja");
        echo json_encode(array("belanja" => TRUE));
    }
    
    public function ajax_delete_all()
    {
        $list_id = $this->input->post('id');
        foreach ($list_id as $id) {
            $this->data->delete($id);
			helper_log("trash", "Menghapus Data belanja");
        }
        echo json_encode(array("status" => TRUE));
    }
	
	private function validation()
    {
        //$data = array('success' => false, 'messages' => array());
         $data = array('success' => false, 'messages' => array());
        
       //$this->form_validation->set_rules("kode", "Kode Standart belanja", "trim|required");
        $this->form_validation->set_rules("cabor_id", "Pilih Cabor", "trim|required");
        $this->form_validation->set_rules("tahun", "Tahun", "trim|required");
        $this->form_validation->set_rules("jumlah", "Jumlah", "trim|required");
		
		$this->form_validation->set_error_delimiters('<p class="text-danger">', '</p>');
        
        if($this->form_validation->run()){
            $data['success'] = true;
        }else{
            foreach ($_POST as $key => $value) {
                $data['messages'][$key] = form_error($key);
            }
        }
        echo json_encode($data);
        return $this->form_validation->run();
    }
	
    
   
	
	

	
	
}
