<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class PengajuanRencana extends CI_Controller {

	/**
	 * code by rifqie rusyadi
	 * email rifqie.rusyadi@gmail.com
	 */
	
	public $folder = 'data/pengajuanRencana/';
	
	public function __construct()
	{
		parent::__construct();
		$this->load->model('pengajuanRencana_m', 'data');
		$this->load->helper('my_helper');
		signin();
		group(array('1'));
	}
	
	//halaman index
	public function index()
	{
		$tahun = null;
		$periode = null;
		$status = null;

		if(isset($_GET['tahun'])){
			$tahun = $_GET['tahun'];
		}


		if(isset($_GET['status'])){
			$status = $_GET['status'];
		}

		$data['head'] 		= 'Data Permohonan Rencana Anggaran';
		$data['record'] 	= $this->data->get_record($tahun, $status);
		$data['content'] 	= $this->folder.'default';
		$data['style'] 		= $this->folder.'style';
		$data['js'] 		= $this->folder.'js';
		
		$this->load->view('template/default', $data);
	}

	private function send_mail($id=null, $active=null)
	{
		//Load email library
		$this->load->library('email');
		$this->load->library('encrypt');
		$find=$this->db->get_where('users', array('sekolah_id'=>$id, 'deleted_at'=>null))->row();
		//SMTP & mail configuration
		$config = array(
			'protocol'  => 'smtp',
			'smtp_host' => 'ssl://smtp.googlemail.com',
			'smtp_port' => 465,
			'smtp_user' => 'rifqie@gmail.com',
			'smtp_pass' => 'Mautauaj4',
			'mailtype'  => 'html',
			'charset'   => 'utf-8'
		);
		$this->email->initialize($config);
		$this->email->set_mailtype("html");
		$this->email->set_newline("\r\n");

		//Email content
		$htmlContent = '<h3>Data Aktivasi SIRKAS Dinas Pendidikan Kota Banjarmasin</h3>';
		if($active == 1){
			$htmlContent .= '<p>Anda telah mendaftarkan diri anda atas nama sekolah '.$find->fullname.' yang pada saat ini telah AKTIF.</p>';
			$htmlContent .= '<p>Jika anda belum dapat melakukan akses pada halaman login hubungi administrator.</p>';
		}else{
			$htmlContent .= '<p>Anda telah mendaftarkan diri anda atas nama '.$find->fullname.' yang pada saat ini telah NON AKTIF.</p>';
			$htmlContent .= '<p>Jika anda ingin dapat melakukan akses pada halaman login hubungi administrator.</p>';
		}
		

		$this->email->to($find->email);
		$this->email->from('rifqie@gmail.com','SIRKAS Dinas Pendidikan Kota Banjarmasin');
		$this->email->subject('no-replay : Data Aktivasi Permohonan RKAS Sekolah Dasar');
		$this->email->message($htmlContent);
		//Send email
		$this->email->send();
	}
	
}
