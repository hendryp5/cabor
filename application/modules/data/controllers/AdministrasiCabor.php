<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class AdministrasiCabor extends CI_Controller {

	/**
	 * code by rifqie rusyadi
	 * email rifqie.rusyadi@gmail.com
	 */
	
	public $folder = 'data/administrasiCabor/';
	
	public function __construct()
	{
		parent::__construct();
		$this->load->model('administrasiCabor_m', 'data');
		$this->load->helper('my_helper');
		signin();
		group(array('1'));
	}
	
	//halaman index
	public function index()
	{
		$data['head'] 		= 'Data Administrasi Pengurus Provinsi';
		$data['record'] 	= FALSE;
		$data['content'] 	= $this->folder.'default';
		$data['style'] 		= $this->folder.'style';
		$data['js'] 		= $this->folder.'js';
		
		$this->load->view('template/default', $data);
	}
	
	public function ajax_list()
    {
        $record	= $this->data->get_datatables();
        $data 	= array();
        $no 	= $_POST['start'];
		
        foreach ($record as $row) {
            $no++;
						$col = array();
						$col[] = $no;
            $col[] = $row->kode.'<br><b>'.$row->cabor.'</b><br>'.$row->nickname.'<br>'.$row->alamat.'<br>'.$row->kota.'-'.$row->kecamatan.'-'.$row->kelurahan.'<br>'.$row->telpon.'<br>'.$row->email;
			
            //add html for action
            $col[] = '<a href="'.site_url('profil/info/').$row->id.'" class="btn btn-xs btn-flat btn-success" data-toggle="tooltip" title="Data Administrasi"><i class="fa fa-file-text"></i></a>';
 
            $data[] = $col;
        }
 
        $output = array(
                        "draw" => $_POST['draw'],
                        "recordsTotal" => $this->data->count_all(),
                        "recordsFiltered" => $this->data->count_filtered(),
                        "data" => $data,
                );
        
		echo json_encode($output);
    }
}
