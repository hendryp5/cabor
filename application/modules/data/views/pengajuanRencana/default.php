<div class="row">
	<div class="col-md-12">
		<div id="message"></div>
		<div class="box box-primary box-solid">
			<div class="box-header with-border">
				<h3 class="box-title"><?= isset($head) ? $head : ''; ?></h3>
				<div class="box-tools pull-right">
					<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
					<button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
				</div>
			</div>
			<!-- box-body -->
			<div class="box-body">
					<div class="row text-muted well well-sm" style="padding: 5px !important;margin-left: 0px !important;margin-right: 0px !important;border-radius:0px !important;-webkit-box-shadow:none !important;box-shadow:none !important;">
					<?= form_open('',array('method'=>'GET')); ?>
					<div class="col-md-4">
						<div class="form-group">
							<?php 
							echo form_label('Tahun Anggaran','tahun');
							$selected = set_value('tahun');
							$tahun = array(''=>'Pilih Salah Satu','2020'=>'2020','2021'=>'2021');
							echo form_dropdown('tahun', $tahun, $selected, "class='form-control select2' name='tahun' id='tahun' style='width:100%'");
							?>
						</div>
					</div>

					<div class="col-md-6">
						<div class="form-group">
							<?php 
							echo form_label('Status Anggaran','status');
							$selected = set_value('status');
							$status = array(''=>'Pilih Salah Satu','1'=>'Draf','2'=>'Pengajuan','3'=>'Diterima','4'=>'Ditolak');
							echo form_dropdown('status', $status, $selected, "class='form-control select2' name='status' id='status' style='width:100%'");
							?>
						</div>
					</div>

					<div class="col-md-2">
						<div class="form-group">
							<label>&nbsp;</label>
							<button type="submit" class="form-control btn btn-sm btn-flat btn-warning"><i class="fa fa-search"></i> Pencarian</button>
						</div>
					</div>
					<?= form_close(); ?>
				</div>
				<div class="row">
					<div class="col-md-12">
						<span id="key" style="display: none;"><?= $this->security->get_csrf_hash(); ?></span>
						<div class="table-responsive">
						<table id="tableIDX" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
							<thead>
								<tr>
									<th>Kode Registrasi</th>
									<th>Nama Pengurus Provinsi</th>
									<th>Cabor</th>
									<th>Tahun</th>
									<th>Jumlah Platform</th>
									<th>Jumlah Rencana</th>
									<th>Selisih</th>
									<th>Status</th>
									<th width="8px"></th>
								</tr>
							</thead>
							<tbody>
								<?php if($record): ?>
								<?php foreach($record as $row): 
								 $selisih = $row->jumlah - jumlah_rencana_anggaran($row->kertas_id);
								?>
								<tr>
									<td><?= $row->kode; ?></td>
									<td><?= $row->cabor; ?></td>
									<td><?= cabang_olahraga($row->jenis_cabor_id); ?></td>
									<td  class="text-center"><?= $row->tahun; ?></td>
									<td class="text-right"><?= rupiah(kuota_cabor($row->jenis_cabor_id,$row->tahun)); ?></td>
									<td class="text-right"><?= rupiah(jumlah_rencana_anggaran($row->kertas_id)) ?></td>
									<td class="text-right"><?= rupiah($selisih); ?></td>
									<td><?= status($row->status); ?></td>
									<td><a class="btn btn-xs btn-flat btn-info" data-toggle="tooltip" title="Detail RKAS" href="<?= site_url('rkas/pengajuan/').$row->cabor_id.'/'.$row->kertas_id.'/'.$row->tahun; ?>"><i class="glyphicon glyphicon-list"></i></a></td>
								</tr>
								<?php endforeach; ?>
								<?php endif; ?>
							</tbody>
						</table>
						</div>
					</div>
				</div>
			</div>
			<!-- ./box-body -->
		</div>
	</div>
</div>