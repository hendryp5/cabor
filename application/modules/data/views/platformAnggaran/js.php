<script>
$(function () {
	$('.select2').select2();
	$('.number').number(true);
});

function updateTotal() {

  var jumlah_perubahan           = parseFloat($('#jumlah_perubahan').val());
  var jumlah         = parseFloat($('#jumlah').val());

  var jumlah_total      = parseFloat();
  //keras
 
  /*$('#bphtb').val(bphtb);*/
 
  $('#jumlah_total').val(jumlah_total);
}

$("#biaya_upgrade").keyup(updateTotal);

function delhistory(id) {
  var r = confirm("Anda Yakin Ingin Menghapus Data Ini.");
  if (r == true) {
	console.log("You pressed OK!");
		var process = '<?= site_url('data/platformAnggaran/deleted_history/'); ?>'+id;
	  // ajax delete data to database
        $.ajax({
            url : process,
            type: "POST",
            data: {'<?php echo $this->security->get_csrf_token_name(); ?>' : '<?php echo $this->security->get_csrf_hash(); ?>'},
            dataType: "JSON",
            success: function(data)
            {
                $('#message').append('<div class="alert alert-danger">' +
                        '<span class="glyphicon glyphicon-ok"></span>' +
                        ' Data berhasil di hapus.' +
                        '</div>');
                       
                // tutup pesan
                $('.alert-danger').delay(250).show(10, function() {
                    $(this).delay(1000).hide(10, function() {
                    $(this).remove();
                    });
                })
                
                location.reload();
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error deleting data');
            }
        });
  } else {
    console.log("You pressed Cancel!");
  }
}

</script>