<div class="row">
	<div class="col-md-12">
		<div id="message"></div>
		<div class="box box-primary box-solid">
			<div class="box-header with-border">
				<h3 class="box-title"><?= isset($head) ? $head : ''; ?></h3>
				<div class="box-tools pull-right">
					<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
					<button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
				</div>
			</div>
			<form id="formID" role="form" action="<?= site_url('data/platformAnggaran/perubahan_anggaran/'.$record->id) ?>" method="post">
			<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
			<input type="hidden" name="jumlah" value="<?php echo $record->jumlah; ?>" />
			<input type="hidden" name="cabor_idx" value="<?php echo $record->jenis_cabor_id; ?>" />
			
			<!-- box-body -->
			<div class="box-body">
				<div class="row">
					
					<div class="col-md-12">
		            <div class="form-group <?php echo form_error('cabor_id') ? 'has-error' : null; ?>">
		                <?php 
		                echo form_label('Cabang Olahraga','cabor_id');
		                $selected = set_value('cabor_id',$record->jenis_cabor_id);
		                echo form_dropdown('cabor_id', $cabor, $selected, "disabled class='form-control select2' name='cabor_id' id='cabor_id'");
		                echo form_error('cabor_id') ? form_error('cabor_id', '<p class="help-block">','</p>') : '';
		                ?>
		            </div>
		        </div>

		        <div class="col-md-12">
						<div class="form-group <?php echo form_error('jumlah_perubahan') ? 'has-error' : null; ?>">
							<?php
							echo form_label('Jumlah Perubahan','jumlah_perubahan');
							$data = array('class'=>'form-control number','name'=>'jumlah_perubahan','id'=>'jumlah_perubahan','type'=>'text','value'=>set_value('jumlah_perubahan'));
							echo form_input($data);
							echo form_error('jumlah_perubahan') ? form_error('jumlah_perubahan', '<p class="help-block">','</p>') : '';
							?>
						</div>
					</div>

					<div class="col-md-12">
						<div class="form-group <?php echo form_error('jenis') ? 'has-error' : null; ?>">
							<?php 
							echo form_label('Jenis Perubahan Anggaran','jenis');
							$selected = set_value('jenis');
							$status = array('0'=>'Pilih','1'=>'Penambahan','2'=>'Pengurangan');
							echo form_dropdown('jenis', $status, $selected, "class='form-control select2' name='jenis' id='jenis'");
							echo form_error('jenis') ? form_error('jeniss', '<p class="help-block">','</p>') : '';
							?>
						</div>
					</div>

					

					<div class="col-md-12">
						<div class="form-group <?php echo form_error('tahun') ? 'has-error' : null; ?>">
							<?php
							echo form_label('Tahun Anggaran','tahun');
							$data = array('readonly class'=>'form-control','name'=>'tahun','id'=>'tahun','type'=>'text','value'=>set_value('tahun', $record->tahun));
							echo form_input($data);
							echo form_error('tahun') ? form_error('tahun', '<p class="help-block">','</p>') : '';
							?>
						</div>
					</div>

					<div class="col-md-12">
			            <div class="form-group <?php echo form_error('keterangan') ? 'has-error' : null; ?>">
			                <?php
			                echo form_label('Keterangan Perubahan','keterangan');
			                $data = array('required class'=>'form-control','name'=>'keterangan','rows'=>'2','cols'=>'4','id'=>'keterangan','type'=>'text','value'=>set_value('keterangan'));
			                echo form_textarea($data);
			                echo form_error('keterangan') ? form_error('keterangan', '<p class="help-block">','</p>') : '';
			                ?>
			            </div>
			        </div>
					
				</div>
			</div>
			<!-- ./box-body -->
			<div class="box-footer">
				<button type="submit" class="btn btn-sm btn-flat btn-info"><i class="fa fa-save"></i> Simpan & Keluar</button>
				<button type="reset" class="btn btn-sm btn-flat btn-warning"><i class="fa fa-refresh"></i> Reset</button>
				<button type="button" class="btn btn-sm btn-flat btn-danger" onclick="back();"><i class="fa fa-close"></i> Keluar</button>
			</div>
			</form>
		</div>
	</div>
</div>