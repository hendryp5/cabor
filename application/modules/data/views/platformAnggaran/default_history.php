<div class="row">
	<div class="col-md-12">
		<div id="message"></div>
		<div class="box box-primary box-solid">
			<div class="box-header with-border">
				<h3 class="box-title"><?= isset($head) ? $head : ''; ?></h3>
				<div class="box-tools pull-right">
					<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
					<button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
				</div>
			</div>
			<!-- box-body -->
			<div class="box-body">
				<div class="row">
					<div class="col-md-12">
						
						<span id="key" style="display: none;"><?= $this->security->get_csrf_hash(); ?></span>
						<table class="table table-bordered">
							<thead>
								<tr class="text-center">
									<th>Tanggal Perubahan</th>
									<th>Cabang Olahraga</th>
									<th>Tahun</th>
									<th>Jumlah Perubahan</th>
									<th>Jenis Perubahan</th>
									<th>Keterangan</th>
									<th width="60px">#</th>
								</tr>
							</thead>
							<tbody>
								<?php if ($record): ?>
								<?php foreach ($record as $row):  ?>
								<tr style="background-color: #f4f4f4;">
									<td><?= $row->created_at ? ddMMMyyyy($row->created_at) : '-'; ?></td>
									<td><?= $row->jenis_cabor_id ? cabang_olahraga($row->jenis_cabor_id) : '-'; ?></td>
									<td><?= $row->tahun ? $row->tahun : '-'; ?></td>
									<td><?= $row->jumlah_kuota ? rupiah($row->jumlah_kuota) : '-'; ?></td>
									<td><?= $row->jenis ? jenis_perubahan($row->jenis) : '-'; ?></td>
									<td><?= $row->keterangan ? $row->keterangan : '-'; ?></td>
									<td><button class="btn btn-xs btn-flat btn-danger" onclick="delhistory(<?= $row->id; ?>)" id="delDetail"><i class="fa fa-trash"></i></button></td>
								</tr>
							<?php endforeach; ?>
						<?php else: ?>
							<tr>
								<td colspan="7">Belum ada data</td>
							</tr>
						<?php endif; ?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
			<!-- ./box-body -->
		</div>
	</div>
</div>