<script>
$(function () {
	$('.select2').select2();
});

$(function () {
$('#tableIDX').DataTable({
      paging: true,
      lengthChange: true,
      searching: true,
      ordering: true,
      info: true,
      autoWidth: true,
      language: {
        lengthMenu: "Tampilkan _MENU_ Baris",
        zeroRecords: "Maaf - Data Tidak Ditemukan",
        info: "Lihat Halaman _PAGE_ Dari _PAGES_ Total _MAX_ Data",
        infoEmpty: "Tidak Ada Data Tersedia",
        infoFiltered: "(filtered from _MAX_ total records)",
        paginate: {
            first:"Awal",
            last:"Akhir",
            next:"Lanjut",
            previous:"Sebelum"
            },
        search:"Pencarian:",
        },
	  	responsive: true,
        columnDefs: [
            { 
                targets:[ 0 ],
		    	orderable: false,
		    	responsivePriority: 1
            },
            { 
                targets:[ -1 ],
                orderable: false,
		    	responsivePriority: 2
            },
        ]
    });
});
</script>