<div class="row">
	<div class="col-md-12">
		<div id="message"></div>
		<div class="box box-primary box-solid">
			<div class="box-header with-border">
				<h3 class="box-title"><?= isset($head) ? $head : ''; ?></h3>
				<div class="box-tools pull-right">
					<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
					<button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
				</div>
			</div>
			<!-- box-body -->
			<div class="box-body">
					<div class="row text-muted well well-sm" style="padding: 5px !important;margin-left: 0px !important;margin-right: 0px !important;border-radius:0px !important;-webkit-box-shadow:none !important;box-shadow:none !important;">
					<?= form_open('',array('method'=>'GET')); ?>
					<div class="col-md-3">
						<div class="form-group">
							<?php 
							echo form_label('Tahun Anggaran','tahun');
							$selected = set_value('tahun');
							$tahun = array(''=>'Pilih Salah Satu','2019'=>'2019','2020'=>'2020');
							echo form_dropdown('tahun', $tahun, $selected, "class='form-control select2' name='tahun' id='tahun'");
							?>
						</div>
					</div>

					<div class="col-md-3">
						<div class="form-group">
							<?php 
							echo form_label('Periode','periode');
							$selected = set_value('periode');
							$periode = array(''=>'Pilih Salah Satu','1'=>'Triwulan I','2'=>'Triwulan II','3'=>'Triwulan III','4'=>'Triwulan IV');
							echo form_dropdown('periode', $periode, $selected, "class='form-control select2' name='periode' id='periode'");
							?>
						</div>
					</div>

					<div class="col-md-4">
						<div class="form-group">
							<?php 
							echo form_label('Status','status');
							$selected = set_value('status');
							$status = array(''=>'Pilih Salah Satu','1'=>'Draf','2'=>'Pengajuan','3'=>'Diterima','4'=>'Ditolak');
							echo form_dropdown('status', $status, $selected, "class='form-control select2' name='status' id='status'");
							?>
						</div>
					</div>

					<div class="col-md-2">
						<div class="form-group">
							<label>&nbsp;</label>
							<button type="submit" class="form-control btn btn-sm btn-flat btn-default"><i class="fa fa-search"></i> Pencarian</button>
						</div>
					</div>
					<?= form_close(); ?>
				</div>
				<div class="row">
					<div class="col-md-12">
						<span id="key" style="display: none;"><?= $this->security->get_csrf_hash(); ?></span>
						<table id="tableIDX" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
							<thead>
								<tr>
									<th>NPSN</th>
									<th>Nama Sekolah</th>
									<th>Kecamatan</th>
									<th>Kelurahan</th>
									<th>Telpon/HP</th>
									<th>Tahun</th>
									<th>Periode</th>
									<th>Status</th>
									<th width="8px"></th>
								</tr>
							</thead>
							<tbody>
								<?php if($record): ?>
								<?php foreach($record as $row): ?>
								<tr>
									<td><?= $row->npsn; ?></td>
									<td><?= $row->sekolah; ?></td>
									<td><?= $row->kecamatan; ?></td>
									<td><?= $row->kelurahan; ?></td>
									<td><?= $row->telpon; ?></td>
									<td><?= $row->tahun; ?></td>
									<td><?= periode($row->periode); ?></td>
									<td><?= status($row->status); ?></td>
									<td><a class="btn btn-xs btn-flat btn-default" data-toggle="tooltip" title="Detail RKAS" href="<?= site_url('rkas/pengajuan/').$row->npsn.'/'.$row->kertas_id; ?>"><i class="glyphicon glyphicon-list"></i></a></td>
								</tr>
								<?php endforeach; ?>
								<?php endif; ?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
			<!-- ./box-body -->
		</div>
	</div>
</div>