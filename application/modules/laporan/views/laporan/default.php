<div class="row">
	<form action="" method="get">
		<div class="col-md-2 no-print">
			<?php 
			$selected = set_value('bentuk');
			$bentuk = array(''=>'Pilih Tingkatan Sekolah','1'=>'SD','2'=>'SMP');
			echo form_dropdown('bentuk', $bentuk, $selected, "required class='form-control input-sm select2' name='bentuk' id='bentuk'");
			?>
		</div>
		<div class="col-md-2 no-print">
			<?php 
			$selected = set_value('tahun');
			$tahun = array(''=>'Pilih Tahun Anggaran','2019'=>'2019','2020'=>'2020','2021'=>'2021');
			echo form_dropdown('tahun', $tahun, $selected, "required class='form-control input-sm select2' name='tahun' id='tahun'");
			?>
		</div>
		<div class="col-md-2 no-print">
			<button type="submit" class="btn btn-sm btn-flat btn-default"><i class="fa fa-search"></i> Filter</button> <button type="button" class="btn btn-sm btn-flat btn-default no-print" style="margin-top:3px;margin-bottom:3px;" onclick="window.print()"><i class="fa fa-print"></i> Print</button>
		</div>
	</form>
	<div class="col-md-12 text-center">
		<h4></h4>
		<p></p>
	</div>

	<div class="col-md-12">
		<!-- box-body -->
		<?php if($record): ?>
		<!-- program standart -->
		<table class="table table-bordered">
		<thead>
		<tr class="text-center">
			<th class="text-center" >No</th>
			<th class="text-center" >Nama Sekolah</th>
			<th class="text-center" >Kode Rekening</th>
			<th class="text-center" >Pagu</th>
			<th class="text-center" >Triwulan I</th>
			<th class="text-center" >Triwulan II</th>
			<th class="text-center" >Triwulan III</th>
			<th class="text-center" >Triwulan IV</th>
			<th class="text-center" >Jumlah</th>
		</tr>
		</thead>
		<tbody>
		<?php 
		$no=1; 
		$total_tw1 	= 0;
		$total_tw2 	= 0;
		$total_tw3 	= 0;
		$total_tw4 	= 0;
		$total 		= 0;
		?>
		<?php foreach($record as $row): ?>
		<?php 
			$total_tw1 += belanja_triwulan($row->sekolah_id, $x, 1) ? belanja_triwulan($row->sekolah_id, $x, 1)->jumlah : 0;
			$total_tw2 += belanja_triwulan($row->sekolah_id, $x, 2) ? belanja_triwulan($row->sekolah_id, $x, 2)->jumlah : 0;
			$total_tw3 += belanja_triwulan($row->sekolah_id, $x, 3) ? belanja_triwulan($row->sekolah_id, $x, 3)->jumlah : 0;
			$total_tw4 += belanja_triwulan($row->sekolah_id, $x, 4) ? belanja_triwulan($row->sekolah_id, $x, 4)->jumlah : 0;
		?>
		<tr>
			<td><?= $no.'.'; ?></td>
			<td><?= sekolah(null, $row->sekolah_id)->sekolah; ?></td>
			<td class="text-center">4.3.1.06.01.</td>
			<td class="text-center">0</td>
			<td class="text-right"><?= belanja_triwulan($row->sekolah_id, $x, 1) ? rupiah(belanja_triwulan($row->sekolah_id, $x, 1)->jumlah) : 'Rp.0'; ?></td>
			<td class="text-right"><?= belanja_triwulan($row->sekolah_id, $x, 2) ? rupiah(belanja_triwulan($row->sekolah_id, $x, 2)->jumlah) : 'Rp.0'; ?></td>
			<td class="text-right"><?= belanja_triwulan($row->sekolah_id, $x, 3) ? rupiah(belanja_triwulan($row->sekolah_id, $x, 3)->jumlah) : 'Rp.0'; ?></td>
			<td class="text-right"><?= belanja_triwulan($row->sekolah_id, $x, 4) ? rupiah(belanja_triwulan($row->sekolah_id, $x, 4)->jumlah) : 'Rp.0'; ?></td>
			<td class="text-right"><?= jml_belanja_triwulan($row->sekolah_id, $x) ? rupiah(jml_belanja_triwulan($row->sekolah_id, $x)->jumlah) : 'Rp.0'; ?></td>
		</tr>
		<?php $no++; ?>
		<?php endforeach; ?>
		</tbody>
		<tfoot>
			<tr>
				<th colspan="4" class="text-center">Total</th>
				<th class="text-right"><?= rupiah($total_tw1); ?></th>
				<th class="text-right"><?= rupiah($total_tw2); ?></th>
				<th class="text-right"><?= rupiah($total_tw3); ?></th>
				<th class="text-right"><?= rupiah($total_tw4); ?></th>
				<th class="text-right"><?= rupiah($total_tw1+$total_tw2+$total_tw3+$total_tw4); ?></th>
			</tr>
		</tfoot>
		</table>

		<?php else: ?>
		<div class="callout callout-danger">
			<p>Belum Ada Realisasi Tersedia. Silahkan Sekolah Untuk Membuat Terlebih Dahulu</p>
		</div>
		<?php endif; ?>			
			<!-- ./box-body -->
	</div>
</div>