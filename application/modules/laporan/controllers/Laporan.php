<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Laporan extends CI_Controller {

	/**
	 * code by rifqie rusyadi
	 * email rifqie.rusyadi@gmail.com
	 */
	
	public $folder = 'laporan/laporan/';
	
	public function __construct()
	{
		parent::__construct();
		$this->load->model('laporan_m', 'data');
		$this->load->helper('my_helper');
		signin();
		//group(array('1'));
	}
	
    //halaman index
    public function index($tahun=null, $bentuk=null)
	{
		$tahun = null;
		$bentuk = null;
		$head = '';
		if($_GET['tahun']){
			$tahun = $_GET['tahun'];
			
		}

		if($_GET['bentuk']){
			$bentuk = $_GET['bentuk'];
			if($bentuk == 1){
				$head = 'TINGKAT SEKOLAH DASAR TAHUN ';
			}else{
				$head = 'TINGKAT SEKOLAH MENENGAH PERTAMA TAHUN';
			}
		}
		
		$data['head'] 		= $head.$tahun;
		$data['record'] 	= $this->data->get_record($tahun, $bentuk);
		$data['content'] 	= $this->folder.'default';
		$data['style'] 		= $this->folder.'style';
        $data['js'] 		= $this->folder.'js';
		$data['x'] 			= $tahun ? $tahun : date('Y');
		$data['z'] 			= $bentuk ? $bentuk : null;
        //$data['kertas']     = $id;
		$this->load->view('template/print', $data);
	}

}
