<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Penerima extends CI_Controller {

	/**
	 * code by rifqie rusyadi
	 * email rifqie.rusyadi@gmail.com
	 */
	
	public $folder = 'laporan/penerima/';
	
	public function __construct()
	{
		parent::__construct();
		$this->load->model('penerima_m', 'data');
		$this->load->helper('my_helper');
		signin();
		//group(array('1'));
	}
	
    //halaman index
    public function index($tahun=null, $bentuk=null)
	{
		$tahun = null;
		$bentuk = null;
		$head = 'TINGKAT SEKOLAH DASAR DAN MENENGAH PERTAMA ';
		if($_GET['tahun']){
			$tahun = $_GET['tahun'];
			
		}

		if($_GET['bentuk']){
			$bentuk = $_GET['bentuk'];
			if($bentuk == 1){
				$head = 'TINGKAT SEKOLAH DASAR TAHUN ';
			}else{
				$head = 'TINGKAT SEKOLAH MENENGAH PERTAMA TAHUN';
			}
		}
		
		$data['head'] 		= $head.$tahun;
		$data['record'] 	= $this->data->get_record($tahun, $bentuk);
		$data['content'] 	= $this->folder.'default';
		$data['style'] 		= $this->folder.'style';
        $data['js'] 		= $this->folder.'js';
		$data['x'] 			= $tahun ? $tahun : date('Y');
		$data['z'] 			= $bentuk ? $bentuk : null;
        //$data['kertas']     = $id;
		$this->load->view('template/print', $data);
	}
	
	public function triwulan($id=null)
	{
        $find = $this->db->get_where('users', array('id'=>$this->session->userdata('userid')))->row()->sekolah_id;
        $data['head'] 		= 'Rencana Kegiatan dan Anggaran Sekolah';
		$data['record'] 	= $this->data->get_data($find, $id);
		$data['content'] 	= $this->folder.'triwulan';
		$data['style'] 		= $this->folder.'style';
        $data['js'] 		= $this->folder.'js';
        $data['sekolah'] 	= $this->data->get_sekolah($find);
        $data['standart'] 	= $this->data->get_standart();
        $data['kertas']     = $id;	
		$this->load->view('template/print', $data);
	}
	
	public function sptjm($id=null)
	{
        $find = $this->db->get_where('users', array('id'=>$this->session->userdata('userid')))->row()->sekolah_id;
        $data['head'] 		= 'Surat Pernyataan Tanggung Jawab Mutlak (SPTJM)';
		$data['record'] 	= $this->data->get_data($find, $id);
		$data['content'] 	= $this->folder.'sptjm';
		$data['style'] 		= $this->folder.'style';
        $data['js'] 		= $this->folder.'js';
        $data['sekolah'] 	= $this->data->get_sekolah($find);
        $data['standart'] 	= $this->data->get_standart();
        $data['kertas']     = $id;	
		$this->load->view('template/print', $data);
	}
	
	public function sptmh($id=null)
	{
        $find = $this->db->get_where('users', array('id'=>$this->session->userdata('userid')))->row()->sekolah_id;
        $data['head'] 		= 'Surat Pernyataan Telah Menerima Hibah (SPTMH)';
		$data['record'] 	= $this->data->get_data($find, $id);
		$data['content'] 	= $this->folder.'sptmh';
		$data['style'] 		= $this->folder.'style';
        $data['js'] 		= $this->folder.'js';
        $data['sekolah'] 	= $this->data->get_sekolah($find);
        $data['standart'] 	= $this->data->get_standart();
        $data['kertas']     = $id;	
		$this->load->view('template/print', $data);
    }
}
