<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cabor_m extends MY_Model
{
	public $table = 'cabor'; // you MUST mention the table name
	public $primary_key = 'id'; // you MUST mention the primary key
	public $fillable = array(); // If you want, you can set an array with the fields that can be filled by insert/update
	public $protected = array(); // ...Or you can set an array with the fields that cannot be filled by insert/update
	
	//ajax datatable
    public $column_order = array('id','kode','cabor',null); //set kolom field database pada datatable secara berurutan
    public $column_search = array('kode','cabor'); //set kolom field database pada datatable untuk pencarian
    public $order = array('id' => 'asc'); //order baku 
	
	public function __construct()
	{
		$this->timestamps = TRUE;
		$this->soft_deletes = TRUE;
		parent::__construct();
	}
	
	public function get_new()
    {
        $record = new stdClass();
        $record->id = '';
        $record->kode = '';
        $record->cabor = '';
		return $record;
    }
	
	//urusan lawan datatable
    private function _get_datatables_query()
    {
        $this->db->from($this->table);
        $i = 0;
        foreach ($this->column_search as $item) // loop column 
        {
            if($_POST['search']['value']) // if datatable send POST for search
            {
                if($i===0) // first loop
                {
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                }
                else
                {
                    $this->db->or_like($item, $_POST['search']['value']);
                }
 
                if(count($this->column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }
         
        if(isset($_POST['order'])) // here order processing
        {
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } 
        else if(isset($this->order))
        {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }
 
    function count_filtered()
    {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }
 
    public function count_all()
    {
        $this->db->where('deleted_at', NULL);
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }
    
    //urusan lawan ambil data
    function get_datatables()
    {
        
        $this->_get_datatables_query();
        if($_POST['length'] != -1)

        $this->db->where('deleted_at', NULL);
        $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }
	
	function get_id($id=null)
    {
        $this->db->where('id', $id);
		$this->db->where('deleted_at', NULL);
        $query = $this->db->get($this->table);
        return $query->row();
    }

    public function get_kode() 
        {
            
            $query1 = $this->db->query("SELECT right(kode,4) as kode1 FROM cabor");
            
            $code  = '';

            if($query1->num_rows() > 0) {
                foreach($query1->result() as $row){
                    $temporary = (int)$row->kode1+1;
                    $code = sprintf("%05s", $temporary);
                }
            }else{
                $code = "00001";
            }
            
            $char  = "C";
           
            return $char.$code;
        }

    public function get_kota()
    {
        $query = $this->db->order_by('id', 'ASC')->get('kota');
        if($query->num_rows() > 0){
        $dropdown[''] = 'Pilih Salah Satu';
        foreach ($query->result() as $row)
        {
            $dropdown[$row->id] = $row->kota;
        }
        }else{
            $dropdown[''] = 'Belum Ada Daftar Kota / Kabupaten Tersedia';
        }
        return $dropdown;
    }


     public function get_kecamatan($id=null)
    {
        $this->db->where('kota_id', $id);
        $query = $this->db->order_by('id', 'ASC')->get('kecamatan');
        if($query->num_rows() > 0){
        $dropdown[''] = 'Pilih Salah Satu';
        foreach ($query->result() as $row)
        {
            $dropdown[$row->id] = $row->kecamatan;
        } 
        }else{
            $dropdown[''] = 'Belum Ada Daftar Kecamatan Tersedia';
        }
        return $dropdown;
    }

    public function get_kelurahan($id=null)
    {
        $this->db->where('kecamatan_id', $id);
        $query = $this->db->order_by('id', 'ASC')->get('kelurahan');
        if($query->num_rows() > 0){
        $dropdown[''] = 'Pilih Salah Satu';
        foreach ($query->result() as $row)
        {
            $dropdown[$row->id] = $row->kelurahan;
        } 
        }else{
            $dropdown[''] = 'Belum Ada Daftar Kelurahan Tersedia';
        }
        return $dropdown;
    }

    public function get_jenis_cabor()
    {
        $query = $this->db->order_by('id', 'ASC')->get_where('jenis_cabor',array('deleted_at'=>null));
        if($query->num_rows() > 0){
        //$dropdown[''] = 'Pilih Cabang Olahraga';
        foreach ($query->result() as $row)
        {
            $dropdown[$row->id] = $row->cabor;
        }
        }else{
            $dropdown[''] = 'Belum Ada Daftar Cabang Olahraga Tersedia';
        }
        return $dropdown;
    }

    public function insert_data($cabor = NULL, $user = NULL){
        $this->db->trans_start(); # Starting Transaction
        //$this->db->trans_strict(FALSE); 
        $this->db->insert('cabor', $cabor); # Inserting data
        $cabor_id = $this->db->insert_id();
        $this->db->insert('users', $user); # Inserting data
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return FALSE;
        } else {
            $this->db->trans_commit();
            return $cabor_id;
        }
    }

    public function get_cabor_jenis($id=null) {
        $this->db->where('cabor_id', $id);
        $this->db->where('deleted_at', NULL);
        $sql = $this->db->get('cabor_jenis');
        if($sql->num_rows() > 0){
            return $sql->result();
        }else{
            return FALSE;
        }
    }
    
    public function delete_jenis_cabor($id=null) {
		$this->db->where('cabor_id', $id);
        $this->db->update('cabor_jenis', array('deleted_at'=>date('Y-m-d H:i:s')));
        return $this->db->affected_rows();        
	}
}