<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cabor extends CI_Controller {

	/**
	 * code by rifqie rusyadi
	 * email rifqie.rusyadi@gmail.com
	 */
	
	public $folder = 'master/cabor/';
	
	public function __construct()
	{
		parent::__construct();
		$this->load->model('cabor_m', 'data');
		$this->load->helper('my_helper');
		signin();
		//group(array('1'));
	}
	
	//halaman index
	public function index()
	{
		$data['head'] 		= 'Registrasi Pengurus Provinsi';
		$data['record'] 	= FALSE;
		$data['content'] 	= $this->folder.'default';
		$data['style'] 		= $this->folder.'style';
		$data['js'] 		= $this->folder.'js';
		
		$this->load->view('template/default', $data);
	}
	
	public function created()
	{
		$data['head'] 		= 'Tambah Registrasi Pengurus Provinsi';
        $data['record']     = $this->data->get_new();
		$data['kode'] 	    = $this->data->get_kode();
        $data['kota']       = $this->data->get_kota();
        $data['jenis_cabor']= $this->data->get_jenis_cabor();
		$data['content'] 	= $this->folder.'form';
		$data['style'] 		= $this->folder.'style';
        $data['js'] 		= $this->folder.'js';
        
        //$data['blok'] 	    = $this->data->get_blok();
		
		$this->load->view('template/default', $data);
	}
	
	public function updated($id)
	{
		$data['head'] 		= 'Ubah Registrasi Pengurus Provinsi';
		$data['record'] 	= $this->data->get_id($id);
        $data['kota']       = $this->data->get_kota();
        $data['jenis_cabor']= $this->data->get_jenis_cabor();
		$data['content'] 	= $this->folder.'form_edit';
		$data['style'] 		= $this->folder.'style';
        $data['js'] 		= $this->folder.'js';

        $data['cabor_jenis']= $this->data->get_cabor_jenis($id);
        
        //$data['blok'] 	    = $this->data->get_blok();
		
		$this->load->view('template/default', $data);
	}
	
	public function ajax_list()
    {
        $record	= $this->data->get_datatables();
        $data 	= array();
        $no 	= $_POST['start'];
		
        foreach ($record as $row) {
            $no++;
            $col = array();
            $col[] = '<input type="checkbox" class="data-check" value="'.$row->id.'">';
            $col[] = $row->kode;
            $col[] = $row->cabor;
            $col[] = jenis_cabor($row->id) ? jenis_cabor($row->id) : 'NA';
            $col[] = $row->alamat;
            $col[] = $row->telpon;
            
            //add html for action
            $col[] = '<a class="btn btn-xs btn-flat btn-warning" onclick="edit_data();" href="'.site_url('master/cabor/updated/'.$row->id).'" data-toggle="tooltip" title="Edit"><i class="glyphicon glyphicon-pencil"></i></a>
                  <a class="btn btn-xs btn-flat btn-danger" data-toggle="tooltip" title="Hapus" onclick="deleted('."'".$row->id."'".')"><i class="glyphicon glyphicon-trash"></i></a>';
 
            $data[] = $col;
        }
 
        $output = array(
                        "draw" => $_POST['draw'],
                        "recordsTotal" => $this->data->count_all(),
                        "recordsFiltered" => $this->data->count_filtered(),
                        "data" => $data,
                );
        
		echo json_encode($output);
    }
	
	public function save_ajax()
    {
        if($this->validation()){
            $cabor = array(
                'kode' => $this->data->get_kode(),
                'cabor' => $this->input->post('cabor', TRUE),
                'nickname' => $this->input->post('nickname', TRUE),
                //'npsn' => $this->input->post('npsn', TRUE),
                //'bentuk' => $this->input->post('bentuk', TRUE),
                'status' => $this->input->post('status', TRUE),
                'kota_id' => $this->input->post('kota', TRUE),
                'kecamatan_id' => $this->input->post('kecamatan', TRUE),
                'kelurahan_id' => $this->input->post('kelurahan', TRUE),
                'telpon' => $this->input->post('telpon', TRUE),
                'alamat' => $this->input->post('alamat', TRUE),
                'created_at' => date('Y-m-d H:i:s')
            );
            // $insert = $this->data->insert($sekolah);
            $user = array(
                'kode_cabor' => $this->input->post('kode_cabor', TRUE),
                'fullname' => $this->input->post('cabor', TRUE),
                'email' => $this->input->post('email', TRUE),
                'password' => password_hash($this->input->post('password'), PASSWORD_BCRYPT),
                'telpon' => $this->input->post('telpon', TRUE),
                'level' => 3,
                //'bentuk' => $this->input->post('bentuk', TRUE),
                'active' => 1,
                'created_at' => date('Y-m-d H:i:s')
            );

            $insert = $this->data->insert_data($cabor, $user);
            if($insert){
                $jenis_cabor = $this->input->post('jenis_cabor');
                $result = array();
                foreach($jenis_cabor AS $key => $val){
                    if($_POST['jenis_cabor'][$key] != ''){
                        $result[] = array(
                            "cabor_id"  => $insert,
                            "jenis_cabor_id"  => $_POST['jenis_cabor'][$key]
                        );
                    }
                }
                $this->db->insert_batch('cabor_jenis', $result);
                
                helper_log("add", "Registrasi Cabor");
                $this->session->set_flashdata('flashconfirm','Proses Pendaftaran Berhasil, Mohon Cek Email Secara Berkala Jika Sudah Kami Aktivasi.');
                //$this->send_mail($user['fullname'], $user['email'], $user['password']);
                redirect('master/cabor');
            }else{
                $this->session->set_flashdata('flasherror','Proses Pendaftaran Gagal, Mohon Dicoba Kembali.');
                redirect('master/cabor');
            }
            
        }else{
            $this->index();
        }
    }
    
    public function ajax_update($id)
    {
        //$perusahaan_id = $this->db->get_where('users', array('id'=>$this->session->userdata('userID')))->row();
        $data = array(
                //'kode' => $this->data->get_kode(),
                'cabor' => $this->input->post('cabor', TRUE),
                'nickname' => $this->input->post('nickname', TRUE),
                //'npsn' => $this->input->post('npsn', TRUE),
                //'bentuk' => $this->input->post('bentuk', TRUE),
                //'status' => $this->input->post('status', TRUE),
                'kota_id' => $this->input->post('kota', TRUE),
                'kecamatan_id' => $this->input->post('kecamatan', TRUE),
                'kelurahan_id' => $this->input->post('kelurahan', TRUE),
                'telpon' => $this->input->post('telpon', TRUE),
                'alamat' => $this->input->post('alamat', TRUE),
                'updated_at' => date('Y-m-d H:i:s')
            );
		
        if($this->validation($id)){
            $update = $this->data->update($data, $id);
            if($update){
                $this->data->delete_jenis_cabor($id);
                
                $jenis_cabor = $this->input->post('jenis_cabor');
                $result = array();
                foreach($jenis_cabor AS $key => $val){
                    if($_POST['jenis_cabor'][$key] != ''){
                        $result[] = array(
                            "cabor_id"  => $id,
                            "jenis_cabor_id"  => $_POST['jenis_cabor'][$key]
                        );
                    }
                }
                $this->db->insert_batch('cabor_jenis', $result);
            }
			helper_log("edit", "Merubah Data cabor");
        }
    }
    
    public function ajax_delete($id)
    {
        $this->data->delete($id);
		helper_log("trash", "Menghapus Data cabor");
        echo json_encode(array("cabor" => TRUE));
    }
    
    public function ajax_delete_all()
    {
        $list_id = $this->input->post('id');
        foreach ($list_id as $id) {
            $this->data->delete($id);
			helper_log("trash", "Menghapus Data cabor");
        }
        echo json_encode(array("status" => TRUE));
    }
	
	private function validation($id=null)
    {
        $data = array('success' => false, 'messages' => array());
        
       //$this->form_validation->set_rules("kode", "Kode Standart cabor", "trim|required");
        $this->form_validation->set_rules("cabor", "Nama cabor", "trim|required");
		
		$this->form_validation->set_error_delimiters('<p class="text-danger">', '</p>');
        
        if($this->form_validation->run()){
            $data['success'] = true;
        }else{
            foreach ($_POST as $key => $value) {
                $data['messages'][$key] = form_error($key);
            }
        }
        echo json_encode($data);
        return $this->form_validation->run();
    }

    

     public function get_kecamatan(){
        $record = $this->data->get_id($this->uri->segment(4));
        $id = $this->input->post('id');
        $kecamatan = $this->data->get_kecamatan($id);
        if(!empty($kecamatan)){
            //$selected = (set_value('parent')) ? set_value('parent') : '';
            $selected = set_value('kecamatan', $record ? $record->kecamatan_id : '');
            echo form_dropdown('kecamatan', $kecamatan, $selected, "class='form-control select2' name='kecamatan' id='kecamatan'");
        }else{
            echo form_dropdown('kecamatan', array(''=>'Pilih Salah Satu'), '', "class='form-control select2' name='kecamatan' id='kecamatan'");
        }
    }

    public function get_kelurahan(){
        $record = $this->data->get_id($this->uri->segment(4));
        $id = $this->input->post('id');
        $kelurahan = $this->data->get_kelurahan($id);
        if(!empty($kelurahan)){
            //$selected = (set_value('parent')) ? set_value('parent') : '';
            $selected = set_value('kelurahan', $record ? $record->kelurahan_id : '');
            echo form_dropdown('kelurahan', $kelurahan, $selected, "class='form-control select2' name='kelurahan' id='kelurahan'");
        }else{
            echo form_dropdown('kelurahan', array(''=>'Pilih Salah Satu'), '', "class='form-control select2' name='kelurahan' id='kelurahan'");
        }
    }
}
