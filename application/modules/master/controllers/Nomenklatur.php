<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Nomenklatur extends CI_Controller {

	/**
	 * code by rifqie rusyadi
	 * email rifqie.rusyadi@gmail.com
	 */
	
	public $folder = 'master/nomenklatur/';
	
	public function __construct()
	{
		parent::__construct();
		$this->load->model('nomenklatur_m', 'data');
		$this->load->helper('my_helper');
		signin();
		//group(array('1'));
	}
	
	//halaman index
	public function index()
	{
		$data['head'] 		= 'Data Nomenklatur';
		$data['record'] 	= FALSE;
		$data['content'] 	= $this->folder.'default';
		$data['style'] 		= $this->folder.'style';
		$data['js'] 		= $this->folder.'js';
		
		$this->load->view('template/default', $data);
	}
	
	public function created()
	{
		$data['head'] 		= 'Tambah Data Nomenklatur';
        $data['record']     = $this->data->get_new();
        $data['program']    = $this->data->get_program();
        $data['kegiatan']    = $this->data->get_kegiatan();
        $data['kategori']    = $this->data->get_kategori();
		$data['belanja'] 	= $this->data->get_belanja();
		$data['content'] 	= $this->folder.'form';
		$data['style'] 		= $this->folder.'style';
        $data['js'] 		= $this->folder.'js';
        
        $data['blok'] 	    = $this->data->get_blok();
		
		$this->load->view('template/default', $data);
	}
	
	public function updated($id)
	{
		$data['head'] 		= 'Ubah Data nomenklatur';
		$data['record'] 	= $this->data->get_id($id);
       $data['program']    = $this->data->get_program();
        $data['kegiatan']    = $this->data->get_kegiatan();
        $data['kategori']    = $this->data->get_kategori();
        $data['belanja']    = $this->data->get_belanja();
		$data['content'] 	= $this->folder.'form';
		$data['style'] 		= $this->folder.'style';
        $data['js'] 		= $this->folder.'js';
        
        $data['blok'] 	    = $this->data->get_blok();
		
		$this->load->view('template/default', $data);
	}
	
	public function ajax_list()
    {
        $record	= $this->data->get_datatables();
        $data 	= array();
        $no 	= $_POST['start'];
		
        foreach ($record as $row) {
            $no++;
            $col = array();
            $col[] = '<input type="checkbox" class="data-check" value="'.$row->id.'">';
            $col[] = program($row->program_id);
            $col[] = kegiatan($row->kegiatan_id);
            $col[] = kategori($row->kategori_id);
            $col[] = belanja($row->belanja_id);
            
            //add html for action
            $col[] = '<a class="btn btn-xs btn-flat btn-warning" onclick="edit_data();" href="'.site_url('master/nomenklatur/updated/'.$row->id).'" data-toggle="tooltip" title="Edit"><i class="glyphicon glyphicon-pencil"></i></a>
                  <a class="btn btn-xs btn-flat btn-danger" data-toggle="tooltip" title="Hapus" onclick="deleted('."'".$row->id."'".')"><i class="glyphicon glyphicon-trash"></i></a>';
 
            $data[] = $col;
        }
 
        $output = array(
                        "draw" => $_POST['draw'],
                        "recordsTotal" => $this->data->count_all(),
                        "recordsFiltered" => $this->data->count_filtered(),
                        "data" => $data,
                );
        
		echo json_encode($output);
    }
	
	public function ajax_save()
    {
        $data = array(
            'program_id' => $this->input->post('program_id'),    
            'kegiatan_id' => $this->input->post('kegiatan_id'),    
            'kategori_id' => $this->input->post('kategori_id'),    
            'belanja_id' => $this->input->post('belanja_id'),    
            );
        
        if($this->validation()){
            $insert = $this->data->insert($data);
			helper_log("add", "Menambah Data nomenklatur");
        }
    }
    
    public function ajax_update($id)
    {
        
        $data = array(
            'program_id' => $this->input->post('program_id'),    
            'kegiatan_id' => $this->input->post('kegiatan_id'),    
            'kategori_id' => $this->input->post('kategori_id'),    
            'belanja_id' => $this->input->post('belanja_id'),   
        );
		
        if($this->validation($id)){
            $this->data->update($data, $id);
			helper_log("edit", "Merubah Data nomenklatur");
        }
    }
    
    public function ajax_delete($id)
    {
        $this->data->delete($id);
		helper_log("trash", "Menghapus Data nomenklatur");
        echo json_encode(array("nomenklatur" => TRUE));
    }
    
    public function ajax_delete_all()
    {
        $list_id = $this->input->post('id');
        foreach ($list_id as $id) {
            $this->data->delete($id);
			helper_log("trash", "Menghapus Data nomenklatur");
        }
        echo json_encode(array("status" => TRUE));
    }
	
	private function validation($id=null)
    {
        $data = array('success' => false, 'messages' => array());
        
        $this->form_validation->set_rules("program_id", "Program", "trim|required");
        $this->form_validation->set_rules("belanja_id", "Belanja", "trim|required");
        $this->form_validation->set_rules("kegiatan_id", "Kagiatan", "trim|required");
        $this->form_validation->set_rules("kategori_id", "Kategori", "trim|required");
		
		$this->form_validation->set_error_delimiters('<p class="text-danger">', '</p>');
        
        if($this->form_validation->run()){
            $data['success'] = true;
        }else{
            foreach ($_POST as $key => $value) {
                $data['messages'][$key] = form_error($key);
            }
        }
        echo json_encode($data);
        return $this->form_validation->run();
    }
}
