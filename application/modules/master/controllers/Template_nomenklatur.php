<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class template_nomenklatur extends CI_Controller {

	/**
	 * code by rifqie rusyadi
	 * email rifqie.rusyadi@gmail.com
	 */
	
	public $folder = 'master/template_nomenklatur/';
	
	public function __construct()
	{
		parent::__construct();
		$this->load->model('template_nomenklatur_m', 'data');
		$this->load->helper('my_helper');
		signin();
		//group(array('1'));
	}
	
	//halaman index
	public function index()
	{
		$data['head'] 		= 'Data Template Nomenklatur';
		$data['record'] 	= FALSE;
		$data['content'] 	= $this->folder.'default';
		$data['style'] 		= $this->folder.'style';
		$data['js'] 		= $this->folder.'js';
		
		$this->load->view('template/default', $data);
	}
	
	public function created()
	{
		$data['head'] 		= 'Tambah Data Template Nomenklatur';
        $data['record']     = $this->data->get_new();
		$data['content'] 	= $this->folder.'form';
		$data['style'] 		= $this->folder.'style';
        $data['js'] 		= $this->folder.'js';
        $data['blok'] 	    = $this->data->get_blok();
		
		$this->load->view('template/default', $data);
	}

    public function template($id,$id2)
    {
       
        $data['head']       = 'Template Nomenklatur';
        $data['record']     = $this->data->get_id($id);
        $data['content']    = $this->folder.'template';
        $data['style']      = $this->folder.'style';
        $data['js']         = $this->folder.'js';
        $data['tahun']     = $id2;
        
        $data['standart']   = $this->data->get_standart();
        
        $this->load->view('template/default', $data);
    }

    public function created_kategori()
    {
        $data['id']         = $this->input->post('id');
        $data['program']       = $this->input->post('program');
        $data['kegiatan']       = $this->input->post('kegiatan');
        $data['tahun']       = $this->input->post('tahun');
        $data['record']     = $this->data->get_new();
        $data['kategori']   = $this->data->get_kategori();
        $data['belanja']   = $this->data->get_belanja();
        //$data['satuan']     = $this->data->get_satuan();
        $data['link']       = 'ajax_save_kategori';

        $this->load->view('master/template_nomenklatur/form_kategori', $data);
    }

    public function ajax_save_kategori()
    {
            $data = array(
                'template_id' => $this->input->post('id'),
                'program_id' => $this->input->post('program_id'),
                //'bos' => $this->input->post('bos'),
                'kegiatan_id' => $this->input->post('kegiatan_id'),
                'kategori_id' => $this->input->post('kategori'),
                'belanja_id' => $this->input->post('belanja'),
                'tahun' => $this->input->post('tahun'),
                //'rekening' => $this->input->post('rekening'),
                //'uraian' => $this->input->post('uraian'),
                'created_at' => date('Y-m-d H:i:s')
            );
            
            if($this->validation()){
                $insert = $this->db->insert('template_nomenklatur', $data);
                helper_log("add", "Menambah Template Nomenklatur");
                $this->session->set_flashdata('flashconfirm','Berhasil di tambah');
                redirect('master/template_nomenklatur/template/'.$this->input->post('id').'/'.$this->input->post('tahun'));
            }else{
                $this->session->set_flashdata('flasherror','Gagal Menambah kan');
                redirect('master/template_nomenklatur/template/'.$this->input->post('id').'/'.$this->input->post('tahun'));
            }
    }

    public function ajax_delete_data($id)
    {
        $data = array(
            'deleted_at' => date('Y-m-d H:i:s')
        );
        
      
            $this->data->update_data($data, $id);
            helper_log("trash", "Menghapus Data Nomenklatur");
            $this->session->set_flashdata('flashconfirm','Berhasil Menghapus');
            echo json_encode(array("success" => TRUE));
       
    }
	
	
	public function ajax_list()
    {
        $record	= $this->data->get_datatables();
        $data 	= array();
        $no 	= $_POST['start'];
		
        foreach ($record as $row) {
            $no++;
            $col = array();
            $col[] = '<input type="checkbox" class="data-check" value="'.$row->id.'">';
            $col[] = $row->tahun;
            $col[] = $row->keterangan ? $row->keterangan : '-';
            
            //add html for action
            $col[] = '<a class="btn btn-xs btn-flat btn-default" onclick="edit_data();" href="'.site_url('master/template_nomenklatur/template/'.$row->id.'/'.$row->tahun).'" data-toggle="tooltip" title="Template"><i class="fa fa-list"></i></a> 
                  <a class="btn btn-xs btn-flat btn-danger" data-toggle="tooltip" title="Hapus" onclick="deleted('."'".$row->id."'".')"><i class="glyphicon glyphicon-trash"></i></a>';
 
            $data[] = $col;
        }
 
        $output = array(
                        "draw" => $_POST['draw'],
                        "recordsTotal" => $this->data->count_all(),
                        "recordsFiltered" => $this->data->count_filtered(),
                        "data" => $data,
                );
        
		echo json_encode($output);
    }
	
	public function ajax_save()
    {
        $data = array(
            'tahun' => $this->input->post('tahun'),    
            'keterangan' => $this->input->post('keterangan'),    
            );     
        
        if($this->validation()){
            if(cek_tahun_template($this->input->post('tahun'))) {
            echo json_encode(array("success" => FALSE));
        } else {
            $insert = $this->data->insert($data);
			helper_log("add", "Menambah Data template_nomenklatur");
        }

        }
    }
    
   /* public function ajax_update($id)
    {
        
        $data = array(
           'tahun' => $this->input->post('tahun'),    
            'keterangan' => $this->input->post('keterangan'),    
        );
		
        if($this->validation($id)){
            $this->data->update($data, $id);
			helper_log("edit", "Merubah Data template_nomenklatur");
        }
    }*/
    
    public function ajax_delete($id)
    {
        $this->data->delete($id);
		helper_log("trash", "Menghapus Data template_nomenklatur");
        echo json_encode(array("success" => TRUE));
    }
    
    public function ajax_delete_all()
    {
        $list_id = $this->input->post('id');
        foreach ($list_id as $id) {
            $this->data->delete($id);
			helper_log("trash", "Menghapus Data template_nomenklatur");
        }
        echo json_encode(array("status" => TRUE));
    }
	
	private function validation($id=null)
    {
        $data = array('success' => false, 'messages' => array());
        
        $this->form_validation->set_rules("tahun", "Tahun", "trim|required");
		$this->form_validation->set_error_delimiters('<p class="text-danger">', '</p>');
        
        if($this->form_validation->run()){
            $data['success'] = true;
        }else{
            foreach ($_POST as $key => $value) {
                $data['messages'][$key] = form_error($key);
            }
        }
        echo json_encode($data);
        return $this->form_validation->run();
    }
}
