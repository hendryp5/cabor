<div class="row">
	<div class="col-md-12">
		<div id="message"></div>
		<div class="box box-primary box-solid">
			<div class="box-header with-border">
				<h3 class="box-title"><?= isset($head) ? $head : ''; ?></h3>
				<div class="box-tools pull-right">
					<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
					<button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
				</div>
			</div>
			<form id="formID" role="form" action="<?= site_url('master/cabor/save_ajax') ?>" method="post">
			<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
			<!-- box-body -->
			<div class="box-body">
				<div class="row">
                <div class="col-md-12">
                    <div class="form-group <?php echo form_error('kode_cabor') ? 'has-error' : null; ?>">
                        <?php
                        echo form_label('Kode Registrasi','kode_cabor');
                        $data = array('readonly class'=>'form-control','name'=>'kode_cabor','id'=>'kode_cabor','type'=>'text','value'=>set_value('kode_cabor',$kode));
                        echo form_input($data);
                        echo form_error('kode_cabor') ? form_error('kode_cabor', '<p class="help-block">','</p>') : '';
                        ?>
                    </div>
                </div>
				
				<div class="col-md-12">
                    <div class="form-group <?php echo form_error('cabor') ? 'has-error' : null; ?>">
                        <?php
                        echo form_label('Nama Pengprov','cabor');
                        $data = array('required class'=>'form-control','name'=>'cabor','id'=>'cabor','type'=>'text','value'=>set_value('cabor'));
                        echo form_input($data);
                        echo form_error('cabor') ? form_error('cabor', '<p class="help-block">','</p>') : 'Contoh : Asosiasi Bola Tangan Indonesia';
                        ?>
                    </div>
                </div>

                <div class="col-md-12">
                    <div class="form-group <?php echo form_error('nickname') ? 'has-error' : null; ?>">
                        <?php
                        echo form_label('Kode Pengprov','nickname');
                        $data = array('required class'=>'form-control','name'=>'nickname','id'=>'nickname','type'=>'text','value'=>set_value('nickname'));
                        echo form_input($data);
                        echo form_error('nickname') ? form_error('nickname', '<p class="help-block">','</p>') : 'Contoh : ABTI';
                        ?>
                    </div>
                </div>

                <div class="col-md-12">
                    <div class="form-group <?php echo form_error('jenis_cabor') ? 'has-error' : null; ?>">
                        <?php 
                        echo form_label('Cabang Olahraga','jenis_cabor');
                        $selected = set_value('jenis_cabor[]');
                        echo form_dropdown('jenis_cabor[]', $jenis_cabor, $selected, "required multiple class='form-control select2' data-placeholder='Pilih Cabang Olahraga' style='width: 100%;' ");
                        echo form_error('jenis_cabor[]') ? form_error('jenis_cabor[]', '<p class="help-block">','</p>') : 'Pilih Cabang Olahraga. Bisa Lebih Dari Satu Pilihan';
                        ?>
                    </div>
                </div>

                <div class="col-md-12">
                    <div class="form-group <?php echo form_error('alamat') ? 'has-error' : null; ?>">
                        <?php
                        echo form_label('Alamat Sekertariat','alamat');
                        $data = array('required class'=>'form-control','name'=>'alamat','rows'=>'2','cols'=>'4','id'=>'alamat','type'=>'text','value'=>set_value('alamat'));
                        echo form_textarea($data);
                        echo form_error('alamat') ? form_error('alamat', '<p class="help-block">','</p>') : '';
                        ?>
                    </div>
                </div>

                <div class="col-md-12">
                    <div class="form-group <?php echo form_error('kota') ? 'has-error' : null; ?>">
                        <?php 
                        echo form_label('Kota / Kabupaten','kota');
                        $selected = set_value('kota');
                        echo form_dropdown('kota', $kota, $selected, "required class='form-control select2' name='kota' id='kota'");
                        echo form_error('kota') ? form_error('kota', '<p class="help-block">','</p>') : 'Pilih Kota/Kabupaten';
                        ?>
                    </div>
                </div>

                <div class="col-md-12">
                    <div class="form-group <?php echo form_error('kecamatan') ? 'has-error' : null; ?>">
                        <?php 
                        echo form_label('Kecamatan','kecamatan');
                        $selected = set_value('kecamatan');
                        $kecamatan = array(''=>'Pilih Salah Satu');
                        echo form_dropdown('kecamatan', $kecamatan, $selected, "required class='form-control select2' name='kecamatan' id='kecamatan'");
                        echo form_error('kecamatan') ? form_error('kecamatan', '<p class="help-block">','</p>') : 'Pilih Kecamatan';
                        ?>
                    </div>
                </div>

                <div class="col-md-12">
                    <div class="form-group <?php echo form_error('kelurahan') ? 'has-error' : null; ?>">
                        <?php 
                        echo form_label('Kelurahan','kelurahan');
                        $selected = set_value('kelurahan');
                        $kelurahan = array(''=>'Pilih Salah Satu');
                        echo form_dropdown('kelurahan', $kelurahan, $selected, "required class='form-control select2' name='kelurahan' id='kelurahan'");
                        echo form_error('kelurahan') ? form_error('kelurahan', '<p class="help-block">','</p>') : 'Pilih Kelurahan';
                        ?>
                    </div>
                </div>

                <div class="col-md-12">
                    <div class="form-group <?php echo form_error('telpon') ? 'has-error' : null; ?>">
                        <?php
                        echo form_label('Telpon/HP','telpon');
                        $data = array('required class'=>'form-control','name'=>'telpon','id'=>'telpon','type'=>'text','value'=>set_value('telpon'));
                        echo form_input($data);
                        echo form_error('telpon') ? form_error('telpon', '<p class="help-block">','</p>') : 'Masukan Nomor Telpon/HP';
                        ?>
                    </div>
                </div>

        <div class="col-md-12"><h4>Data Login Sistem Informasi</h4></div>
        <div class="col-md-12">
            <div class="form-group <?php echo form_error('email') ? 'has-error' : null; ?>">
                <?php
                echo form_label('Email','email');
                $data = array('required class'=>'form-control','name'=>'email','id'=>'email','type'=>'email','value'=>set_value('email'));
                echo form_input($data);
                echo form_error('email') ? form_error('email', '<p class="help-block">','</p>') : 'Masukan Alamat Email Aktif Untuk Kebutuhan Login Sistem. Disarankan Menggunakan Email Pengprov.';
                ?>
            </div>
        </div>
        <div class="col-md-12">
            <div class="form-group <?php echo form_error('password') ? 'has-error' : null; ?>">
                <?php
                echo form_label('Password','password');
                $data = array('required class'=>'form-control','name'=>'password','id'=>'password','type'=>'password','value'=>set_value('password'));
                echo form_input($data);
                echo form_error('password') ? form_error('password', '<p class="help-block">','</p>') : 'Masukan Minimal 8 Digit Password';
                ?>
            </div>
        </div>

        <div class="col-md-12">
            <div class="form-group <?php echo form_error('repassword') ? 'has-error' : null; ?>">
                <?php
                echo form_label('Re Password','repassword');
                $data = array('required class'=>'form-control','name'=>'repassword','id'=>'repassword','type'=>'password','value'=>set_value('repassword'));
                echo form_input($data);
                echo form_error('repassword') ? form_error('repassword', '<p class="help-block">','</p>') : 'Ulangi Password Diatas';
                ?>
            </div>
        </div>
				</div>
			</div>
			<!-- ./box-body -->
			<div class="box-footer">
				<button type="submit" class="btn btn-sm btn-flat btn-info"><i class="fa fa-save"></i> Simpan & Keluar</button>
				<button type="reset" class="btn btn-sm btn-flat btn-warning"><i class="fa fa-refresh"></i> Reset</button>
				<button type="button" class="btn btn-sm btn-flat btn-danger" onclick="back();"><i class="fa fa-close"></i> Keluar</button>
			</div>
			</form>
		</div>
	</div>
</div>