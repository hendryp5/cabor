<script src="<?= base_url('asset/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js'); ?>"></script>
<script>
$(function () {
	$('.select2').select2();
	//$('#informasi').wysihtml5();

	$("#kota").change(function(){
		var kota = $("#kota").val();
		$.ajax({
				type: "POST",
				async: false,
				url : "<?php echo site_url('master/cabor/get_kecamatan')?>",
				data: {
				'id': kota,
				'<?php echo $this->security->get_csrf_token_name(); ?>' : '<?php echo $this->security->get_csrf_hash(); ?>'
				},
				success: function(data){
					$('#kecamatan').html(data);
				}
		});
	});

	$("#kotax").ready(function(){
		var kotax = $("#kotax").val();
		$.ajax({
				type: "POST",
				async: false,
				url : "<?php echo site_url('master/cabor/get_kecamatan/'.$this->uri->segment(4))?>",
				data: {
				'id': kotax,
				'<?php echo $this->security->get_csrf_token_name(); ?>' : '<?php echo $this->security->get_csrf_hash(); ?>'
				},
				success: function(data){
					$('#kecamatan').html(data);
				}
		});
	});


	$("#kecamatan").change(function(){
		var kecamatan = $("#kecamatan").val();
		$.ajax({
				type: "POST",
				async: false,
				url : "<?php echo site_url('master/cabor/get_kelurahan')?>",
				data: {
				'id': kecamatan,
				'<?php echo $this->security->get_csrf_token_name(); ?>' : '<?php echo $this->security->get_csrf_hash(); ?>'
				},
				success: function(data){
					$('#kelurahan').html(data);
				}
		});
	});

	$("#kecamatanx").ready(function(){
		var kecamatan = $("#kecamatan").val();
		$.ajax({
				type: "POST",
				async: false,
				url : "<?php echo site_url('master/cabor/get_kelurahan/'.$this->uri->segment(4))?>",
				data: {
				'id': kecamatan,
				'<?php echo $this->security->get_csrf_token_name(); ?>' : '<?php echo $this->security->get_csrf_hash(); ?>'
				},
				success: function(data){
					$('#kelurahan').html(data);
				}
		});
	});

	// $('#tanggal').datepicker({
	// 	format:'dd-mm-yyyy'
	// });
	//$('#tangal').inputmask('dd-mm-yyyy');
});
</script>