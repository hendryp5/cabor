<form id="formID" role="form" action="<?= site_url('master/template_nomenklatur/'.$link); ?>" method="post">
<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
<input type="hidden" name="id" value="<?= $id; ?>" />
<input type="hidden" name="program_id" value="<?= $program; ?>" />
<input type="hidden" name="kegiatan_id" value="<?= $kegiatan; ?>" />
<input type="hidden" name="tahun" value="<?= $tahun; ?>" />

<!-- box-body -->
<div class="box-body">
	<div class="row">
		<div class="col-md-12">
			<dl>
				<dt>Program</dt>
				<dd><?= $program ? kode_program($program).'-'.program($program) : ''; ?></dd>
			</dl>

			<dl>
				<dt>Kegiatan</dt>
				<dd><?= $kegiatan ? kegiatan($kegiatan) : ''; ?></dd>
			</dl>
		</div>
	
		<div class="col-md-12">
			<div class="form-group <?php echo form_error('kategori') ? 'has-error' : null; ?>">
				<?php 
				echo form_label('Kategori','kategori');
				$selected = set_value('kategori', $record->kategori_id);
				//$kategori = array(''=>'Pilih Salah Satu','5.2.1'=>'5.2.1 - kegiatan Pegawai','5.2.2'=>'5.2.2 - kegiatan Barang dan Jasa','5.2.3'=>'5.2.3 - kegiatan Modal');
				echo form_dropdown('kategori', $kategori, $selected, "required class='form-control select2' name='kategori' id='kategori' style='width: 100% !important;
				padding: 0;'");
				echo form_error('kategori') ? form_error('kategori', '<p class="help-block">','</p>') : '';
				?>
			</div>
		</div>

		<div class="col-md-12">
			<div class="form-group <?php echo form_error('belanja') ? 'has-error' : null; ?>">
				<?php 
				echo form_label('Belanja','belanja');
				$selected = set_value('belanja', $record->belanja_id);
				//$kategori = array(''=>'Pilih Salah Satu','5.2.1'=>'5.2.1 - kegiatan Pegawai','5.2.2'=>'5.2.2 - kegiatan Barang dan Jasa','5.2.3'=>'5.2.3 - kegiatan Modal');
				echo form_dropdown('belanja', $belanja, $selected, "required class='form-control select2' name='belanja' id='belanja' style='width: 100% !important;
				padding: 0;'");
				echo form_error('belanja') ? form_error('belanja', '<p class="help-block">','</p>') : '';
				?>
			</div>
		</div>


		
	</div>
</div>
<!-- ./box-body -->
<div class="box-footer">
<button type="submit" class="btn btn-sm btn-flat btn-success" ><i class="fa fa-save"></i> Simpan</button>
<button type="button" class="btn btn-sm btn-flat btn-danger" data-dismiss="modal"><i class="fa fa-close"></i> Keluar</button>
</div>
</form>
<script>
	$('.select2').select2({ width: 'resolve' });
</script>