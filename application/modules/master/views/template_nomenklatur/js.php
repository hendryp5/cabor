<script src="<?= base_url('asset/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js'); ?>"></script>
<script>
$(function () {
	$('.select2').select2();
	$('#informasi').wysihtml5();
	// $('#tanggal').datepicker({
	// 	format:'dd-mm-yyyy'
	// });
	//$('#tangal').inputmask('dd-mm-yyyy');

	$(document).on('click', '#getKategori', function(e){
		e.preventDefault();
		var uid = $(this).data('id'); // get id of clicked row
		var umodul = $(this).data('modul'); // get id of clicked row
		var uprogram = $(this).data('program'); // get id of clicked row
		var ukegiatan = $(this).data('kegiatan'); // get id of clicked row
		var utahun = $(this).data('tahun'); // get id of clicked row
		$('#kategori-content').html(''); // leave this div blank
		$('#kategori-loader').show(); // load ajax loader on button click
		$.ajax({
			url: '<?= site_url('master/template_nomenklatur/created_kategori'); ?>',
			type: 'POST',
			data: {
					'id': uid,
					'modul' : umodul,
					'program' : uprogram,
					'kegiatan' : ukegiatan,
					'tahun' : utahun,
					'<?php echo $this->security->get_csrf_token_name(); ?>' : '<?php echo $this->security->get_csrf_hash(); ?>'
			},
			dataType: 'html'
		})
		.done(function(data){
			$('#kategori-content').html(''); // blank before load.
			$('#kategori-content').html(data); // load here
			$('#kategori-loader').hide(); // hide loader
		})
		.fail(function(){
			$('#kategori-content').html('<i class="glyphicon glyphicon-info-sign"></i> Something went wrong, Please try again...');
			$('#kategori-loader').hide();
		});
	});

	
});

function delUraian(id) {
  var r = confirm("Anda Yakin Ingin Menghapus Data Ini.");
  if (r == true) {
	console.log("You pressed OK!");
		var process = '<?= site_url('master/template_nomenklatur/ajax_delete_data/'); ?>'+id;
	  // ajax delete data to database
        $.ajax({
            url : process,
            type: "POST",
            data: {'<?php echo $this->security->get_csrf_token_name(); ?>' : '<?php echo $this->security->get_csrf_hash(); ?>'},
            dataType: "JSON",
            success: function(data)
            {
                $('#message').append('<div class="alert alert-danger">' +
                        '<span class="glyphicon glyphicon-ok"></span>' +
                        ' Data berhasil di hapus.' +
                        '</div>');
                       
                // tutup pesan
                $('.alert-danger').delay(250).show(10, function() {
                    $(this).delay(1000).hide(10, function() {
                    $(this).remove();
                    });
                })
                
                location.reload();
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error deleting data');
            }
        });
  } else {
    console.log("You pressed Cancel!");
  }
}
</script>