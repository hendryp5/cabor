<div class="row">
	<div class="col-md-12">
		<div id="message"></div>
		<?php if($this->session->flashdata('flashconfirm')): ?>
		<div class="alert alert-success alert-dismissable">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		<?php echo $this->session->flashdata('flashconfirm'); ?>
		</div>
		<?php endif; ?>
		<?php if($this->session->flashdata('flasherror')): ?>
		<div class="alert alert-warning alert-dismissable">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		<?php echo $this->session->flashdata('flasherror'); ?>
		</div>
		<?php endif; ?>
		<div class="box box-primary box-solid">
			<div class="box-header with-border">
				<h3 class="box-title"><?= isset($head) ? $head : ''; ?></h3>
				<div class="box-tools pull-right">
					<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
					<button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
				</div>
			</div>
			<!-- box-body -->
			<div class="box-body">
				<div class="row">
					<div class="col-md-12">
						<a class="btn btn-sm btn-flat btn-default" href="<?= site_url('master/template_nomenklatur') ?>" ><i class="fa fa-arrow-left"></i> Kembali</a>
						<span id="key" style="display: none;"><?= $this->security->get_csrf_hash(); ?></span>
						<p></p>
						
						<?php  if($standart): ?>
						<table class="table table-bordered">
						<thead>
						<tr class="text-center">
								<th class="text-center">Kode</th>
								<th class="text-center">Uraian</th>
				
								<th class="text-center">#</th>
						</tr>
						</thead>
						<tbody>
						<?php  foreach($standart as $row): ?>
						<tr style="font-weight: 600; background-color: #ddd;">
							<td><?= $row->kode; ?></td>
							<td><?= $row->program; ?></td>
							<td class="text-right"></td>				
							</td>
						</tr>
						<?php 
						//uraian;
						$uraian = $this->db->order_by('id','ASC')->group_by('id')->get_where('kegiatan', array('program_id'=>$row->id,'deleted_at'=>null))->result(); 
						if($uraian):
						//uraian
							foreach($uraian as $baris):
							?>
							<tr style="background-color: #f4f4f4;">
								<td></td>
								<td><b><?= $baris->kegiatan; ?></b></td>					
								<td>
									<button class="btn btn-xs btn-flat btn-success" data-toggle="modal" data-target="#kategori-modal" data-modul="kategori" data-id="<?= $record->id; ?>" data-program="<?= $row->id; ?>" data-kegiatan="<?= $baris->id; ?>" data-tahun="<?= $record->tahun; ?>" id="getKategori"><i class="fa fa-plus"></i></button>
								</td>
							</td>
							</tr>
							<?php 
							//detail
							$detail = $this->db->order_by('kategori_id','ASC')->group_by('kategori_id')->get_where('template_nomenklatur', array('program_id'=>$row->id,'kegiatan_id'=>$baris->id,'tahun'=>$tahun,'deleted_at'=>null))->result(); 
							//detail
						
							foreach($detail as $kolom):
							?>
							
							<tr>
								<td class="text-right"></td>
								
								<td><b>- <?= kategori($kolom->kategori_id); ?></b></td>
								
								<td>
									
								</td>
							</tr>
								
								
							<?php 
							//detail
							
							$belanja = $this->db->order_by('belanja_id','ASC')->get_where('template_nomenklatur', array('kategori_id'=>$kolom->kategori_id,'program_id'=>$row->id,'kegiatan_id'=>$baris->id,'tahun'=>$tahun,'deleted_at'=>null))->result(); 
						//detail
							$no = 0;

							foreach($belanja as $isi):
								$no++;
							?>
							<tr>
								<td class="text-right"></td>
								<td><?= $no; ?> .<?= belanja($isi->belanja_id); ?></td>
								<td>
									
									<button class="btn btn-xs btn-flat btn-danger" onclick="delUraian(<?= $isi->id; ?>)" id="delUraian"><i class="fa fa-trash"></i></button>
								</td>
								
							</td>
							</tr>
							<?php endforeach; //endforeach detail ?>
							<?php endforeach; //endforeach detail ?>
							<?php 
							endforeach; //endforeach uraian
						endif; //endif uraian; 
						?>

						<?php endforeach; ?>
						</tbody>
					
						</table>
						<?php endif; ?>

						
					</div>
				</div>
			</div>
			<!-- ./box-body -->
		</div>
	</div>
</div>

<div id="kategori-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
  <div class="modal-dialog modal-lg"> 
     <div class="modal-content">  
        <div class="modal-header"> 
           <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button> 
           <h4 class="modal-title">
           <i class="fa fa-calendar-o"></i> Kategori dan Belanja
           </h4> 
        </div> 
        <div class="modal-body">                     
           <div id="kategori-loader" style="display: none; text-align: center;">
           <!-- ajax loader -->
           <img src="<?= base_url('asset/loading.gif'); ?>">
           </div>             
           <!-- mysql data will be load here -->                          
           <div id="kategori-content"></div>
        </div>       
    </div> 
  </div>
</div>


