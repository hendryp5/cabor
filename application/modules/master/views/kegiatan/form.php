<div class="row">
	<div class="col-md-12">
		<div id="message"></div>
		<div class="box box-primary box-solid">
			<div class="box-header with-border">
				<h3 class="box-title"><?= isset($head) ? $head : ''; ?></h3>
				<div class="box-tools pull-right">
					<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
					<button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
				</div>
			</div>
			<form id="formID" role="form" action="" method="post">
			<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
			<!-- box-body -->
			<div class="box-body">
				<div class="row">
				

					<div class="col-md-12">
						<div class="form-group <?php echo form_error('kegiatan') ? 'has-error' : null; ?>">
							<?php
							echo form_label('Kegiatan','kegiatan');
							$data = array('class'=>'form-control','name'=>'kegiatan','id'=>'kegiatan','type'=>'text','value'=>set_value('kegiatan', $record->kegiatan));
							echo form_input($data);
							echo form_error('kegiatan') ? form_error('kegiatan', '<p class="help-block">','</p>') : '';
							?>
						</div>
					</div>

					<div class="col-md-12">
		            	<div class="form-group <?php echo form_error('program_id') ? 'has-error' : null; ?>">
			                <?php 
			                echo form_label('Program','program_id');
			                $selected = set_value('program_id',$record->program_id);
			                echo form_dropdown('program_id', $program, $selected, "required class='form-control select2' name='program_id' id='program_id'");
			                echo form_error('program_id') ? form_error('program_id', '<p class="help-block">','</p>') : 'Pilih Program';
			                ?>
		            	</div>
		        	</div>

				</div>
			</div>
			<!-- ./box-body -->
			<div class="box-footer">
				<button type="button" class="btn btn-sm btn-flat btn-info" onclick="saveout();"><i class="fa fa-save"></i> Simpan & Keluar</button>
				<button type="reset" class="btn btn-sm btn-flat btn-warning"><i class="fa fa-refresh"></i> Reset</button>
				<button type="button" class="btn btn-sm btn-flat btn-danger" onclick="back();"><i class="fa fa-close"></i> Keluar</button>
			</div>
			</form>
		</div>
	</div>
</div>