<div class="row">
	<div class="col-md-12">
		<div id="message"></div>
		<div class="box box-primary box-solid">
			<div class="box-header with-border">
				<h3 class="box-title"><?= isset($head) ? $head : ''; ?></h3>
				<div class="box-tools pull-right">
					<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
					<button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
				</div>
			</div>
			<form id="formID" role="form" action="" method="post">
			<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
			<!-- box-body -->
			<div class="box-body">
				<div class="row">

				 <div class="col-md-12">
		            <div class="form-group <?php echo form_error('program_id') ? 'has-error' : null; ?>">
		                <?php 
		                echo form_label('Program','program_id');
		                $selected = set_value('program_id',$record->program_id);
		                echo form_dropdown('program_id', $program, $selected, "required class='form-control select2' name='program_id' id='program_id'");
		                echo form_error('program_id') ? form_error('program_id', '<p class="help-block">','</p>') : 'Pilih program_id Lokasi cabor';
		                ?>
		            </div>
		        </div>

				 <div class="col-md-12">
		            <div class="form-group <?php echo form_error('kegiatan_id') ? 'has-error' : null; ?>">
		                <?php 
		                echo form_label('Kegiatan','kegiatan_id');
		                $selected = set_value('kegiatan_id',$record->kegiatan_id);
		                echo form_dropdown('kegiatan_id', $kegiatan, $selected, "required class='form-control select2' name='kegiatan_id' id='kegiatan_id'");
		                echo form_error('kegiatan_id') ? form_error('kegiatan_id', '<p class="help-block">','</p>') : 'Pilih kegiatan_id Lokasi cabor';
		                ?>
		            </div>
		        </div>


		         <div class="col-md-12">
		            <div class="form-group <?php echo form_error('kategori_id') ? 'has-error' : null; ?>">
		                <?php 
		                echo form_label('Kategori','kategori_id');
		                $selected = set_value('kategori_id',$record->kategori_id);
		                echo form_dropdown('kategori_id', $kategori, $selected, "required class='form-control select2' name='kategori_id' id='kategori_id'");
		                echo form_error('kategori_id') ? form_error('kategori_id', '<p class="help-block">','</p>') : 'Pilih kategori_id Lokasi cabor';
		                ?>
		            </div>
		        </div>

		         <div class="col-md-12">
		            <div class="form-group <?php echo form_error('belanja_id') ? 'has-error' : null; ?>">
		                <?php 
		                echo form_label('Belanja','belanja_id');
		                $selected = set_value('belanja_id',$record->belanja_id);
		                echo form_dropdown('belanja_id', $belanja, $selected, "required class='form-control select2' name='belanja_id' id='belanja_id'");
		                echo form_error('belanja_id') ? form_error('belanja_id', '<p class="help-block">','</p>') : 'Pilih belanja_id Lokasi cabor';
		                ?>
		            </div>
		        </div>

				</div>
			</div>
			<!-- ./box-body -->
			<div class="box-footer">
				<button type="button" class="btn btn-sm btn-flat btn-info" onclick="saveout();"><i class="fa fa-save"></i> Simpan & Keluar</button>
				<button type="reset" class="btn btn-sm btn-flat btn-warning"><i class="fa fa-refresh"></i> Reset</button>
				<button type="button" class="btn btn-sm btn-flat btn-danger" onclick="back();"><i class="fa fa-close"></i> Keluar</button>
			</div>
			</form>
		</div>
	</div>
</div>