<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Registrasi_m extends MY_Model
{
	public $table = 'sekolah'; // you MUST mention the table name
	public $primary_key = 'id'; // you MUST mention the primary key
	public $fillable = array(); // If you want, you can set an array with the fields that can be filled by insert/update
	public $protected = array(); // ...Or you can set an array with the fields that cannot be filled by insert/update
	
	//ajax datatable
    public $column_order = array(null); //set kolom field database pada datatable secara berurutan
    public $column_search = array(); //set kolom field database pada datatable untuk pencarian
    public $order = array('id' => 'DESC'); //order baku 
	
	public function __construct()
	{
		$this->timestamps = TRUE;
		$this->soft_deletes = FALSE;
		parent::__construct();
    }
    
    public function insert_data($sekolah = NULL, $user = NULL){
        $this->db->trans_start(); # Starting Transaction
        //$this->db->trans_strict(FALSE); 
        $this->db->insert('sekolah', $sekolah); # Inserting data
        $this->db->insert('users', $user); # Inserting data
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return FALSE;
        } else {
            $this->db->trans_commit();
            return TRUE;
        }
    }

    public function get_kode() {
		$query = $this->db->query("SELECT MAX(RIGHT(kode,5)) AS kode FROM sirkas_sekolah");
		$kode = "";
	  
		if($query->num_rows() > 0){ 
			  foreach($query->result() as $k){
				  $tmp = ((int)$k->kode)+1;
				  $kode = sprintf("%05s", $tmp);
			  }
		 }else{
		  $kode = "00001";
		}
		$karakter = "S"; 
		return $karakter.$kode;
    }

    public function get_kecamatan()
	{
		$query = $this->db->order_by('id', 'ASC')->get('kecamatan');
        if($query->num_rows() > 0){
        $dropdown[''] = 'Pilih Salah Satu';
		foreach ($query->result() as $row)
		{
			$dropdown[$row->id] = $row->nama;
		}
        }else{
            $dropdown[''] = 'Belum Ada Daftar Kecamatan Tersedia';
        }
		return $dropdown;
    }
    
    public function get_kelurahan($id=null)
	{
		$this->db->where('kecamatan_id', $id);
        $query = $this->db->order_by('id', 'ASC')->get('kelurahan');
        if($query->num_rows() > 0){
        $dropdown[''] = 'Pilih Salah Satu';
		foreach ($query->result() as $row)
		{
			$dropdown[$row->id] = $row->nama;
		} 
        }else{
            $dropdown[''] = 'Belum Ada Daftar Kelurahan Tersedia';
        }
		return $dropdown;
	}
}