<?php if($record): ?>
<ul class="timeline">
    <?php foreach($record as $row): ?>
    <!-- timeline time label -->
    <!-- <li class="time-label">
        <span class="bg-gray">
            <?php echo ddMMMyyyy($row->tanggal); ?>
        </span>
    </li> -->
    <!-- /.timeline-label -->
    <!-- timeline item -->
    <li>
        <!-- timeline icon -->
        <i class="fa fa-file-text bg-red"></i>
        <div class="timeline-item bg-gray">
            <span class="time"><?php echo ddMMMyyyy($row->tanggal); ?></span>
            <h3 class="timeline-header"><a href="#"><?= $row->judul; ?></a></h3>
            <div class="timeline-body">
                <?= $row->informasi; ?>
            </div>
            <div class="timeline-footer">
            <?php //echo ddMMMyyyy($row->tanggal); ?>
                <!-- <a class="btn btn-primary btn-xs"></a> -->
            </div>
        </div>
    </li>
    <?php endforeach; ?>
    <!-- END timeline item -->
</ul>
<?php endif; ?>