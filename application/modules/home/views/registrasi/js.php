<script>
$(function () {
	$('.select2').select2();
	$("#kecamatan").change(function(){
		var kecamatan = $("#kecamatan").val();
		$.ajax({
				type: "POST",
				async: false,
				url : "<?php echo site_url('home/registrasi/get_kelurahan')?>",
				data: {
				'id': kecamatan,
				'<?php echo $this->security->get_csrf_token_name(); ?>' : '<?php echo $this->security->get_csrf_hash(); ?>'
				},
				success: function(data){
					$('#kelurahan').html(data);
				}
		});
	});
});
</script>