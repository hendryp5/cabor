<h3><b>REGISTRASI SEKOLAH</b></h3>
<?php if($this->session->flashdata('flashconfirm')): ?>
<div class="alert alert-success alert-dismissable">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    <?php echo $this->session->flashdata('flashconfirm'); ?>
</div>
<?php endif; ?>
<?php if($this->session->flashdata('flasherror')): ?>
<div class="alert alert-warning alert-dismissable">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    <?php echo $this->session->flashdata('flasherror'); ?>
</div>
<?php endif; ?>
<form id="formID" role="form" action="<?= site_url('home/registrasi/register'); ?>" method="post">
<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
<!-- box-body -->
    <div class="row">
        <div class="col-md-6">
            <div class="form-group <?php echo form_error('sekolah') ? 'has-error' : null; ?>">
                <?php
                echo form_label('Nama Sekolah','sekolah');
                $data = array('required class'=>'form-control','name'=>'sekolah','id'=>'sekolah','type'=>'text','value'=>set_value('sekolah'));
                echo form_input($data);
                echo form_error('sekolah') ? form_error('sekolah', '<p class="help-block">','</p>') : 'Contoh : Sekolah Dasar Negeri 1 Banjarmasin';
                ?>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group <?php echo form_error('npsn') ? 'has-error' : null; ?>">
                <?php
                echo form_label('NPSN Sekolah','npsn');
                $data = array('required class'=>'form-control','name'=>'npsn','id'=>'npsn','type'=>'text','value'=>set_value('npsn'));
                echo form_input($data);
                echo form_error('npsn') ? form_error('npsn', '<p class="help-block">','</p>') : 'Contoh : 12345678';
                ?>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group <?php echo form_error('bentuk') ? 'has-error' : null; ?>">
                <?php 
                echo form_label('Bentuk Pendidikan','bentuk');
                $selected = set_value('bentuk');
                $bentuk = array(''=>'Pilih Salah Satu','1'=>'SD','2'=>'SMP');
                echo form_dropdown('bentuk', $bentuk, $selected, "required class='form-control select2' name='bentuk' id='bentuk'");
                echo form_error('bentuk') ? form_error('bentuk', '<p class="help-block">','</p>') : 'Pilih Bentuk Pendidikan';
                ?>
            </div>
        </div>

        <div class="col-md-6">
            <div class="form-group <?php echo form_error('status') ? 'has-error' : null; ?>">
                <?php 
                echo form_label('Status Sekolah','status');
                $selected = set_value('status');
                $status = array(''=>'Pilih Salah Satu','1'=>'Negeri','2'=>'Swasta');
                echo form_dropdown('status', $status, $selected, "required class='form-control select2' name='status' id='status'");
                echo form_error('status') ? form_error('status', '<p class="help-block">','</p>') : 'Pilih Status Sekolah Negeri atau Swasta';
                ?>
            </div>
        </div>

        <div class="col-md-6">
            <div class="form-group <?php echo form_error('kecamatan') ? 'has-error' : null; ?>">
                <?php 
                echo form_label('Kecamatan','kecamatan');
                $selected = set_value('kecamatan');
                echo form_dropdown('kecamatan', $kecamatan, $selected, "required class='form-control select2' name='kecamatan' id='kecamatan'");
                echo form_error('kecamatan') ? form_error('kecamatan', '<p class="help-block">','</p>') : 'Pilih Kecamatan Lokasi Sekolah';
                ?>
            </div>
        </div>

        <div class="col-md-6">
            <div class="form-group <?php echo form_error('kelurahan') ? 'has-error' : null; ?>">
                <?php 
                echo form_label('Kelurahan','kelurahan');
                $selected = set_value('kelurahan');
                $kelurahan = array(''=>'Pilih Salah Satu');
                echo form_dropdown('kelurahan', $kelurahan, $selected, "required class='form-control select2' name='kelurahan' id='kelurahan'");
                echo form_error('kelurahan') ? form_error('kelurahan', '<p class="help-block">','</p>') : 'Pilih Kelurahan Lokasi Sekolah';
                ?>
            </div>
        </div>

        <div class="col-md-12">
            <div class="form-group <?php echo form_error('telpon') ? 'has-error' : null; ?>">
                <?php
                echo form_label('Telpon/HP Sekolah','telpon');
                $data = array('required class'=>'form-control','name'=>'telpon','id'=>'telpon','type'=>'text','value'=>set_value('telpon'));
                echo form_input($data);
                echo form_error('telpon') ? form_error('telpon', '<p class="help-block">','</p>') : 'Masukan Nomor Telpon/HP Sekolah atau Pengelola Sekolah';
                ?>
            </div>
        </div>

        <div class="col-md-12"><h4>Data Login Sistem Informasi</h4></div>
        <div class="col-md-12">
            <div class="form-group <?php echo form_error('email') ? 'has-error' : null; ?>">
                <?php
                echo form_label('Email Sekolah','email');
                $data = array('required class'=>'form-control','name'=>'email','id'=>'email','type'=>'email','value'=>set_value('email'));
                echo form_input($data);
                echo form_error('email') ? form_error('email', '<p class="help-block">','</p>') : 'Masukan Alamat Email Aktif Untuk Kebutuhan Login Sistem. Disarankan Menggunakan Email Sekolah.';
                ?>
            </div>
        </div>
        <div class="col-md-12">
            <div class="form-group <?php echo form_error('password') ? 'has-error' : null; ?>">
                <?php
                echo form_label('Password','password');
                $data = array('required class'=>'form-control','name'=>'password','id'=>'password','type'=>'password','value'=>set_value('password'));
                echo form_input($data);
                echo form_error('password') ? form_error('password', '<p class="help-block">','</p>') : 'Masukan Minimal 8 Digit Password';
                ?>
            </div>
        </div>

        <div class="col-md-12">
            <div class="form-group <?php echo form_error('repassword') ? 'has-error' : null; ?>">
                <?php
                echo form_label('Re Password','repassword');
                $data = array('required class'=>'form-control','name'=>'repassword','id'=>'repassword','type'=>'password','value'=>set_value('repassword'));
                echo form_input($data);
                echo form_error('repassword') ? form_error('repassword', '<p class="help-block">','</p>') : 'Ulangi Password Diatas';
                ?>
            </div>
        </div>
        
    </div>
<!-- ./box-body -->
    <button type="submit" class="btn btn-sm btn-flat btn-primary"><i class="fa fa-save"></i> Registrasi</button>
    <button type="reset" class="btn btn-sm btn-flat btn-warning"><i class="fa fa-refresh"></i> Reset</button>
</form>