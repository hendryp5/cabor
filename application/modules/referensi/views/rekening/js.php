<script src="<?= base_url('asset/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js'); ?>"></script>
<script>
$(function () {
	$('.select2').select2();
});

$(document).on('click', '#UploadData', function(e){
		e.preventDefault();
		$('#upload-content').html(''); // leave this div blank
		$('#upload-loader').show(); // load ajax loader on button click
		$.ajax({
			url: '<?= site_url('referensi/rekening/get_file'); ?>',
			type: 'POST',
			data: {
					'<?php echo $this->security->get_csrf_token_name(); ?>' : '<?php echo $this->security->get_csrf_hash(); ?>'
			},
			dataType: 'html'
		})
		.done(function(data){
			$('#upload-content').html(''); // blank before load.
			$('#upload-content').html(data); // load here
			$('#upload-loader').hide(); // hide loader
		})
		.fail(function(){
			$('#upload-content').html('<i class="glyphicon glyphicon-info-sign"></i> Something went wrong, Please try again...');
			$('#upload-loader').hide();
		});
	});
</script>