<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Rekening extends CI_Controller {

	/**
	 * code by rifqie rusyadi
	 * email rifqie.rusyadi@gmail.com
	 */
	
	public $folder = 'referensi/rekening/';
	
	public function __construct()
	{
		parent::__construct();
		$this->load->model('rekening_m', 'data');
        $this->load->helper('my_helper');
        require_once APPPATH."libraries/PHPExcel.php";
        require_once APPPATH."libraries/PHPExcel/IOFactory.php";
		signin();
		//group(array('1'));
	}
	
	//halaman index
	public function index()
	{
		$data['head'] 		= 'Referensi Kode Rekening';
		$data['record'] 	= FALSE;
		$data['content'] 	= $this->folder.'default';
		$data['style'] 		= $this->folder.'style';
		$data['js'] 		= $this->folder.'js';
		
		$this->load->view('template/default', $data);
	}
	
	public function created()
	{
		$data['head'] 		= 'Tambah Referensi Kode Rekening';
		$data['record'] 	= $this->data->get_new();
		$data['content'] 	= $this->folder.'form';
		$data['style'] 		= $this->folder.'style';
        $data['js'] 		= $this->folder.'js';
        
        $data['blok'] 	    = $this->data->get_blok();
		
		$this->load->view('template/default', $data);
	}
	
	public function updated($id)
	{
		$data['head'] 		= 'Ubah Referensi Kode Rekening';
		$data['record'] 	= $this->data->get_id($id);
		$data['content'] 	= $this->folder.'form';
		$data['style'] 		= $this->folder.'style';
        $data['js'] 		= $this->folder.'js';
        
        $data['blok'] 	    = $this->data->get_blok();
		
		$this->load->view('template/default', $data);
	}
	
	public function ajax_list()
    {
        $record	= $this->data->get_datatables();
        $data 	= array();
        $no 	= $_POST['start'];
		
        foreach ($record as $row) {
            $no++;
            $col = array();
            $col[] = '<input type="checkbox" class="data-check" value="'.$row->id.'">';
            $col[] = $row->kode;
            $col[] = $row->uraian;
            
            //add html for action
            $col[] = '<a class="btn btn-xs btn-flat btn-warning" onclick="edit_data();" href="'.site_url('referensi/rekening/updated/'.$row->id).'" data-toggle="tooltip" title="Edit"><i class="glyphicon glyphicon-pencil"></i></a>
                  <a class="btn btn-xs btn-flat btn-danger" data-toggle="tooltip" title="Hapus" onclick="deleted('."'".$row->id."'".')"><i class="glyphicon glyphicon-trash"></i></a>';
 
            $data[] = $col;
        }
 
        $output = array(
                        "draw" => $_POST['draw'],
                        "recordsTotal" => $this->data->count_all(),
                        "recordsFiltered" => $this->data->count_filtered(),
                        "data" => $data,
                );
        
		echo json_encode($output);
    }
	
	public function ajax_save()
    {
        $data = array(
            'kode' => $this->input->post('kode'),    
            'uraian' => $this->input->post('uraian')
        );
        
        if($this->validation()){
            $insert = $this->data->insert($data);
			helper_log("add", "Menambah Referensi Kode Rekening");
        }
    }
    
    public function ajax_update($id)
    {
        $perusahaan_id = $this->db->get_where('users', array('id'=>$this->session->userdata('userID')))->row();
        $data = array(
            'kode' => $this->input->post('kode'),    
            'uraian' => $this->input->post('uraian')
        );
		
        if($this->validation($id)){
            $this->data->update($data, $id);
			helper_log("edit", "Merubah Referensi Kode Rekening");
        }
    }
    
    public function ajax_delete($id)
    {
        $this->data->delete($id);
		helper_log("trash", "Menghapus Referensi Kode Rekening");
        echo json_encode(array("rekening" => TRUE));
    }
    
    public function ajax_delete_all()
    {
        $list_id = $this->input->post('id');
        foreach ($list_id as $id) {
            $this->data->delete($id);
			helper_log("trash", "Menghapus Referensi Kode Rekening");
        }
        echo json_encode(array("rekening" => TRUE));
    }
	
	private function validation($id=null)
    {
        $data = array('success' => false, 'messages' => array());
        
        $this->form_validation->set_rules("kode", "Kode Kode Rekening", "trim|required");
        $this->form_validation->set_rules("uraian", "Uraian Kode Rekening", "trim|required");
		
		$this->form_validation->set_error_delimiters('<p class="text-danger">', '</p>');
        
        if($this->form_validation->run()){
            $data['success'] = true;
        }else{
            foreach ($_POST as $key => $value) {
                $data['messages'][$key] = form_error($key);
            }
        }
        echo json_encode($data);
        return $this->form_validation->run();
    }

    public function get_file()
	{
		$this->load->view('referensi/rekening/file');
    }

    public function upload(){
        //$fileName = time().'_'.$_FILES['file']['name'];
        //$fileName = 'rek_'.time().'_'.$_FILES['file']['name']; 
        $fileName = 'rek_'.time().'_'.$_FILES['file']['name'];        
 
        $config['upload_path'] = './dokumen/'; //buat folder dengan nama assets di root folder
        $config['file_name'] = $fileName;
        $config['allowed_types'] = 'xls|xlsx';
        $config['max_size'] = 50000;
        $config['overwrite']  = TRUE;
         
        $this->load->library('upload');
        $this->upload->initialize($config);
         
        if(! $this->upload->do_upload('file')) $this->upload->display_errors();
            
            $media = $this->upload->data();
            $inputFileName = './dokumen/'.$media['file_name'];
			
            $objPHPExcel = new PHPExcel(); 
            try {
                $inputFileType = IOFactory::identify($inputFileName);
                $objReader = IOFactory::createReader($inputFileType);
                $objPHPExcel = $objReader->load($inputFileName);
            } catch(Exception $e) {
                die('Error loading file "'.pathinfo($inputFileName,PATHINFO_BASENAME).'": '.$e->getMessage());
            }
        
            $sheet = $objPHPExcel->getSheet(0);
            $highestRow = $sheet->getHighestRow();
            $highestColumn = $sheet->getHighestColumn();
			 
			$find = $this->db->get('ref_rekening')->result();
			
			if($find){
				//$this->db->delete('pangkat_sapk');
				//$this->db->truncate('ref_rekening'); 
			}
			
			for ($row = 4; $row <= $highestRow; $row++){ //  Read a row of data into an array                 
                
                $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, NULL,TRUE,FALSE);
                //Sesuaikan sama nama kolom tabel di database                                
                $limit = 1;
                
                $data = array(
                    "kode"=> $rowData[0][0],
					"uraian"=> $rowData[0][1],
                );
                 
                $find = $this->db->get_where('ref_rekening',array('kode'=>$data['kode']))->row();
                if(!$find){
                    $this->db->insert('ref_rekening', $data);
                }
                //sesuaikan nama dengan nama tabel
				//$this->db->insert('ref_rekening', $data);
				
                //delete_files($media['file_path']);
				//unlink($inputFileName);
				//if (file_exists($inputFileName)) unlink($inputFileName);
            }   
        
		$this->session->set_flashdata('flashconfirm','Data berhasil di import');
        redirect('referensi/rekening');
	}
    
    private function upload_file(){
		$this->load->library('upload');
        $config['upload_path'] = './dokumen/'; //path folder
        $nmfile = "rekening_".time(); //nama file saya beri nama langsung dan diikuti fungsi time
        $config['allowed_types'] = 'xls|xlsx'; //type yang dapat diakses bisa anda sesuaikan
        $config['max_size'] = '5000'; //maksimum besar file 2M
        $config['max_width']  = '5000'; //lebar maksimum 5000 px
		$config['max_height']  = '5000'; //tinggi maksimu 5000 px
		$config['overwrite']  = TRUE; //tinggi maksimu 5000 px
        $config['file_name'] = $nmfile; //nama yang terupload nantinya
        $this->upload->initialize($config);
	}
}
