<div class="row">
	<div class="col-md-12 text-center">
		<h4>Rencana Penggunaan Anggaran<br>Berdasarkan Program dan Kegiatan <br>Tahun <?= $tahun; ?></h4>
	</div>
	<div class="col-md-12">
		<table>
			<tr>
				<td>Jumlah Cabang Olahraga</td>
				<td> : </td>
				<td><?= count(jumlah_cabor(date('Y')+1)) ?></td>
			</tr>
			<tr>
				<td>Jumlah Kuota Anggaran</td>
				<td> : </td>
				<td><?= rupiah(cek_kuota(date('Y')+1)) ?></td>
			</tr>
		</table>
	</div>
	<div class="col-md-12">
		<!-- box-body -->
		<?php if($record): ?>
		<!-- program standart -->
						<table class="table table-bordered">
						<thead>
						<tr class="text-center">
								<th class="text-center">Kode</th>
								<th class="text-center">Uraian</th>
								<th class="text-center">Jumlah</th>
								
						</tr>
						</thead>
						<tbody>
						<?php 
						$total = 0;
						$total_semua = 0;
						foreach($record as $row): 
						?>
						<tr style="font-weight: 600; background-color: #ddd;">
							<td><?= kode_program($row->id); ?></td>
							<td><?= program($row->id); ?></td>
							<td class="text-right">
							<b><?php 
							$total = jumlah_program_total($row->id, $tahun);
							echo rupiah($total); 
							$total_semua += $total;
							?></b>
							</td>
						</tr>
							<!-- kegiatan -->
							<?php 
							$kegiatan = $this->db->order_by('id','ASC')->group_by('id')->get_where('kegiatan', array('program_id'=>$row->id,'deleted_at'=>null))->result(); 
							if($kegiatan):
							//uraian
							foreach($kegiatan as $baris):
							?>
								<tr style="background-color: #f4f4f4;">
									<td></td>
									<td><b><?= kegiatan($baris->id); ?></b></td>
									<td class="text-right"><b><?= rupiah(jumlah_kegiatan_total($row->id, $baris->id, $tahun)); ?></b></td>
								</tr>
							<?php endforeach; ?>
							<?php endif; ?>
							<?php endforeach; ?>
						</tbody>
						<tfoot>
							<tr>
								<th>Total Jumlah</th>
								<th class="text-right"></th>
								<th class="text-right"><?php echo rupiah($total_semua); ?></th>
							</tr>
						</tfoot>
						</table>
		<?php else: ?>
		<div class="callout callout-danger">
			<p>Belum Ada Program Tersedia. Silahkan Untuk Membuat Terlebih Dahulu</p>
		</div>
		<?php endif; ?>			
			<!-- ./box-body -->
	</div>
</div>