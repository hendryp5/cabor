<div class="row">
	<div class="col-md-12 text-center">
		<h4>Rencana Penggunaan Anggaran</h4>
	</div>
	<div class="col-md-12">
		<table>
			
			<tr>
				<td>Kode Registrasi</td>
				<td> : </td>
				<td> <?= $cabor->kode; ?></td>
			</tr>
			<tr>
				<td width="140px">Pengurus Provinsi</td>
				<td> : </td>
				<td> <?= $cabor->cabor; ?></td>
			</tr>
			<tr>
				<td>Alamat Sekertariat</td>
				<td> : </td>
				<td> <?= $cabor->alamat; ?></td>
			</tr>
			<tr>
				<td>Kota</td>
				<td> : </td>
				<td> <?= kota($cabor->kota_id); ?></td>
			</tr>
			<tr>
				<td>Kecamatan</td>
				<td> : </td>
				<td><?= kecamatan($cabor->kecamatan_id); ?></td>
			</tr>
			<tr>
				<td>Kelurahan</td>
				<td> : </td>
				<td><?= kelurahan($cabor->kelurahan_id); ?></td>
			</tr>
			<tr>
				<td>No Telepon</td>
				<td> : </td>
				<td><?= $cabor->telpon; ?></td>
			</tr>
			<tr>
				<td>Cabang Olahraga</td>
				<td> : </td>
				<td><?= cabang_olahraga($record->jenis_cabor_id) ? cabang_olahraga($record->jenis_cabor_id) : '-' ; ?></td>
			</tr>
			<tr>
				<td>Jumlah Kuota Anggaran</td>
				<td> : </td>
				<td><?= kuota_cabor($record->jenis_cabor_id,$record->tahun) ? rupiah(kuota_cabor($record->jenis_cabor_id,$record->tahun)) : 'Belum diatur' ; ?></td>
			</tr>
		</table>
	</div>
	<div class="col-md-12">
		<button type="button" class="btn btn-xs btn-flat btn-default no-print" style="margin-top:3px;margin-bottom:3px;" onclick="window.print()"><i class="fa fa-print"></i> Print</button>
	</div>
	<div class="col-md-12">
		<!-- box-body -->
		<?php if($record): ?>
		<!-- program standart -->
		<?php if($standart): ?>
						<table class="table table-bordered">
						<thead>
						<tr class="text-center">
								<th class="text-center">Kode</th>
								<th class="text-center">Uraian</th>
								<th class="text-center">Jumlah</th>
								
						</tr>
						</thead>
						<tbody>
						<?php foreach($standart as $row): ?>
						<tr style="font-weight: 600; background-color: #ddd;">
							<td><?= kode_program($row->id); ?></td>
							<td><?= program($row->id); ?></td>
							<td class="text-right"><b><?= rupiah(jumlah_program($record->id, $row->id, $record->tahun)); ?></b></td>
							
						</tr>
						<?php 
		//uraian;
						$uraian = $this->db->order_by('id','ASC')->group_by('id')->get_where('kegiatan', array('program_id'=>$row->id,'deleted_at'=>null))->result(); 
						if($uraian):
						//uraian
							foreach($uraian as $baris):
							?>
							<tr style="background-color: #f4f4f4;">
								<td></td>
								<td><b><?= kegiatan($baris->id); ?></b></td>
							
								<td class="text-right"><b><?= rupiah(jumlah_kegiatan($record->id, $row->id, $baris->id, $record->tahun)); ?></b></td>
								
								
							</tr>
							<?php 
							//detail
							$detail = $this->db->order_by('kategori_id','ASC')->group_by('kategori_id')->get_where('template_nomenklatur', array('program_id'=>$row->id,'kegiatan_id'=>$baris->id,'tahun'=>$record->tahun,'deleted_at'=>null))->result(); 
							//detail
						
							foreach($detail as $kolom):
							?>
			<tr>
								<td class="text-right"></td>
								
								<td><b>- <?= kategori($kolom->kategori_id); ?></b></td>
								<td class="text-right"><b><?= rupiah(jumlah_kategori($record->id, $row->id, $baris->id,$kolom->kategori_id, $record->tahun)); ?></b></td>
								
							</tr>
								
								
							<?php 
							//detail
							$belanja = $this->db->order_by('belanja_id','ASC')->get_where('template_nomenklatur', array('kategori_id'=>$kolom->kategori_id,'program_id'=>$row->id,'kegiatan_id'=>$baris->id,'tahun'=>$record->tahun,'deleted_at'=>null))->result(); 
							//detail
							$no = 0;
							foreach($belanja as $isi):
								$no++;
							?>
							
							<?php 
							$jumlah_detail = jumlah_detail($record->id,$row->id,$baris->id,$kolom->kategori_id,$isi->belanja_id,$record->tahun); 
							if($jumlah_detail != 0):
							?>
							<tr>
								<td class="text-right"></td>
								
								<td><?= $no; ?> .<?= belanja($isi->belanja_id); ?></td>
								
								<td class="text-right"><?= rupiah(jumlah_detail($record->id,$row->id,$baris->id,$kolom->kategori_id,$isi->belanja_id,$record->tahun)); ?></td>
								
								
							</tr>
							<?php endif; ?>
							<?php endforeach; //endforeach detail ?>
							<?php endforeach; //endforeach detail ?>
							<?php 
							endforeach; //endforeach uraian
						endif; //endif uraian; 
						?>

		<?php endforeach; ?>
		</tbody>
		<tfoot>
							<tr>
								<th>Total Jumlah</th>
								<th class="text-right"></th>
								<th class="text-right"><?php echo rupiah(jumlah_total_semua($record->id)->jumlah); ?></th>
							</tr>
						</tfoot>
		</table>
		<table width="100%" style="text-align: center">
			<tr>
				<td width="50%">Menyetujui,<br>Ketua<br><br><br><br><?= kepala($cabor->id, $cabor->kode) ? kepala($cabor->id, $cabor->kode)->nama : '-'; ?></td>
				<td width="50%"><br>Bendahara<br><br><br><br><?= bendahara($cabor->id, $cabor->kode) ? bendahara($cabor->id, $cabor->kode)->nama : '-'; ?></td>
			</tr>
		</table>
		<?php endif; ?>
		<?php else: ?>
		<div class="callout callout-danger">
			<p>Belum Ada Kertas Kerja Tersedia. Silahkan Untuk Membuat</p>
		</div>
		<?php endif; ?>			
			<!-- ./box-body -->
	</div>
</div>