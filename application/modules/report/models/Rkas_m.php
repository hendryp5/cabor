<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Rkas_m extends MY_Model
{
	public $table = 'kertas'; // you MUST mention the table name
	public $primary_key = 'id'; // you MUST mention the primary key
	public $fillable = array(); // If you want, you can set an array with the fields that can be filled by insert/update
	public $protected = array(); // ...Or you can set an array with the fields that cannot be filled by insert/update
	
	//ajax datatable
    public $column_order = array(null); //set kolom field database pada datatable secara berurutan
    public $column_search = array(null); //set kolom field database pada datatable untuk pencarian
    public $order = array('id' => 'asc'); //order baku 
	
	public function __construct()
	{
		$this->timestamps = TRUE;
		$this->soft_deletes = TRUE;
		parent::__construct();
	}
	
	public function get_new()
    {
        $cost = 0;
        $bentuk = $this->session->userdata('bentuk');

        $record = new stdClass();
        $record->id = '';
        $record->tahun = '';
        $record->siswa = '';
        $record->unitcost = '';
        $record->silpa = '';
		return $record;
    }

    public function get_cabor($id=null)
    {
        $this->db->where('id', $id);
        $this->db->where('deleted_at', NULL);
        $query = $this->db->get('cabor');
        return $query->row();
    }
    
    public function get_record($id=null, $periode=null)
    {
        //$this->db->where('id', $rkas);
        if($periode){
            $this->db->where('tahun', $periode);
        }else{
            $this->db->where('tahun', date('Y'));
        }

        $this->db->where('sekolah_id', $id);
        $this->db->where('deleted_at', NULL);
        $query = $this->db->get($this->table);
        return $query->result();
    }

    public function get_data($id=null, $rkas=null, $tahun=null)
    {
        $this->db->where('id', $rkas);
        $this->db->where('cabor_id', $id);
        $this->db->where('tahun', $tahun);
        $this->db->where('deleted_at', NULL);
        $query = $this->db->get($this->table);
        return $query->row();
    }
	
	
	public function get_id($id=null)
    {
        $this->db->where('id', $id);
		$this->db->where('deleted_at', NULL);
        $query = $this->db->get($this->table);
        return $query->row();
    }

    public function get_standart()
    {
        $this->db->where('deleted_at', NULL);
        //$this->db->group_by('program_id');
        $query = $this->db->get('program');
        return $query->result();
    }
}