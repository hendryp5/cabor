<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kategori extends CI_Controller {

	/**
	 * code by rifqie rusyadi
	 * email rifqie.rusyadi@gmail.com
	 */
	
	public $folder = 'report/kategori/';
	
	public function __construct()
	{
		parent::__construct();
		$this->load->model('kategori_m', 'data');
		$this->load->helper('my_helper');
		signin();
		//group(array('1'));
	}
	
	
	public function index()
	{
		// if($_GET['tahun']){
		// 	$tahun = $_GET['tahun'];
		// }else{
		// 	$tahun = date('Y')+1;
		//}

		$tahun = date('Y')+1;
		
		$data['head'] 		= 'RENCANA PENGGUNAAN ANGGARAN BERDASARKAN KATEGORI';
		$data['record'] 	= $this->data->get_data();
		$data['content'] 	= $this->folder.'default';
		$data['style'] 		= $this->folder.'style';
		$data['js'] 		= $this->folder.'js';
		$data['tahun'] 		= $tahun;
		
		$this->load->view('template/print', $data);
	}

	public function excel()
	{
		// if($_GET['tahun']){
		// 	$tahun = $_GET['tahun'];
		// }else{
		// 	$tahun = date('Y')+1;
		//}

		$tahun = date('Y')+1;
		
		$data['head'] 		= 'RENCANA PENGGUNAAN ANGGARAN BERDASARKAN KATEGORI';
		$data['record'] 	= $this->data->get_data();
		$data['content'] 	= $this->folder.'default';
		$data['style'] 		= $this->folder.'style';
		$data['js'] 		= $this->folder.'js';
		$data['tahun'] 		= $tahun;
		$data['namefile'] 	= 'RKA-PROGRAM-KEGIATAN-KATEGORI-PRESTASI-'.$tahun;
		
		$this->load->view('template/excel', $data);
	}
	
	
}
