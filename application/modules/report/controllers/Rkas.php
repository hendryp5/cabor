<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Rkas extends CI_Controller {

	/**
	 * code by rifqie rusyadi
	 * email rifqie.rusyadi@gmail.com
	 */
	
	public $folder = 'report/rkas/';
	
	public function __construct()
	{
		parent::__construct();
		$this->load->model('rkas_m', 'data');
		$this->load->helper('my_helper');
		signin();
		//group(array('1'));
	}
	
    //halaman index
    public function index($periode=null)
	{
		$periode = null;
		if($_GET['tahun']){
			$periode = $_GET['tahun'];
		}
		
		//$find = $this->db->get_where('users', array('id'=>$this->session->userdata('userid')))->row()->sekolah_id;
		$sekolah_id = $this->db->get_where('users', array('id'=>$this->session->userdata('userid')))->row();
        if($sekolah_id){
            $find = $sekolah_id->sekolah_id;
        }else{
            $find = $this->session->userdata('npsn');
		}
		  
		if(!$find){
			if($_GET['npsn']){
				$find = $_GET['npsn'];
			}
		}

        $data['head'] 		= 'Print Rencana Kegiatan dan Anggaran Sekolah';
		$data['record'] 	= $this->data->get_record($find,$periode);
		$data['content'] 	= $this->folder.'default';
		$data['style'] 		= $this->folder.'style';
        $data['js'] 		= $this->folder.'js';
        $data['sekolah'] 	= $this->data->get_sekolah($find);
		$data['standart'] 	= $this->data->get_standart();
		$data['periode'] 	= $periode ? $periode : date('Y');
        //$data['kertas']     = $id;
		$this->load->view('template/print', $data);
	}

	/*public function download($periode=null)
	{
		ini_set('memory_limit','-1');

		$this->load->library('pdf');

		$periode = null;
		if($_GET['tahun']){
			$periode = $_GET['tahun'];
		}
		
		//$find = $this->db->get_where('users', array('id'=>$this->session->userdata('userid')))->row()->sekolah_id;
		$sekolah_id = $this->db->get_where('users', array('id'=>$this->session->userdata('userid')))->row();
        if($sekolah_id){
            $find = $sekolah_id->sekolah_id;
        }else{
            $find = $this->session->userdata('npsn');
		}
		  
		if(!$find){
			if($_GET['npsn']){
				$find = $_GET['npsn'];
			}
		}

		

        $data['head'] 		= 'Print Rencana Kegiatan dan Anggaran Sekolah';
		$data['record'] 	= $this->data->get_record($find,$periode);
		$data['content'] 	= $this->folder.'download';
		$data['sekolah'] 	= $this->data->get_sekolah($find);
		$data['standart'] 	= $this->data->get_standart();
		$data['periode'] 	= $periode ? $periode : date('Y');

		
		 $this->pdf->setPaper('A4', 'landscape');
    	 $this->pdf->filename = "rkas.pdf";
		 $this->pdf->load_view('template/print', $data);
		
        //$this->load->view('template/print', $data);
	}*/
	
	public function tahun($id=null, $cabor_id=null, $tahun=null)
	{
		// $find = $this->db->get_where('users', array('id'=>$this->session->userdata('userid')))->row()->sekolah_id;
		// if(!$find){
		// 	$find = $npsn;
		// }

		$kode_cabor = $this->db->get_where('users', array('id'=>$this->session->userdata('userid')))->row();
        if($kode_cabor){
            $kode_cabor1 = cabor_id($kode_cabor->kode_cabor);
        }else{
            $kode_cabor1 = cabor_id($this->session->userdata('kode_cabor'));
        }

        $data['head'] 		= 'Rencana Anggaran Cabor';
		$data['record'] 	= $this->data->get_data($kode_cabor1, $id, $tahun);
		$data['content'] 	= $this->folder.'default';
		$data['style'] 		= $this->folder.'style';
        $data['js'] 		= $this->folder.'js';
        $data['cabor']   	= $this->data->get_cabor($kode_cabor1);
        $data['standart'] 	= $this->data->get_standart();
        $data['kertas']     = $id;	
		$this->load->view('template/print', $data);
	}
	
	
}
