<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Profil extends CI_Controller {

	/**
	 * code by rifqie rusyadi
	 * email rifqie.rusyadi@gmail.com
	 */
	
	public $folder = 'profil/profil/';
	
	public function __construct()
	{
		parent::__construct();
        $this->load->model('profil_m', 'data');
        $this->load->helper('my_helper');
		signin();
	}
	
	//halaman index
	public function index()
	{
        $cabor_id = $this->db->get_where('users', array('id'=>$this->session->userdata('userid')))->row();
        if($cabor_id){
            $kode_cabor = $cabor_id->kode_cabor;
        }else{
            $kode_cabor = $this->session->userdata('kode_cabor');
        }

        $data['head'] 		= 'Profil Pengurus Provinsi';
		$data['record'] 	= $this->data->get_id($kode_cabor);
		$data['content'] 	= $this->folder.'default';
		$data['style'] 		= $this->folder.'style';
        $data['js'] 		= $this->folder.'js';
        
		
		$this->load->view('template/default', $data);
    }
     public function get_kecamatan(){
        $record = $this->data->get_by_id($this->uri->segment(4));
        $id = $this->input->post('id');
        $kecamatan = $this->data->get_kecamatan($id);
        if(!empty($kecamatan)){
            //$selected = (set_value('parent')) ? set_value('parent') : '';
            $selected = set_value('kecamatan', $record ? $record->kecamatan_id : '');
            echo form_dropdown('kecamatan', $kecamatan, $selected, "class='form-control select2' name='kecamatan' id='kecamatan'");
        }else{
            echo form_dropdown('kecamatan', array(''=>'Pilih Salah Satu'), '', "class='form-control select2' name='kecamatan' id='kecamatan'");
        }
    }

    public function get_kelurahan(){
        $record = $this->data->get_by_id($this->uri->segment(4));
        $id = $this->input->post('id');
        $kelurahan = $this->data->get_kelurahan($id);
        if(!empty($kelurahan)){
            //$selected = (set_value('parent')) ? set_value('parent') : '';
			$selected = set_value('kelurahan', $record ? $record->kelurahan_id : '');
            echo form_dropdown('kelurahan', $kelurahan, $selected, "class='form-control select2' name='kelurahan' id='kelurahan'");
        }else{
            echo form_dropdown('kelurahan', array(''=>'Pilih Salah Satu'), '', "class='form-control select2' name='kelurahan' id='kelurahan'");
        }
    }
	
	public function created()
	{
		redirect('profil');
	}
	
	public function updated($id)
	{
		$data['head'] 		= 'Ubah Profil cabor';
		$data['record'] 	= $this->data->get_by_id($id);
		$data['content'] 	= $this->folder.'form';
		$data['style'] 		= $this->folder.'style';
        $data['js'] 		= $this->folder.'js';
        
        $data['kota'] 	= $this->data->get_kota();
        
		$this->load->view('template/default', $data);
	}
    
    public function ajax_update($id)
    {
        $data = array(
            'cabor' => $this->input->post('cabor'),
            'kode' => $this->input->post('kode'),
            //'bentuk' => $this->input->post('bentuk'),
            //'status' => $this->input->post('status'),
            'alamat' => $this->input->post('alamat'),
            'kota_id' => $this->input->post('kota'),
            'kecamatan_id' => $this->input->post('kecamatan'),
            'kelurahan_id' => $this->input->post('kelurahan'),
            'telpon' => $this->input->post('telpon'),
            //'bank' => $this->input->post('bank'),
            //'rekening' => $this->input->post('rekening'),
            'updateded_at' => date('Y-m-d H:i:s')
        );
		
        if($this->validation($id)){
            $this->data->update($data, $id);
			helper_log("edit", "Merubah Profil cabor");
        }
    }
    
	
	private function validation($id=null)
    {
        //$id = $this->input->post('id');
		$data = array('success' => false, 'messages' => array());
        $this->form_validation->set_rules("cabor", "Nama cabor", "trim|required");
		//$this->form_validation->set_rules("kode", "Kode cabor", "trim|required|callback_kode");
		//$this->form_validation->set_rules("bentuk", "Bentuk Pendidikan", "trim|required");
		//$this->form_validation->set_rules("status", "Status cabor", "trim|required");
        $this->form_validation->set_rules("alamat", "Alamat", "trim|required");
        $this->form_validation->set_rules("kecamatan", "Kecamatan", "trim|required");
		$this->form_validation->set_rules("kelurahan", "Kelurahan", "trim|required");
        $this->form_validation->set_rules("telpon", "Telpon/HP cabor", "trim|required");
        //$this->form_validation->set_rules("bank", "Bank", "trim|required");
        //$this->form_validation->set_rules("rekening", "Rekening", "trim|required");
        
		$this->form_validation->set_error_delimiters('<p class="text-danger">', '</p>');
        if($this->form_validation->run()){
            $data['success'] = true;
        }else{
            foreach ($_POST as $key => $value) {
                $data['messages'][$key] = form_error($key);
            }
        }
        echo json_encode($data);
        return $this->form_validation->run();
    }

    /*public function kode($str=null)
	{
        $id = $this->uri->segment(4);
        $query = $this->db->get_where('cabor', array('kode'=>$str,'deleted_at'=>null,'id !='=>$id));
		if($query->num_rows() > 0){
			$this->form_validation->set_message('kode', '{field} sudah tersedia atau telah digunakan.');
			return FALSE;
		}else{
		 	return TRUE;
		}
    }*/


    //data kepala cabor
    public function get_kepala()
	{
        $find = $this->db->get_where('pengurus', array('cabor_id'=>$this->input->post('id'),'kode_cabor'=>$this->input->post('kode'),'jabatan'=>'1','deleted_at'=>NULL))->row();

        if($find){
            $data['record'] = $find;
        }else{
            $data['record'] = $this->data->get_pengurus();
        }
        
        $data['id'] 		= $this->input->post('id');
        $data['kode'] 		= $this->input->post('kode');
        $data['js'] 		= $this->folder.'js';

		$this->load->view('profil/profil/kepsek', $data);
    }

    public function save_kepala()
	{
        if($this->input->post('nip') && $this->input->post('nama') && $this->input->post('telpon')){
            $data = array(
                'cabor_id' => $this->input->post('id'),
                'kode_cabor' => $this->input->post('kode_cabor'),
                'nip' => $this->input->post('nip'),
                'nama' => $this->input->post('nama'),
                'telpon' => $this->input->post('telpon'),
                'jabatan' => 1,
            );

            $find = $this->db->get_where('pengurus', array('cabor_id'=>$this->input->post('id'),'kode_cabor'=>$this->input->post('kode_cabor'),'jabatan'=>'1','deleted_at'=>NULL))->row();
            if($find){
                $data['updated_at'] = date('Y-m-d H:i:s');
                $this->db->where('id', $find->id)->update('pengurus', $data);
                $record = $this->db->affected_rows();
               // echo json_encode(array("success" => TRUE));
                helper_log("edit", "Ubah Data Kepala cabor");
            }else{
                $data['created_at'] = date('Y-m-d H:i:s');
                $this->db->insert('pengurus', $data);
                $record = $this->db->insert_id();
                //echo json_encode(array("success" => TRUE));
                helper_log("add", "Menambah Data Kepala cabor");
            }

            if($record){
                //redirect('profil/profil');
                echo json_encode(array("status" => TRUE));
            }
        }else{
            echo json_encode(array("status" => FALSE));
        }
    }

    //data bendahara cabor
    public function get_bendahara()
	{
        $find = $this->db->get_where('pengurus', array('cabor_id'=>$this->input->post('id'),'kode_cabor'=>$this->input->post('kode'),'jabatan'=>'2','deleted_at'=>NULL))->row();

        if($find){
            $data['record'] = $find;
        }else{
            $data['record'] = $this->data->get_pengurus();
        }
        
        $data['id'] 		= $this->input->post('id');
        $data['kode'] 		= $this->input->post('kode');
        $data['js'] 		= $this->folder.'js';

		$this->load->view('profil/profil/bendahara', $data);
    }

    public function save_bendahara()
	{
        if($this->input->post('nip') && $this->input->post('nama') && $this->input->post('telpon')){
            $data = array(
                'cabor_id' => $this->input->post('id'),
                'kode_cabor' => $this->input->post('kode_cabor'),
                'nip' => $this->input->post('nip'),
                'nama' => $this->input->post('nama'),
                'telpon' => $this->input->post('telpon'),
                'jabatan' => 2,
            );

            $find = $this->db->get_where('pengurus', array('cabor_id'=>$this->input->post('id'),'kode_cabor'=>$this->input->post('kode_cabor'),'jabatan'=>'2','deleted_at'=>NULL))->row();
            
            if($find){
                $data['updated_at'] = date('Y-m-d H:i:s');
                $this->db->where('id', $find->id)->update('pengurus', $data);
                $record = $this->db->affected_rows();
                //echo json_encode(array("success" => TRUE));
                helper_log("add", "Menambah Data Bendahara cabor");
            }else{
                $data['created_at'] = date('Y-m-d H:i:s');
                $this->db->insert('pengurus', $data);
                $record = $this->db->insert_id();
                //echo json_encode(array("success" => TRUE));
                helper_log("add", "Menambah Data Bendahara cabor");
            }

            if($record){
                //redirect('profil/profil');
                echo json_encode(array("status" => TRUE));
            }
        }else{
            echo json_encode(array("status" => FALSE));
        }
    }


    //data sekretaris cabor
    public function get_sekretaris()
	{
        $find = $this->db->get_where('pengurus', array('cabor_id'=>$this->input->post('id'),'kode_cabor'=>$this->input->post('kode_cabor'),'jabatan'=>'3','deleted_at'=>NULL))->row();

        if($find){
            $data['record'] = $find;
        }else{
            $data['record'] = $this->data->get_pengurus();
        }
        
        $data['id'] 		= $this->input->post('id');
        $data['kode'] 		= $this->input->post('kode');
        $data['js'] 		= $this->folder.'js';

		$this->load->view('profil/profil/sekretaris', $data);
    }

    public function save_sekretaris()
	{
        if($this->input->post('nip') && $this->input->post('nama') && $this->input->post('telpon')){
            $data = array(
                'cabor_id' => $this->input->post('id'),
                'kode_cabor' => $this->input->post('kode_cabor'),
                'nip' => $this->input->post('nip'),
                'nama' => $this->input->post('nama'),
                'telpon' => $this->input->post('telpon'),
                'jabatan' => 3,
            );

            $find = $this->db->get_where('pengurus', array('cabor_id'=>$this->input->post('id'),'kode_cabor'=>$this->input->post('kode_cabor'),'jabatan'=>'3','deleted_at'=>NULL))->row();
            if($find){
                $data['updated_at'] = date('Y-m-d H:i:s');
                $this->db->where('id', $find->id)->update('pengurus', $data);
                $record = $this->db->affected_rows();
                //echo json_encode(array("success" => TRUE));
                helper_log("add", "Menambah Data sekretaris cabor");
            }else{
                $data['created_at'] = date('Y-m-d H:i:s');
                $this->db->insert('pengurus', $data);
                $record = $this->db->insert_id();
                //echo json_encode(array("success" => TRUE));
                helper_log("add", "Menambah Data sekretaris cabor");
            }

            if($record){
                //redirect('profil/profil');
                echo json_encode(array("status" => TRUE));
            }
        }else{
            echo json_encode(array("status" => FALSE));
        }
    }

    //data yayasan cabor
   
}
