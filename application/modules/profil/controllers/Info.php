<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Info extends CI_Controller {

	/**
	 * code by rifqie rusyadi
	 * email rifqie.rusyadi@gmail.com
	 */
	
	public $folder = 'profil/info/';
	
	public function __construct()
	{
		parent::__construct();
        $this->load->model('info_m', 'data');
        $this->load->helper('my_helper');
		signin();
	}
	
	//halaman index
	public function index()
	{
        $id = $this->uri->segment(3);
        //$perusahaan_id = $this->db->get_where('users', array('id'=>$id))->row();

        $data['head'] 		= 'Profil Pengurus Provinsi';
		$data['record'] 	= $this->data->get_id($id);
		$data['content'] 	= $this->folder.'default';
		$data['style'] 		= $this->folder.'style';
		$data['js'] 		= $this->folder.'js';
		
		$this->load->view('template/default', $data);
	}

}
