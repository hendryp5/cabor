
<form id="formID" role="form" action="" method="post">
<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
<input type="hidden" name="id" value="<?= $id; ?>" />
<input type="hidden" name="kode_cabor" value="<?= $kode; ?>" />
<!-- box-body -->
<div class="box-body">
	<div class="row">
		<div class="col-md-12">
			<div class="form-group <?php echo form_error('nama') ? 'has-error' : null; ?>">
				<?php
				echo form_label('Nama Ketua','nama');
				$data = array('required class'=>'form-control','name'=>'nama','id'=>'nama','type'=>'text','value'=>set_value('nama', $record->nama));
				echo form_input($data);
				echo form_error('nama') ? form_error('nama', '<p class="help-block">','</p>') : '';
				?>
			</div>
		</div>

		<div class="col-md-12">
			<div class="form-group <?php echo form_error('nip') ? 'has-error' : null; ?>">
				<?php
				echo form_label('NIP/NIK','nip');
				$data = array('required class'=>'form-control','name'=>'nip','id'=>'nip','type'=>'text','value'=>set_value('nip', $record->nip));
				echo form_input($data);
				echo form_error('nip') ? form_error('nip', '<p class="help-block">','</p>') : '';
				?>
			</div>
		</div>

		<div class="col-md-12">
			<div class="form-group <?php echo form_error('telpon') ? 'has-error' : null; ?>">
				<?php
				echo form_label('Telpon/Hanphone','telpon');
				$data = array('required class'=>'form-control','name'=>'telpon','id'=>'telpon','type'=>'text','value'=>set_value('telpon', $record->telpon));
				echo form_input($data);
				echo form_error('telpon') ? form_error('telpon', '<p class="help-block">','</p>') : '';
				?>
			</div>
		</div>
	</div>
</div>
<!-- ./box-body -->
<div class="box-footer">
<button type="button" class="btn btn-sm btn-flat btn-success" onclick="save_kepala()" data-dismiss="modal" ><i class="fa fa-save"></i> Simpan</button>
<button type="button" class="btn btn-sm btn-flat btn-danger" data-dismiss="modal"><i class="fa fa-close"></i> Keluar</button>
</div>
</form>

<script>
function save_kepala()
{   
	//console.log('klik save');
	$.ajax({
		url : "<?= site_url('profil/profil/save_kepala'); ?>",
		type: "POST",
		data: $('#formID').serialize(),
		dataType: "JSON",
		success: function(data)
		{
			if(data.status){
					//$('#kepala-modal').modal('hide');
				
					$('#message').append('<div class="alert alert-success">' +
					'<span class="glyphicon glyphicon-ok"></span>' +
					' Data berhasil disimpan.' +
					'</div>');
			
					$('.form-group').removeClass('has-error')
									.removeClass('has-success');
					$('.text-danger').remove();
					$('#formID')[0].reset();
					
					// tutup pesan
					$('.alert-success').delay(250).show(10, function() {
						$(this).delay(1000).hide(10, function() {
						$(this).remove();
						});
					})
					location.reload();
			}else{
				$('#message').append('<div class="alert alert-danger">' +
					'<span class="glyphicon glyphicon-alert"></span>&nbsp;&nbsp;' +
					' Ada Kesalahan Dalam Menyimpan' +
					'</div>');

				// tutup pesan
				$('.alert-danger').delay(3000).show(10, function() {
						$(this).delay(1000).hide(10, function() {
						$(this).remove();
						});
					})
			}
		},
		error: function (jqXHR, textStatus, errorThrown)
		{
			alert('Ada kesalahan dalam proses penyimpanan/pembaharuan data. Mohon Lengkapi Seluruh Data');
		}
	});
}
</script>
		