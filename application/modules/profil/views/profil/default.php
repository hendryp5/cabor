<div class="row">
	<div class="col-md-12">
		<div id="message"></div>
		<div class="box box-primary box-solid">
			<div class="box-header with-border">
				<h3 class="box-title"><?= isset($head) ? $head : ''; ?></h3>
				<div class="box-tools pull-right">
					<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
					<button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
				</div>
			</div>
			<!-- box-body -->
			<div class="box-body">
				<div class="row">
					<div class="col-md-12">
						<span id="key" style="display: none;"><?= $this->security->get_csrf_hash(); ?></span>		
						<h3><?= $record->cabor; ?></h3>
					</div>

					<div class="col-md-6">
						<dl>
                     <dt>Kode Registrasi</dt>
                     <dd><?= $record->kode ? $record->kode : '-'; ?></dd>

                     <dt>Pengurus Provinsi</dt>
							<dd><?= $record->cabor ? $record->cabor : '-'; ?></dd>
                     
                     <dt>Kode</dt>
							<dd><?= $record->nickname ? $record->nickname : '-'; ?></dd>

                     <dt>Cabang Olahraga</dt>
							<dd><?php $record->id ? print_r(jenis_cabor($record->id)) : '-'; ?></dd>

							<dt>Alamat Sekertariat</dt>

							<dd><?= $record->alamat ? $record->alamat : '-'; ?></dd>

                     <dt>Kota/Kabupaten</dt>
                     <dd><?= $record->kota_id ? kota($record->kota_id) : '-'; ?></dd>

							<dt>Kecamatan</dt>
							<dd><?= $record->kecamatan_id ? kecamatan($record->kecamatan_id) : '-'; ?></dd>

							<dt>Kelurahan</dt>
							<dd><?= $record->kelurahan_id ? kelurahan($record->kelurahan_id) : '-'; ?></dd>

							<dt>Telepon/HP</dt>
							<dd><?= $record->telpon ? $record->telpon : '-'; ?></dd>

							

						</dl>
					</div>
					<div class="col-md-6">
						<dl>
							<dt>Data Ketua <button class="btn btn-xs btn-flat btn-info" data-toggle="modal" data-target="#kepala-modal" data-id="<?= $record->id; ?>" data-kode="<?= $record->kode; ?>" id="getkepala"><i class="fa fa-plus"></i></button></dt>
                     <?php if(kepala($record->id, $record->kode)): ?>
                     <dd><?= kepala($record->id, $record->kode) ? kepala($record->id, $record->kode)->nama.'<br>'.kepala($record->id, $record->kode)->nip.'<br>'.kepala($record->id, $record->kode)->telpon : ''; ?></dd>
                     <?php endif; ?>
                     <br>
							<dt>Data Bendahara <button class="btn btn-xs btn-flat btn-info" data-toggle="modal" data-target="#bendahara-modal" data-id="<?= $record->id; ?>" data-kode="<?= $record->kode; ?>" id="getBendahara"><i class="fa fa-plus"></i></button></dt>
                     <?php if(kepala($record->id, $record->kode)): ?>
                     <dd><?= bendahara($record->id, $record->kode) ? bendahara($record->id, $record->kode)->nama.'<br>'.bendahara($record->id, $record->kode)->nip.'<br>'.bendahara($record->id, $record->kode)->telpon : ''; ?></dd>
                     <?php endif; ?>
                     <br>
							<dt>Data Sekertaris <button class="btn btn-xs btn-flat btn-info" data-toggle="modal" data-target="#sekretaris-modal" data-id="<?= $record->id; ?>" data-kode="<?= $record->kode; ?>" id="getsekretaris"><i class="fa fa-plus"></i></button></dt>
                     <?php if(sekretaris($record->id, $record->kode)): ?>
                     <dd><?= sekretaris($record->id, $record->kode) ? sekretaris($record->id, $record->kode)->nama.'<br>'.sekretaris($record->id, $record->kode)->nip.'<br>'.sekretaris($record->id, $record->kode)->telpon: ''; ?></dd>
                     <?php endif; ?>
                     <br>
                     <dt>Dokumen/SK Kepengurusan <button class="btn btn-xs btn-flat btn-info" data-toggle="modal" data-target="#file-modal" data-modul="file" data-id="<?= $record->id; ?>" data-kode="<?= $record->kode; ?>" id="getFile"><i class="fa fa-upload"></i></button></dt>
                     <?php if($record->dokumen): ?>
                     <dd><?= $record->dokumen ? '<a href="'.base_url('dokumen/'.$record->kode.'/'.$record->dokumen).'" target="_blank"><i class="fa fa-file-text"></i></a>': '-' ; ?></dd>
                     <?php endif; ?>

             
                   
						</dl>
					</div>
					<div class="col-md-12">
						<a class="btn btn-sm btn-flat btn-success" href="<?= site_url('profil/profil/updated/'.$record->id); ?>"><i class="fa fa-edit"></i> Ubah Profil cabor</a>
					</div>
				</div>
			</div>
			<!-- ./box-body -->
		</div>
	</div>
</div>

<div id="kepala-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
  <div class="modal-dialog modal-lg"> 
     <div class="modal-content">  
        <div class="modal-header"> 
           <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button> 
           <h4 class="modal-title">
           <i class="fa fa-calendar-o"></i> Data Ketua Pengprov
           </h4> 
        </div> 
        <div class="modal-body">                     
           <div id="kepala-loader" style="display: none; text-align: center;">
           <!-- ajax loader -->
           <img src="<?= base_url('asset/loading.gif'); ?>">
           </div>             
           <!-- mysql data will be load here -->                          
           <div id="kepala-content"></div>
        </div>       
    </div> 
  </div>
</div>

<div id="bendahara-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
  <div class="modal-dialog modal-lg"> 
     <div class="modal-content">  
        <div class="modal-header"> 
           <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button> 
           <h4 class="modal-title">
           <i class="fa fa-calendar-o"></i> Data Bendahara Pengprov
           </h4> 
        </div> 
        <div class="modal-body">                     
           <div id="bendahara-loader" style="display: none; text-align: center;">
           <!-- ajax loader -->
           <img src="<?= base_url('asset/loading.gif'); ?>">
           </div>             
           <!-- mysql data will be load here -->                          
           <div id="bendahara-content"></div>
        </div>       
    </div> 
  </div>
</div>


<div id="sekretaris-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
  <div class="modal-dialog modal-lg"> 
     <div class="modal-content">  
        <div class="modal-header"> 
           <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button> 
           <h4 class="modal-title">
           <i class="fa fa-calendar-o"></i> Data Sekertaris Pengprov
           </h4> 
        </div> 
        <div class="modal-body">                     
           <div id="sekretaris-loader" style="display: none; text-align: center;">
           <!-- ajax loader -->
           <img src="<?= base_url('asset/loading.gif'); ?>">
           </div>             
           <!-- mysql data will be load here -->                          
           <div id="sekretaris-content"></div>
        </div>       
    </div> 
  </div>
</div>

<div id="file-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
  <div class="modal-dialog modal-lg"> 
     <div class="modal-content">  
        <div class="modal-header"> 
           <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button> 
           <h4 class="modal-title">
           <i class="fa fa-calendar-o"></i> Upload Dokumen
           </h4> 
        </div> 
        <div class="modal-body">                     
           <div id="file-loader" style="display: none; text-align: center;">
           <!-- ajax loader -->
           <img src="<?= base_url('asset/loading.gif'); ?>">
           </div>             
           <!-- mysql data will be load here -->                          
           <div id="file-content"></div>
        </div>       
    </div> 
  </div>
</div>