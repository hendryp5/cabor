<div class="row">
	<div class="col-md-12">
		<div id="message"></div>
		<div class="box box-primary box-solid">
			<div class="box-header with-border">
				<h3 class="box-title"><?= isset($head) ? $head : ''; ?></h3>
				<div class="box-tools pull-right">
					<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
					<button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
				</div>
			</div>
			<form id="formID" role="form" action="" method="post">
			<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
			<input type="hidden" name="kotax" id="kotax" value="<?= set_value('kotax', $record->kota_id); ?>"/>
			<input type="hidden" name="kecamatanx" id="kecamatanx" value="<?= set_value('kecamatanx', $record->kecamatan_id); ?>"/>
			<!-- box-body -->
			<div class="box-body">
				<div class="row">

				<div class="col-md-12">
					<div class="form-group <?php echo form_error('kode') ? 'has-error' : null; ?>">
						<?php
						echo form_label('Data Registrasi','kode');
						$data = array('readonly readonly class'=>'form-control','name'=>'kode','id'=>'kode','type'=>'text','value'=>set_value('kode', $record->kode));
						echo form_input($data);
						echo form_error('kode') ? form_error('kode', '<p class="help-block">','</p>') : '';
						?>
					</div>
				</div>
				
				<div class="col-md-12">
					<div class="form-group <?php echo form_error('cabor') ? 'has-error' : null; ?>">
						<?php
						echo form_label('Nama Pengprov','cabor');
						$data = array('required readonly class'=>'form-control','name'=>'cabor','id'=>'cabor','type'=>'text','value'=>set_value('cabor', $record->cabor));
						echo form_input($data);
						echo form_error('cabor') ? form_error('cabor', '<p class="help-block">','</p>') : 'Contoh : Asosiasi Bola Tangan Indonesia';
						?>
					</div>
				</div>

				<div class="col-md-12">
					<div class="form-group <?php echo form_error('nickname') ? 'has-error' : null; ?>">
						<?php
						echo form_label('Kode Pengprov','nickname');
						$data = array('required readonly class'=>'form-control','name'=>'nickname','id'=>'nickname','type'=>'text','value'=>set_value('nickname', $record->nickname));
						echo form_input($data);
						echo form_error('nickname') ? form_error('nickname', '<p class="help-block">','</p>') : 'Contoh : ABTI';
						?>
					</div>
				</div>
				

				<div class="col-md-12">
					<div class="form-group <?php echo form_error('alamat') ? 'has-error' : null; ?>">
						<?php
						echo form_label('Alamat Sekertariat','alamat');
						$data = array('required class'=>'form-control','name'=>'alamat','id'=>'alamat','type'=>'text','value'=>set_value('alamat', $record->alamat));
						echo form_input($data);
						echo form_error('alamat') ? form_error('alamat', '<p class="help-block">','</p>') : '';
						?>
					</div>
				</div>

				<div class="col-md-12">
					<div class="form-group <?php echo form_error('kota') ? 'has-error' : null; ?>">
						<?php 
						echo form_label('Kota/Kabupaten','kota');
						$selected = set_value('kota', $record->kota_id);
						echo form_dropdown('kota', $kota, $selected, "required class='form-control select2' name='kota' id='kota'");
						echo form_error('kota') ? form_error('kota', '<p class="help-block">','</p>') : 'Pilih Kota/Kabupaten';
						?>
					</div>
				</div>

				<div class="col-md-12">
					<div class="form-group <?php echo form_error('kecamatan') ? 'has-error' : null; ?>">
						<?php 
						echo form_label('Kecamatan','kecamatan');
						$selected = set_value('kecamatan', $record->kecamatan_id);
						$kecamatan = array(''=>'Pilih Salah Satu');
						echo form_dropdown('kecamatan', $kecamatan, $selected, "required class='form-control select2' name='kecamatan' id='kecamatan'");
						echo form_error('kecamatan') ? form_error('kecamatan', '<p class="help-block">','</p>') : 'Pilih Kecamatan';
						?>
					</div>
				</div>

				<div class="col-md-12">
					<div class="form-group <?php echo form_error('kelurahan') ? 'has-error' : null; ?>">
						<?php 
						echo form_label('Kelurahan','kelurahan');
						$selected = set_value('kelurahan', $record->kelurahan_id);
						$kelurahan = array(''=>'Pilih Salah Satu');
						echo form_dropdown('kelurahan', $kelurahan, $selected, "required class='form-control select2' name='kelurahan' id='kelurahan'");
						echo form_error('kelurahan') ? form_error('kelurahan', '<p class="help-block">','</p>') : 'Pilih Kelurahan';
						?>
					</div>
				</div>
				<div class="col-md-12">
					<div class="form-group <?php echo form_error('telpon') ? 'has-error' : null; ?>">
						<?php
						echo form_label('Telpon/HP','telpon');
						$data = array('required class'=>'form-control','name'=>'telpon','id'=>'telpon','type'=>'text','value'=>set_value('telpon', $record->telpon));
						echo form_input($data);
						echo form_error('telpon') ? form_error('telpon', '<p class="help-block">','</p>') : 'Masukan Nomor Telpon/HP';
						?>
					</div>
				</div>

				
			</div>
			<!-- ./box-body -->
			<div class="box-footer">
				<button type="button" class="btn btn-sm btn-flat btn-info" onclick="saveout();"><i class="fa fa-save"></i> Simpan & Keluar</button>
				<button type="reset" class="btn btn-sm btn-flat btn-warning"><i class="fa fa-refresh"></i> Reset</button>
				<button type="button" class="btn btn-sm btn-flat btn-danger" onclick="back();"><i class="fa fa-close"></i> Keluar</button>
			</div>
			</form>
		</div>
	</div>
</div>