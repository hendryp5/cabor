<link rel="stylesheet" href="<?= base_url('asset/dist/css/print_fullpage2.css'); ?>" />
<style>.select{border:1 solid #000;font-size:14px;margin-left:12px;}</style>
<style type="text/css">
.tg  {border-collapse:collapse;border-spacing:0;}
.tg td{font-family:Arial, sans-serif;font-size:14px;patding:10px 5px;border-style:solid;border-witdh:1px;overflow:hitden;word-break:normal;border-color:black;}
.tg th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;patding:10px 5px;border-style:solid;border-witdh:1px;overflow:hitden;word-break:normal;border-color:black;}
.tg .tg-nrw1{font-size:10px;text-align:center;vertical-align:top}
.tg .tg-6yti{font-size:10px;font-family:serif !important;;text-align:center;vertical-align:top}
.tg .tg-jpc1{font-size:10px;text-align:left;vertical-align:top}
@media print{ .no-print, .no-print *{display: none !important;} }
th{
  border: 1px solid grey;
  padding-left:3px;
}
td{
  border: 1px solid grey;
  padding-left:3px;
}
</style>
<br>
<div class="book">
<div class="page">
    
    <div class="title">
            <div class="judul">
                <h4>DATA ADMINISTRASI<br><?= @$perusahaan; ?></h4>
            </div>
    </div>
	<!-- box-body -->
  <div class="box-body">
				<div class="row">
					<div class="col-xs-12 no-print">
						<a class="btn btn-sm btn-flat btn-danger" href="#" onclick="history.back()"><i class="fa fa-arrow-left"></i> Kembali</a>
						<a class="btn btn-sm btn-flat btn-default" href="#" onclick='print()'><i class="fa fa-print"></i> Print</a><br><br>
					</div>
					<div class="col-xs-12">
          <table width="95%">
						<tr>
              <th max-width="10px" >No</th>
              <th >Uraian</th>
							<th colspan="4">Keterangan</th>
            </tr>
						<tr>
              <td>1</td>							
              <td>Nama Perusahaan</td>
							<td colspan="4"><?= $record->perusahaan ? $record->perusahaan : '-'; ?></td>
            </tr><tr>
              <td>2</td>	
              <td>NPWP Perusahaan</td>
							<td colspan="4"><?= $record->npwp ? $record->npwp : '-'; ?></td>
            </tr><tr>
              <td>3</td>	
							<td>Alamat Perusahaan</td>
							<td colspan="4"><?= $record->alamat ? $record->alamat : '-'; ?></td>
            </tr><tr>
              <td>4</td>	
							<td>Nomor Telepon/HP</td>
							<td colspan="4"><?= $record->telpon ? $record->telpon : '-'; ?></td>
            </tr><tr>
              <td>5</td>								
							<td>Nomer SK IUP/IUPK</td>
							<td colspan="4"><?= $record->skiup ? $record->skiup : '-'; ?></td>
            </tr><tr>
              <td>6</td>	
							<td>Status IUP/IUPK</td>
							<td colspan="4"><?= $record->status_id ? status($record->status_id) : '-'; ?></td>
            </tr><tr>
              <td>7</td>	
							<td>Kode WIUP/WIUPK</td>
							<td colspan="4"><?= $record->wiupk ? $record->wiupk : '-'; ?></td>
            </tr><tr>
              <td>8</td>	
							<td>Nomor SK Penetapan Tanda Batas Waktu WIUP/WIUPK</td>
							<td colspan="4"><?= $record->wiupk ? $record->sk_penetapan : '-'; ?></td>
            </tr><tr>
              <td>9</td>	
							<td>Komoditas</td>
							<td colspan="4"><?= $record->komoditas_id ? komoditas($record->komoditas_id) : '-'; ?></td>
            </tr><tr>
              <td>10</td>	
							<td>Jangka Waktu WIUP/WIUPK</td>
							<td colspan="4"><?= $record->waktu ? $record->waktu : '-'; ?></td>
            </tr><tr>
              <td>11</td>	
							<td>Kepala Teknik Tambang</td>
							<td colspan="4"><?= $record->kepala ? $record->kepala : '-'; ?></td>
            </tr><tr>
              <td>12</td>	
							<td>Persetujuan Dokumen Lingkungan (Nomor dan Tanggal)</td>
							<td colspan="4"><?= $record->no_dokumen ? $record->no_dokumen : '-'; ?></td>
            </tr><tr>
              <td>13</td>	
							<td>Persetujuan Studi Kelayakan/FS (Nomor dan Tanggal)</td>
							<td colspan="4"><?= $record->no_studi ? $record->no_studi : '-'; ?></td>
            </tr><tr>
              <td rowspan="3">14</td>	
							<td >Kapasitas Produksi Pertahun</td>
              <td colspan="4"></td>
            <tr>
							<td>a. Tambang  </td>
              <td colspan="4"><?= $record->produksi_tambang ? number_format($record->produksi_tambang,2) : '-'; ?> Ton</td>
            </tr><tr>
							<td>b. Pengolahan  </td>
              <td colspan="4"><?= $record->produksi_pengolahan ? number_format($record->produksi_pengolahan,2) : '-'; ?> Ton</td>
            </tr><tr>
              <td rowspan="3">15</td>	
							<td >Rencana Produksi <?= @$produksi_tahun ?> </td>
              <td colspan="4"></td>
            </tr><tr>
              <td>a. Tambang </td>
              <td colspan="4"><?= @number_format($produksi_tambang,2) ?> Ton</td>
            </tr><tr>
              <td>b. Pengolahan </td>
              <td colspan="4"><?= @number_format($produksi_pengolahan,2) ?> Ton</td>
            </tr><tr>
            
							<td rowspan="4">16</td>
							<td rowspan="3">Luas Wilayah Izin Operasi Produksi </td>
							<td colspan="3"><center>Kawasan Hutan</center></td>
							<td rowspan="2"><center>Bukan Kawasan Hutan </center></td>
            </tr><tr>
              <td><center>HK</center></td>
              <td><center>HL</center></td>
              <td><center>HP</center></td>
            </tr><tr>
              <td><center><?= $record->luas_hk ? number_format($record->luas_hk,2) : '-'; ?> ha<center></td>
              <td><center><?= $record->luas_hl ? number_format($record->luas_hl,2) : '-'; ?> ha<center></td>
              <td><center><?= $record->luas_hp ? number_format($record->luas_hp,2) : '-'; ?> ha<center></td>
              <td><center><?= $record->luas_bkh ? number_format($record->luas_bkh,2) : '-'; ?> ha<center></td>
            </tr><tr>
							<td>Total Luas Wilayah</td>
              <td colspan="4"><?= $record->luas_total ? number_format($record->luas_total,2) : '-'; ?> ha</td>
            </tr><tr>
              <td>17</td>
							<td>Luas Project Area </td>
							<td colspan="4"><?= $record->luas_project ? number_format($record->luas_project,2) : '-'; ?> ha</td>
            </tr>

            <?php  
              $operasi = operasi($record->id);
              $o_length = !empty($operasi)?sizeof($operasi):0;
              $eksplorasi = eksplorasi($record->id);
              $e_length = !empty($eksplorasi)?sizeof($eksplorasi):0;
              
              // OPERASI
							if($operasi){
                $io = 0;
								foreach($operasi as $row){
                  $io++;
              if ($io == 1) { 
            ?>
            <tr>
              <td rowspan="<?= $o_length*2+$e_length*2 ?>">18</td>
							<td rowspan="<?= $o_length*2 ?>">Ijin Pinjam Pakai Kawasan Hutan (IPPKH) Untuk Operasi Produksi </td>
							<td rowspan="<?= $o_length ?>">No. </td>
              <td colspan="4"><?= $row->no ?></td>
            </tr>
            <?php }else{ ?>
            <tr>
              <td colspan="4"><?= $row->no ?></td>
            </tr>
            <?php 
                  }// endif 
                }// endforeach

              $io = 0;
              foreach($operasi as $row){
                $io++;
                if ($io == 1) { 
            ?>
            <tr>
							<td rowspan="<?= $o_length ?>">Tanggal. </td>
              <td colspan="4"><?= ddmmyyyy($row->tanggal) ?></td>
            </tr>
            <?php }else{ ?>
            <tr>
              <td colspan="4"><?= ddmmyyyy($row->tanggal) ?></td>
            </tr>
            <?php 
                  }// endif 
                }// endforeach
              }// endif 
              // END OF OPERASI

              // EKSPLORASI
							if($eksplorasi){
                $ie = 0;
								foreach($eksplorasi as $row){
                  $ie++;
              if ($ie == 1) { 
            ?>
            <tr>
							<td rowspan="<?= $e_length*2 ?>">Ijin Pinjam Pakai Kawasan Hutan (IPPKH) Untuk Eksplorasi Lanjutan </td>
							<td rowspan="<?= $e_length ?>">No. </td>
              <td colspan="4"><?= $row->no ?></td>
            </tr>
            <?php }else{ ?>
            <tr>
              <td colspan="4"><?= $row->no ?></td>
            </tr>
            <?php 
                  }// endif 
                }// endforeach

              $ie = 0;
              foreach($eksplorasi as $row){
                $ie++;
                if ($ie == 1) { 
            ?>
            <tr>
							<td rowspan="<?= $e_length ?>">Tanggal. </td>
              <td colspan="4"><?= ddmmyyyy($row->tanggal) ?></td>
            </tr>
            <?php }else{ ?>
            <tr>
              <td colspan="4"><?= ddmmyyyy($row->tanggal) ?></td>
            </tr>
            <?php 
                  }// endif 
                }// endforeach
              }// endif 
              // END OF EKSPLORASI

            ?>
							
            <tr>
              <td rowspan="2">19</td>
							<td>Luas Wilayah Pinjam Pakai Kawasan Hutan Untuk Operasi Produksi </td>
              <td></td>
              <td colspan="4"><?= $record->luas_ippkh_produksi ? number_format($record->luas_ippkh_produksi,2) : '-'; ?></td>
						</tr><tr>
							<td>Luas Wilayah Pinjam Pakai Kawasan Hutan Untuk Eksplorasi Lanjutan </td><td></td>
              <td colspan="4"><?= $record->luas_ippkh_eksplorasi ? number_format($record->luas_ippkh_eksplorasi,2) : '-'; ?> ha</td>
            </tr>
            
            <?php  
            $saham = saham($record->id);
            $s_length = !empty($saham)?sizeof($saham):0;
            if($saham){
              $is = 0;
              foreach($saham as $row){
                $is++;
                if ($is == 1) { 
            ?>
            <tr>
              <td>20</td>
							<td rowspan="<?= $s_length ?>">Pemegang Saham </td>
							<td colspan="5"><?= $is.'. '.$row->nama ?></td>
            </tr>
            <?php }else{ ?>
              <td colspan="5"><?= $is.'. '.$row->nama ?></td>
            <?php }// endif 
                }// endforeach
              }// endif
            ?>

            <?php  
							$direksi = pengurus($record->id, 1);
              $komisaris = pengurus($record->id, 2);
              
              $d_length = !empty($direksi)?sizeof($direksi):0;
              $k_length = !empty($komisaris)?sizeof($komisaris):0;
              // DIREKSI
							if($direksi){
                $idi = 0;
								foreach($direksi as $row){
                  $idi++;
              if ($idi == 1) { 
            ?>
            <tr>
              <td rowspan="<?= $d_length+1+$e_length+1 ?>">21</td>
							<td rowspan="<?= $d_length+1+$e_length+1 ?>">Susunan Pengurus dan NPWP Pengurus </td>
							<td colspan="5">Direksi: </td>
            </tr>
            <tr>
              <td colspan="2"><?= $idi.'. '.$row->nama ?></td><td colspan="3"><?= $row->npwp ?></td>
            </tr>
            <?php }else{ ?>
            <tr>
              <td colspan="2"><?= $idi.'. '.$row->nama ?></td><td colspan="3"><?= $row->npwp ?></td>
            </tr>
            <?php 
                  }// endif 
                }// endforeach

              $idi = 0;
              }// endif 
              // END OF DIREKSI

              // KOMISARI :)
							if($komisaris){
                $ik = 0;
								foreach($komisaris as $row){
                  $ik++;
              if ($ik == 1) { 
            ?>
            <tr>
							<td colspan="5">Komisaris: </td>
            </tr>
            <tr>
              <td colspan="2"><?= $ik.'. '.$row->nama ?></td><td colspan="3"><?= $row->npwp ?></td>
            </tr>
            <?php }else{ ?>
            <tr>
              <td colspan="2"><?= $ik.'. '.$row->nama ?></td><td colspan="3"><?= $row->npwp ?></td>
            </tr>
            <?php 
                  }// endif 
                }// endforeach

              }// endif 
              // END OF KOMISARI :)

            ?>
							
          </table>
					</div>
				</div>
			</div>
			<!-- ./box-body -->
    <br>
    <br>
</div>
</div>
<script src="<?= base_url('asset/plugins/jquery/dist/jquery.min.js'); ?>"></script>
<script>
print();
</script>
