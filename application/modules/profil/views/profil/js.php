<script>
$(function () {
	$('.select2').select2();

	$("#kota").change(function(){
		var kota = $("#kota").val();
		$.ajax({
				type: "POST",
				async: false,
				url : "<?php echo site_url('profil/profil/get_kecamatan')?>",
				data: {
				'id': kota,
				'<?php echo $this->security->get_csrf_token_name(); ?>' : '<?php echo $this->security->get_csrf_hash(); ?>'
				},
				success: function(data){
					$('#kelurahan').html(data);
				}
		});
	});
	
	$("#kotax").ready(function(){
		var kotax = $("#kotax").val();
		$.ajax({
				type: "POST",
				async: false,
				url : "<?php echo site_url('profil/profil/get_kecamatan/'.$this->uri->segment(4))?>",
				data: {
				'id': kotax,
				'<?php echo $this->security->get_csrf_token_name(); ?>' : '<?php echo $this->security->get_csrf_hash(); ?>'
				},
				success: function(data){
					$('#kecamatan').html(data);
				}
		});
	});

	$("#kecamatan").change(function(){
		var kecamatan = $("#kecamatan").val();
		$.ajax({
				type: "POST",
				async: false,
				url : "<?php echo site_url('profil/profil/get_kelurahan')?>",
				data: {
				'id': kecamatan,
				'<?php echo $this->security->get_csrf_token_name(); ?>' : '<?php echo $this->security->get_csrf_hash(); ?>'
				},
				success: function(data){
					$('#kelurahan').html(data);
				}
		});
	});

	$("#kecamatanx").ready(function(){
		var kecamatan = $("#kecamatan").val();
		$.ajax({
				type: "POST",
				async: false,
				url : "<?php echo site_url('profil/profil/get_kelurahan/'.$this->uri->segment(4))?>",
				data: {
				'id': kecamatan,
				'<?php echo $this->security->get_csrf_token_name(); ?>' : '<?php echo $this->security->get_csrf_hash(); ?>'
				},
				success: function(data){
					$('#kelurahan').html(data);
				}
		});
	});

	$(document).on('click', '#getkepala', function(e){
		e.preventDefault();
		var uid = $(this).data('id'); // get id of clicked row
		var ukode = $(this).data('kode'); // get id of clicked row
		$('#kepala-content').html(''); // leave this div blank
		$('#kepala-loader').show(); // load ajax loader on button click
		$.ajax({
			url: '<?= site_url('profil/profil/get_kepala'); ?>',
			type: 'POST',
			data: {
					'id': uid,
					'kode' : ukode,
					'<?php echo $this->security->get_csrf_token_name(); ?>' : '<?php echo $this->security->get_csrf_hash(); ?>'
			},
			dataType: 'html'
		})
		.done(function(data){
			$('#kepala-content').html(''); // blank before load.
			$('#kepala-content').html(data); // load here
			$('#kepala-loader').hide(); // hide loader
		})
		.fail(function(){
			$('#kepala-content').html('<i class="glyphicon glyphicon-info-sign"></i> Something went wrong, Please try again...');
			$('#kepala-loader').hide();
		});
	});


	$(document).on('click', '#getBendahara', function(e){
		e.preventDefault();
		var uid = $(this).data('id'); // get id of clicked row
		var ukode = $(this).data('kode'); // get id of clicked row
		$('#bendahara-content').html(''); // leave this div blank
		$('#bendahara-loader').show(); // load ajax loader on button click
		$.ajax({
			url: '<?= site_url('profil/profil/get_bendahara'); ?>',
			type: 'POST',
			data: {
					'id': uid,
					'kode' : ukode,
					'<?php echo $this->security->get_csrf_token_name(); ?>' : '<?php echo $this->security->get_csrf_hash(); ?>'
			},
			dataType: 'html'
		})
		.done(function(data){
			$('#bendahara-content').html(''); // blank before load.
			$('#bendahara-content').html(data); // load here
			$('#bendahara-loader').hide(); // hide loader
		})
		.fail(function(){
			$('#bendahara-content').html('<i class="glyphicon glyphicon-info-sign"></i> Something went wrong, Please try again...');
			$('#bendahara-loader').hide();
		});
	});

	$(document).on('click', '#getsekretaris', function(e){
		e.preventDefault();
		var uid = $(this).data('id'); // get id of clicked row
		var ukode = $(this).data('kode'); // get id of clicked row
		$('#sekretaris-content').html(''); // leave this div blank
		$('#sekretaris-loader').show(); // load ajax loader on button click
		$.ajax({
			url: '<?= site_url('profil/profil/get_sekretaris'); ?>',
			type: 'POST',
			data: {
					'id': uid,
					'kode' : ukode,
					'<?php echo $this->security->get_csrf_token_name(); ?>' : '<?php echo $this->security->get_csrf_hash(); ?>'
			},
			dataType: 'html'
		})
		.done(function(data){
			$('#sekretaris-content').html(''); // blank before load.
			$('#sekretaris-content').html(data); // load here
			$('#sekretaris-loader').hide(); // hide loader
		})
		.fail(function(){
			$('#sekretaris-content').html('<i class="glyphicon glyphicon-info-sign"></i> Something went wrong, Please try again...');
			$('#sekretaris-loader').hide();
		});
	});

	
	$(document).on('click', '#getFile', function(e){
		e.preventDefault();
		var uid = $(this).data('id'); // get id of clicked row
		var umodul = $(this).data('modul'); // get id of clicked row
		var ukode = $(this).data('kode'); // get id of clicked row
		$('#file-content').html(''); // leave this div blank
		$('#file-loader').show(); // load ajax loader on button click
		$.ajax({
			url: '<?= site_url('profil/file/get_file'); ?>',
			type: 'POST',
			data: {
					'id': uid,
					'modul' : umodul,
					'kode' : ukode,
					'<?php echo $this->security->get_csrf_token_name(); ?>' : '<?php echo $this->security->get_csrf_hash(); ?>'
			},
			dataType: 'html'
		})
		.done(function(data){
			$('#file-content').html(''); // blank before load.
			$('#file-content').html(data); // load here
			$('#file-loader').hide(); // hide loader
		})
		.fail(function(){
			$('#file-content').html('<i class="glyphicon glyphicon-info-sign"></i> Something went wrong, Please try again...');
			$('#file-loader').hide();
		});
	});

});
</script>