<div class="row">
	<div class="col-md-12">
		<div id="message"></div>
		<div class="box box-primary box-solid">
			<div class="box-header with-border">
				<h3 class="box-title"><?= isset($head) ? $head : ''; ?></h3>
				<div class="box-tools pull-right">
					<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
					<button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
				</div>
			</div>
			<form id="formID" role="form" action="" method="post">
			<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" />
			<!-- box-body -->
			<div class="box-body">
				<div class="row">
					<div class="col-md-6">
						<div class="form-group <?php echo form_error('perusahaan') ? 'has-error' : null; ?>">
							<?php
							echo form_label('Nama Perusahaan','perusahaan');
							$data = array('class'=>'form-control','name'=>'perusahaan','id'=>'perusahaan','type'=>'text','value'=>set_value('perusahaan', $record->perusahaan));
							echo form_input($data);
							echo form_error('perusahaan') ? form_error('perusahaan', '<p class="help-block">','</p>') : '';
							?>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group <?php echo form_error('npwp') ? 'has-error' : null; ?>">
							<?php
							echo form_label('NPWP Perusahaan','npwp');
							$data = array('class'=>'form-control','name'=>'npwp','id'=>'npwp','type'=>'text','value'=>set_value('npwp', $record->npwp));
							echo form_input($data);
							echo form_error('npwp') ? form_error('npwp', '<p class="help-block">','</p>') : '';
							?>
						</div>
					</div>

					<div class="col-md-6">
						<div class="form-group <?php echo form_error('alamat') ? 'has-error' : null; ?>">
							<?php
							echo form_label('Alamat Perusahaan','alamat');
							$data = array('class'=>'form-control','name'=>'alamat','id'=>'alamat','type'=>'text','value'=>set_value('alamat', $record->alamat));
							echo form_input($data);
							echo form_error('alamat') ? form_error('alamat', '<p class="help-block">','</p>') : '';
							?>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group <?php echo form_error('telpon') ? 'has-error' : null; ?>">
							<?php
							echo form_label('Telpon/HP Perusahaan','telpon');
							$data = array('class'=>'form-control','name'=>'telpon','id'=>'telpon','type'=>'text','value'=>set_value('telpon', $record->telpon));
							echo form_input($data);
							echo form_error('telpon') ? form_error('telpon', '<p class="help-block">','</p>') : '';
							?>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group <?php echo form_error('skiup') ? 'has-error' : null; ?>">
							<?php
							echo form_label('Nomor SK IUP/IUPK','skiup');
							$data = array('class'=>'form-control','name'=>'skiup','id'=>'skiup','type'=>'text','value'=>set_value('skiup', $record->skiup));
							echo form_input($data);
							echo form_error('skiup') ? form_error('skiup', '<p class="help-block">','</p>') : '';
							?>
						</div>
					</div>

					<div class="col-md-6">
						<div class="form-group <?php echo form_error('status') ? 'has-error' : null; ?>">
							<?php
							echo form_label('Status IUP/IUPK','status');
							$data = array('class'=>'form-control','name'=>'status','id'=>'status','type'=>'text','value'=>set_value('status', $record->status));
							echo form_input($data);
							echo form_error('status') ? form_error('status', '<p class="help-block">','</p>') : '';
							?>
						</div>
					</div>

					<div class="col-md-6">
						<div class="form-group <?php echo form_error('wiupk') ? 'has-error' : null; ?>">
							<?php
							echo form_label('Kode WIUP/WIUPK','wiupk');
							$data = array('class'=>'form-control','name'=>'wiupk','id'=>'wiupk','type'=>'text','value'=>set_value('wiupk', $record->wiupk));
							echo form_input($data);
							echo form_error('wiupk') ? form_error('wiupk', '<p class="help-block">','</p>') : '';
							?>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group <?php echo form_error('sk_penetapan') ? 'has-error' : null; ?>">
							<?php
							echo form_label('SK Penetapan Tanda Batas Waktu WIUP/WIUPK','sk_penetapan');
							$data = array('class'=>'form-control','name'=>'sk_penetapan','id'=>'sk_penetapan','type'=>'text','value'=>set_value('sk_penetapan', $record->sk_penetapan));
							echo form_input($data);
							echo form_error('sk_penetapan') ? form_error('sk_penetapan', '<p class="help-block">','</p>') : '';
							?>
						</div>
					</div>
					<div class="col-md-12">
						<div class="form-group <?php echo form_error('komoditas') ? 'has-error' : null; ?>">
							<?php
							echo form_label('Komoditas','komoditas');
							$data = array('class'=>'form-control','name'=>'komoditas','id'=>'komoditas','type'=>'text','value'=>set_value('komoditas', $record->komoditas));
							echo form_input($data);
							echo form_error('komoditas') ? form_error('komoditas', '<p class="help-block">','</p>') : '';
							?>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group <?php echo form_error('waktu') ? 'has-error' : null; ?>">
							<?php
							echo form_label('Jangka Waktu WIUP/WIUPK','waktu');
							$data = array('class'=>'form-control','name'=>'waktu','id'=>'waktu','type'=>'text','value'=>set_value('waktu', $record->waktu));
							echo form_input($data);
							echo form_error('waktu') ? form_error('waktu', '<p class="help-block">','</p>') : '';
							?>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group <?php echo form_error('kepala') ? 'has-error' : null; ?>">
							<?php
							echo form_label('Kepala Teknik Tambang','kepala');
							$data = array('class'=>'form-control','name'=>'kepala','id'=>'kepala','type'=>'text','value'=>set_value('kepala', $record->kepala));
							echo form_input($data);
							echo form_error('kepala') ? form_error('kepala', '<p class="help-block">','</p>') : '';
							?>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group <?php echo form_error('no_dokumen') ? 'has-error' : null; ?>">
							<?php
							echo form_label('Nomor Persetujuan Dokumen Lingkungan','no_dokumen');
							$data = array('class'=>'form-control','name'=>'no_dokumen','id'=>'no_dokumen','type'=>'text','value'=>set_value('no_dokumen', $record->no_dokumen));
							echo form_input($data);
							echo form_error('no_dokumen') ? form_error('no_dokumen', '<p class="help-block">','</p>') : '';
							?>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group <?php echo form_error('tgl_dokumen') ? 'has-error' : null; ?>">
							<?php
							echo form_label('Tanggal Persetujuan Dokumen Lingkungan','tgl_dokumen');
							$data = array('class'=>'form-control','name'=>'tgl_dokumen','id'=>'tgl_dokumen','type'=>'text','value'=>set_value('tgl_dokumen', ddmmyyyy($record->tgl_dokumen)));
							echo form_input($data);
							echo form_error('tgl_dokumen') ? form_error('tgl_dokumen', '<p class="help-block">','</p>') : '';
							?>
						</div>
					</div>

					<div class="col-md-6">
						<div class="form-group <?php echo form_error('no_studi') ? 'has-error' : null; ?>">
							<?php
							echo form_label('Nomor Persetujuan Studi Kelayakan/FS','no_studi');
							$data = array('class'=>'form-control','name'=>'no_studi','id'=>'no_studi','type'=>'text','value'=>set_value('no_studi', $record->no_studi));
							echo form_input($data);
							echo form_error('no_studi') ? form_error('no_studi', '<p class="help-block">','</p>') : '';
							?>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group <?php echo form_error('tgl_studi') ? 'has-error' : null; ?>">
							<?php
							echo form_label('Tanggal Persetujuan Studi Kelayakan/FS','tgl_studi');
							$data = array('class'=>'form-control','name'=>'tgl_studi','id'=>'tgl_studi','type'=>'text','value'=>set_value('tgl_studi', ddmmyyyy($record->tgl_studi)));
							echo form_input($data);
							echo form_error('tgl_studi') ? form_error('tgl_studi', '<p class="help-block">','</p>') : '';
							?>
						</div>
					</div>

					<div class="col-md-6">
						<div class="form-group <?php echo form_error('produksi_tambang') ? 'has-error' : null; ?>">
							<?php
							echo form_label('Kapasitas Produksi Tambang Pertahun','produksi_tambang');
							$data = array('class'=>'form-control','name'=>'produksi_tambang','id'=>'produksi_tambang','type'=>'text','value'=>set_value('produksi_tambang', $record->produksi_tambang));
							echo form_input($data);
							echo form_error('produksi_tambang') ? form_error('produksi_tambang', '<p class="help-block">','</p>') : '';
							?>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group <?php echo form_error('produksi_pengolahan') ? 'has-error' : null; ?>">
							<?php
							echo form_label('Kapasitas Produksi Pengolahan Pertahun','produksi_pengolahan');
							$data = array('class'=>'form-control','name'=>'produksi_pengolahan','id'=>'produksi_pengolahan','type'=>'text','value'=>set_value('produksi_pengolahan', $record->produksi_pengolahan));
							echo form_input($data);
							echo form_error('produksi_pengolahan') ? form_error('produksi_pengolahan', '<p class="help-block">','</p>') : '';
							?>
						</div>
					</div>
					<div class="col-md-3">
						<div class="form-group <?php echo form_error('luas_hk') ? 'has-error' : null; ?>">
							<?php
							echo form_label('Luas Kawasan Hutan HK','luas_hk');
							$data = array('class'=>'form-control','name'=>'luas_hk','id'=>'luas_hk','type'=>'text','value'=>set_value('luas_hk', $record->luas_hk));
							echo form_input($data);
							echo form_error('luas_hk') ? form_error('luas_hk', '<p class="help-block">','</p>') : '';
							?>
						</div>
					</div>
					<div class="col-md-3">
						<div class="form-group <?php echo form_error('luas_hl') ? 'has-error' : null; ?>">
							<?php
							echo form_label('Luas Kawasan Hutan HL','luas_hl');
							$data = array('class'=>'form-control','name'=>'luas_hl','id'=>'luas_hl','type'=>'text','value'=>set_value('luas_hl', $record->luas_hl));
							echo form_input($data);
							echo form_error('luas_hl') ? form_error('luas_hl', '<p class="help-block">','</p>') : '';
							?>
						</div>
					</div>

					<div class="col-md-3">
						<div class="form-group <?php echo form_error('luas_hp') ? 'has-error' : null; ?>">
							<?php
							echo form_label('Luas Kawasan Hutan HP','luas_hp');
							$data = array('class'=>'form-control','name'=>'luas_hp','id'=>'luas_hp','type'=>'text','value'=>set_value('luas_hp', $record->luas_hp));
							echo form_input($data);
							echo form_error('luas_hp') ? form_error('luas_hp', '<p class="help-block">','</p>') : '';
							?>
						</div>
					</div>

					<div class="col-md-3">
						<div class="form-group <?php echo form_error('luas_bkh') ? 'has-error' : null; ?>">
							<?php
							echo form_label('Luas Bukan Kawasan Hutan','luas_bkh');
							$data = array('class'=>'form-control','name'=>'luas_bkh','id'=>'luas_bkh','type'=>'text','value'=>set_value('luas_bkh', $record->luas_bkh));
							echo form_input($data);
							echo form_error('luas_bkh') ? form_error('luas_bkh', '<p class="help-block">','</p>') : '';
							?>
						</div>
					</div>

					<div class="col-md-6">
						<div class="form-group <?php echo form_error('luas_total') ? 'has-error' : null; ?>">
							<?php
							echo form_label('Total Luas Wilayah','luas_total');
							$data = array('class'=>'form-control','name'=>'luas_total','id'=>'luas_total','type'=>'text','value'=>set_value('luas_total', $record->luas_total));
							echo form_input($data);
							echo form_error('luas_total') ? form_error('luas_total', '<p class="help-block">','</p>') : '';
							?>
						</div>
					</div>

					<div class="col-md-6">
						<div class="form-group <?php echo form_error('luas_project') ? 'has-error' : null; ?>">
							<?php
							echo form_label('Total Luas Project Area','luas_project');
							$data = array('class'=>'form-control','name'=>'luas_project','id'=>'luas_project','type'=>'text','value'=>set_value('luas_project', $record->luas_project));
							echo form_input($data);
							echo form_error('luas_project') ? form_error('luas_project', '<p class="help-block">','</p>') : '';
							?>
						</div>
					</div>

					<div class="col-md-6">
						<div class="form-group <?php echo form_error('luas_ippkh_produksi') ? 'has-error' : null; ?>">
							<?php
							echo form_label('Luas Wilayah PPPKH Untuk Operasi Produksi','luas_ippkh_produksi');
							$data = array('class'=>'form-control','name'=>'luas_ippkh_produksi','id'=>'luas_ippkh_produksi','type'=>'text','value'=>set_value('luas_ippkh_produksi', $record->luas_ippkh_produksi));
							echo form_input($data);
							echo form_error('luas_ippkh_produksi') ? form_error('luas_ippkh_produksi', '<p class="help-block">','</p>') : '';
							?>
						</div>
					</div>

					<div class="col-md-6">
						<div class="form-group <?php echo form_error('luas_ippkh_eksplorasi') ? 'has-error' : null; ?>">
							<?php
							echo form_label('Luas Wilayah PPPKH Untuk Eksplorasi Lanjutan','  luas_ippkh_eksplorasi  ');
							$data = array('class'=>'form-control','name'=>'luas_ippkh_eksplorasi','id'=>'luas_ippkh_eksplorasi','type'=>'text','value'=>set_value('luas_ippkh_eksplorasi', $record->luas_ippkh_eksplorasi));
							echo form_input($data);
							echo form_error('luas_ippkh_eksplorasi') ? form_error('luas_ippkh_eksplorasi', '<p class="help-block">','</p>') : '';
							?>
						</div>
					</div>
				</div>
			</div>
			<!-- ./box-body -->
			<div class="box-footer">
				<button type="button" class="btn btn-sm btn-flat btn-info" onclick="saveout();"><i class="fa fa-save"></i> Simpan & Keluar</button>
				<button type="reset" class="btn btn-sm btn-flat btn-warning"><i class="fa fa-refresh"></i> Reset</button>
				<button type="button" class="btn btn-sm btn-flat btn-danger" onclick="back();"><i class="fa fa-close"></i> Keluar</button>
			</div>
			</form>
		</div>
	</div>
</div>