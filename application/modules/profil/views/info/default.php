<div class="row">
	<div class="col-md-12">
		<div id="message"></div>
		<div class="box box-primary box-solid">
			<div class="box-header with-border">
				<h3 class="box-title"><?= isset($head) ? $head : ''; ?></h3>
				<div class="box-tools pull-right">
					<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
					<button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
				</div>
			</div>
			<!-- box-body -->
			<div class="box-body">
				<div class="row">
					<div class="col-md-12">
						<button class="btn btn-sm btn-flat btn-default" onclick="history.go(-1)" ><i class="fa fa-arrow-left"></i> Kembali</button>
						<span id="key" style="display: none;"><?= $this->security->get_csrf_hash(); ?></span>		
						<h3><?= $record->cabor; ?></h3>
					</div>

					<div class="col-md-6">
						<dl>
                     <dt>Kode Registrasi</dt>
                     <dd><?= $record->kode ? $record->kode : '-'; ?></dd>

                     <dt>Nama Pengurus Provinsi</dt>
							<dd><?= $record->cabor ? $record->cabor : '-'; ?></dd>

                     <dt>Kode</dt>
							<dd><?= $record->nickname ? $record->nickname : '-'; ?></dd>

                     <dt>Cabang Olahraga</dt>
							<dd><?php $record->id ? print_r(jenis_cabor($record->id)) : '-'; ?></dd>

							
                     
                     <!-- <dt>Jenjang cabor</dt>
                     <dd><?= $record->bentuk ? jenjang($record->bentuk) : '-'; ?></dd>
                     
                     <dt>Status cabor</dt>
							<dd><?= $record->status ? jenis($record->status) : '-'; ?></dd> -->

							<dt>Alamat Sekertariat</dt>
							<dd><?= $record->alamat ? $record->alamat : '-'; ?></dd>
                     <dt>Kota/Kabupaten</dt>
                     <dd><?= $record->kota_id ? kota($record->kota_id) : '-'; ?></dd>

               <dt>Kecamatan</dt>
              <dd><?= $record->kecamatan_id ? kecamatan($record->kecamatan_id) : '-'; ?></dd>

              <dt>Kelurahan</dt>
              <dd><?= $record->kelurahan_id ? kelurahan($record->kelurahan_id) : '-'; ?></dd>

							<dt>Telepon/HP</dt>
							<dd><?= $record->telpon ? $record->telpon : '-'; ?></dd>
              
						</dl>
               </div>
               <div class="col-md-6">
						<dl>
                     <dt>Data Ketua Cabang Olahraga</dt>
                     <?php if(kepala($record->id, $record->kode)): ?>
							<dd><?= kepala($record->id, $record->kode)->nama.'<br>'.kepala($record->id, $record->kode)->nip.'<br>'.kepala($record->id, $record->kode)->telpon; ?></dd>
                     <?php endif; ?>
                     <br>
                     <dt>Data Bendahara Cabang Olahraga</dt>
                     <?php if(bendahara($record->id, $record->kode)): ?>
							<dd><?= bendahara($record->id, $record->kode)->nama.'<br>'.bendahara($record->id, $record->kode)->nip.'<br>'.bendahara($record->id, $record->kode)->telpon; ?></dd>
                     <?php endif; ?>
                     <br>
                     <dt>Data Sekretaris Cabang Olahraga</dt>
                     <?php if(sekretaris($record->id, $record->kode)): ?>
							<dd><?= sekretaris($record->id, $record->kode)->nama.'<br>'.sekretaris($record->id, $record->kode)->nip.'<br>'.sekretaris($record->id, $record->kode)->telpon; ?></dd>
                     <?php endif; ?>
                     <br>
                     <dt>Dokumen/SK Pengurus </dt>
                     <?php if($record->dokumen): ?>
                     <dd><?= $record->dokumen ? '<a href="'.base_url('dokumen/'.$record->kode.'/'.$record->dokumen).'" target="_blank"><i class="fa fa-file-text"></i></a>': '-' ; ?></dd>
                     <?php endif; ?>
						</dl>
					</div>
				</div>
			</div>
			<!-- ./box-body -->
		</div>
	</div>
</div>

<div id="produksi-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
  <div class="modal-dialog modal-lg"> 
     <div class="modal-content">  
        <div class="modal-header"> 
           <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button> 
           <h4 class="modal-title">
           <i class="fa fa-calendar-o"></i> Rencana Produksi Tahunan
           </h4> 
        </div> 
        <div class="modal-body">                     
           <div id="modal-loader" style="display: none; text-align: center;">
           <!-- ajax loader -->
           <img src="<?= base_url('asset/loading.gif'); ?> ">
           </div>             
           <!-- mysql data will be load here -->                          
           <div id="dynamic-content"></div>
        </div>       
    </div> 
  </div>
</div>

<!-- operasi -->
<div id="operasi-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
  <div class="modal-dialog modal-lg"> 
     <div class="modal-content">  
        <div class="modal-header"> 
           <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button> 
           <h4 class="modal-title">
           <i class="fa fa-calendar-o"></i> Ijin Pinjam Kawasan Hutan (IPPKH) Untuk Operasi Produksi
           </h4> 
        </div> 
        <div class="modal-body">                     
           <div id="operasi-loader" style="display: none; text-align: center;">
           <!-- ajax loader -->
           <img src="<?= base_url('asset/loading.gif'); ?> ">
           </div>             
           <!-- mysql data will be load here -->                          
           <div id="operasi-content"></div>
        </div>       
    </div> 
  </div>
</div>

<!-- eksplorasi -->
<div id="eksplorasi-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
  <div class="modal-dialog modal-lg"> 
     <div class="modal-content">  
        <div class="modal-header"> 
           <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button> 
           <h4 class="modal-title">
           <i class="fa fa-calendar-o"></i> Ijin Pinjam Kawasan Hutan (IPPKH) Untuk Operasi Ekplorasi
           </h4> 
        </div> 
        <div class="modal-body">                     
           <div id="eksplorasi-loader" style="display: none; text-align: center;">
           <!-- ajax loader -->
           <img src="<?= base_url('asset/loading.gif'); ?> ">
           </div>             
           <!-- mysql data will be load here -->                          
           <div id="eksplorasi-content"></div>
        </div>       
    </div> 
  </div>
</div>

<!-- saham -->
<div id="saham-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
  <div class="modal-dialog modal-lg"> 
     <div class="modal-content">  
        <div class="modal-header"> 
           <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button> 
           <h4 class="modal-title">
           <i class="fa fa-calendar-o"></i> Pemegang Saham
           </h4> 
        </div> 
        <div class="modal-body">                     
           <div id="saham-loader" style="display: none; text-align: center;">
           <!-- ajax loader -->
           <img src="<?= base_url('asset/loading.gif'); ?> ">
           </div>             
           <!-- mysql data will be load here -->                          
           <div id="saham-content"></div>
        </div>       
    </div> 
  </div>
</div>

<!-- pengurus -->
<div id="pengurus-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
  <div class="modal-dialog modal-lg"> 
     <div class="modal-content">  
        <div class="modal-header"> 
           <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button> 
           <h4 class="modal-title">
           <i class="fa fa-calendar-o"></i> Susunan Pengurus dan NPWP Pengurus
           </h4> 
        </div> 
        <div class="modal-body">                     
           <div id="pengurus-loader" style="display: none; text-align: center;">
           <!-- ajax loader -->
           <img src="<?= base_url('asset/loading.gif'); ?> ">
           </div>             
           <!-- mysql data will be load here -->                          
           <div id="pengurus-content"></div>
        </div>       
    </div> 
  </div>
</div>