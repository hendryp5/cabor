<div class="row">
	<div class="col-md-12">
		<?php if($this->session->flashdata('flashconfirm')): ?>
		<div class="alert alert-success alert-dismissable">
		  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		  <i class="icon fa fa-check"></i> Sukses! <?php echo $this->session->flashdata('flashconfirm'); ?>.
		</div>
		<?php endif; ?>
		<?php if($this->session->flashdata('flasherror')): ?>
		<div class="alert alert-info alert-dismissable">
		  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		  <i class="icon fa fa-info"></i> Perhatian! <?php echo $this->session->flashdata('flasherror'); ?>.
		</div>
		<?php endif; ?>
		<div class="box box-primary box-solid">
			<div class="box-header with-border">
				<h3 class="box-title"><?= isset($head) ? $head : ''; ?></h3>
				<div class="box-tools pull-right">
					<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
					<button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
				</div>
			</div>
			<!-- box-body -->
			<div class="box-body">
			
			<div class="row">
			<?php if(group(array('1','2'))): ?>
				<div class="col-md-6 col-sm-12 col-xs-12">
				<div class="info-box bg-green">
					<span class="info-box-icon"><i class="fa fa-money"></i></span>

					<div class="info-box-content">
					<span class="info-box-text">Jumlah Anggaran Cabor <?= date('Y')+1; ?></span>
					<span class="info-box-number"><?= rupiah(cek_kuota(date('Y')+1)) ?></span>

					<div class="progress">
						<!-- <div class="progress-bar" style="width: 70%"></div> -->
					</div>
						<span class="progress-description">
							<!-- 70% Realisasi -->
						</span>
					</div>
					<!-- /.info-box-content -->
				</div>
				<!-- /.info-box -->
				</div>

				<div class="col-md-6 col-sm-12 col-xs-12">
				<div class="info-box bg-yellow">
					<span class="info-box-icon"><i class="fa fa-users"></i></span>

					<div class="info-box-content">
					<span class="info-box-text"> Jumlah Cabor Penerima <?= date('Y')+1; ?></span>
					<span class="info-box-number"><?= count(jumlah_cabor(date('Y')+1)) ?></span>

					<div class="progress">
						<!-- <div class="progress-bar" style="width: 70%"></div> -->
					</div>
						<span class="progress-description">
							<!-- 70% Realisasi -->
						</span>
					</div>
					<!-- /.info-box-content -->
				</div>
				<!-- /.info-box -->
				</div>

				<div class="col-md-6 col-sm-12 col-xs-12">
				<div class="info-box bg-red">
					<span class="info-box-icon"><i class="fa fa-hourglass"></i></span>

					<div class="info-box-content">
					<span class="info-box-text">Jumlah Draf Rencana Anggaran <?= date('Y')+1; ?></span>
					<span class="info-box-number"><?= rupiah(jumlah_draf_rencana(date('Y')+1)) ?></span>

					<div class="progress">
						<!-- <div class="progress-bar" style="width: 70%"></div> -->
					</div>
						<span class="progress-description">
							<!-- 70% Realisasi -->
						</span>
					</div>
					<!-- /.info-box-content -->
				</div>
				<!-- /.info-box -->
				</div>

				<div class="col-md-6 col-sm-12 col-xs-12">
				<div class="info-box bg-aqua">
					<span class="info-box-icon"><i class="fa fa-check-square"></i></span>

					<div class="info-box-content">
					<span class="info-box-text">Jumlah Pengajuan Anggaran <?= date('Y')+1; ?></span>
					<span class="info-box-number"><?= rupiah(jumlah_pengajuan_rencana(date('Y')+1)) ?></span>

					<div class="progress">
						<!-- <div class="progress-bar" style="width: 70%"></div> -->
					</div>
						<span class="progress-description">
							<!-- 70% Realisasi -->
						</span>
					</div>
					<!-- /.info-box-content -->
				</div>
				<!-- /.info-box -->
				</div>

				<div class="col-md-6 col-sm-12 col-xs-12">
				<div class="info-box bg-purple">
					<span class="info-box-icon"><i class="fa fa-check-square"></i></span>

					<div class="info-box-content">
					<span class="info-box-text">Jumlah Persetujuan Anggaran <?= date('Y')+1; ?></span>
					<span class="info-box-number"><?= rupiah(jumlah_setuju_rencana(date('Y')+1)) ?></span>

					<div class="progress">
						<!-- <div class="progress-bar" style="width: 70%"></div> -->
					</div>
						<span class="progress-description">
							<!-- 70% Realisasi -->
						</span>
					</div>
					<!-- /.info-box-content -->
				</div>
				<!-- /.info-box -->
				</div>

				<div class="col-md-6 col-sm-12 col-xs-12">
				<div class="info-box bg-gray">
					<span class="info-box-icon"><i class="fa fa-money"></i></span>

					<div class="info-box-content">
					<span class="info-box-text">Jumlah Realisasi Anggaran <?= date('Y')+1; ?></span>
					<span class="info-box-number"><?= rupiah(0); ?></span>

					<div class="progress">
						<!-- <div class="progress-bar" style="width: 70%"></div> -->
					</div>
						<span class="progress-description">
							<!-- 70% Realisasi -->
						</span>
					</div>
					<!-- /.info-box-content -->
				</div>
				<!-- /.info-box -->
				</div>

				<div class="col-md-3 col-sm-6 col-xs-12">
				<div class="info-box bg-aqua">
					<span class="info-box-icon"><i class="fa fa-list"></i></span>

					<div class="info-box-content">
					<span class="info-box-text">Draf <?= date('Y')+1; ?></span>
					<span class="info-box-number"><?= jumlah_status_pengajuan(date('Y')+1, 1); ?></span>

					<div class="progress">
						<!-- <div class="progress-bar" style="width: 70%"></div> -->
					</div>
						<span class="progress-description">
							<!-- 70% Realisasi -->
						</span>
					</div>
					<!-- /.info-box-content -->
				</div>
				<!-- /.info-box -->
				</div>

				<div class="col-md-3 col-sm-6 col-xs-12">
				<div class="info-box bg-blue">
					<span class="info-box-icon"><i class="fa fa-list"></i></span>

					<div class="info-box-content">
					<span class="info-box-text">Pengajuan <?= date('Y')+1; ?></span>
					<span class="info-box-number"><?= jumlah_status_pengajuan(date('Y')+1, 2); ?></span>

					<div class="progress">
						<!-- <div class="progress-bar" style="width: 70%"></div> -->
					</div>
						<span class="progress-description">
							<!-- 70% Realisasi -->
						</span>
					</div>
					<!-- /.info-box-content -->
				</div>
				<!-- /.info-box -->
				</div>

				<div class="col-md-3 col-sm-6 col-xs-12">
				<div class="info-box bg-green">
					<span class="info-box-icon"><i class="fa fa-list"></i></span>

					<div class="info-box-content">
					<span class="info-box-text">Setuju <?= date('Y')+1; ?></span>
					<span class="info-box-number"><?= jumlah_status_pengajuan(date('Y')+1, 3); ?></span>

					<div class="progress">
						<!-- <div class="progress-bar" style="width: 70%"></div> -->
					</div>
						<span class="progress-description">
							<!-- 70% Realisasi -->
						</span>
					</div>
					<!-- /.info-box-content -->
				</div>
				<!-- /.info-box -->
				</div>

				<div class="col-md-3 col-sm-6 col-xs-12">
				<div class="info-box bg-red">
					<span class="info-box-icon"><i class="fa fa-list"></i></span>

					<div class="info-box-content">
					<span class="info-box-text">Tolak <?= date('Y')+1; ?></span>
					<span class="info-box-number"><?= jumlah_status_pengajuan(date('Y')+1, 4); ?></span>

					<div class="progress">
						<!-- <div class="progress-bar" style="width: 70%"></div> -->
					</div>
						<span class="progress-description">
							<!-- 70% Realisasi -->
						</span>
					</div>
					<!-- /.info-box-content -->
				</div>
				<!-- /.info-box -->
				</div>
			<?php else: ?>
				
				<div class="col-md-6 col-sm-12 col-xs-12">
				<div class="info-box bg-green">
					<span class="info-box-icon"><i class="fa fa-money"></i></span>

					<div class="info-box-content">
					<span class="info-box-text">Jumlah Kuota Anggaran Cabor <?= date('Y')+1; ?></span>
					<span class="info-box-number"><?= rupiah(cek_kuota_cabor(date('Y')+1)) ?></span>

					<div class="progress">
						<!-- <div class="progress-bar" style="width: 70%"></div> -->
					</div>
						<span class="progress-description">
							<!-- 70% Realisasi -->
						</span>
					</div>
					<!-- /.info-box-content -->
				</div>
				<!-- /.info-box -->
				</div>

				<div class="col-md-6 col-sm-12 col-xs-12">
				<div class="info-box bg-yellow">
					<span class="info-box-icon"><i class="fa fa-money"></i></span>

					<div class="info-box-content">
					<span class="info-box-text">Jumlah Realisasi Anggaran <?= date('Y')+1; ?></span>
					<span class="info-box-number"><?= rupiah(0) ?></span>

					<div class="progress">
						<!-- <div class="progress-bar" style="width: 70%"></div> -->
					</div>
						<span class="progress-description">
							<!-- 70% Realisasi -->
						</span>
					</div>
					<!-- /.info-box-content -->
				</div>
				<!-- /.info-box -->
				</div>

				<?php if(daftar_kuota_cabor(date('Y')+1)): ?>
				<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="box box-solid bg-blue-gradient">
					<div class="box-body no-padding">
					<div class="table-responsive">
						<table class="table no-margin">
						<thead>
						<tr>
							<th>ID</th>
							<th>CABANG OLAHRAGA</th>
							<th>TAHUN</th>
							<th>KUOTA ANGGARAN</th>
							<th>RENCANA ANGGARAN</th>
						</tr>
						</thead>
						<tbody>
						<?php $no = 1; ?>
						<?php foreach(daftar_kuota_cabor(date('Y')+1) as $row): ?>
						<tr>
							<td><?= $no; ?></td>
							<td><?= $row->cabor; ?></td>
							<td><?= $row->tahun; ?></td>
							<td><?= $row->jumlah ? rupiah($row->jumlah) : rupiah(0); ?></td>
							<td><?= rupiah(0); ?></td>
						</tr>
						<?php $no++; ?>
						<?php endforeach; ?>
						
						</tbody>
						</table>
					</div>
					<!-- /.table-responsive -->
					</div>
					<!-- /.box-body -->
				</div>
				</div>
				<?php endif; ?>

				
			<?php endif; ?>
			</div>

			<?php if($record): ?>
			<?php foreach($record as $row): ?>
			<div class="row">
				<div class="col-md-12">
				<div class="alert alert-dismissible bg-gray">
				<h4><i class="icon fa fa-file-text-o"></i> <?= $row->judul; ?></h4>
				<?= $row->informasi; ?>
				</div>	
				</div>
			</div>
			<?php endforeach; ?>
			<?php endif; ?>
			</div>
			<!-- ./box-body -->
			
			<!-- <div class="box-footer">
				<div class="row">
					<div class="col-md-12">
						<!-- <?php //if($record): ?>
						<?php
							// $list = array();
							// $i = 1;
							// foreach($record as $option){
							// 	$visis = $option['visi'];
							// 	$misis = $option['misi'];
							// 	$tujuans = $option['tujuan'];
							// 	$sasarans = $option['sasaran'];
							// 	$list[$visis][$misis][$tujuans][$i] = $sasarans;
							// 	$i++;
							//}
						?>
						<?php
							// $all= array_chunk($list, 1, TRUE);
							// //var_dump($list);
							// foreach($all as $a){
						?> -->
							<!-- <ul>
								<?php
								// foreach($a as $b => $c){
								// 	echo '<p><b>'.$b.'</b></p>';
								// 	foreach($c as $d => $e){
								// 		echo '<dt><u>'.$d.'</u></dt>';
								// 		foreach($e as $f => $g){
								// 			echo '<dd><i>'.$f.'</i></dd>';
								// 			foreach($g as $h => $i){
								// 				echo '<li>'.$i.'</li>';
								// 			}
								// 		}
								// 	}
								// }
								// ?>
							</ul>
						<?php
							//}
						?> -->
						<?php //endif; ?>
					<!-- </div>
				</div>
			</div> -->
		</div>
	</div>
</div>