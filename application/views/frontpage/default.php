<!DOCTYPE html>
<html  >
<head>
<!-- Site made with Mobirise Website Builder v4.10.10, https://mobirise.com -->
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="generator" content="">
<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1">
<link rel="shortcut icon" href="<?= base_url('asset/theme/images/banjar-122x122.png'); ?>" type="image/x-icon">
<meta name="description" content="">
<title>BAKULKAS DISDIK KOTA BANJARMASIN</title>
<link rel="preload" as="style" href="<?= base_url('asset/theme/web/assets/mobirise-icons/mobirise-icons.css'); ?>">
<link rel="stylesheet" href="<?= base_url('asset/theme/web/assets/mobirise-icons/mobirise-icons.css'); ?>">
<link rel="preload" as="style" href="<?= base_url('asset/theme/bootstrap/css/bootstrap.min.css'); ?>">
<link rel="stylesheet" href="<?= base_url('asset/theme/bootstrap/css/bootstrap.min.css'); ?>">
<link rel="preload" as="style" href="<?= base_url('asset/theme/bootstrap/css/bootstrap-grid.min.css'); ?>">
<link rel="stylesheet" href="<?= base_url('asset/theme/bootstrap/css/bootstrap-grid.min.css'); ?>">
<link rel="preload" as="style" href="<?= base_url('asset/theme/bootstrap/css/bootstrap-reboot.min.css'); ?>">
<link rel="stylesheet" href="<?= base_url('asset/theme/bootstrap/css/bootstrap-reboot.min.css'); ?>">
<link rel="stylesheet" href="<?= base_url('asset/theme/socicon/css/styles.css'); ?>">
<link rel="stylesheet" href="<?= base_url('asset/theme/tether/tether.min.css'); ?>">
<link rel="stylesheet" href="<?= base_url('asset/theme/dropdown/css/style.css'); ?>">
<link rel="stylesheet" href="<?= base_url('asset/theme/theme/css/style.css'); ?>">
<link rel="preload" as="style" href="<?= base_url('asset/theme/mobirise/css/mbr-additional.css'); ?>">
<link rel="stylesheet" href="<?= base_url('asset/theme/mobirise/css/mbr-additional.css'); ?>" type="text/css">
</head>
<body>
  <section class="menu cid-rKd9txO68b" once="menu" id="menu2-7">
    <nav class="navbar navbar-expand beta-menu navbar-dropdown align-items-center navbar-fixed-top navbar-toggleable-sm">
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <div class="hamburger">
                <span></span>
                <span></span>
                <span></span>
                <span></span>
            </div>
        </button>
        <div class="menu-logo">
            <div class="navbar-brand">
                <span class="navbar-logo">
                    <a href="<?= site_url(); ?>">
                        <img src="<?= base_url('asset/theme/images/banjar-122x122.png'); ?>" alt="Mobirise" title="" style="height: 3.8rem;">
                    </a>
                </span>
                <span class="navbar-caption-wrap"><a class="navbar-caption text-white display-4" href="<?= site_url(); ?>">
                        BAKULKAS DISDIK</a></span>
            </div>
        </div>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav nav-dropdown nav-right" data-app-modern-menu="true"><li class="nav-item">
                    <a class="nav-link link text-white display-4" href="<?= site_url(); ?>">Beranda</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link link text-white display-4" href="<?= site_url('home/kontak'); ?>">
                        Kontak</a>
                </li><li class="nav-item"><a class="nav-link link text-white display-4" href="<?= site_url('login'); ?>">
                        Login</a></li></ul>
            
        </div>
    </nav>
</section>

<section class="engine"><a href=""></a></section><section class="carousel slide cid-rKd57fe8UP" data-interval="false" id="slider1-0">
    <div class="full-screen"><div class="mbr-slider slide carousel" data-pause="true" data-keyboard="false" data-ride="carousel" data-interval="4000"><div class="carousel-inner" role="listbox"><div class="carousel-item slider-fullscreen-image active" data-bg-video-slide="false" style="background-image: url(<?= base_url('asset/theme/images/danabos-1-1028x490.png'); ?>);"><div class="container container-slide"><div class="image_wrapper"><img src="<?= base_url('asset/theme/images/danabos-1-1028x490.png'); ?>"><div class="carousel-caption justify-content-center"><div class="col-10 align-center"></div></div></div></div></div><div class="carousel-item slider-fullscreen-image" data-bg-video-slide="false" style="background-image: url(<?= base_url('asset/theme/images/bos1-1-1028x490.png'); ?>);"><div class="container container-slide"><div class="image_wrapper"><img src="<?= base_url('asset/theme/images/bos1-1-1028x490.png'); ?>"><div class="carousel-caption justify-content-center"><div class="col-10 align-center"></div></div></div></div></div><div class="carousel-item slider-fullscreen-image" data-bg-video-slide="false" style="background-image: url(<?= base_url('asset/theme/images/bos2-1028x490.png'); ?>);"><div class="container container-slide"><div class="image_wrapper"><img src="<?= base_url('asset/theme/images/bos2-1028x490.png'); ?>"><div class="carousel-caption justify-content-center"><div class="col-10 align-center"></div></div></div></div></div></div><a data-app-prevent-settings="" class="carousel-control carousel-control-prev" role="button" data-slide="prev" href="#slider1-0"><span aria-hidden="true" class="mbri-left mbr-iconfont"></span><span class="sr-only">Previous</span></a><a data-app-prevent-settings="" class="carousel-control carousel-control-next" role="button" data-slide="next" href="#slider1-0"><span aria-hidden="true" class="mbri-right mbr-iconfont"></span><span class="sr-only">Next</span></a></div></div>
</section>

<section class="mbr-section content4 cid-rKd5iRQYQZ" id="content4-2">
    <div class="container">
        <div class="media-container-row">
            <div class="title col-12 col-md-8">
                <h2 class="align-center pb-3 mbr-fonts-style display-2">
                    INFORMASI BAKULKAS</h2>
            </div>
        </div>
    </div>
</section>

<section class="mbr-section article content1 cid-rKd5kH1bnT" id="content1-4">
    <div class="container">
        <div class="media-container-row">
            <div class="mbr-text col-12 mbr-fonts-style display-7 col-md-10">
                <?= $this->load->view($content); ?>
            </div>
        </div>
    </div>
</section>
<section class="map1 cid-rKddKQn4rU" id="map1-8">
    <div class="google-map"><iframe frameborder="0" style="border:0" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3983.133760084042!2d114.59183271475767!3d-3.317096797585228!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2de423c0b6476a13%3A0xffabd70f99bb6f35!2sDinas%20Pendidikan%20Kota%20Banjarmasin!5e0!3m2!1sid!2sid!4v1575943992472!5m2!1sid!2sid" allowfullscreen=""></iframe></div>
</section>

<section once="footers" class="cid-rKd5ySQRrF" id="footer7-6">
    <div class="container">
        <div class="media-container-row align-center mbr-white">
            <div class="row row-links">
                <ul class="foot-menu">
                <li class="foot-menu-item mbr-fonts-style display-7"><a href="#top">Beranda</a></li><li class="foot-menu-item mbr-fonts-style display-7">Kontak</li><li class="foot-menu-item mbr-fonts-style display-7">Login</li></ul>
            </div>
            
            <div class="row row-copirayt">
                <p class="mbr-text mb-0 mbr-fonts-style mbr-white align-center display-7">
                    © Dinas Pendidikan Kota Banjarmasin - All Rights Reserved
                </p>
            </div>
        </div>
    </div>
</section>


  <script src="<?= base_url('asset/theme/web/assets/jquery/jquery.min.js'); ?>"></script>
  <script src="<?= base_url('asset/theme/popper/popper.min.js'); ?>"></script>
  <script src="<?= base_url('asset/theme/bootstrap/js/bootstrap.min.js'); ?>"></script>
  <script async src="<?= base_url('asset/theme/smoothscroll/smooth-scroll.js'); ?>"></script>
  <script async src="<?= base_url('asset/theme/ytplayer/jquery.mb.ytplayer.min.js'); ?>"></script>
  <script async src="<?= base_url('asset/theme/tether/tether.min.js'); ?>"></script>
  <script async src="<?= base_url('asset/theme/touchswipe/jquery.touch-swipe.min.js'); ?>"></script>
  <script async src="<?= base_url('asset/theme/bootstrapcarouselswipe/bootstrap-carousel-swipe.js'); ?>"></script>
  <script async src="<?= base_url('asset/theme/dropdown/js/nav-dropdown.js'); ?>"></script>
  <script async src="<?= base_url('asset/theme/dropdown/js/navbar-dropdown.js'); ?>"></script>
  <script async src="<?= base_url('asset/theme/vimeoplayer/jquery.mb.vimeo_player.js'); ?>"></script>
  <script async src="<?= base_url('asset/theme/theme/js/script.js'); ?>"></script>
  <script async src="<?= base_url('asset/theme/slidervideo/script.js'); ?>"></script>
</body>
</html>