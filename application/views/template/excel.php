<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title><?= isset($title) ? $title : 'SEGI 3 CARAKA | DINAS PEMUDA DAN OLAHRAGA PROVINSI KALIMANTAN SELATAN'; ?></title>
	<style type="text/css">
	body{
		font-family: sans-serif;
	}
	table{
		margin: 20px auto;
		border-collapse: collapse;
	}
	table th,
	table td{
		border: 1px solid #3c3c3c;
		padding: 3px 8px;
 
	}
	a{
		background: blue;
		color: #fff;
		padding: 8px 10px;
		text-decoration: none;
		border-radius: 2px;
	}
  .no-print, .no-print *
  {
      display: none !important;
  }
	</style>
</head>
<!-- ADD THE CLASS layout-top-nav TO REMOVE THE SIDEBAR. -->
<body>
<?php
  $file = isset($namefile) ? $namefile : 'RekapAnggaran';
	header("Content-type: application/vnd-ms-excel");
	header("Content-Disposition: attachment; filename=".$file.".xls");
	?>
<?= isset($content) ? $this->load->view($content) : ''; ?>
</body>
</html>
