<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title><?= isset($title) ? $title : 'SEGI 3 CARAKA | DINAS PEMUDA DAN OLAHRAGA PROVINSI KALIMANTAN SELATAN'; ?></title>
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <link rel="stylesheet" href="<?= base_url('asset/bootstrap/css/bootstrap.min.css'); ?>">
  <link rel="stylesheet" href="<?= base_url('asset/plugins/select2/select2.min.css'); ?>" />
  <link rel="stylesheet" href="<?= base_url('asset/plugins/datatables/dataTables.bootstrap.css'); ?>">
	<link rel="stylesheet" href="<?= base_url('asset/plugins/datepicker/datepicker3.css'); ?>" />
  <link rel="stylesheet" href="<?= base_url('asset/font-awesome/css/font-awesome.min.css'); ?>">
  <link rel="stylesheet" href="<?= base_url('asset/ionicons/css/ionicons.min.css'); ?>">
  <link rel="stylesheet" href="<?= base_url('asset/dist/css/AdminLTE.min.css'); ?>">
  <link rel="stylesheet" href="<?= base_url('asset/dist/css/skins/_all-skins.min.css'); ?>">
  <!-- primitive diagram -->
	<link rel="apple-touch-icon" sizes="57x57" href="<?php echo base_url(); ?>asset/images/apple-icon-57x57.png">
  <link rel="apple-touch-icon" sizes="60x60" href="<?php echo base_url(); ?>asset/images/apple-icon-60x60.png">
  <link rel="apple-touch-icon" sizes="72x72" href="<?php echo base_url(); ?>asset/images/apple-icon-72x72.png">
  <link rel="apple-touch-icon" sizes="76x76" href="<?php echo base_url(); ?>asset/images/apple-icon-76x76.png">
  <link rel="apple-touch-icon" sizes="114x114" href="<?php echo base_url(); ?>asset/images/apple-icon-114x114.png">
  <link rel="apple-touch-icon" sizes="120x120" href="<?php echo base_url(); ?>asset/images/apple-icon-120x120.png">
  <link rel="apple-touch-icon" sizes="144x144" href="<?php echo base_url(); ?>asset/images/apple-icon-144x144.png">
  <link rel="apple-touch-icon" sizes="152x152" href="<?php echo base_url(); ?>asset/images/apple-icon-152x152.png">
  <link rel="apple-touch-icon" sizes="180x180" href="<?php echo base_url(); ?>asset/images/apple-icon-180x180.png">
  <link rel="icon" type="image/png" sizes="192x192"  href="<?php echo base_url(); ?>asset/images/android-icon-192x192.png">
  <link rel="icon" type="image/png" sizes="32x32" href="<?php echo base_url(); ?>asset/images/favicon-32x32.png">
  <link rel="icon" type="image/png" sizes="96x96" href="<?php echo base_url(); ?>asset/images/favicon-96x96.png">
  <link rel="icon" type="image/png" sizes="16x16" href="<?php echo base_url(); ?>asset/images/favicon-16x16.png">
  <link rel="manifest" href="<?php echo base_url(); ?>asset/images/manifest.json">
  <meta name="msapplication-TileColor" content="#ffffff">
  <meta name="msapplication-TileImage" content="<?php echo base_url(); ?>asset/images/ms-icon-144x144.png">
  <meta name="theme-color" content="#ffffff">
	<?= isset($style) ? $this->load->view($style) : ''; ?>
	<!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
	<style>body{font-size: 11px;}.nav-tabs-custom>.nav-tabs>li.active {border-top-color: #00a65a !important;}@media(min-width: 1024px){.main-header{top:0;left: 0;position: fixed;right: 0;z-index: 999;}.content-wrapper{padding-top:50px; padding-bottom:50px;}}.print{font-size: 9px;}.main-footer{bottom:0;left: 0;position: fixed;right: 0;z-index: 999;}.besar{text-transform:uppercase;}.content-wrapper{background-color: #eaeaea;}</style>
</head>
<!-- ADD THE CLASS layout-top-nav TO REMOVE THE SIDEBAR. -->
<body class="hold-transition skin-blue layout-top-nav">
<div class="wrapper">
  <header class="main-header">
    <nav class="navbar navbar-static-top">
      <div class="container">
        <div class="navbar-header">
          <a href="#" class="navbar-brand"><b>SEGI 3 CARAKA</b></a>
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse">
            <i class="fa fa-bars"></i>
          </button>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse pull-left" id="navbar-collapse">
          <ul class="nav navbar-nav">
            <li><a href="<?= site_url('dashboard'); ?>"><i class="fa fa-dashboard"></i> Beranda</a></li>
            
            <?php if(group(array('3'))): ?>
            <li><a href="<?= site_url('profil'); ?>"><i class="fa fa-bank"></i> Profil Cabang Olahraga</a></li>
            <li><a href="<?= site_url('rkas/kertas'); ?>"><i class="fa fa-money"></i> Rencana Anggaran</a></li>
            <li><a href="<?= site_url('rkas/kertas/realisasi'); ?>"><i class="fa fa-cart-plus"></i> Realisasi Anggaran</a></li>
            <?php endif; ?>

           <?php if(group(array('1','2'))): ?>
            <li class="dropdown">
              <a href="" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-bank"></i> Pengurus Provinsi <span class="caret"></span></a>
              <ul class="dropdown-menu" role="menu">
                <li><a href="<?= site_url('data/administrasiCabor'); ?>"><i class="fa fa-chevron-right"></i> Data Profil Pengprov</a></li>
                <li><a href="<?= site_url('data/registrasiCabor'); ?>"><i class="fa fa-chevron-right"></i> Data Registrasi Pengprov</a></li>
                <li class="divider"></li>
                <li><a href="<?= site_url('data/platformAnggaran'); ?>"><i class="fa fa-chevron-right"></i> Data Kuota Anggaran Cabor</a></li>
              </ul>
            </li>

              <li><a href="<?= site_url('data/pengajuanRencana'); ?>"><i class="fa fa-th-list"></i> Daftar Rencana Anggaran</a></li>            
            <?php endif; ?>
            
		
          <?php if(group(array('1','2'))): ?>
            <li class="dropdown">
              <a href="" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-folder"></i> Data Master <span class="caret"></span></a>
              <ul class="dropdown-menu" role="menu">
              <li><a href="<?= site_url('master/jenis_cabor'); ?>"><i class="fa fa-chevron-right"></i> Master Cabang Olahraga</a></li>
              <li class="divider"></li>
              <li><a href="<?= site_url('master/program'); ?>"><i class="fa fa-chevron-right"></i> Master Program Anggaran</a></li>
              <li><a href="<?= site_url('master/kegiatan'); ?>"><i class="fa fa-chevron-right"></i> Master Kegiatan Anggaran</a></li>
              <li><a href="<?= site_url('master/kategori'); ?>"><i class="fa fa-chevron-right"></i> Master Kategori Anggaran</a></li>  
              <li><a href="<?= site_url('master/belanja'); ?>"><i class="fa fa-chevron-right"></i> Master Belanja Anggaran</a></li>
              <li><a href="<?= site_url('master/template_nomenklatur'); ?>"><i class="fa fa-chevron-right"></i> Master Template Anggaran</a></li>
              <li class="divider"></li>
              <li><a href="<?= site_url('master/cabor'); ?>"><i class="fa fa-chevron-right"></i> Registrasi Pengurus Provinsi</a></li>  
              <li><a href="<?= site_url('master/kuota'); ?>"><i class="fa fa-chevron-right"></i> Kuota Anggaran Cabang Olahraga</a></li>  
              <li class="divider"></li>
              <li><a href="<?= site_url('setting/user'); ?>"><i class="fa fa-chevron-right"></i> Pengaturan Pengguna</a></li>
              <li class="divider"></li>
              <li><a href="<?= site_url('setting/backup'); ?>"><i class="fa fa-chevron-right"></i> Backup Database</a></li>
              </ul>
            </li>
            <?php endif; ?>

            <?php if(group(array('1','2'))): ?>
            <li class="dropdown">
              <a href="" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-print"></i> Laporan <span class="caret"></span></a>
              <ul class="dropdown-menu" role="menu">
                <li><a href=""><i class="fa fa-file-text"></i> Daftar Cabang Olahraga</a></li>
                <li class="divider"></li>
                <li><a href=""><i class="fa fa-file-text"></i> Daftar Pengajuan Anggaran</a></li>
                <li><a href=""><i class="fa fa-file-text"></i> Daftar Rencana Anggaran</a></li>
                <li><a href=""><i class="fa fa-file-text"></i> Daftar Realisasi Anggaran</a></li>
              </ul>
            </li>
            <li><a href="<?= site_url('setting/informasi'); ?>"><i class="fa fa-newspaper-o"></i> Informasi</a></li>
            <?php endif; ?>

            <?php if(group(array('3'))): ?>
            <li class="dropdown">
              <a href="" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-print"></i> Laporan <span class="caret"></span></a>
              <ul class="dropdown-menu" role="menu">
                <li><a href=""><i class="fa fa-file-text"></i> Profil Cabang Olahraga</a></li>
                <li class="divider"></li>
                <li><a href=""><i class="fa fa-file-text"></i> Rekap Rencana Anggaran</a></li>
                <li><a href=""><i class="fa fa-file-text"></i> Rekap Realisasi Anggaran</a></li>
              </ul>
            </li>
            <?php endif; ?>

            <li><a href="<?= site_url('logout'); ?>"><i class="fa fa-sign-out"></i> Keluar</a></li>
          </ul>
        </div>
        <!-- /.navbar-collapse -->
        <!-- Navbar Right Menu -->
        <div class="navbar-custom-menu">
          <ul class="nav navbar-nav">
						<!-- User Account Menu -->
            <li class="dropdown user user-menu">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                <img src="<?= base_url('asset/dist/img/avatar5.png'); ?>" class="user-image" alt="User Image">
                <span class="hidden-xs">&nbsp;</span>
              </a>
              <ul class="dropdown-menu">
                <li class="user-header">
                  <img src="<?= base_url('asset/dist/img/avatar5.png'); ?>" class="img-circle" alt="User Image">
								  <p>
                    SEGI 3 CARAKA
                    <small>Versi 1.0 2020</small>
                  </p>
                </li>
                <!-- Menu Body -->
                <!-- Menu Footer-->
                <li class="user-footer">
                  <div class="pull-left">
                    <a href="<?= site_url('setting/password/updated/'.$this->session->userdata('userid')); ?>" class="btn btn-default btn-flat"><i class="fa fa-key"></i> Ganti Password</a>
                  </div>
									<div class="pull-right">
                    <a href="<?= site_url('logout') ?>" class="btn btn-default btn-flat"><i class="fa fa-sign-out"></i> Keluar</a>
                  </div>
                </li>
              </ul>
            </li>
          </ul>
        </div>
      </div>
    </nav>
  </header>
  <!-- Full Width Column -->
  <div class="content-wrapper">
    <div class="container">
      <!-- Content Header (Page header) -->
      <section class="content-header">
        <!-- <h1>E-PELAPORAN<small>Beta 1.0</small></h1> -->
        <!-- <ol class="breadcrumb">
          <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        </ol> -->
      </section>

      <!-- Main content -->
      <section class="content">
        <?= isset($content) ? $this->load->view($content) : ''; ?>
      </section>
      <!-- /.content -->
    </div>
  </div>
  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <div class="container">
      <div class="pull-right hidden-xs">
        <b>Version</b> 1.0
      </div>
      <strong>Copyright &copy; 2020 <a href="#">SEGI 3 CARAKA</a>.</strong> All rights
      reserved.
    </div>
  </footer>
</div>
<!-- ./wrapper -->
<script src="<?= base_url('asset/plugins/jQuery/jquery-2.2.3.min.js'); ?>"></script>
<script src="<?= base_url('asset/bootstrap/js/bootstrap.min.js'); ?>"></script>
<script src="<?= base_url('asset/plugins/slimScroll/jquery.slimscroll.min.js'); ?>"></script>
<script src="<?= base_url('asset/plugins/fastclick/fastclick.js'); ?>"></script>
<script src="<?= base_url('asset/plugins/tableexport/jquery.min.js'); ?>"></script>
<script src="<?= base_url('asset/plugins/datatables/jquery.dataTables.min.js'); ?>"></script>
<script src="<?= base_url('asset/plugins/datatables/dataTables.bootstrap.min.js'); ?>"></script>
<script src="<?= base_url('asset/plugins/select2/select2.full.min.js'); ?>"></script>
<script src="<?= base_url('asset/plugins/datepicker/bootstrap-datepicker.js'); ?>"></script>
<script src="<?= base_url('asset/plugins/datepicker/locales/bootstrap-datepicker.id.js'); ?>"></script>
<script src="<?= base_url('asset/plugins/input-mask/jquery.inputmask.js'); ?>"></script>
<script src="<?= base_url('asset/plugins/input-mask/jquery.inputmask.date.extensions.js'); ?>"></script>
<script src="<?= base_url('asset/plugins/number/jquery.number.min.js'); ?>"></script>
<script src="<?= base_url('asset/app.min.js'); ?>"></script>
<?= isset($js) ? $this->load->view($js) : ''; ?>
</body>
</html>
