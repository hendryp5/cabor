<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

function helper_log($tipe = "", $str = ""){
    $CI =& get_instance();
 
    if (strtolower($tipe) == "login"){
        $log_tipe   = 1;
    }
    elseif(strtolower($tipe) == "logout")
    {
        $log_tipe   = 0;
    }
    elseif(strtolower($tipe) == "add"){
        $log_tipe   = 2;
    }
    elseif(strtolower($tipe) == "edit"){
        $log_tipe  = 3;
    }
    elseif(strtolower($tipe) == "trash"){
        $log_tipe  = 4;
    }
	
    elseif(strtolower($tipe) == "restore"){
        $log_tipe  = 5;
    }
    else{
        $log_tipe  = 6;
    }
    // paramter
    $param['log_user']      = $CI->session->userdata('userID');
    $param['log_tipe']      = $log_tipe;
    $param['log_desc']      = $str;
	$param['log_ip']        = $CI->input->ip_address();
    //load model log
    $CI->load->model('m_log');
    //save to database
    $CI->m_log->save_log($param);
}

function indikator_log($tipe = "", $bfr = "", $aftr=""){
    $CI =& get_instance();
 
    if (strtolower($tipe) == "add"){
        $log_tipe   = 1;
    }
    elseif(strtolower($tipe) == "edit"){
        $log_tipe  = 2;
    }
    elseif(strtolower($tipe) == "trash"){
        $log_tipe  = 3;
    }
    else{
        $log_tipe  = 4;
    }
    // paramter
    $param['log_user_id']     = $CI->session->userdata('userid');
    $param['log_satker']      = $CI->session->userdata('satker');
    $param['log_before']      = $bfr;
    $param['log_after']       = $aftr;
    $param['log_tipe']        = $log_tipe;
    $param['log_ip']          = $CI->input->ip_address();
    //load model log
    $CI->load->model('m_log');
    //save to database
    $CI->m_log->indikator_log($param);
}

function level($id=null)
{
	if($id == 1){
		$level = 'Administrator';
	}elseif($id == 2){
		$level = 'Manager';
	}elseif($id == 3){
		$level = 'Cabor';
	}else{
		$level = 'Unknown';
	}
	
	return $level;
}

function signin()
{
	$CI =& get_instance();
	if(!$CI->session->userdata('signin')){
		$CI->session->set_flashdata('flasherror','Anda Harus Login Terlebih Dahulu.');
		redirect('login');
	}
}

function admin()
{
	$CI =& get_instance();
	if(!$CI->session->userdata('signin')){
		$CI->session->set_flashdata('flasherror','Anda Harus Login Terlebih Dahulu.');
		redirect('login');
	}
	
	if($CI->session->userdata('level') != 1){
		$CI->session->set_flashdata('flasherror','Anda Tidak Memiliki Hak Akses Untuk Modul Tersebut.');
		redirect('dashboard');
	}
}

function group($group)
{
	$CI =& get_instance();
	if(!in_array($CI->session->userdata('level'), $group)){
        return FALSE;
        $CI->session->set_flashdata('flasherror','Anda Tidak Memiliki Hak Akses Untuk Modul Tersebut.');
        redirect('dashboard');
	}else{
        return TRUE;
    }
}