<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

//untuk menghilangkan tanda , pada number
if (! function_exists('replacecoma'))
{
	function replacecoma($number)
	{
		return str_replace(',','',$number);
	}
}

if (! function_exists('replacecomadot'))
{
	function replacecomadot($number)
	{
		return str_replace(',','.',$number);
	}
}

// untuk menghilangkan tanda . pada number
if (! function_exists('replacedot'))
{
	function replacedot($number)
	{
		return str_replace('.','',$number);
	}
}

// untuk memotong paragraf ketika ketemu <!--more-->
if (! function_exists('readmore'))
{
	function readmore($article)
	{
		$i = strpos($article,'<!--more-->');
		if($i !== FALSE){
			$i += strlen('<!--more-->');
			return substr($article, 0, $i);
		}else{
			return $article;
		}
	}
}

// untuk tanggal indonesia 01 Januari 2015
if ( ! function_exists('ddMMMyyyy'))
{
	function ddMMMyyyy($tgl)
	{
		if($tgl != null){
			$ubah = date("Y-m-d", strtotime($tgl));
			$pecah = explode("-",$ubah);
			$tanggal = $pecah[2];
			$bulan = bulan($pecah[1]);
			$tahun = $pecah[0];
			return strtoupper($tanggal.' '.$bulan.' '.$tahun);
		}
	}
}

// untuk tanggal indonesia 31-12-2015
if ( ! function_exists('ddmmyyyy'))
{
	function ddmmyyyy($tgl)
	{
		if($tgl != null){
			$ubah = date("Y-m-d", strtotime($tgl));
			$pecah = explode("-",$ubah);
			$tanggal = $pecah[2];
			$bulan = $pecah[1];
			$tahun = $pecah[0];
			return $tanggal.'-'.$bulan.'-'.$tahun;
		}
	}
}

// untuk tanggal standart iso 2015-12-31
if ( ! function_exists('yyyymmdd'))
{
	function yyyymmdd($tgl)
	{
		if($tgl != null){
			$ubah = gmdate($tgl, time()+60*60*8);
			$pecah = explode("-",$ubah);
			$tanggal = $pecah[0];
			$bulan = $pecah[1];
			$tahun = $pecah[2];
			return $tahun.'-'.$bulan.'-'.$tanggal;
		}
	}
}

// untuk tanggal saja
if ( ! function_exists('dd'))
{
	function dd($tgl)
	{
		if($tgl != null){
			$ubah = date("Y-m-d", strtotime($tgl));
			$pecah = explode("-",$ubah);
			$tanggal = $pecah[2];
			$bulan = $pecah[1];
			$tahun = $pecah[0];
			return number_format($tanggal);
		}
	}
}

// untuk bulan saja
if ( ! function_exists('mm'))
{
	function mm($tgl)
	{
		if($tgl != null){
			$ubah = date("Y-m-d", strtotime($tgl));
			$pecah = explode("-",$ubah);
			$tanggal = $pecah[2];
			$bulan = $pecah[1];
			$tahun = $pecah[0];
			return number_format($bulan);
		}
	}
}

// untuk tahun saja
if ( ! function_exists('yyyy'))
{
	function yyyy($tgl)
	{
		if($tgl != null){
			$ubah = date("Y-m-d", strtotime($tgl));
			$pecah = explode("-",$ubah);
			$tanggal = $pecah[2];
			$bulan = $pecah[1];
			$tahun = $pecah[0];
			return $tahun;
		}
	}
}

// untuk bulan bahasa indonesia
if ( ! function_exists('bulan'))
{
	function bulan($bln)
	{
		switch ($bln)
		{
			case 1:
				return "Januari";
				break;
			case 2:
				return "Februari";
				break;
			case 3:
				return "Maret";
				break;
			case 4:
				return "April";
				break;
			case 5:
				return "Mei";
				break;
			case 6:
				return "Juni";
				break;
			case 7:
				return "Juli";
				break;
			case 8:
				return "Agustus";
				break;
			case 9:
				return "September";
				break;
			case 10:
				return "Oktober";
				break;
			case 11:
				return "November";
				break;
			case 12:
				return "Desember";
				break;
		}
	}
}


// untuk nama hari
if ( ! function_exists('hari'))
{
	function hari($tanggal)
	{
		//$ubah = gmdate($tanggal, time()+60*60*8);
		$ubah = date("Y-m-d", strtotime($tanggal));
		$pecah = explode("-",$ubah);
		$tgl = $pecah[2];
		$bln = $pecah[1];
		$thn = $pecah[0];

		$nama = date("l", mktime(0,0,0,$bln,$tgl,$thn));
		$nama_hari = "";
		if($nama=="Sunday") {$nama_hari="Minggu";}
		else if($nama=="Monday") {$nama_hari="Senin";}
		else if($nama=="Tuesday") {$nama_hari="Selasa";}
		else if($nama=="Wednesday") {$nama_hari="Rabu";}
		else if($nama=="Thursday") {$nama_hari="Kamis";}
		else if($nama=="Friday") {$nama_hari="Jumat";}
		else if($nama=="Saturday") {$nama_hari="Sabtu";}
		return $nama_hari;
	}
}


// untuk hitung mundur
if ( ! function_exists('countdown'))
{
	function countdown($wkt)
	{
		$waktu=array(	365*24*60*60	=> "tahun",
						30*24*60*60		=> "bulan",
						7*24*60*60		=> "minggu",
						24*60*60		=> "hari",
						60*60			=> "jam",
						60				=> "menit",
						1				=> "detik");

		$hitung = strtotime(gmdate ("Y-m-d H:i:s", time () +60 * 60 * 8))-$wkt;
		$hasil = array();
		if($hitung<5)
		{
			$hasil = 'kurang dari 5 detik yang lalu';
		}
		else
		{
			$stop = 0;
			foreach($waktu as $periode => $satuan)
			{
				if($stop>=6 || ($stop>0 && $periode<60)) break;
				$bagi = floor($hitung/$periode);
				if($bagi > 0)
				{
					$hasil[] = $bagi.' '.$satuan;
					$hitung -= $bagi*$periode;
					$stop++;
				}
				else if($stop>0) $stop++;
			}
			$hasil=implode(' ',$hasil).' yang lalu';
		}
		return $hasil;
	}
}

// untuk cek usia
if ( ! function_exists('age'))
{
	function age($tgl)
	{
		if($tgl != null){
			$tanggal['lahir'] = $tgl;
			$tanggal['sekarang'] = date('Y-m-d');
			$lahir = $tanggal['lahir'];
			$selisih = time()-strtotime($lahir);
			$tahun = floor($selisih / 31536000);
			$bulan = floor(($selisih % 31536000) / 2592000);
			return $tahun.' THN, '.$bulan.' BLN';
		}
	}
}

// untuk angka terbilang
if ( ! function_exists('terbilang'))
{
	function terbilang($number)
	{
		$before_comma = trim(to_word($number));
		//$after_comma = trim(comma($number));
		//return ucwords($results = $before_comma.' koma '.$after_comma);
		return ucwords($results = $before_comma);
	}

	function to_word($number)
	{
		$words = "";
		$arr_number = array(
		"",
		"satu",
		"dua",
		"tiga",
		"empat",
		"lima",
		"enam",
		"tujuh",
		"delapan",
		"sembilan",
		"sepuluh",
		"sebelas");

		if($number<12)
		{
			$words = " ".$arr_number[$number];
		}
		else if($number<20)
		{
			$words = to_word($number-10)." belas";
		}
		else if($number<100)
		{
			$words = to_word($number/10)." puluh ".to_word($number%10);
		}
		else if($number<200)
		{
			$words = "seratus ".to_word($number-100);
		}
		else if($number<1000)
		{
			$words = to_word($number/100)." ratus ".to_word($number%100);
		}
		else if($number<2000)
		{
			$words = "seribu ".to_word($number-1000);
		}
		else if($number<1000000)
		{
			$words = to_word($number/1000)." ribu ".to_word($number%1000);
		}
		else if($number<1000000000)
		{
			$words = to_word($number/1000000)." juta ".to_word($number%1000000);
		}
		else if($number<1000000000000)
		{
			$words = to_word($number/1000000000)." miliyar ".to_word($number%1000000000);
		}
		else
		{
			$words = "undefined";
		}
		return $words;
	}

	function comma($number)
	{
		$after_comma = stristr($number,',');
		$arr_number = array(
		"nol",
		"satu",
		"dua",
		"tiga",
		"empat",
		"lima",
		"enam",
		"tujuh",
		"delapan",
		"sembilan");

		$results = "";
		$length = strlen($after_comma);
		$i = 1;
		while($i<$length)
		{
			$get = substr($after_comma,$i,1);
			$results .= " ".$arr_number[$get];
			$i++;
		}
		return $results;
	}
}


// untuk list bulan dalam bahasa indonesia
if ( ! function_exists('list_bulan'))
{
	function list_bulan($kosong = 0)
	{
		$CI =& get_instance();
		$CI->lang->load('calendar');

		if($kosong) $result[0] = 'Semua bulan';
		$result['01'] = $CI->lang->line('cal_january');
		$result['02'] = $CI->lang->line('cal_february');
		$result['03'] = $CI->lang->line('cal_march');
		$result['04'] = $CI->lang->line('cal_april');
		$result['05'] = $CI->lang->line('cal_may');
		$result['06'] = $CI->lang->line('cal_june');
		$result['07'] = $CI->lang->line('cal_july');
		$result['08'] = $CI->lang->line('cal_august');
		$result['09'] = $CI->lang->line('cal_september');
		$result['10'] = $CI->lang->line('cal_october');
		$result['11'] = $CI->lang->line('cal_november');
		$result['12'] = $CI->lang->line('cal_december');
		
		return $result;
	}
}

// untuk list tahun
if ( ! function_exists('list_tahun'))
{
	function list_tahun($kosong = 0, $dari = -10, $sampai = 0)
	{
		$CI =& get_instance();
		$CI->lang->load('calendar');

		if($kosong) $result[0] = 'Semua Tahun';
		
		$y = date("Y");
		for($i = $dari; $i <= $sampai; $i++)
		{
			$result[$y + $i] = $y + $i;
		}
		return $result;
	}
}

// untuk list tahun
if ( ! function_exists('status'))
{
	function status($str)
	{
		switch ($str)
		{
			case 1:
				return "Draf";
				break;
			case 2:
				return "Pengajuan";
				break;
			case 3:
				return "Disetujui";
				break;
			case 4:
				return "Ditolak";
				break;
		}
	}
}

// untuk currency rupiah
if ( ! function_exists('rupiah'))
{
	function rupiah($value)
	{
		if($value < 0)
		{
			return '( Rp '.number_format(abs($value), 0, '', '.').' )';
		}
		else
		{
			return 'Rp '.number_format($value, 0, '', '.').'  ';
		}
	}
}

if (! function_exists('satuan'))
{
	function satuan($kode=null)
	{
		$CI =& get_instance();
		$CI->db->where('id', $kode);
		$CI->db->where('deleted_at', null);
		$query = $CI->db->get('ref_satuan');
        if($query->num_rows() > 0){
			return $query->row()->satuan;
		}else{
            return '-';
        }
	}
}


if (!function_exists('kota')) {
    function kota($id = null)
    {
		$CI = &get_instance();
		$CI->db->where('id', $id);
		$query = $CI->db->get('kota');
        if ($query->num_rows() > 0) {
            return $query->row()->kota;
        } else {
            return false;
        }
    }
}

if (!function_exists('kecamatan')) {
    function kecamatan($id = null)
    {
		$CI = &get_instance();
		$CI->db->where('id', $id);
		$query = $CI->db->get('kecamatan');
        if ($query->num_rows() > 0) {
            return $query->row()->kecamatan;
        } else {
            return false;
        }
    }
}

if (!function_exists('kelurahan')) {
    function kelurahan($id = null)
    {
		$CI = &get_instance();
		$CI->db->where('id', $id);
		$query = $CI->db->get('kelurahan');
        if ($query->num_rows() > 0) {
            return $query->row()->kelurahan;
        } else {
            return false;
        }
    }
}

if (!function_exists('cabor')) {
    function cabor($id = null, $kode=null)
    {
		$CI = &get_instance();
		if($id){
			$CI->db->where('id', $id);
		}
		
		if($kode){
			$CI->db->where('kode', $kode);
		}
		$CI->db->where('deleted_at', NULL);
		$query = $CI->db->get('cabor');
        if ($query->num_rows() > 0) {
            return $query->row();
        } else {
            return false;
        }
    }
}

if (!function_exists('kepala')) {
    function kepala($id = null, $npsn=null)
    {
		$CI = &get_instance();
		$CI->db->where('cabor_id', $id);
		$CI->db->where('kode_cabor', $npsn);
		$CI->db->where('jabatan', 1);
		$CI->db->where('deleted_at', NULL);
		$query = $CI->db->get('pengurus');
        if ($query->num_rows() > 0) {
            return $query->row();
        } else {
            return false;
        }
    }
}

if (!function_exists('bendahara')) {
    function bendahara($id = null, $npsn=null)
    {
		$CI = &get_instance();
		$CI->db->where('cabor_id', $id);
		$CI->db->where('kode_cabor', $npsn);
		$CI->db->where('jabatan', 2);
		$CI->db->where('deleted_at', NULL);
		$query = $CI->db->get('pengurus');
        if ($query->num_rows() > 0) {
            return $query->row();
        } else {
            return false;
        }
    }
}

if (!function_exists('sekretaris')) {
    function sekretaris($id = null, $npsn=null)
    {
		$CI = &get_instance();
		$CI->db->where('cabor_id', $id);
		$CI->db->where('kode_cabor', $npsn);
		$CI->db->where('jabatan', 3);
		$CI->db->where('deleted_at', NULL);
		$query = $CI->db->get('pengurus');
        if ($query->num_rows() > 0) {
            return $query->row();
        } else {
            return false;
        }
    }
}



if (!function_exists('real_belanja')) {
    function real_belanja($id = null)
    {
		$jumlah = 0;
		$CI = &get_instance();
		$CI->db->where('detail_id', $id);
		$CI->db->where('deleted_at', NULL);
		$query = $CI->db->get('bku_belanja');
        if ($query->num_rows() > 0) {
			$hasil = $query->result();
			foreach($hasil as $row){
				$jumlah += $row->jumlah;
			}
			return $jumlah;
        } else {
            return false;
        }
    }
}



if (!function_exists('jml_total_realisasi')) {
    function jml_total_realisasi($kertas_id = null)
    {
		$CI = &get_instance();
		$CI->db->select('sum(jumlah) as jumlah', false);
		$CI->db->where('kertas_id', $kertas_id);
		$CI->db->where('deleted_at', null);
		$query = $CI->db->get('bku_belanja');
        if ($query->num_rows() > 0) {
            return $query->row();
        } else {
            return false;
        }
    }
}

if (!function_exists('jml_program_realisasi')) {
    function jml_program_realisasi($kertas_id = null, $program_id=null)
    {
		$CI = &get_instance();
		$CI->db->select('sum(jumlah) as jumlah', false);
		$CI->db->where('kertas_id', $kertas_id);
		$CI->db->where('program_id', $program_id);
		$CI->db->where('deleted_at', null);
		$query = $CI->db->get('bku_belanja');
        if ($query->num_rows() > 0) {
            return $query->row();
        } else {
            return false;
        }
    }
}

if (!function_exists('jml_anggaran')) {
    function jml_anggaran($kertas_id = null)
    {
		$CI = &get_instance();
		$CI->db->select('sum(jumlah) as jumlah', false);
		$CI->db->where('kertas_id', $kertas_id);
		$query = $CI->db->get('vw_kertas');
        if ($query->num_rows() > 0) {
            return $query->row();
        } else {
            return false;
        }
    }
}

if (!function_exists('jml_program_bos')) {
    function jml_program_bos($kertas_id = null, $program_id=null, $bos=null)
    {
		$CI = &get_instance();
		$CI->db->select('sum(jumlah) as jumlah, bos', false);
		$CI->db->where_in('kertas_id', $kertas_id);
		$CI->db->where('program_id', $program_id);
		//$CI->db->where('belanja', $belanja);
		$CI->db->where('bos', $bos);
		$query = $CI->db->get('vw_kertas');
        if ($query->num_rows() > 0) {
            return $query->row();
        } else {
            return false;
        }
    }
}

if (!function_exists('jml_baris_bos')) {
    function jml_baris_bos($kertas_id = null, $program_id=null, $uraian_id, $bos=null)
    {
		$CI = &get_instance();
		$CI->db->select('sum(jumlah) as jumlah, bos', false);
		$CI->db->where('kertas_id', $kertas_id);
		$CI->db->where('program_id', $program_id);
		$CI->db->where('uraian_id', $uraian_id);
		//$CI->db->where('belanja', $belanja);
		$CI->db->where('bos', $bos);
		$query = $CI->db->get('vw_kertas');
        if ($query->num_rows() > 0) {
            return $query->row();
        } else {
            return false;
        }
    }
}

if (!function_exists('baris_tw')) {
    function baris_tw($kertas_id = null, $id=null)
    {
		$CI = &get_instance();
		$CI->db->select('sum(jumlah) as jumlah, periode', false);
		$CI->db->where('kertas_id', $kertas_id);
		$CI->db->where('id', $id);
		//$CI->db->where('belanja', $belanja);
		$query = $CI->db->get('vw_kertas');
        if ($query->num_rows() > 0) {
            return $query->row();
        } else {
            return false;
        }
    }
}

if (!function_exists('jml_baris_tw')) {
    function jml_baris_tw($kertas_id = null, $uraian_id=null)
    {
		$CI = &get_instance();
		$CI->db->select('sum(jumlah) as jumlah, periode', false);
		$CI->db->where('kertas_id', $kertas_id);
		$CI->db->where('uraian_id', $uraian_id);
		//$CI->db->where('belanja', $belanja);
		$query = $CI->db->get('vw_kertas');
        if ($query->num_rows() > 0) {
            return $query->row();
        } else {
            return false;
        }
    }
}

if (!function_exists('jml_program_tw')) {
    function jml_program_tw($kertas_id = null, $program_id =null, $periode=null)
    {
		$CI = &get_instance();
		$CI->db->select('sum(jumlah) as jumlah, periode', false);
		$CI->db->where_in('kertas_id', $kertas_id);
		$CI->db->where('program_id', $program_id);
		$CI->db->where('periode', $periode);
		//$CI->db->where('belanja', $belanja);
		$query = $CI->db->get('vw_kertas');
        if ($query->num_rows() > 0) {
            return $query->row();
        } else {
            return false;
        }
    }
}

if (!function_exists('total_bos')) {
    function total_bos($kertas_id = null, $program_id=null, $bos=null)
    {
		$CI = &get_instance();
		$CI->db->select('sum(jumlah) as jumlah, bos', false);
		$CI->db->where_in('kertas_id', $kertas_id);
		$CI->db->where('program_id', $program_id);
		//$CI->db->where('belanja', $belanja);
		$CI->db->where('bos', $bos);
		$query = $CI->db->get('vw_kertas');
        if ($query->num_rows() > 0) {
            return $query->row();
        } else {
            return false;
        }
    }
}

if (!function_exists('jml_realisasi')) {
    function jml_realisasi($kertas_id = null, $program_id=null, $uraian_id=null)
    {
		$CI = &get_instance();
		$CI->db->select('sum(jumlah) as jumlah', false);
		$CI->db->where('kertas_id', $kertas_id);
		$CI->db->where('program_id', $program_id);
		$CI->db->where('uraian_id', $uraian_id);
		$CI->db->where('deleted_at', null);
		$query = $CI->db->get('bku_belanja');
        if ($query->num_rows() > 0) {
            return $query->row();
        } else {
            return false;
        }
    }
}


//rkas versi 1
if (!function_exists('jml_baris')) {
    function jml_baris($kertas_id = null, $program_id=null, $kegiatan_id=null,$kategori_id=null)
    {
		$CI = &get_instance();
		$CI->db->select('sum(jumlah) as jumlah', false);
		$CI->db->where('kertas_id', $kertas_id);
		$CI->db->where('program_id', $program_id);
		$CI->db->where('kegiatan_id', $kegiatan_id);
		$CI->db->where('kategori_id', $kategori_id);
		
		$query = $CI->db->get('kertas_detail');
        if ($query->num_rows() > 0) {
            return $query->row();
        } else {
            return false;
        }
    }
}

if (!function_exists('jumlah_total_semua')) {
    function jumlah_total_semua($id = null)
    {
		$CI = &get_instance();
		$CI->db->select('sum(jumlah) as jumlah', false);
		$CI->db->from('kertas');
		$CI->db->join('kertas_detail','kertas_detail.kertas_id = kertas.id','left');
		$CI->db->where('kertas_detail.kertas_id', $id);
		$CI->db->where('kertas_detail.deleted_at', null);
	
		
		$query = $CI->db->get();
        if ($query->num_rows() > 0) {
            return $query->row();
        } else {
            return false;
        }
    }
}

if (!function_exists('jumlah_total_semua_realisasi')) {
    function jumlah_total_semua_realisasi($id = null)
    {
		$CI = &get_instance();
		$CI->db->select('sum(jumlah) as jumlah', false);
		$CI->db->from('kertas');
		$CI->db->join('kertas_realisasi','kertas_realisasi.kertas_id = kertas.id','left');
		$CI->db->where('kertas_realisasi.kertas_id', $id);
		$CI->db->where('kertas_realisasi.deleted_at', null);
	
		
		$query = $CI->db->get();
        if ($query->num_rows() > 0) {
            return $query->row();
        } else {
            return false;
        }
    }
}

if (!function_exists('jml_baris_detail')) {
    function jml_baris_detail($id = null, $belanja=null, $periode=null)
    {
		$CI = &get_instance();
		$CI->db->where('id', $id);
		$CI->db->where('belanja', $belanja);
		$CI->db->where('periode', $periode);
		$query = $CI->db->get('vw_kertas');
        if ($query->num_rows() > 0) {
            return $query->row();
        } else {
            return false;
        }
    }
}

if (!function_exists('jml_program')) {
    function jml_program($kertas_id = null, $program_id=null)
    {
		$CI = &get_instance();
		$CI->db->select('sum(jumlah) as jumlah', false);
		$CI->db->where_in('kertas_id', $kertas_id);
		$CI->db->where('program_id', $program_id);
		$query = $CI->db->get('vw_kertas');
        if ($query->num_rows() > 0) {
            return $query->row();
        } else {
            return false;
        }
    }
}

if (!function_exists('jml_program_belanja')) {
    function jml_program_belanja($kertas_id = null, $program_id=null, $belanja=null, $periode=null)
    {
		$CI = &get_instance();
		$CI->db->select('sum(jumlah) as jumlah, belanja', false);
		$CI->db->where_in('kertas_id', $kertas_id);
		$CI->db->where('program_id', $program_id);
		$CI->db->where('belanja', $belanja);
		$CI->db->where('periode', $periode);
		$query = $CI->db->get('vw_kertas');
        if ($query->num_rows() > 0) {
            return $query->row();
        } else {
            return false;
        }
    }
}

if (!function_exists('jml_rkas_baris')) {
    function jml_rkas_baris($kertas_id = null, $program_id =null, $periode=null)
    {
		$CI = &get_instance();
		$CI->db->select('jumlah, periode');
		$CI->db->where_in('kertas_id', $kertas_id);
		$CI->db->where('program_id', $program_id);
		$CI->db->where('periode', $periode);
		//$CI->db->where('belanja', $belanja);
		$query = $CI->db->get('vw_kertas');
        if ($query->num_rows() > 0) {
            return $query->row();
        } else {
            return false;
        }
    }
}

if (!function_exists('jml_total')) {
    function jml_total($kertas_id = null)
    {
		$CI = &get_instance();
		$CI->db->select('sum(jumlah) as jumlah', false);
		$CI->db->where_in('kertas_id', $kertas_id);
		$query = $CI->db->get('vw_kertas');
        if ($query->num_rows() > 0) {
            return $query->row();
        } else {
            return false;
        }
    }
}

if (!function_exists('jml_total_belanja')) {
    function jml_total_belanja($kertas_id = null, $belanja=null, $periode=null)
    {
		$CI = &get_instance();
		$CI->db->select('sum(jumlah) as jumlah', false);
		$CI->db->where_in('kertas_id', $kertas_id);
		$CI->db->where('belanja', $belanja);
		$CI->db->where('periode', $periode);
		$query = $CI->db->get('vw_kertas');
        if ($query->num_rows() > 0) {
            return $query->row();
        } else {
            return false;
        }
    }
}

//end rkas versi 1


if (!function_exists('belanja_triwulan')) {
    function belanja_triwulan($sekolah_id = null, $tahun=null, $periode=null)
    {
		$CI = &get_instance();
		$CI->db->select('sum(jumlah) as jumlah', false);
		$CI->db->where('sekolah_id', $sekolah_id);
		$CI->db->where('tahun', $tahun);
		$CI->db->where('periode', $periode);
		$query = $CI->db->get('vw_kertas');
        if ($query->num_rows() > 0) {
            return $query->row();
        } else {
            return false;
        }
    }
}

if (!function_exists('jml_belanja_triwulan')) {
    function jml_belanja_triwulan($sekolah_id = null, $tahun=null)
    {
		$CI = &get_instance();
		$CI->db->select('sum(jumlah) as jumlah', false);
		$CI->db->where('sekolah_id', $sekolah_id);
		$CI->db->where('tahun', $tahun);
		$query = $CI->db->get('vw_kertas');
        if ($query->num_rows() > 0) {
            return $query->row();
        } else {
            return false;
        }
    }
}

if ( ! function_exists('bln_romawi'))
{
	function bln_romawi($bln)
	{
		switch ($bln)
		{
			case 1:
				return "I";
				break;
			case 2:
				return "II";
				break;
			case 3:
				return "III";
				break;
			case 4:
				return "IV";
				break;
			case 5:
				return "V";
				break;
			case 6:
				return "VI";
				break;
			case 7:
				return "VII";
				break;
			case 8:
				return "VIII";
				break;
			case 9:
				return "IX";
				break;
			case 10:
				return "X";
				break;
			case 11:
				return "XI";
				break;
			case 12:
				return "XII";
				break;
		}
	}
}

if ( ! function_exists('periode'))
{
	function periode($str)
	{
		switch ($str)
		{
			case 1:
				return "Triwulan I";
				break;
			case 2:
				return "Triwulan II";
				break;
			case 3:
				return "Triwulan III";
				break;
			case 4:
				return "Triwulan IV";
				break;
		}
	}
}

if ( ! function_exists('jenis_perubahan'))
{
	function jenis_perubahan($str)
	{
		switch ($str)
		{
			case 1:
				return "Penambahan";
				break;
			case 2:
				return "Pengurangan";
				break;
			
		}
	}
}

if (!function_exists('folder')) {
    function folder($kode = null)
    {
        $path = 'dokumen/' . $kode . '/';
        $user_name = 'www-data:www-data';
        if (!file_exists($path)) {
            mkdir($path, 0775, true);
            //chown($path, $user_name);
            return true;
        } else {
            return false;
        }
    }
}

if (!function_exists('cek_profil')) {
    function cek_profil($kode = null)
    {
		$CI = &get_instance();
		$CI->db->where('id', $kode);
		$query = $CI->db->get('cabor');
        if ($query->num_rows() > 0) {
			$result = $query->row();
			if($result->cabor && $result->kode && $result->alamat && $result->kota_id  && $result->kecamatan_id && $result->kelurahan_id){
				return TRUE;
			}else{
				return FALSE;
			}
        } else {
            return false;
        }
    }
}

if ( ! function_exists('jenjang'))
{
	function jenjang($str)
	{
		switch ($str)
		{
			case 5:
				return "SD";
				break;
			case 6:
				return "SDM";
				break;
		}
	}
}

if ( ! function_exists('jenis'))
{
	function jenis($str)
	{
		switch ($str)
		{
			case 1:
				return "Negeri";
				break;
			case 2:
				return "Swasta";
				break;
		}
	}
}

//untuk cetak cover rkas

if (!function_exists('sisa_saldo')) {
    function sisa_saldo($npsn = null, $tahun=null)
    {
		$CI = &get_instance();
		$CI->db->where('sekolah_id', $npsn);
		$CI->db->where('tahun', $tahun);
		$CI->db->where('deleted_at', null);
		$query = $CI->db->get('kertas');
        if ($query->num_rows() > 0) {
			$result = $query->row();
			return $result->silpa;
        } else {
            return false;
        }
    }
}

if (!function_exists('jumlah_siswa')) {
    function jumlah_siswa($npsn = null, $tahun=null)
    {
		$CI = &get_instance();
		$CI->db->where('sekolah_id', $npsn);
		$CI->db->where('tahun', $tahun);
		$CI->db->where('periode', 1);
		$CI->db->where('deleted_at', null);
		$query = $CI->db->get('kertas');
        if ($query->num_rows() > 0) {
			$result = $query->row();
			return $result->siswa;
        } else {
            return false;
        }
    }
}

if (!function_exists('program_sekolah')) {
    function program_sekolah($kode = null)
    {
		$CI = &get_instance();
		$CI->db->like('kode', $kode);
		$CI->db->where('deleted_at', null);
		$query = $CI->db->get('ref_program');
        if ($query->num_rows() > 0) {
			$result = $query->row();
			return $result->program;
        } else {
            return false;
        }
    }
}

if (!function_exists('kategori')) {
    function kategori($kode = null)
    {
		$CI = &get_instance();
		$CI->db->where('id', $kode);
		$CI->db->where('deleted_at', null);
		$query = $CI->db->get('kategori');
        if ($query->num_rows() > 0) {
			$result = $query->row();
			return $result->kategori;
        } else {
            return false;
        }
    }
}

if (!function_exists('kegiatan')) {
    function kegiatan($kode = null)
    {
		$CI = &get_instance();
		$CI->db->where('id', $kode);
		$CI->db->where('deleted_at', null);
		$query = $CI->db->get('kegiatan');
        if ($query->num_rows() > 0) {
			$result = $query->row();
			return $result->kegiatan;
        } else {
            return false;
        }
    }
}

if (!function_exists('program')) {
    function program($kode = null)
    {
		$CI = &get_instance();
		$CI->db->where('id', $kode);
		$CI->db->where('deleted_at', null);
		$query = $CI->db->get('program');
        if ($query->num_rows() > 0) {
			$result = $query->row();
			return $result->program;
        } else {
            return false;
        }
    }
}

if (!function_exists('kode_program')) {
    function kode_program($kode = null)
    {
		$CI = &get_instance();
		$CI->db->where('id', $kode);
		$CI->db->where('deleted_at', null);
		$query = $CI->db->get('program');
        if ($query->num_rows() > 0) {
			$result = $query->row();
			return $result->kode;
        } else {
            return false;
        }
    }
}

if (!function_exists('belanja')) {
    function belanja($kode = null)
    {
		$CI = &get_instance();
		$CI->db->where('id', $kode);
		$CI->db->where('deleted_at', null);
		$query = $CI->db->get('belanja');
        if ($query->num_rows() > 0) {
			$result = $query->row();
			return $result->belanja;
        } else {
            return false;
        }
    }
}

if (!function_exists('nilai_program')) {
    function nilai_program($npsn = null, $tahun=null, $kode=null)
    {
		$CI = &get_instance();
		$program_id = null;
		$program_id = $CI->db->get_where('ref_program', array('deleted_at'=>null, 'kode'=>$kode))->row()->id;
		
		$CI->db->select('sum(jumlah) as jumlah');
		$CI->db->where('sekolah_id', $npsn);
		$CI->db->where('tahun', $tahun);
		$CI->db->where('program_id', $program_id);
		$query = $CI->db->get('vw_kertas');
        if ($query->num_rows() > 0) {
			$result = $query->row();
			return $result->jumlah;
        } else {
            return false;
        }
    }
}

if (!function_exists('cek_kuota')) {
    function cek_kuota($id = null)
    {
		$CI = &get_instance();
		$CI->db->where('tahun', $id);
		
		$CI->db->where('deleted_at', NULL);
		$query = $CI->db->get('kuota');
        if ($query->num_rows() > 0) {
            return $query->row()->kuota;
        } else {
            return false;
        }
    }
}

if (!function_exists('cek_kuota_cabor')) {
    function cek_kuota_cabor($tahun = null)
    {
		$hasil = 0;
		$CI = &get_instance();
		$CI->db->where('kode', $CI->session->userdata('kode_cabor'));
		$CI->db->where('tahun', $tahun);

		$query = $CI->db->get('vw_kuota_cabor');
		if ($query->num_rows() > 0) {
			foreach($query->result() as $row){
				$hasil += $row->jumlah;
			}
			return $hasil;
        } else {
            return FALSE;
        }
    }
}

if (!function_exists('daftar_kuota_cabor')) {
    function daftar_kuota_cabor($tahun = null)
    {
		$CI = &get_instance();
		$CI->db->where('kode', $CI->session->userdata('kode_cabor'));
		$CI->db->where('tahun', $tahun);

		$query = $CI->db->get('vw_kuota_cabor');
		if ($query->num_rows() > 0) {
			return $query->result();
        } else {
            return FALSE;
        }
    }
}

if (!function_exists('cek_kuota_terpakai')) {
    function cek_kuota_terpakai($id = null)
    {
		$CI = &get_instance();
		$CI->db->select('sum(jumlah_kuota) as jumlah_kuota');
		$CI->db->where('tahun', $id);
		$CI->db->where('deleted_at', NULL);
		$query = $CI->db->get('kuota_cabor');
        if ($query->num_rows() > 0) {
            return $query->row()->jumlah_kuota;
        } else {
            return false;
        }
    }
}

if (!function_exists('cek_isi_rencana')) {
    function cek_isi_rencana($id = null,$id2= null,$id3= null,$id4= null,$id5= null,$id6= null)
    {
		$CI = &get_instance();
		$CI->db->where('kertas_id', $id);
		$CI->db->where('program_id', $id2);
		$CI->db->where('kegiatan_id', $id3);
		$CI->db->where('kategori_id', $id4);
		$CI->db->where('belanja_id', $id5);
		$CI->db->where('tahun', $id6);
		$CI->db->where('deleted_at', NULL);
		$query = $CI->db->get('kertas_detail');
        if ($query->num_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }
}

if (!function_exists('cek_isi_realisasi')) {
    function cek_isi_realisasi($id = null,$id2= null,$id3= null,$id4= null,$id5= null,$id6= null)
    {
		$CI = &get_instance();
		$CI->db->where('kertas_id', $id);
		$CI->db->where('program_id', $id2);
		$CI->db->where('kegiatan_id', $id3);
		$CI->db->where('kategori_id', $id4);
		$CI->db->where('belanja_id', $id5);
		$CI->db->where('tahun', $id6);
		$CI->db->where('deleted_at', NULL);
		$query = $CI->db->get('kertas_realisasi');
        if ($query->num_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }
}

if (!function_exists('cek_isi_dokumen')) {
    function cek_isi_dokumen($id = null,$id2= null,$id3= null,$id4= null,$id5= null,$id6= null)
    {
		$CI = &get_instance();
		$CI->db->where('kertas_id', $id);
		$CI->db->where('program_id', $id2);
		$CI->db->where('kegiatan_id', $id3);
		$CI->db->where('kategori_id', $id4);
		$CI->db->where('belanja_id', $id5);
		$CI->db->where('tahun', $id6);
		$CI->db->where('deleted_at', NULL);
		$query = $CI->db->get('kertas_realisasi');
        if ($query->num_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }
}

if (!function_exists('jumlah_detail')) {
    function jumlah_detail($id = null,$id2= null,$id3= null,$id4= null,$id5= null,$id6= null)
    {
		$CI = &get_instance();
		$CI->db->where('kertas_id', $id);
		$CI->db->where('program_id', $id2);
		$CI->db->where('kegiatan_id', $id3);
		$CI->db->where('kategori_id', $id4);
		$CI->db->where('belanja_id', $id5);
		$CI->db->where('tahun', $id6);
		$CI->db->where('deleted_at', NULL);
		$query = $CI->db->get('kertas_detail');
        if ($query->num_rows() > 0) {
            return $query->row()->jumlah;
        } else {
            return 0;
        }
    }
}

if (!function_exists('jumlah_detail_realisasi')) {
    function jumlah_detail_realisasi($id = null,$id2= null,$id3= null,$id4= null,$id5= null,$id6= null)
    {
		$CI = &get_instance();
		$CI->db->where('kertas_id', $id);
		$CI->db->where('program_id', $id2);
		$CI->db->where('kegiatan_id', $id3);
		$CI->db->where('kategori_id', $id4);
		$CI->db->where('belanja_id', $id5);
		$CI->db->where('tahun', $id6);
		$CI->db->where('deleted_at', NULL);
		$query = $CI->db->get('kertas_realisasi');
        if ($query->num_rows() > 0) {
            return $query->row()->jumlah;
        } else {
            return 0;
        }
    }
}

if (!function_exists('kertas_detail_id')) {
    function kertas_detail_id($id = null,$id2= null,$id3= null,$id4= null,$id5= null,$id6= null)
    {
		$CI = &get_instance();
		$CI->db->where('kertas_id', $id);
		$CI->db->where('program_id', $id2);
		$CI->db->where('kegiatan_id', $id3);
		$CI->db->where('kategori_id', $id4);
		$CI->db->where('belanja_id', $id5);
		$CI->db->where('tahun', $id6);
		$CI->db->where('deleted_at', NULL);
		$query = $CI->db->get('kertas_detail');
        if ($query->num_rows() > 0) {
            return $query->row()->id;
        } else {
            return '-';
        }
    }
}

if (!function_exists('kertas_realisasi_id')) {
    function kertas_realisasi_id($id = null,$id2= null,$id3= null,$id4= null,$id5= null,$id6= null)
    {
		$CI = &get_instance();
		$CI->db->where('kertas_id', $id);
		$CI->db->where('program_id', $id2);
		$CI->db->where('kegiatan_id', $id3);
		$CI->db->where('kategori_id', $id4);
		$CI->db->where('belanja_id', $id5);
		$CI->db->where('tahun', $id6);
		$CI->db->where('deleted_at', NULL);
		$query = $CI->db->get('kertas_realisasi');
        if ($query->num_rows() > 0) {
            return $query->row()->id;
        } else {
            return '-';
        }
    }
}

if (!function_exists('kertas_realisasi_dokumen')) {
    function kertas_realisasi_dokumen($id = null,$id2= null,$id3= null,$id4= null,$id5= null,$id6= null)
    {
		$CI = &get_instance();
		$CI->db->where('kertas_id', $id);
		$CI->db->where('program_id', $id2);
		$CI->db->where('kegiatan_id', $id3);
		$CI->db->where('kategori_id', $id4);
		$CI->db->where('belanja_id', $id5);
		$CI->db->where('tahun', $id6);
		$CI->db->where('deleted_at', NULL);
		$query = $CI->db->get('kertas_realisasi');
        if ($query->num_rows() > 0) {
            return $query->row()->dokumen;
        } else {
            return '-';
        }
    }
}

if (!function_exists('cabor_id')) {
    function cabor_id($id = null)
    {
		$CI = &get_instance();
		$CI->db->where('kode', $id);
		$CI->db->where('deleted_at', NULL);
		$query = $CI->db->get('cabor');
        if ($query->num_rows() > 0) {
            return $query->row()->id;
        } else {
            return '-';
        }
    }
}

if (!function_exists('kode_cabor')) {
    function kode_cabor($id = null)
    {
		$CI = &get_instance();
		$CI->db->where('id', $id);
		$CI->db->where('deleted_at', NULL);
		$query = $CI->db->get('cabor');
        if ($query->num_rows() > 0) {
            return $query->row()->kode;
        } else {
            return '-';
        }
    }
}

if (!function_exists('kuota_cabor')) {
    function kuota_cabor($id = null,$id2 = null)
    {
		$CI = &get_instance();
		$CI->db->where('jenis_cabor_id', $id);
		$CI->db->where('tahun', $id2);
		$CI->db->where('deleted_at', NULL);
		$query = $CI->db->get('platform_anggaran');
        if ($query->num_rows() > 0) {
            return $query->row()->jumlah;
        } else {
            return 0;
        }
    }
}

if (!function_exists('kuota_cabor1')) {
    function kuota_cabor1($id = null,$id2 = null)
    {
		$CI = &get_instance();
		$CI->db->select('sum(jumlah) as jumlah');
		$CI->db->from('platform_anggaran');
		$CI->db->join('kertas','kertas.jenis_cabor_id = platform_anggaran.jenis_cabor_id', 'left');
		$CI->db->where('kertas.id', $id);
		$CI->db->where('kertas.tahun', $id2);
		$CI->db->where('kertas.deleted_at', NULL);
		$query = $CI->db->get();
        if ($query->num_rows() > 0) {
            return $query->row()->jumlah;
        } else {
            return '-';
        }
    }
}

if (!function_exists('kuota_terpakai')) {
    function kuota_terpakai($id = null,$id2 = null)
    {
		$CI = &get_instance();
		$CI->db->select('sum(jumlah) as jumlah');
		$CI->db->where('kertas_id', $id);
		$CI->db->where('tahun', $id2);
		$CI->db->where('deleted_at', NULL);
		$query = $CI->db->get('kertas_detail');
        if ($query->num_rows() > 0) {
            return $query->row()->jumlah;
        } else {
            return '-';
        }
    }
}

if (!function_exists('jumlah_anggaran')) {
    function jumlah_anggaran($id = null,$id2 = null)
    {
		$CI = &get_instance();
		$CI->db->where('kertas_id', $id);
		$CI->db->where('tahun', $id2);
		$CI->db->where('deleted_at', NULL);
		$query = $CI->db->get('kertas_detail');
        if ($query->num_rows() > 0) {
            return $query->row()->jumlah;
        } else {
            return '-';
        }
    }
}

if (!function_exists('cek_tahun_template')) {
    function cek_tahun_template($tahun = null)
    {
		$CI = &get_instance();
		$CI->db->where('tahun', $tahun);
		$CI->db->where('deleted_at', null);
		$query = $CI->db->get('template');
        if ($query->num_rows() > 0) {
			return true;
        } else {
            return false;
        }
    }
}

if (!function_exists('jumlah_rencana_anggaran')) {
    function jumlah_rencana_anggaran($id = null)
    {
		$CI = &get_instance();
		$CI->db->select('sum(jumlah) as jumlah');
		$CI->db->where('kertas_id', $id);
		$CI->db->where('deleted_at', NULL);
		$query = $CI->db->get('kertas_detail');
        if ($query->num_rows() > 0) {
            return $query->row()->jumlah;
        } else {
            return 0;
        }
    }
}

if (!function_exists('cabang_olahraga')) {
    function cabang_olahraga($id = null)
    {
		$CI = &get_instance();
		$CI->db->where('id', $id);
		$CI->db->where('deleted_at', NULL);
		$query = $CI->db->get('jenis_cabor');
        if ($query->num_rows() > 0) {
			return $query->row()->cabor;
        } else {
            return FALSE;
        }
    }
}

if (!function_exists('jenis_cabor')) {
    function jenis_cabor($id = null)
    {
		$CI = &get_instance();
		$CI->db->where('cabor_id', $id);
		$CI->db->where('deleted_at', NULL);
		$query = $CI->db->get('cabor_jenis');
        if ($query->num_rows() > 0) {
			$data = array();
			foreach($query->result() as $row){
				$data[] = cabang_olahraga($row->jenis_cabor_id); 
			}
			return $data;
        } else {
            return FALSE;
        }
    }
}

if (!function_exists('jumlah_cabor')) {
    function jumlah_cabor($tahun = null)
    {
		$CI = &get_instance();
		$CI->db->where('tahun', $tahun);
		$CI->db->where('deleted_at', NULL);
		$query = $CI->db->get('platform_anggaran');
        if ($query->num_rows() > 0) {
			return $query->result();
        } else {
            return FALSE;
        }
    }
}

if (!function_exists('jumlah_draf_rencana')) {
    function jumlah_draf_rencana($tahun = null)
    {
		$CI = &get_instance();
		$CI->db->where('status', 1);
		$CI->db->where('tahun', $tahun);
		$query = $CI->db->get('vw_rencana_cabor');
        if ($query->num_rows() > 0) {
			$data = 0;
			foreach($query->result() as $row){
				$data += $row->jumlah; 
			}
			return $data;
        } else {
            return FALSE;
        }
    }
}

if (!function_exists('jumlah_pengajuan_rencana')) {
    function jumlah_pengajuan_rencana($tahun = null)
    {
		$CI = &get_instance();
		$CI->db->where('status', 2);
		$CI->db->where('tahun', $tahun);
		$query = $CI->db->get('vw_rencana_cabor');
        if ($query->num_rows() > 0) {
			$data = 0;
			foreach($query->result() as $row){
				$data += $row->jumlah; 
			}
			return $data;
        } else {
            return FALSE;
        }
    }
}

if (!function_exists('jumlah_setuju_rencana')) {
    function jumlah_setuju_rencana($tahun = null)
    {
		$CI = &get_instance();
		$CI->db->where('status', 3);
		$CI->db->where('tahun', $tahun);
		$query = $CI->db->get('vw_rencana_cabor');
        if ($query->num_rows() > 0) {
			$data = 0;
			foreach($query->result() as $row){
				$data += $row->jumlah; 
			}
			return $data;
        } else {
            return FALSE;
        }
    }
}

if (!function_exists('jumlah_status_pengajuan')) {
    function jumlah_status_pengajuan($tahun = null, $status = null)
    {
		$CI = &get_instance();
		$CI->db->where('status', $status);
		$CI->db->where('tahun', $tahun);
		$CI->db->group_by('kertas_id');
		$query = $CI->db->get('vw_rencana_cabor');
        if ($query->num_rows() > 0) {
			return $query->num_rows();
        } else {
            return 0;
        }
    }
}

if (!function_exists('jumlah_kategori')) {
    function jumlah_kategori($id1 = null, $id2= null,$id3= null,$id4= null, $id5= null)
    {
		$CI = &get_instance();
		$CI->db->select('sum(jumlah) as jumlah');
		$CI->db->where('kertas_id', $id1);
		$CI->db->where('program_id', $id2);
		$CI->db->where('kegiatan_id', $id3);
		$CI->db->where('kategori_id', $id4);
		$CI->db->where('tahun', $id5);
		$query = $CI->db->get('vw_rencana_cabor');
        if ($query->num_rows() > 0) {
			return $query->row()->jumlah;
        } else {
            return 0;
        }
    }
}

if (!function_exists('jumlah_kegiatan')) {
    function jumlah_kegiatan($id1 = null, $id2= null,$id3= null,$id4= null)
    {
		$CI = &get_instance();
		$CI->db->select('sum(jumlah) as jumlah');
		$CI->db->where('kertas_id', $id1);
		$CI->db->where('program_id', $id2);
		$CI->db->where('kegiatan_id', $id3);
		$CI->db->where('tahun', $id4);
		$query = $CI->db->get('vw_rencana_cabor');
        if ($query->num_rows() > 0) {
			return $query->row()->jumlah;
        } else {
            return 0;
        }
    }
}

if (!function_exists('jumlah_program')) {
    function jumlah_program($id1 = null, $id2= null,$id3= null)
    {
		$CI = &get_instance();
		$CI->db->select('sum(jumlah) as jumlah');
		$CI->db->where('kertas_id', $id1);
		$CI->db->where('program_id', $id2);
		$CI->db->where('tahun', $id3);
		$query = $CI->db->get('vw_rencana_cabor');
        if ($query->num_rows() > 0) {
			return $query->row()->jumlah;
        } else {
            return 0;
        }
    }
}

if (!function_exists('jumlah_program_total')) {
    function jumlah_program_total($id1 = null, $id2= null)
    {
		$CI = &get_instance();
		$CI->db->select('sum(jumlah) as jumlah');
		$CI->db->where('program_id', $id1);
		$CI->db->where('tahun', $id2);
		$query = $CI->db->get('vw_rencana_cabor');
        if ($query->num_rows() > 0) {
			return $query->row()->jumlah;
        } else {
            return 0;
        }
    }
}

if (!function_exists('jumlah_kegiatan_total')) {
    function jumlah_kegiatan_total($id1 = null, $id2= null, $id3=null)
    {
		$CI = &get_instance();
		$CI->db->select('sum(jumlah) as jumlah');
		$CI->db->where('program_id', $id1);
		$CI->db->where('kegiatan_id', $id2);
		$CI->db->where('tahun', $id3);
		$query = $CI->db->get('vw_rencana_cabor');
        if ($query->num_rows() > 0) {
			return $query->row()->jumlah;
        } else {
            return 0;
        }
    }
}

if (!function_exists('jumlah_kategori_total')) {
    function jumlah_kategori_total($id1 = null, $id2= null, $id3=null, $id4=null)
    {
		$CI = &get_instance();
		$CI->db->select('sum(jumlah) as jumlah');
		$CI->db->where('program_id', $id1);
		$CI->db->where('kegiatan_id', $id2);
		$CI->db->where('kategori_id', $id3);
		$CI->db->where('tahun', $id4);
		$query = $CI->db->get('vw_rencana_cabor');
        if ($query->num_rows() > 0) {
			return $query->row()->jumlah;
        } else {
            return 0;
        }
    }
}